﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EllowCart.QueryBank.Roles
{
    public class ProductCategoryManage
    {


        private string sqlGetAllProductCategory = @"SELECT A.ProductCategoryId,A.CategoryName,A.CreatedOn,A.IsActive,a.Description
                                                    FROM ProductCategory_Master A WHERE IsActive=1 order by ProductCategoryId desc";

        private string sqlGetParentCategory = @"select ProductCategoryId,CategoryName,IsActive,CreatedOn
                                                from ProductCategory_Master where ParentCategoryID=0 and IsActive=1";

        public string GetParentCategory()
        {
            return sqlGetParentCategory;
        }

        public string GetAllProductCategory()
        {
            return sqlGetAllProductCategory;
        }


        public string DeleteProductCategory(long ProductCategoryId)
        {

            string sqlDeleteProductCategory = @"UPDATE  ProductCategory_Master SET IsActive=0 where ProductCategoryId=" + ProductCategoryId;
            return sqlDeleteProductCategory;

        }
        public string UpdateProductCategory(string CategoryName, long ParentCategoryID, string Description,  bool IsActive,string UpdatedBy, long ProductCategoryId)

        {
            string UpdateProductCategory = @" Update ProductCategory_Master Set   CategoryName='" + CategoryName + "',ParentCategoryID=" + ParentCategoryID + ",Description='" + Description + "' where ProductCategoryId=" + ProductCategoryId;


            return UpdateProductCategory;
        }
        public string GetAProductCategory(long ProductCategoryId)
        {
            string sqlGetAProductCategory = @"SELECT A.ProductCategoryId,A.CategoryName,A.CreatedOn,A.IsActive,A.ParentCategoryID,A.Description
                                                FROM ProductCategory_Master A
                                                where ProductCategoryId=" + ProductCategoryId;
            return sqlGetAProductCategory;
        }


    }
}
