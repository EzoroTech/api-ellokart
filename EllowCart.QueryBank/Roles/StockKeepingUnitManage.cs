﻿using System;
using System.Collections.Generic;
using System.Text;
using EllowCart.DTO.Stock.InputModels;

namespace EllowCart.QueryBank.Roles
{
   public class StockKeepingUnitManage
    {

        private string AddSku = @"INSERT INTO StockKeepingUnit 
			                                    (StockKeepingUnitName
			                                    ,IsActive
			                                    ,CreatedOn
			                                    ,CreatedBy	
                                                 ,UpdatedOn
                                                 , UpdatedBy
                                                  )
                                    Values('{0}','{1}','{2}','{3}','{4}','{5}')";

        public string CreateSku()
        {
            return AddSku;
        }

        public string DeleteSKU(GetSKU _input)
        {

            string sqlDeleteSKU = @" Update StockKeepingUnit Set   IsActive='0'  where StockKeepingUnitId=" + _input.StockKeepingUnitId;
            return sqlDeleteSKU;

        }

        public string GetAstockKeepingUnit(GetSKU _SkuId)
        {
            string sqlGetASku = @"Select StockKeepingUnitName,IsActive,
                                            CreatedOn,CreatedBy 
                                            From StockKeepingUnit where StockKeepingUnitId = " + _SkuId.StockKeepingUnitId;
            return sqlGetASku;
        }


        public string UpdateSKU(int stockKeepingUnitId, string stockKeepingUnitName,  DateTime CreatedDate, string Createdby, DateTime UpdatedDate, string UpdatedBy, bool IsActive)

        {
            string UpdateStockKeepingUnit= @" Update StockKeepingUnit Set   StockKeepingUnitName='" + stockKeepingUnitName +  "',UpdatedOn=GETDATE(),UpdatedBy='" + UpdatedBy + "',IsActive='" + IsActive + "' where StockKeepingUnitId=" + stockKeepingUnitId;


            return UpdateStockKeepingUnit;
        }

    }
}
