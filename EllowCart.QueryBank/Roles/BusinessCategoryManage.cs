﻿using EllowCart.DTO.BusinessCategory.InputModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace EllowCart.QueryBank.Roles
{
   public class BusinessCategoryManage
    {
        private string sqlAddBusinessCategory = @" Insert into BussinessCategory(
                                                            
                                                            BussinessCategoryName,
                                                            BussinessCategoryDecr,
                                                            BussinessTypeId,
                                                            CreatedOn,
                                                            CreatedBy,
                                                            UpdatedOn,
                                                            UpdatedBy,
                                                            IsActive ) 
                                                            values ('{0}','{1}',{2},'{3}','{4}','{5}','{6}','{7}') ";

        private string sqlGetAllBusinessCategory = @"SELECT A.BussinessCategoryId, A.BussinessCategoryName, A.BussinessCategoryDecr,
                                                     A.CreatedOn, B.Name, A.CreatedBy,A.IsActive,B.BussinessTypeId,B.Name as BusinessType
                                                     FROM  BussinessCategory AS A LEFT OUTER JOIN
                                                     BussinessType AS B ON A.BussinessTypeId = B.BussinessTypeId where A.IsActive=1";




        public string GetAddBusinessCategory()
        {
            return sqlAddBusinessCategory;
        }
        public string GetAllBusinessCategory()
        {
            return sqlGetAllBusinessCategory;
        }


        public string DeleteBusinessCategory(GetBusinessCategoryAdminInput obj)
        {

            string sqlDeleteBusinessCategory = @"UPDATE BussinessCategory SET IsActive=0 WHERE BussinessCategoryId=" + obj.BussinessCategoryId;
            return sqlDeleteBusinessCategory;

        }
        public string UpdateBusinessCategory(int BusinessCategId, string name, string descr, long BusinessTypeId, DateTime CreatedDate, string Createdby, DateTime UpdatedDate, string UpdatedBy, bool IsActive)

        {
            string UpdateBusinessCateg = @" Update BussinessCategory Set   BussinessCategoryName='" + name + "',BussinessCategoryDecr='" + descr + "',BussinessTypeId=" + BusinessTypeId + ",UpdatedOn=GETDATE(),UpdatedBy='" + UpdatedBy + "',IsActive='" + IsActive + "' where BussinessCategoryId=" + BusinessCategId;


            return UpdateBusinessCateg;
        }
        public string GetABusinessCategory(GetBusinessCategoryAdminInput BusinessCategOut)
        {
            string sqlGetABusinessCategory = @"SELECT A.BussinessCategoryId, A.BussinessCategoryName, A.BussinessCategoryDecr,
                                                     A.CreatedOn, A.IsActive, B.Name, A.CreatedBy,B.BussinessTypeId
                                                     FROM  BussinessCategory AS A LEFT OUTER JOIN
                                                     BussinessType AS B ON A.BussinessTypeId = B.BussinessTypeId where BussinessCategoryId = " + BusinessCategOut.BussinessCategoryId;
            return sqlGetABusinessCategory;
        }
    }
}
