﻿using EllowCart.DTO.Module.InputModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace EllowCart.QueryBank.Roles
{
    public class VerificationServices
    {
        private string sqlAddModules = @" Insert into ModuleMaster(
                                                            
                                                            Modulename,
                                                            IsActive,
                                                            CreatedOn,
                                                            CreatedBy,
                                                            UpdatedOn,
                                                            UpdatedBy ) 
                                                            values ('{0}','{1}','{2}',{3},'{4}',{5}) ";

        private string sqlGetAllModules = @"Select ModuleId,Modulename,
                                        CreatedOn,CreatedBy,UpdatedOn,UpdatedBy ,IsActive
                                        From ModuleMaster where IsActive=1";


        public string GetAddModules()
        {
            return sqlAddModules;
        }
        public string GetAllModules()
        {
            return sqlGetAllModules;
        }


        public string DeleteModules(ModuleOuput _out)
        {

            string sqlDeleteModule = @" Update ModuleMaster Set   IsActive='0'  where ModuleId=" + _out.ModuleId;
            return sqlDeleteModule;

        }
        public string UpdateModules(string module, bool status, DateTime CreatedDate, string Createdby, DateTime UpdatedDate, string UpdatedBy, int ModuleId)

        {
            string UpdateModule = @" Update ModuleMaster Set   Modulename='" + module + "',IsActive='" + status + "',UpdatedOn=GETDATE(),UpdatedBy='" + UpdatedBy + "' where ModuleId=" + ModuleId;


            return UpdateModule;
        }


        public string GetAModule(ModuleOuput _moduleout)
        {
            string sqlGetAModule = @"Select ModuleId,Modulename,IsActive,
                                            CreatedOn,CreatedBy,UpdatedOn,UpdatedBy 
                                            From ModuleMaster where ModuleId = " + _moduleout.ModuleId;
            return sqlGetAModule;
        }






        private string sqlAddModuleAttribute = @" Insert into ModuleAttribute(
                                                            
                                                            ModuleId,
                                                            Operation,
                                                            IsActive,
                                                            CreatedOn,
                                                            CreatedBy,
                                                            UpdatedOn,
                                                            UpdatedBy ) 
                                                            values ({0},'{1}','{2}','{3}','{4}','{5}','{6}') ";

        private string sqlGetAllModuleAttributes = @"Select AttributeId,A.ModuleId,B.ModuleName,A.IsActive,A.Operation,
                                            A.CreatedOn,A.CreatedBy,A.UpdatedOn,A.UpdatedBy 
                                            From ModuleAttribute A
											left Outer Join ModuleMaster B on B.ModuleId=A.ModuleId
                                            where A.IsActive=1";




        public string GetAddModuleAttribute()
        {
            return sqlAddModuleAttribute;
        }
        public string GetAllModuleAttributes()
        {
            return sqlGetAllModuleAttributes;
        }


        public string DeleteModuleAttribute(ModuleAttributeOutputModel ModuleAttr)
        {

            string sqlDeleteModule = @"Update ModuleAttribute Set   IsActive='0'   where AttributeId=" + ModuleAttr.AttributeId;
            return sqlDeleteModule;

        }
        public string UpdateModuleAttribute(int moduleId, string Operation, bool IsActive, DateTime CreatedDate, string Createdby, DateTime UpdatedDate, string UpdatedBy, int AttributeId)

        {
            string UpdateModuleAttribute = @" Update ModuleAttribute Set   ModuleId=" + moduleId + ",Operation='" + Operation + "',IsActive='" + IsActive + "',UpdatedOn=GETDATE(),UpdatedBy='" + UpdatedBy + "' where AttributeId=" + AttributeId;


            return UpdateModuleAttribute;
        }


        public string GetAModuleAttribute(ModuleAttributeOutputModel ModuleAttrOut)
        {
            string sqlGetAModuleAttribute = @"Select AttributeId,A.ModuleId,B.ModuleName,Operation,A.IsActive,
                                               A.CreatedOn,A.CreatedBy,A.UpdatedOn,A.UpdatedBy 
                                               From ModuleAttribute A
                                               left outer join ModuleMaster B on B.ModuleId=A.ModuleId
                                               where AttributeId=" + ModuleAttrOut.AttributeId;
            return sqlGetAModuleAttribute;
        }

        public string ModuleAttributeGet(ModuleOuput ModuleAttrOut)
        {
            string sqlGetAModuleAttribute = @"Select AttributeId,Operation,AttributeId
                                               From ModuleAttribute
                                               where ModuleId=" + ModuleAttrOut.ModuleId;
            return sqlGetAModuleAttribute;
        }

    }
}
