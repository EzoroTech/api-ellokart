﻿using EllowCart.DTO.SellerCategory.InputModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace EllowCart.QueryBank.Roles
{
    public class SellerCategoryManage
    {

        private string sqlAddSellerCategory = @" Insert into SellerCategory_Master(
                                                            
                                                            SellerCatName,
                                                            IsActive,
                                                            CreatedOn,
                                                            CreatedBy,
                                                            UpdatedOn,
                                                            UpdatedBy,
                                                            BussinessTypeId) 
                                                            values ('{0}','{1}','{2}','{3}','{4}','{5}',{6}) ";

        private string sqlGetAllSellerCategory = @"Select SellerCatId,SellerCatName,A.IsActive,
                                                    A. CreatedOn,A.CreatedBy,A.UpdatedOn,A.UpdatedBy ,A.BussinessTypeId,B.Name AS BusinessTypeName
                                                    From SellerCategory_Master A
											        LEFT OUTER JOIN BussinessType B ON B.BussinessTypeId=A.BussinessTypeId
											        where A.IsActive=1";




        public string GetAddSellerCategory()
        {
            return sqlAddSellerCategory;
        }
        public string GetAllSellerCategory()
        {
            return sqlGetAllSellerCategory;
        }


        public string DeleteSellerCategory(SellerCategoryGetAdmin _getOutput)
        {

            string sqlDeleteSellerCateg = @"Delete from SellerCategory_Master where SellerCatId=" + _getOutput.SellerCatId;
            return sqlDeleteSellerCateg;

        }
        public string UpdateSellerCategory(int SellerCatId, string SellerCategName, bool Isactive, DateTime CreatedDate, string Createdby, DateTime UpdatedDate, string UpdatedBy, int BusinessTypeId)

        {
            string UpdateSellerCateg = @" Update SellerCategory_Master Set   SellerCatName='" + SellerCategName + "',IsActive='" + Isactive + "',UpdatedOn=GETDATE(),UpdatedBy='" + UpdatedBy + "',BussinessTypeId=" + BusinessTypeId + " where SellerCatId = " + SellerCatId;


            return UpdateSellerCateg;
        }
        public string GetASellerCateg(SellerCategoryGetAdmin _obj)
        {
            string sqlGetASellerCateg = @"Select SellerCatId,SellerCatName,A.IsActive,
                                           A.CreatedOn,A.CreatedBy,A.UpdatedOn,A.UpdatedBy, A.BussinessTypeId,B.Name As BusinessTypeName
                                           From SellerCategory_Master A
                                           LEFT OUTER JOIN BussinessType B on B.BussinessTypeId=A.BussinessTypeId
                                           where SellerCatId =" + _obj.SellerCatId;
            return sqlGetASellerCateg;
        }
    }
}
