﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EllowCart.QueryBank.Roles
{
   public class CouponCodeManage
    {


        private string sqlAddCouponCode = @" Insert into CouponCodes(
                                                            
                                                            CouponCode,
                                                            Description,
                                                            IsActive,
                                                            CreatedOn,
                                                            CreatedBy,
                                                            UpdatedOn,
                                                            UpdatedBy ) 
                                                            values ('{0}','{1}','{2}',{3},'{4}','{5}','{6}') ";

        private string sqlGetAllCouponCode = @"Select CouponId,CouponCode,IsActive,
                                            CreatedOn,CreatedBy,UpdatedOn,UpdatedBy 
                                            From CouponCodes";




        public string GetAddCouponCode()
        {
            return sqlAddCouponCode;
        }
        public string GetAllCouponCode()
        {
            return sqlGetAllCouponCode;
        }


        public string DeleteCouponCode(int CouponId)
        {

            string sqlDeleteCouponCode = @"Delete from CouponCodes where CouponId=" + CouponId;
            return sqlDeleteCouponCode;

        }
        public string UpdateCouponCode(string CouponCode, string Description, bool status, DateTime CreatedDate, string Createdby, DateTime UpdatedDate, string UpdatedBy, int CouponId)

        {
            string UpdateCouponCode = @" Update CouponCodes Set   CouponCode='" + CouponCode + "',Description='" + Description + "',IsActive='" + status + "',CreatedOn='" + CreatedDate + "',CreatedBy='" + Createdby + "',UpdatedOn='" + UpdatedDate + "',UpdatedBy='" + UpdatedBy + "' where CouponId=" + CouponId;


            return UpdateCouponCode;
        }
        public string GetACouponCode(int CouponId)
        {
            string sqlGetACouponCode = @"Select CouponId,CouponCode,IsActive,
                                            CreatedOn,CreatedBy,UpdatedOn,UpdatedBy 
                                            From CouponCodes where CouponId = " + CouponId;
            return sqlGetACouponCode;
        }
    }
}
