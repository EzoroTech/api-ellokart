﻿using System;
using System.Collections.Generic;
using System.Text;
using EllowCart.DTO.ProductBrand.InputModels;
namespace EllowCart.QueryBank.Roles
{
    public class ProductBrandManage
    {

        private string sqlAddProductBrand = @" Insert into ProductBrand_Master(                                                          
                                                            BrandCode,
                                                            BrandName,
                                                            IsActive,
                                                            BrandDescription,
                                                           
                                                            ProductCategoryId,
                                                            ManufactureName,
                                                            CreatedOn,
                                                            CreatedBy,
                                                            UpdatedOn
                                                           
                                                            ) 
                                                            values ('{0}','{1}','{2}','{3}',{4},'{5}','{6}','{7}','{8}') ";

        private string sqlGetAllProductBrand = @"Select A.BrandId,A.BrandCode,A.BrandName,A.IsActive,
                                            A.CreatedOn,
                                             A.BrandDescription,B.CategoryName
                                            From ProductBrand_Master A LEFT OUTER JOIN ProductCategory_Master B ON A.ProductCategoryId=B.ProductCategoryId order by BrandId desc ";




        public string GetAddProductBrand()
        {
            return sqlAddProductBrand;
        }
        public string GetAllProductBrands()
        {
            return sqlGetAllProductBrand;
        }


        public string DeleteProductBrand(long BrandId)
        {

            string sqlDeleteProductBrand = @"Delete from ProductBrand_Master where BrandId=" + BrandId;
            return sqlDeleteProductBrand;

        }
        public string UpdateProductBrand(string brandcode, string brandName, bool IsActive,DateTime UpdatedOn,  string UpdatedBy,  string BrandDescription, string manufactureName,int ProductCategoryId, long BrandId)
                                                                                                                                                                                                                                                                                                                        


        {
            string UpdateProductBrand = @" Update ProductBrand_Master Set   BrandCode='" + brandcode + "',BrandName='" + brandName + "',IsActive='" + IsActive + "',UpdatedOn=GETDATE(),UpdatedBy='" + UpdatedBy +  "',BrandDescription='" + BrandDescription + "',ManufactureName='" + manufactureName + "',ProductCategoryId=" + ProductCategoryId + " where BrandId=" + BrandId;


            return UpdateProductBrand;
        }
        public string GetAProductBrand(long BrandId)
        {
            string sqlGetAProductBrand = @"Select A.BrandId,A.BrandCode,A.BrandName,A.IsActive,A.BrandDescription,
                                     A.CreatedOn,A.CreatedBy,A.UpdatedOn,A.UpdatedBy,A.ProductCategoryId,
                                     A.BrandDescription,A.ManufactureName,B.CategoryName
                                     From ProductBrand_Master A
                                     LEFT OUTER JOIN ProductCategory_Master B ON A.ProductCategoryId=B.ProductCategoryId
                                     where BrandId = " + BrandId;
            return sqlGetAProductBrand;
        }

      
    }
}
