﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EllowCart.QueryBank.Roles
{
    public class ProductSpecValueManage
    {



        private string sqlAddProductSpecValue = @" Insert into ProductSpecValue(
                                                            
                                                            ProductSpecId,
                                                            ProductId,
                                                            VarientValue,
                                                            UserBussinessID
                                                            ) 
                                                            values ({0},{1},'{2}',{3}) ";

        private string sqlGetAllProductSpecValue = @"select P3.SpecificationName,P2.ProductName,p1.VarientValue,p4.BussinessName
                                                     From ProductSpecValue P1
                                                     LEFT OUTER JOIN ProductMaster P2 ON P1.ProductId=P2.ProductID
                                                     LEFT OUTER JOIN ProductSpecificationMaster P3 ON P1.ProductSpecId=P3.ProductSpecId
                                                     LEFT OUTER JOIN UserBussiness P4 ON P2.UserBussinessId=P4.UserBussinessId";




        public string GetAddProductSpecValue()
        {
            return sqlAddProductSpecValue;
        }
        public string GetAllProductSpecValue()
        {
            return sqlGetAllProductSpecValue;
        }


        public string DeleteProductSpecValue(int ProductSpecValueId)
        {

            string sqlDeleteProductSpecValue = @"Delete from ProductSpecValue where ProductSpecValueId=" + ProductSpecValueId;
            return sqlDeleteProductSpecValue;

        }
        public string UpdateProductSpecValue(int ProductSpecId, int ProductId, string VarientValue, int UserBussinessID, int ProductSpecValueId)

        {
            string UpdatePrdtSpecValue = @" Update ProductSpecValue Set   ProductSpecId=" + ProductSpecId + ",ProductId=" + ProductId + ",VarientValue='" + VarientValue + "',UserBussinessID=" + UserBussinessID + " where ProductSpecValueId=" + ProductSpecValueId;
            return UpdatePrdtSpecValue;

        }
        public string GetAProductSpecValue(int ProductSpecValueId)
        {
            string sqlGetProductSpecValue = @"select P3.SpecificationName,P2.ProductName,p1.VarientValue,p4.BussinessName
                                                     From ProductSpecValue P1
                                                     LEFT OUTER JOIN ProductMaster P2 ON P1.ProductId=P2.ProductID
                                                     LEFT OUTER JOIN ProductSpecificationMaster P3 ON P1.ProductSpecId=P3.ProductSpecId
                                                     LEFT OUTER JOIN UserBussiness P4 ON P2.UserBussinessId=P4.UserBussinessId where ProductSpecValueId = " + ProductSpecValueId;
            return sqlGetProductSpecValue;
        }
    }
}
