﻿using EllowCart.DTO.Tax.InputModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace EllowCart.QueryBank.Roles
{
    public class TaxMasterManage
    {



        private string sqlAddToTaxMaster = @" Insert into TaxMaster(
                                                           
                                                          
                                                            TaxName,
                                                            TaxPercentage,
                                                            CreatedOn,
                                                            CreatedBy,
                                                            UpdatedOn,
                                                            UpdatedBy,
                                                            IsActive,
                                                            TaxType
                                                            ) 
                                                            values ('{0}','{1}','{2}','{3}','{4}','{5}','{6}','{7}') ";

        private string sqlGetAllTaxes = @"Select TaxId,TaxName,TaxPercentage,
                                          CreatedOn, CreatedBy,UpdatedOn,
                                          UpdatedBy,IsActive,TaxType
                                          From TaxMaster where IsActive=1";




        public string GetAddTax()
        {
            return sqlAddToTaxMaster;
        }
        public string GetAllTaxes()
        {
            return sqlGetAllTaxes;
        }


        public string DeleteAtax(TaxModel _obj)
        {

            string sqlDeleteTax = @"Update TaxMaster set IsActive=0 where TaxId=" + _obj.TaxId;
            return sqlDeleteTax;

        }
        public string UpdateAtax(int TaxId, string TaxName, string TaxPercentage, DateTime CreatedDate, string Createdby, DateTime UpdatedDate, string UpdatedBy, bool IsActive, string TaxType)

        {
            string UpdateTax = @" Update TaxMaster Set   TaxName='" + TaxName + "',TaxPercentage='" + TaxPercentage + "',UpdatedOn=GETDATE(),UpdatedBy='" + UpdatedBy + "',IsActive='" + IsActive + "',TaxType='" + TaxType + "' where TaxId=" + TaxId;


            return UpdateTax;
        }
        public string GetAtax(TaxModel _input)
        {
            string sqlGetAtax = @"Select TaxId,TaxName,TaxPercentage,
                                  CreatedOn, CreatedBy,UpdatedOn,
                                  UpdatedBy,IsActive,TaxType
                                  From TaxMaster where TaxId = " + _input.TaxId;
            return sqlGetAtax;
        }

    }
}
