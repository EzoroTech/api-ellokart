﻿using System;
using System.Collections.Generic;
using System.Text;
using EllowCart.DTO.Verification.InputModels;

namespace EllowCart.QueryBank.Roles
{
    public class Verification
    {

         private string sqlGetAllUserBusiness = @"   Select C.DisplayName,A.CreatedOn,C.BranchId,A.UserBussinessId,B.UserId,    
                                                      C.BranchName,B.MobileNo,E.LocationName,E.Addresslineone,E.Addresslinetwo,E.Pincode,    
                                                      C.ContactPerson,F.MembershipPlan,E.Description,B.EmailID,G.VerifiedDate,H.GSTNO,    
                                                      I.FranchiseName,G.Comments  ,E.Lattittude,E.Longittude  
                                                      From UserBussiness A    
                                                      LEFT OUTER JOIN User_Master B ON A.UserId=B.UserId    
                                                      LEFT OUTER JOIN BusinessBranches C ON C.UserBussinessId=A.UserBussinessId    
                                                      LEFT OUTER JOIN AccountAddress D ON D.AccountId=C.BranchId    
                                                      LEFT OUTER JOIN Address E ON E.AddressId=D.AddressId    
                                                      LEFT OUTER JOIN MembershipPlans_Master F ON F.MembershipPlanId=B.PlanId    
                                                      LEFT OUTER JOIN CustomerSupportData G ON G.UserId=B.UserId    
                                                      LEFT OUTER JOIN UserDocuments H ON H.UserId=B.UserId    
                                                      LEFT OUTER JOIN Franchise_Master I ON I.FranchiseCode=B.FranchiseCode    
                                                      where B.IsActive =0 order by CreatedOn desc";


        private string sqlGetVerifiedSellers = @"Select C.DisplayName,A.CreatedOn,C.BranchId,A.UserBussinessId,B.UserId,
                                                 C.BranchName,B.MobileNo,E.LocationName,E.Addresslineone,E.Addresslinetwo,E.Pincode,
                                                 C.ContactPerson,F.MembershipPlan,E.Description,B.EmailID,G.VerifiedDate,H.GSTNO
                                                From UserBussiness A
												LEFT OUTER JOIN User_Master B ON A.UserId=B.UserId
                                                LEFT OUTER JOIN BusinessBranches C ON C.UserBussinessId=A.UserBussinessId
                                                LEFT OUTER JOIN AccountAddress D ON D.AccountId=C.BranchId
                                                LEFT OUTER JOIN Address E ON E.AddressId=D.AddressId
                                                LEFT OUTER JOIN MembershipPlans_Master F ON F.MembershipPlanId=B.PlanId
												LEFT OUTER JOIN CustomerSupportData G ON G.UserId=B.UserId
												LEFT OUTER JOIN UserDocuments H ON H.UserId=B.UserId
                                                where B.IsActive =1 order by CreatedOn desc ";  


        private string sqlGetUsrToUpgrade = @"SELECT DISTINCT BusinessDisplayName,FranchiseCode,MobileNo,UserId,UserBusinessId FROM UserToUpgrade";

        public string GetOneVerifiedSeller(VerifiedBusiness _input)
        {
            string SqlGetOneVerifiedSeller = @"SELECT C.BranchId,C.BranchName, C.DisplayName,C.ContactPerson,E.Addresslineone,
                                             E.Addresslinetwo,E.Pincode,E.Description
	                                         FROM  UserBussiness A LEFT OUTER JOIN User_Master B ON B.UserId= A.UserId
	                                         LEFT OUTER JOIN BusinessBranches C ON C.UserBussinessId=A.UserBussinessId
	                                         LEFT OUTER JOIN AccountAddress D ON D.AccountId=C.BranchId
	                                         LEFT OUTER JOIN Address E ON E.AddressId=D.AddressId
	                                         WHERE C.BranchId=  " + _input.BranchId;
            return SqlGetOneVerifiedSeller;
        }

        public string SqlGetUpgradedSeller = @"SELECT C.MobileNo,B.BusinessDisplayName ,D.EmailId
                                            FROM BusinessStatus A
                                            LEFT OUTER JOIN UserBussiness B ON A.UserBussinessId=B.UserBussinessId
                                            LEFT OUTER JOIN User_Master C ON C.UserId=B.UserId
                                            LEFT OUTER JOIN BusinessBranches D ON D.UserId=C.UserId WHERE A.BusinessStatus=2";

        public string GetAllBusinessToUpgrade()
        {
            return sqlGetUsrToUpgrade;
        }
        public string GetAllUpgradededBusinesses()
        {
            return SqlGetUpgradedSeller;
        }
        public string GetAllBusinesses()
        {
            return sqlGetAllUserBusiness;
        }
        public string GetAllVerifiedBusinesses()
        {
            return sqlGetVerifiedSellers;
        }
        public string GetAUserBusiness(VerifiedBusinessModel _input)
        {
            string sqlGetAUserBusiness = @"SELECT A.MobileNo,A.EmailID,A.CreatedOn ,C.FranchiseName,A.UserId,B.UserBussinessId,
                                         D.DisplayName AS Business,D.MobileNumber ,F.LocationName,G.MembershipPlan,D.BranchId,
                                         d.BranchName,d.ContactPerson,f.Addresslineone,f.Addresslinetwo,f.Pincode,f.Description
                                        
                                         FROM User_Master A 
                                         LEFT OUTER JOIN Franchise_Master C ON C.Franchisecode=A.FranchiseCode
                                         LEFT OUTER JOIN UserBussiness B ON B.UserId=A.UserId
                                         LEFT OUTER JOIN BusinessBranches D ON D.UserId=B.UserId
                                         LEFT OUTER JOIN AccountAddress E ON E.AccountId =D.BranchId
                                         LEFT OUTER JOIN Address F ON F.AddressId=E.AddressId
                                         LEFT OUTER JOIN MembershipPlans_Master G ON G.MembershipPlanId=A.PlanId
                                        
                                          where A.UserId=" + _input.UserId;
            return sqlGetAUserBusiness;
        }

       

        //public string RejectUserBusiness(BusinessVerificationModel _input)

        //{
        //    string RejectBusiness = @" update User_Master set IsActive="+_input.IsActive+ "where UserId="+_input.UserId;
           


        //    return RejectBusiness;
        //}
        


    }
}
