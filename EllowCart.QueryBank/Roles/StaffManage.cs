﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EllowCart.QueryBank.Roles
{
  public  class StaffManage
    {
        private string StaffReg = @"INSERT INTO EzoroUser 
			                                    (EmployeeCode
			                                    ,Name
			                                    ,Username
			                                    ,Password
			                                    ,IsActive
			                                    ,CreatedOn
			                                    ,CreatedBy			                                   
			                                    ,AccountType)
                                    Values('{0}','{1}','{2}','{3}','{4}','{5}','{6}','{7}')";

        public string StaffRegisteration()
        {
            return StaffReg;
        }

        public string UpdateEzoroStaff(long EmployeeId,string EmployeeCode,string Name,string Username,string Password,bool IsActive,string UpdatedBy,string AccountType)
        {
            string UpdateEzoroStaff = @"UPDATE EzoroUser SET EmployeeCode='"+EmployeeCode+"',Name='"+Name+"',Username='"+Username+"',Password='"+Password+"',IsActive='"+IsActive+"',UpdatedOn=GETDATE(),UpdatedBy='"+UpdatedBy+"',AccountType='"+AccountType+"' WHERE EmployeeId="+EmployeeId;

            return UpdateEzoroStaff;
        }

        public string DeleteEzoroStaff(long EmployeeId)
        {
            string DeleteStaff = @"DELETE EzoroUser WHERE EmployeeId="+EmployeeId;
            return DeleteStaff;
        }
        public string GetAllEzoroStaff()
        {
            string GetAllStaff = @"select  EmployeeId,EmployeeCode,Name,Username,Password,IsActive,CreatedOn,AccountType
                                   from EzoroUser";

            return GetAllStaff;
        }

        public string GetOneEzoroStaff(long EmployeeId)
        {
            string GetOneStaff = @"select  EmployeeId,EmployeeCode,Name,Username,Password,IsActive,CreatedOn,AccountType
                                   from EzoroUser where EmployeeId="+EmployeeId;

            return GetOneStaff;
        }

    }
}
