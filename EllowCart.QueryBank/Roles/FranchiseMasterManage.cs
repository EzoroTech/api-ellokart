﻿using System;
using System.Collections.Generic;
using System.Text;
using EllowCart.DTO.Franchise.InputModels;
namespace EllowCart.QueryBank.Roles
{
    public class FranchiseMasterManage
    {

        private string sqlAddFranchiseMaster = @" Insert into Franchise_Master(
                                                            
                                                            FranchiseCode,
                                                            PhoneNumber,
                                                            EmailId,
                                                            FirstName,
                                                            LastName,
                                                            DOB,
                                                            Gender,
                                                            Password,
                                                            IsActive,
                                                            CreatedOn,
                                                            CreatedBy
                                                            UpdatedOn,
                                                            UpdatedBy
                                                            ) 
                                                            values ('{0}','{1}','{2}','{3}','{4}','{5}','{6}','{7}','{8}','{9}','{10}','{11}','{12}') ";

        private string sqlGetAllFranchiseMaster = @"Select FranchiseId,FranchiseCode,PhoneNumber,
                                                    EmailId,FirstName,LastName,DOB,Gender,
                                                    Password,IsActive,CreatedOn,
                                                    CreatedBy,UpdatedOn,UpdatedBy,FranchiseName
                                                    From Franchise_Master where IsActive=1";




        public string GetAddFranchiseMaster()
        {
            return sqlAddFranchiseMaster;
        }
        public string GetAllFranchiseMaster()
        {
            return sqlGetAllFranchiseMaster;
        }


        public string DeleteFranchiseMaster(FranchiseModel  _input)
        {

            string sqlDeleteFranchiseMaster = @"Update Franchise_Master Set   IsActive='0'  where FranchiseId = " + _input.FranchiseId;
            return sqlDeleteFranchiseMaster;

        }
        public string UpdateFranchiseMaster( string PhoneNumber, string EmailId, string FirstName, string LastName, DateTime DOB, string Gender, string Password, bool IsActive, DateTime UpdatedDate, string UpdatedBy, int FranchiseId,string FranchiseCode)

        {
            string UpdateFrnchseMaster = @" Update Franchise_Master Set   PhoneNumber='" + PhoneNumber + "',EmailId='" + EmailId + "',FirstName='" + FirstName + "',LastName='" + LastName + "',DOB='" + DOB + "',Gender='" + Gender + "',Password='" + Password + "',IsActive='" + IsActive + "',UpdatedOn=GETDATE(),UpdatedBy='" + UpdatedBy + "',FranchiseCode='"+FranchiseCode+"' where FranchiseId=" + FranchiseId;


            return UpdateFrnchseMaster;
        }

        public string GetAFranchise(FranchiseModel _input)
        {
            string sqlGetAFranchise = @"Select FranchiseId,FranchiseCode,PhoneNumber,
                                                       EmailId,FirstName,LastName,DOB,Gender,
                                                        Password,IsActive,CreatedOn,
                                                       CreatedBy,UpdatedOn,UpdatedBy 
                                                        From Franchise_Master where FranchiseId = " + _input.FranchiseId;
            return sqlGetAFranchise;
        }

    }
}
