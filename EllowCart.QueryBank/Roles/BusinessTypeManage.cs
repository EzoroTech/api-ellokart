﻿using EllowCart.DTO.Business.InputModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace EllowCart.QueryBank.Roles
{
    public class BusinessTypeManage
    {


        private string sqlAddBusinessType = @" Insert into BussinessType(
                                                            
                                                            Name,
                                                            Decr,
                                                            CreatedOn,
                                                            CreatedBy,
                                                            UpdatedOn,
                                                            UpdatedBy,
                                                            IsActive ) 
                                                            values ('{0}','{1}','{2}','{3}','{4}','{5}','{6}') ";

        private string sqlGetAllBusinessType = @"Select BussinessTypeId,Name,Decr,
                                            CreatedOn,CreatedBy,UpdatedOn,UpdatedBy,IsActive
                                            From BussinessType where IsActive=1";




        public string GetAddBusinessType()
        {
            return sqlAddBusinessType;
        }
        public string GetAllBusinessType()
        {
            return sqlGetAllBusinessType;
        }


        public string DeleteBusinessTypes(GetBusinessType _Businesstype)
        {

            string sqlBusinessType = @" Update BussinessType Set   IsActive='0'   where BussinessTypeId=" + _Businesstype.BussinessTypeId;
            return sqlBusinessType;

        }
        public string UpdateBusinessType(int BusinessTypeId, string name, string descr, DateTime CreatedDate, string Createdby, DateTime UpdatedDate, string UpdatedBy, bool IsActive)

        {
            string UpdateBusinessType = @" Update BussinessType Set   Name='" + name + "',Decr='" + descr + "',UpdatedOn=GETDATE(),UpdatedBy='" + UpdatedBy + "',IsActive='" + IsActive + "' where BussinessTypeId=" + BusinessTypeId;


            return UpdateBusinessType;
        }


        public string GetABusinessType(GetBusinessType _Businesstype)
        {
            string sqlGetABusinessType = @"Select BussinessTypeId,Name, Decr ,IsActive,
                                            CreatedOn,CreatedBy,UpdatedOn,UpdatedBy 
                                            From BussinessType where BussinessTypeId = " + _Businesstype.BussinessTypeId;
            return sqlGetABusinessType;
        }

    }
}
