﻿using EllowCart.DTO.Roles.InputModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace EllowCart.QueryBank.Roles
{
    public class Role
    {

        private string AddRoles = @" Insert into RoleMaster(
                                                                
                                                             RoleName,
                                                             IsActive,
                                                             CreatedOn,
                                                             CreatedBy,
                                                             UpdatedOn,
                                                             UpdatedBy ) 
                                                             values('{0}','{1}','{2}',{3},'{4}',{5}) ";
        private string sqlGetAllRoles = @"Select RoleId,RoleName,IsActive,
                                            CreatedOn,CreatedBy,UpdatedOn,UpdatedBy 
                                            From RoleMaster where IsActive=1";

        public string GetAddRoles()
        {
            return AddRoles;
        }
        public string GetAllRoles()
        {
            return sqlGetAllRoles;
        }
        public string DeleteRole(RoleSelectModel _role)
        {

            string sqlDeleteRole = @"Delete from RoleMaster where RoleId=" + _role.RoleId;
            return sqlDeleteRole;

        }
        public string UpdateRoles(int roleId, string role, bool status, DateTime CreatedDate, string Createdby, DateTime UpdatedDate, string UpdatedBy)

        {
            string UpdateRole = @" Update RoleMaster Set   RoleName='" + role + "',IsActive='" + status + "',UpdatedOn=GETDATE(),UpdatedBy='" + UpdatedBy + "' where RoleId=" + roleId;


            return UpdateRole;
        }
        public string GetARole(RoleSelectModel _Rinput)
        {
            string sqlGetARole = @"Select RoleId,RoleName,IsActive,
                                   CreatedOn,CreatedBy,UpdatedOn,UpdatedBy 
                                   From RoleMaster where RoleId = " + _Rinput.RoleId;
            return sqlGetARole;
        }




        private string sqlAddRoleAttribute = @" Insert into RoleAttributes(
                                                            RoleId,
                                                            ModuleId,
                                                            ModuleAttributeId,
                                                            IsActive,
                                                            CreatedOn,
                                                            CreatedBy,
                                                            UpdatedOn,
                                                            UpdatedBy ,
                                                            RoleAttributeId ) 
                                                            values ({0},{1},{2},'{3}','{4}','{5}','{6}','{7}',{8}) ";

        private string sqlGetAllRoleAttributes = @"SELECT A.RoleAtributeID,A.RoleId,B.RoleName,C.ModuleId,C.ModuleName,
                                                    D.AttributeId,D.Operation,A.CreatedOn,A.CreatedBy,A.IsActive
                                                    FROM RoleAttributes A
                                                    LEFT OUTER JOIN RoleMaster B ON B.RoleId=A.RoleId
                                                    LEFT OUTER JOIN ModuleMaster C ON C.ModuleId=A.ModuleId
                                                    LEFT OUTER JOIN ModuleAttribute D ON D.AttributeId=A.ModuleAttributeId
                                                    WHERE A.IsActive=1";


        public string GetAddRoleAttribute()
        {
            return sqlAddRoleAttribute;
        }
        public string GetAllRoleAttributes()
        {
            return sqlGetAllRoleAttributes;
        }


        public string DeleteRoleAttribute(RoleAttributeSelectModel _roleAttrSelect)
        {

            string sqlDeleteRoleAttribute = @"Delete from RoleAttributes where AttributeId=" + _roleAttrSelect.RoleAttributeID;
            return sqlDeleteRoleAttribute;

        }
        public string UpdateRoleAttribute(int RoleAttrId, int RoleId, int moduleId, int moduleAttributeId, bool IsActive, DateTime CreatedDate, string Createdby, DateTime UpdatedDate, string UpdatedBy)

        {
            string UpdateRoleAttribute = @" Update RoleAttributes Set   RoleId=" + RoleId + ",ModuleId=" + moduleId + ",moduleAttributeId=" + moduleAttributeId + ",IsActive='" + IsActive + "',UpdatedOn='" + UpdatedDate + "',UpdatedBy='" + UpdatedBy + "' where RoleAtributeId=" + RoleAttrId;


            return UpdateRoleAttribute;
        }


        public string GetARoleAttribute(RoleAttributeSelectModel _rAttr)
        {
            string sqlGetARoleAttribute = @"SELECT A.RoleAtributeID,A.RoleId,B.RoleName,C.ModuleId,C.ModuleName,
                                            D.AttributeId,D.Operation,A.CreatedOn,A.CreatedBy,A.IsActive
                                            FROM RoleAttributes A
                                            LEFT OUTER JOIN RoleMaster B ON B.RoleId=A.RoleId
                                            LEFT OUTER JOIN ModuleMaster C ON C.ModuleId=A.ModuleId
                                            LEFT OUTER JOIN ModuleAttribute D ON D.AttributeId=A.ModuleAttributeId
                                            WHERE A.RoleAtributeID=" + _rAttr.RoleAttributeID;
            return sqlGetARoleAttribute;
        }



    }
}
