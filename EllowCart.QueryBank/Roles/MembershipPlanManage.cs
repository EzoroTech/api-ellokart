﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EllowCart.QueryBank.Roles
{
    public class MembershipPlanManage
    {



        private string sqlAddMembershipPlan = @" Insert into MembershipPlans_Master(
                                                            
                                                            MembershipPlan,
                                                            Price,
                                                            ValidityInMonths,
                                                            Description,
                                                            IsActive,
                                                            TaxId,
                                                            CreatedOn,
                                                            CreatedBy,
                                                            UpdatedOn,
                                                            UpdatedBy
                                                            ) 
                                                            values ('{0}',{1},{2},'{3}','{4}',{5},'{6}','{7}','{8}','{9}') ";

        private string sqlGetAllMembershipPlan = @"Select MembershipPlanId,MembershipPlan,Price,
                                                       ValidityInMonths,Description,IsActive,TaxId,CreatedOn,
                                                       CreatedBy,UpdatedOn,UpdatedBy
                                                       From MembershipPlans_Master";




        public string GetAddMembershipPlan()
        {
            return sqlAddMembershipPlan;
        }
        public string GetAllMembershipPlan()
        {
            return sqlGetAllMembershipPlan;
        }


        public string DeleteMembershipPlan(int MembershipPlanId)
        {

            string sqlDeleteMembershipPlan = @"Delete from MembershipPlans_Master where MembershipPlanId=" + MembershipPlanId;
            return sqlDeleteMembershipPlan;

        }
        public string UpdateMembershipPlan(string name, float Price, int ValidityInMonths, string descr, bool IsActive, DateTime CreatedDate, string Createdby, DateTime UpdatedDate, string UpdatedBy, int MembershipPlanId)

        {
            string UpdateMembershipPlan = @" Update MembershipPlans_Master Set   BussinessCategoryName='" + name + "',Price=" + Price + ",ValidityInMonths=" + ValidityInMonths + ",Description='" + descr + "',IsActive=" + IsActive + ",UpdatedOn=GETDATE(),UpdatedBy='" + UpdatedBy + "' where MembershipPlanId=" + MembershipPlanId;


            return UpdateMembershipPlan;
        }




    }
}
