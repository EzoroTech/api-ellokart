﻿using EllowCart.DTO.ProductSpecification.InputModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace EllowCart.QueryBank.Roles
{
    public class ProductSpecificationManage
    {

        private string sqlAddProductSpecification = @" Insert into ProductSpecificationMaster(                                                           
                                                            SpecificationName,
                                                            Description,
                                                            IsActive,
                                                            CreatedOn,
                                                            CreatedBy)                                                            
                                                            values ('{0}','{1}','{2}','{3}','{4}') ";

        private string sqlGetAllProductSpec = @"Select ProductSpecId,SpecificationName,IsActive,Description,
                                                CreatedOn,CreatedBy,UpdatedOn,UpdatedBy 
                                                From ProductSpecificationMaster where IsActive=1";


        private string sqlGetAllProductSpecAttribute = @"SELECT A.ProductSubCategoryId,B.CategoryName,A.SpecId,C.SpecificationName,A.ProductSpecId 
                                                        FROM ProductSubCatSpec A
                                                        LEFT OUTER JOIN ProductCategory_Master B ON B.ProductCategoryId=A.ProductSubCategoryId
                                                        LEFT OUTER JOIN ProductSpecificationMaster C ON C.ProductSpecId=A.ProductSpecId";

        public string AddProductSpecification()
        {
            return sqlAddProductSpecification;
        }
        public string GetAllProductSpec()
        {
            return sqlGetAllProductSpec;
        }


        public string DeleteProductSpec(long ProductSpecId)
        {

            string sqlDeleteProductSpec = @"UPDATE ProductSpecificationMaster set IsActive=0 where ProductSpecId=" + ProductSpecId;
            return sqlDeleteProductSpec;

        }
        public string UpdateProductSpec(string SpecificationName, string Description, bool status,string UpdatedBy, long ProductSpecificationId)

        {
            string UpdateProductSpecification = @" Update ProductSpecificationMaster Set SpecificationName='" + SpecificationName + "',Description='" + Description + "',IsActive = '" + status + "',UpdatedOn=GETDATE(),UpdatedBy='" + UpdatedBy + "' where ProductSpecId=" + ProductSpecificationId;


            return UpdateProductSpecification;
        }
        public string GetAProductSpecification(long ProductSpecId)
        {
            string sqlGetAProductSpecification = @"Select ProductSpecId,SpecificationName,Description,IsActive,
                                                    CreatedOn,CreatedBy,UpdatedOn,UpdatedBy 
                                                    From ProductSpecificationMaster where ProductSpecId = " + ProductSpecId;
            return sqlGetAProductSpecification;
        }

        public string DeleteProductSpecAttribute(long ProductCategoryId)
        {
            string DeleteProductSpecAttribute = @"DELETE ProductSubCatSpec WHERE ProductSubCategoryId="+ProductCategoryId;

            return DeleteProductSpecAttribute;
        }

        public string GetAllProductSpecAttribute()
        {
            
            return sqlGetAllProductSpecAttribute;
        }
        public string GetONEProductSpecAttribute(long CategoryId)
        {
            string SQLGetOneSpecAttribute = @"SELECT A.ProductSubCategoryId,B.CategoryName,A.SpecId,C.SpecificationName,A.ProductSpecId 
                                              FROM ProductSubCatSpec A
                                              LEFT OUTER JOIN ProductCategory_Master B ON B.ProductCategoryId=A.ProductSubCategoryId
                                              LEFT OUTER JOIN ProductSpecificationMaster C ON C.ProductSpecId=A.ProductSpecId
                                              WHERE A.ProductSubCategoryId="+CategoryId;
            return SQLGetOneSpecAttribute;
        }

    }
}
