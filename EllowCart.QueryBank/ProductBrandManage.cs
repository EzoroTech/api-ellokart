﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EllowCart.QueryBank
{
    public class ProductBrandManage
    {


        private string sqlAddProductBrand = @" Insert into ProductBrand_Master(
                                                            
                                                            BrandCode,
                                                            BrandName,
                                                            IsActive,
                                                            CreatedOn,
                                                            CreatedBy,
                                                            UpdatedOn,
                                                            UpdatedBy,
                                                            ParentBrandId,
                                                            BrandDescription,
                                                            ManufactureName,
                                                            AccountType ) 
                                                            values ('{0}','{1}','{2}','{3}','{4}','{5}','{6}',{7},'{8}','{9}','{10}') ";

        private string sqlGetAllProductBrand = @"Select BrandId,BrandCode,BrandName,IsActive
                                            CreatedOn,CreatedBy,UpdatedOn,UpdatedBy,ParentBrandId,
                                             BrandDescription,ManufactureName,AccountType
                                            From ProductBrand_Master";




        public string GetAddProductBrand()
        {
            return sqlAddProductBrand;
        }
        public string GetAllProductBrands()
        {
            return sqlGetAllProductBrand;
        }


        public string DeleteProductBrand(int BrandId)
        {

            string sqlDeleteProductBrand = @"Delete from ProductBrand_Master where BrandId=" + BrandId;
            return sqlDeleteProductBrand;

        }
        public string UpdateProductBrand(string brandcode, string brandName, bool IsActive, DateTime CreatedDate, string Createdby, DateTime UpdatedDate, string UpdatedBy, int ParentBrandId, string BrandDescription, string ManufactureName, string AccountType, int BrandId)

        {
            string UpdateProductBrand = @" Update ProductBrand_Master Set   BrandCode='" + brandcode + "',BrandName='" + brandName + "',IsActive='" + IsActive + "',CreatedOn='" + CreatedDate + "',CreatedBy='" + Createdby + "',UpdatedOn='" + UpdatedDate + "',UpdatedBy='" + UpdatedBy + "',ParentBrandId=" + ParentBrandId + ",BrandDescription='" + BrandDescription + "',ManufactureName='" + ManufactureName + "',AccountType='" + AccountType + "' where BrandId=" + BrandId;


            return UpdateProductBrand;
        }


    }
}
