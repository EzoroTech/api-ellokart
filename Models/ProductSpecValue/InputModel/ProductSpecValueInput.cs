﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EllowCart.DTO.ProductSpecValue.InputModel
{
   public class ProductSpecValueInput
    {

        public int ProductSpecId { get; set; }
        public int ProductId { get; set; }
        public string VarientValue { get; set; }
        public int UserBussinessID { get; set; }



    }
}
