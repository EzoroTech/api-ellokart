﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EllowCart.DTO.BusinessSystem.InputModels
{
   public class Businesssystem
    {
        public long BussinesSystmId { get; set; }
        public string BussinesSystmName { get; set; }
        public string BussinesSystmDescr { get; set; }
        public string CreatedBy { get; set; }
        public int UpdatedBy { get; set; }
        public int Flag { get; set; }
        public bool IsActive { get; set; }
    }
}
