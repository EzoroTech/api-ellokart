﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EllowCart.DTO.BusinessSystem.OutputModels
{
  public  class BusinesssystemDetails
    {
        public long BussinesSystmId { get; set; }
        public string BussinesSystmName { get; set; }
        public string BussinesSystmDescr { get; set; }
    }
}
