﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EllowCart.DTO.Feed.InputModels
{
   public class AddFeedPostInput
    {
        public long UserId { get; set; }
        public string role { get; set; }
        public long BranchId { get; set; }
        public string AccountType { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public bool IActive { get; set; }
        public long OfferSpecialId { get; set; }
       
        public int NewsfeedType { get; set; }
       
    }
    
}
