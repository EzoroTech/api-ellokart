﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EllowCart.DTO.Feed.InputModels
{
  public  class GetFeedPostListForOfferImageInput
    {
        public long UserId { get; set; }
        public string role { get; set; }
        public long BranchId { get; set; }
        public long FeedId { get; set; }
    }
}
