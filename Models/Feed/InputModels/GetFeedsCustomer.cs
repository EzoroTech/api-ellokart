﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EllowCart.DTO.Feed.InputModels
{
  public  class GetFeedsCustomer
    {
        public int PageSize { get; set; }
        public int Index { get; set; }
        public long BranchId { get; set; }
    }
}
