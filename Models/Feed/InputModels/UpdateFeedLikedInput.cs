﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EllowCart.DTO.Feed.InputModels
{
   public class UpdateFeedLikedInput
    {
        public long CustomerId { get; set; }
        public long FeedId { get; set; }
      
        public int Flag { get; set; }
    }
}
