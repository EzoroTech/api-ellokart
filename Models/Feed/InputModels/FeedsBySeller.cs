﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EllowCart.DTO.Feed.InputModels
{
   public class FeedsBySeller
    {
        public long CustomerId { get; set; }    
        public long BranchId { get; set; }
        public int IntialIndex { get; set; }
        public int PageSize { get; set; }
    }
}
