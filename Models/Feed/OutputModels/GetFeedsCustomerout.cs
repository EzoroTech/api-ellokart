﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EllowCart.DTO.Feed.OutputModels
{
  public  class GetFeedsCustomerout
    {
        public string ImageUrl { get; set; }
        public long NewsFeedId { get; set; }
        public int ViewCount { get; set; }
        public DateTime UpdatedOn { get; set; }
        public long NoOfLikes { get; set; }
    }
}
