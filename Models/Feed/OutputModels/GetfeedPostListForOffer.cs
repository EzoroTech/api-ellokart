﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EllowCart.DTO.Feed.OutputModels
{
  public  class GetfeedPostListForOffer
    {
        public long FeedId { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string PostType { get; set; }
        public long ViewCount { get; set; }
        public float MRP { get; set; }
        public long ProductID { get; set; }
        public string ProductName { get; set; }
        public float OfferQty { get; set; }
        public float OfferPrice { get; set; }
        public long NoOfLikes { get; set; }
    }
}
