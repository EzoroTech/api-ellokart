﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EllowCart.DTO.Feed.OutputModels
{
   public class GetFeedEventsOut
    {
        public long FeedId { get; set; }
        public string Title { get; set; }
        public string PostType { get; set; }
        public string Description { get; set; }
        public long ViewCount { get; set; }
        public long NoOfLikes { get; set; }
        public string ImageUrl { get; set; }
    }
}
