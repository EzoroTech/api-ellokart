﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EllowCart.DTO.Feed.OutputModels
{
  public  class GetFeedPostOut
    {
        public long FeedId { get; set; }
        public string Title { get; set; }
        public string PostType { get; set; }
        public string Description { get; set; }
        public long ViewCount { get; set; }
        public long NoOfLikes { get; set; }
        public string ImageUrl { get; set; }
        public string CreatedOn { get; set; }
        public bool IsPostLiked { get; set; }
        public bool IsPostViewed { get; set; }
        public string ProPic { get; set; }
        public string BranchName { get; set; }
    }
}
