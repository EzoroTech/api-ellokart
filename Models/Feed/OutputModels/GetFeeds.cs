﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EllowCart.DTO.Feed.OutputModels
{
   public class GetFeeds
    {
        public List<GetFeedPostOut> Post { get; set; }
        public List<GetfeedPostListForOffer> offer { get; set; }
        public List<GetFeedEventsOut> Event { get; set; }
    }
}
