﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EllowCart.DTO.Feed.OutputModels
{
   public class GetFeedPostListForOfferImage
    {
        public string ImageUrl { get; set; }
        public string ImageName { get; set; }
        public long NewsFeedId { get; set; }
    }
}
