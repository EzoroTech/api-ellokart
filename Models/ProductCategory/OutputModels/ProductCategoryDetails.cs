﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EllowCart.DTO.ProductCategory.OutputModels
{
  public  class ProductCategoryDetails
    {
        public long ProductCategoryId { get; set; }
        public string CategoryName { get; set; }
        public bool HasChild { get; set; }
        public long ParentCategoryID { get; set; }
    }
}
