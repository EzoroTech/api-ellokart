﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EllowCart.DTO.ProductCategory.OutputModels
{
  public  class ChildToNodeCategoryOut
    {
        public long ProductCategoryId { get; set; }
        public string CategoryName { get; set; }
    }
}
