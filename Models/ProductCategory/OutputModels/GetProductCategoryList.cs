﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EllowCart.DTO.ProductCategory.OutputModels
{
  public  class GetProductCategoryList
    {
        public long ProductCategoryId { get; set; }
        public string CategoryName { get; set; }
        public string ImageName { get; set; }
    }



}
