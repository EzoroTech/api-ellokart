﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace EllowCart.DTO.ProductCategory.InputModels
{
  public   class Productcategory
    {
        public long BusinessCategoryId { get; set; }
        public string CategoryName { get; set; }
        public long ParentCategoryID { get; set; }
        public string Description { get; set; }       
        public bool IsActive { get; set; }
        public string CreatedBy { get; set; }
         
    }
   
}
