﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EllowCart.DTO.ProductCategory.InputModels
{
   public class GetProductCategory
    {
        public long UserBussinessId { get; set; }
        public long ProductCategoryId { get; set; }
    }
}
