﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EllowCart.DTO.ProductCategory.InputModels
{
   public class ProductCategoryInputForAdmin
    {
        public long ProductCategoryId { get; set; }
        public string CategoryName { get; set; }
        public long ParentCategoryID { get; set; }
        public string Description { get; set; }
        public bool IsActive { get; set; }
        public string UpdatedBy { get; set; }

    }
}
