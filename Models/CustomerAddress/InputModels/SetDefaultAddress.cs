﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EllowCart.DTO.CustomerAddress.InputModels
{
   public class SetDefaultAddress
    {
        public long CustomerId { get; set; }
        public long AddressId { get; set; }
        public int SlNo { get; set; }
    }
}
