﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EllowCart.DTO.CustomerAddress.InputModels
{
  public  class EditCustomerAddress
    {
        public long CustomerId { get; set; }
        public long AddressId { get; set; }
        public int SlNo { get; set; }
        public string CustomerName { get; set; }
        public string Addresslineone { get; set; }
        public string Addresslinetwo { get; set; }
        public string Addresslinethree { get; set; }
        public long CityId { get; set; }
        public long StateId { get; set; }
        public string Pincode { get; set; }
        public long CountryId { get; set; }
        public long DistrictId { get; set; }   
        public string AlternateMobileNumber { get; set; }
        public string Lattittude { get; set; }
        public string Longittude { get; set; }
        public bool DefaultAddress { get; set; }
    }
}
