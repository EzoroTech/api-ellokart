﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EllowCart.DTO.CustomerAddress.OutputModels
{
   public class AddCustomeraddressOut
    {
        public long AddressId { get; set; }
        public int SlNo { get; set; }
    }
}
