﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EllowCart.DTO.CustomerAddress.OutputModels
{
   public class RemoveAddressOut
    {
        public int Status { get; set; }
    }
}
