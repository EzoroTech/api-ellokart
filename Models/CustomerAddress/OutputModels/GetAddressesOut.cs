﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EllowCart.DTO.CustomerAddress.OutputModels
{
  public  class GetAddressesOut
    {
        public long AddressId { get; set; }
        public int SlNo { get; set; }
        public string Addresslineone { get; set; }
        public string Addresslinetwo { get; set; }
        public string Addresslinethree { get; set; }
        public long CityId { get; set; }
        public string CityName { get; set; }
        public long StateId { get; set; }
        public string StateName { get; set; }
        public long DistrictId { get; set; }
        public string DistrictName { get; set; }
        public long CountryId { get; set; }
        public string CountryName { get; set; }
        public string AlternateMobileNumber { get; set; }
        public string Pincode { get; set; }
        public string CustomerName { get; set; }
        public string MobileNumber { get; set; }
        public string Lattittude { get; set; }
        public string Longittude { get; set; }
        public bool DefaultAddress { get; set; }

    }
}
