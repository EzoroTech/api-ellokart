﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace EllowCart.DTO.User.InputModels
{
    public class UserRegisteration
    {
        [RegularExpression(@"^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$", ErrorMessage = "Not a valid phone number")]
        public string MobileNo { get; set; }
        public string EmailID { get; set; }
        [Required(ErrorMessage = "Password Required")]
        [StringLength(30, MinimumLength = 8, ErrorMessage = "Invalid")]
        public string Password { get; set; }

      
        //public List<SecurityQuestion> DtSecurity { get; set; }
    }
    //public class SecurityQuestion
    //{
    //    public long QusetionId { get; set; }
    //    public string SequrityAns { get; set; }
    //}
}
