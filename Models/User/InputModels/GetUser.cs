﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EllowCart.DTO.User.InputModels
{
   public class GetUser
    {
        public long UserId { get; set; }

        public int Flag { get; set; }
    }
}
