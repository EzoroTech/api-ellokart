﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace EllowCart.DTO.User.InputModels
{
  public  class AddGST
    {
        [Required(ErrorMessage = "GST")]
        [StringLength(30, MinimumLength = 15, ErrorMessage = "GST Invalid")]
        public string GSTNO { get; set; }
    }
}
