﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EllowCart.DTO.User.InputModels
{
  public  class GetUserCode
    {
        public string MobileNumber { get; set; }
        public string Password { get; set; }
        public string EmailId { get; set; }
        //public int QuestionId { get; set; }
        //public string QuestionAnswer { get; set; }
    }
}
