﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EllowCart.DTO.User.OutputModels
{
  public  class ForgotpasswordOut
    {
        public int UserId { get; set; }
        public int STATUS { get; set; }
    }

    public class SecurityAnsOut
    {

        public int STATUS { get; set; }
    }
}
