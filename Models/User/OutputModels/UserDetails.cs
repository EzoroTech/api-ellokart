﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EllowCart.DTO.User.OutputModels
{
  public  class UserDetails
    {
        public string UserCode { get; set; }
        public string MobileNo { get; set; }
        public string EmailID { get; set; }
        public string Password { get; set; }
        
        public bool IsActive { get; set; }
        
    }
}
