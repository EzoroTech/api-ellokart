﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EllowCart.DTO.User.OutputModels
{
  public class UserRegistrationout
    {
        public long UserId { get; set; }
        public string JwtToken { get; set; }
    }
}
