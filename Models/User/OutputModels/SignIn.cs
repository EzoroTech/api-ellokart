﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EllowCart.DTO.User.OutputModels
{
   public class SignIn
    {
        public long UserId { get; set; }
        public long UserBussinessId { get; set; }
        public long BranchId { get; set; }
        public string BranchName { get; set; }
        public string DisplayName { get; set; }
        public string ImageUrl { get; set; }
        public int Status { get; set; }
        public string Lattittude { get; set; }
        public string Longittude { get; set; }
        public string LocationName { get; set; }
        public string CvrPic { get; set; }
        public Boolean IsProfileCompleted { get; set; }

        public string UserBusinessCode { get; set; }
        public int PlanId { get; set; }

        public string JwtToken { get; set; }
    }
}
