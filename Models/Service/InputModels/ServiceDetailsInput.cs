﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EllowCart.DTO.Service.InputModels
{
   public class ServiceDetailsInput
    {
        public long BranchId { get; set; }
        public long ServiceId { get; set; }
    }
}
