﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EllowCart.DTO.Service.InputModels
{
   public class GetServiceByBranch
    {
        public long BranchId { get; set; }
    }
}
