﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EllowCart.DTO.Service.InputModels
{
   public class AddServices
    {
        public long ServiceId { get; set; }
        public string ServiceName { get; set; }
        public string ServiceCode { get; set; }
        public string ContactPerson { get; set; }
        public string MobileNumber { get; set; }
        public long TimesSlotsId { get; set; }
        public float Amount { get; set; }
        public float Discount { get; set; }
        public string DiscountConditions { get; set; }
        public string Description { get; set; }
        public long BranchId { get; set; }
        public string Flag { get; set; }
    }
}
