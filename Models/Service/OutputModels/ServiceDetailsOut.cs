﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EllowCart.DTO.Service.OutputModels
{
   public class ServiceDetailsOut
    {
        public List<ServiceDetails> Details { get; set; }
        public int Images { get; set; }
    }
}
