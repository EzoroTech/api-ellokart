﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EllowCart.DTO.Service.OutputModels
{
  public  class ServiceDetails
    {
        public string ServiceName { get; set; }
        public long ServiceId { get; set; }
        public string ServiceCode { get; set; }
        public string ContactPerson { get; set; }
        public string MobileNumber { get; set; }
        public long TimeSlotsId { get; set; }
        public float Amount { get; set; }
        public float Discount { get; set; }
        public string DiscountConditions { get; set; }
        public string Description { get; set; }
    
    }
}
