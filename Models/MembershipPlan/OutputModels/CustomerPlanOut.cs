﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EllowCart.DTO.MembershipPlan.OutputModels
{
  public  class CustomerPlanOut
    {
        public long CustomerMembershipId { get; set; }
        public string PlanName { get; set; }
        public float Price { get; set; }
        public int ValidityInMonths { get; set; }
        public string ImageUrl { get; set; }
    }
}
