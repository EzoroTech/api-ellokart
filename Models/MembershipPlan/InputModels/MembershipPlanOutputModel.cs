﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EllowCart.DTO.MembershipPlan.InputModels
{
    public class MembershipPlanOutputModel
    {
        public int MembershipPlanId { get; set; }
        public int Flag { get; set; }

    }
}
