﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EllowCart.DTO.MembershipPlan.InputModels
{
    public class MembershipPlanFeaturesInput
    {
        public string Features { get; set; }
        public DateTime CreatedOn { get; set; }
        public string CreatedBy { get; set; }
        public DateTime UpdatedOn { get; set; }
        public string UpdatedBy { get; set; }

    }
}
