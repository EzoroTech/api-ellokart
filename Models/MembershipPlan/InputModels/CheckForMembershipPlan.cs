﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EllowCart.DTO.MembershipPlan.InputModels
{
    public class CheckForMembershipPlan
    {
        public string ReferalCode { get; set; }
        public long UserId { get; set; }
        public long PlanId { get; set; }
    }

}
