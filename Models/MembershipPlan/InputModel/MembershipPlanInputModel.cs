﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EllowCart.DTO.MembershipPlan.InputModel
{
   public class MembershipPlanInputModel
    {
        public string MembershipPlan { get; set; }
        public float Price { get; set; }
        public int ValidityInMonths { get; set; }
        public string Description { get; set; }
        public bool IsActive { get; set; }
        public int TaxId { get; set; }
        public DateTime CreatedOn { get; set; }
        public string CreatedBy { get; set; }
        public DateTime UpdatedOn { get; set; }
        public string UpdatedBy { get; set; }

    }
}
