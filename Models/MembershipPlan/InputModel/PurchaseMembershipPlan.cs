﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EllowCart.DTO.MembershipPlan.InputModels
{
    public class PurchaseMembershipPlan
    {
        public int PlanId { get; set; }
        public int UserId { get; set; }

    }

    public class GetPlanAddingStatus
    {
        public int Status { get; set; }
    }
}
