﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EllowCart.DTO.Home.OutputModels
{
   public class GetShopDetailsout
    {
        public string BranchName { get; set; }
        public string MobileNumber { get; set; }
        public string Lattittude { get; set; }
        public string Longittude { get; set; }
     
        public string LocationName { get; set; }
        public string Followers { get; set; }
        public string Ratings { get; set; }
        public string ProPic { get; set; }
        public string CvrPic { get; set; }

        public int followstatus { get; set; }

    }
}
