﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EllowCart.DTO.Home.OutputModels
{
  public  class FeedOfferListOutput
    {
        public long FeedId { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public long ViewCount { get; set; }
        public float MRP { get; set; }
        public long ProductID { get; set; }
        public string ProductName { get; set; }
        public int OfferQty { get; set; }
        public float OfferPrice { get; set; }
        public int NoOfLikes { get; set; }
        public long BranchId { get; set; }
        public bool IsOfferLiked { get; set; }
        public bool IsOfferViewed { get; set; }
    }
}
