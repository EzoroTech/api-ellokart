﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EllowCart.DTO.Home.OutputModels
{
  public  class GetNearByStatusOut
    {
        public string ImageUrl { get; set; }
        public long StatusId { get; set; }
        public string StatusName { get; set; }
    }
}
