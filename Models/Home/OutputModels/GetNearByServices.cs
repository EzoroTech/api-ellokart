﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EllowCart.DTO.Home.OutputModels
{
  public  class GetNearByServices
    {
        public long BranchId { get; set; }
        public string BranchName { get; set; }
        public string DisplayName { get; set; }
        public string ServiceName { get; set; }
        public float Amount { get; set; }
        public float Discount { get; set; }
        public long ServiceId { get; set; }
        public string ImageUrl { get; set; }
    }
}
