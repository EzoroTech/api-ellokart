﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EllowCart.DTO.Home.OutputModels
{
  public  class ShopSearchByCategory
    {
        public long BranchId { get; set; }
        public long UserBussinessId { get; set; }
        public string BranchName { get; set; }
        public string DisplayName { get; set; }
        public string MobileNumber { get; set; }
        public string ContactPerson { get; set; }
        public string EmailId { get; set; }
        public string Addresslineone { get; set; }
        public string Addresslinetwo { get; set; }
        public string Addresslinethree { get; set; }
        public string Pincode { get; set; }
        public string Lattittude { get; set; }
        public string Longittude { get; set; }
        public string LocationName { get; set; }
    }
}
