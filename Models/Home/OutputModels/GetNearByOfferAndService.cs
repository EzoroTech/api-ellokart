﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EllowCart.DTO.Home.OutputModels
{
  public  class GetNearByOfferAndService
    {
        public List<GetNearByOffersOut> Offers { get; set; }
        public List<GetNearByServices> Services { get; set; }
    }
}
