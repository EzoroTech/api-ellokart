﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EllowCart.DTO.Home.OutputModels
{
  public  class ShopSearchOutput
    {
        public string BranchId { get; set; }
        public string DisplayName { get; set; }
     
    }
}
