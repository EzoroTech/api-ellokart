﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EllowCart.DTO.Home.OutputModels
{
  public  class GetNearByOffersOut
    {
        public string ImageUrl { get; set; }
        public long ProductID { get; set; }
        public string BranchName { get; set; }
        public long BranchId { get; set; }
        public float DiscountValue { get; set; }
        public string OfferType { get; set; }
        public string  OfferName { get; set; }
        public float OfferPrice { get; set; }
        public long OfferSpecialID { get; set; }
        public float MRP { get; set; }
        public long ProductCategoryId { get; set; }
        public int MinimumOrderValue { get; set; }
        public int MaxOrderValue { get; set; }

        public string ProductName { get; set; }
        public long Stock { get; set; }

        public DateTime ValidTo { get; set; }

    }

    public class GetNearByOffersByName
    {
        public string ImageUrl { get; set; }
        public long ProductID { get; set; }
        public string ProductName { get; set; }
        public string BranchName { get; set; }
        public long BranchId { get; set; }
        public float DiscountValue { get; set; }
        public string OfferType { get; set; }
        public string OfferName { get; set; }
        public float OfferPrice { get; set; }
        public long OfferSpecialID { get; set; }
        public float MRP { get; set; }
        public long ProductCategoryId { get; set; }

        public float Distance { get; set; }
        public int MinimumOrderValue { get; set; }
        public int MaxOrderValue { get; set; }

        public long Stock { get; set; }

        public DateTime ValidTo { get; set; }

    }
}
