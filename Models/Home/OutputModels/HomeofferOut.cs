﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EllowCart.DTO.Home.OutputModels
{
  public  class HomeofferOut
    {
        public float Distance { get; set; }
        public float MaxDiscountValue { get; set; }
        public string CategoryName { get; set; }
        public string ImageName { get; set; }
        public long ProductCategoryId { get; set; }
    }
}
