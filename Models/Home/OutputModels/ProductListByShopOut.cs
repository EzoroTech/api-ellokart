﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EllowCart.DTO.Home.OutputModels
{
   public class ProductListByShopOut
    {
        public string ImageUrl { get; set; }
        public long ProductID { get; set; }
        public float MRP { get; set; }
        public string ProductName { get; set; }
        public int StockInHand { get; set; }
        public float DiscountValue { get; set; }
        public string OfferName { get; set; }
        public long BranchId { get; set; }
        public string DisplayName { get; set; }
        public float OfferPrice { get; set; }
        public string OfferType { get; set; }
        public string CategoryName { get; set; }
        public long Stock { get; set; }
        public long MinimumOrderValue { get; set; }
        public long MaxOrderValue { get; set; }
    }
}
