﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EllowCart.DTO.Home.OutputModels
{
   public class GetFeedsOut
    {
        public List<FeedPostListOutput> Post { get; set; }
        public List<FeedEventListOutput> Event { get; set; }
        public List<FeedOfferListOutput> Offer { get; set; }
    }
}
