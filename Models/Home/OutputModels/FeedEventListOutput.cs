﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EllowCart.DTO.Home.OutputModels
{
  public  class FeedEventListOutput
    {
        public int FeedId { get; set; }
        public string Title { get; set; }
        public int PostType { get; set; }
        public string Description { get; set; }
        public int ViewCount { get; set; }
        public long NoOfLikes { get; set; }
        public string ImageUrl { get; set; }
        public long BranchId { get; set; }
        public bool IsEventLiked { get; set; }
        public bool IsEventViewed { get; set; }
    }
}
