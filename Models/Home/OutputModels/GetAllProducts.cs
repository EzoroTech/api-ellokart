﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EllowCart.DTO.Home.OutputModels
{
   public class GetAllProducts
    {
        public long ProductID { get; set; }
        public long BranchId { get; set; }
        public string ProductName { get; set; }
        public string BranchName { get; set; }
    }
}
