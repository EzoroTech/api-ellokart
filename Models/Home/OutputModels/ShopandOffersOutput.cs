﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EllowCart.DTO.Home.OutputModels
{
   public class ShopandOffersOutput
    {
        public List<GetNearByShopsOut> Shops { get; set; }
        public List<HomeofferOut> Offers { get; set; }
    }
}
