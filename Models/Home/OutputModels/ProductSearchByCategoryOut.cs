﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EllowCart.DTO.Home.OutputModels
{
   public class ProductSearchByCategoryOut
    {
        public string MRP { get; set; }
        public long ProductID { get; set; }
        public string BranchName { get; set; }
        public string DiscountValue { get; set; }
        public long BranchId { get; set; }
        public string ProductName { get; set; }
        public string OfferPrice { get; set; }
        public string DiscountType { get; set; }
    }
}
