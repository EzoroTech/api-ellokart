﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EllowCart.DTO.Home.OutputModels
{
  public  class GetPremiumSellersOut
    {
        public long UserBusinessId { get; set; }
        public string ShopName { get; set; }
        public long Branch { get; set; }
        public string LocationName { get; set; }
        public int Followers { get; set; }
        public string Ratings { get; set; }
        public string Distance { get; set; }
        public string Lattittude { get; set; }
        public string Longittude { get; set; }
        public string BranchLogo { get; set; }
        public string MobileNumber { get; set; }

        public long MaxDiscountValue { get; set; }
        public string BusinessCategory { get; set; }

    }
}
