﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EllowCart.DTO.Home.InputModels
{
   public class ShopSearchByCategoryInput
    {
        public long SellerCategoryId { get; set; }
    }
}
