﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EllowCart.DTO.Home.InputModels
{
   public class GetNearByStatus
    {
        public string Lattitude { get; set; }
        public string Longitude { get; set; }
        public int IntialIndex { get; set; }
        public int PageSize { get; set; }
    }
}
