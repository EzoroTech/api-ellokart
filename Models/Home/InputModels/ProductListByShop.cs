﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EllowCart.DTO.Home.InputModels
{
  public  class ProductListByShop
    {
        public long CustomerId { get; set; }
        public long BranchId { get; set; }
        public int Index { get; set; }
        public int PageSize { get; set; }
        public long CategoryId { get; set; }
    }
}
