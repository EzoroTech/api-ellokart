﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EllowCart.DTO.Home.InputModels
{
   public class GetNearByShopsInput
    {
        public long CustomerId { get; set; }
        public string Lattitude { get; set; }
        public string Longitude { get; set; }
        public int Index { get; set; }
        public int PageSize { get; set; }
        public int Flag { get; set; }
        public long CategoryId { get; set; }
    }
}
