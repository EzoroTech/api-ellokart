﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EllowCart.DTO.Home.InputModels
{
  public  class GetShopDetailsInput
    {
        public long CustomerId { get; set; }
        public long BranchId { get; set; }
    }
}
