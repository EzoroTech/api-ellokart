﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EllowCart.DTO.Upgrade.InputModel
{
    public class UserBusinessDtls
    {
        public string BusinessDisplayName { get; set; }
        public string EmailID { get; set; }
        public string FranchiseCode { get; set; }
        public string GSTNO { get; set; }
        public string MobileNo { get; set; }
        public DateTime CreatedOn { get; set; }
        public string MembershipPlan { get; set; }
        public int BusinessStatus { get; set; }

    }

    public class GetBusinessMdl
    {

        public int UserId { get; set; }
    }

    public class CustomerSupportDataMdl
    {

        public int UserId { get; set; }
        public string Comments { get; set; }
    }


}
