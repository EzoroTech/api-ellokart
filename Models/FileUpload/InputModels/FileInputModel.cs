﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Text;



namespace EllowCart.DTO.FileUpload.InputModels
{
  public  class FileInputModel
    {
        public string Username { get; set; }
        public int Password { get; set; }
        public List<IFormFile> FileToUpload { get; set; }
    }
}
