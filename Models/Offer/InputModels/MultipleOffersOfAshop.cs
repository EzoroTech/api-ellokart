﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EllowCart.DTO.Offer.InputModels
{
    public class MultipleOffersOfAshop
    {

        public int BranchId { get; set; }

    }

    public class GetShopByLocation
    {
        public string Lattitude { get; set; }
        public string Longitude { get; set; }
    }

    public class GetNearByShops
    {
        public List<NearByShops> shops { get; set; }

    }
    public class NearByShops
    {
        public string ShopName { get; set; }
        public int UserBussinessId { get; set; }
        public int BranchId { get; set; }
        public string Lattittude { get; set; }
        public string Longittude { get; set; }
        public string LocationName { get; set; }
        public string MobileNumber { get; set; }
        public string BranchLogo { get; set; }
        public long Distance { get; set; }
        public int LikesCount { get; set; }
        public int ViewCount { get; set; }

    }

    public class GetShopList
    {
        public string ShopName { get; set; }
        public int UserBussinessId { get; set; }
        public int BranchId { get; set; }
        public string Lattittude { get; set; }
        public string Longittude { get; set; }
        public string LocationName { get; set; }
        public string MobileNumber { get; set; }
        public string BranchLogo { get; set; }
        public long Distance { get; set; }
        public int LikesCount { get; set; }
        public int ViewCount { get; set; }

        public List<NearByOffers> offers { get; set; }
    }
  

    public class NearByOffers
    {
        public string OfferName { get; set; }
        public int DiscountUnit { get; set; }
        public int DiscountValue { get; set; }
        public string OfferMode { get; set; }
        public string ProductName { get; set; }
        public string OfferType { get; set; }
        public int OfferPrice { get; set; }
    }

}
