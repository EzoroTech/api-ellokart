﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EllowCart.DTO.Offer.InputModels
{
   public class OfferTypeSearch
    {

        public string SearchParam { get; set; }


    }
    public class OfferTypeSearchOut
    {

        public string OfferName { get; set; }
        public int DiscountUnit { get; set; }
        public int DiscountValue { get; set; }
        public string OfferType { get; set; }
        public int OfferPrice { get; set; }
        public string ImageUrl { get; set; }
    }
}
