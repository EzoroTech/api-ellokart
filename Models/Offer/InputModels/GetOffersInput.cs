﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EllowCart.DTO.Offer.InputModels
{
  public  class GetOffersInput
    {
        public long CustomerId { get; set; }
        public string Lattitude { get; set; }
        public string Longitude { get; set; }
        public long BranchId { get; set; }
        public int Index { get; set; }
        public int PageSize { get; set; }
    }

    

}
