﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EllowCart.DTO.Offer.InputModels
{
   public class EditOfferdetailsInput
    {
        public long UserId { get; set; }
        public string role { get; set; }
        public long branchId { get; set; }
        public long ProductId { get; set; }
       
        public string OfferName { get; set; }
        public int DiscountValue { get; set; }
        public int DiscountUnit { get; set; }
        public int MinimumOrderValue { get; set; }
        public int MaxOrderValue { get; set; }
        public int MaxDiscountAmt { get; set; }
        public DateTime ValidFrom { get; set; }
        public DateTime ValidTo { get; set; }
        public long OfferSpecialId { get; set; }
       
    }
}
