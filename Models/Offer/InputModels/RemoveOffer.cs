﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EllowCart.DTO.Offer.InputModels
{
   public class RemoveOffer
    {
        public long UserId { get; set; }
        public string role { get; set; }
        public long BranchId { get; set; }
        public long OfferSpecialId { get; set; }
    }
}
