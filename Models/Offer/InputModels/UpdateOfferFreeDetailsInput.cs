﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EllowCart.DTO.Offer.InputModels
{
    public class UpdateOfferFreeDetailsInput
    {
        public long UserId { get; set; }
        public string role { get; set; }
        public long branchId { get; set; }
        public long ProductId { get; set; }
        public long OfferMasterId { get; set; }
        public string OfferName { get; set; }
        public float DiscountValue { get; set; }
        public bool DiscountUnit { get; set; }
        public int MinimumOrderValue { get; set; }
        public int MaxOrderValue { get; set; }
        public int MaxDiscountAmt { get; set; }
        public DateTime ValidFrom { get; set; }
        public DateTime ValidTo { get; set; }
        public int OfferSpecialId { get; set; }
        public List<combo> dtOfferComboProductType { get; set; }
    }
    public class combo
    {
        public long ComboProductID { get; set; }
        public long OfferQty { get; set; }
    }
}
