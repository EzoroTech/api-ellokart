﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EllowCart.DTO.Offer.OutputModels
{
  public  class GetAllOfferNames
    {
        public long OfferNameId { get; set; }
        public string OfferName { get; set; }
        public string OfferImage { get; set; }
        public string OfferCode { get; set; }
    }
}
