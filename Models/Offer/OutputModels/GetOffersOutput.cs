﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EllowCart.DTO.Offer.OutputModels
{
   public class GetOffersOutput
    {
        public string ImageUrl { get; set; }
        public long ProductID { get; set; }
        public string BranchName { get; set; }
        public long BranchId { get; set; }
        public float DiscountValue { get; set; }
        public string OfferType { get; set; }
        public float OfferPrice { get; set; }
        public long OfferSpecialID { get; set; }
        public string OfferName { get; set; }
        public string MRP { get; set; }
    }
}
