﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EllowCart.DTO.Offer.OutputModels
{
  public  class AddProductOfferout
    {
        public long OfferSpecialID { get; set; }
        public float OfferPrice { get; set; }
        public string Offername { get; set; }       
        public long ProductId { get; set; }
        public string ImageUrl { get; set; }
        public int DiscountValue { get; set; }
        public string DiscountUnit { get; set; }
        public DateTime ValidFRom { get; set; }
        public DateTime ValidTo { get; set; }
        public float MRP { get; set; }
    }
}
