﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EllowCart.DTO.Offer.OutputModels
{
  public  class GetOffermaster
    {
        public long OfferMasterId { get; set; }
        public string OfferName { get; set; }
        public string Description { get; set; }
    }
}
