﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EllowCart.DTO.Offer.OutputModels
{
   public class GetOfferListout
    {
        public int DiscountValue { get; set; }
        public float Price { get; set; }
        public long ProductID { get; set; }
        public string OfferName { get; set; }
        public long ComboProductID { get; set; }
        public string OfferType { get; set; }
        public float OfferPrice { get; set; }
        public long OfferID { get; set; }
    }
}
