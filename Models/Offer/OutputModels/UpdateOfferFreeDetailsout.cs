﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EllowCart.DTO.Offer.OutputModels
{
  public  class UpdateOfferFreeDetailsout
    {
        public long ProductID { get; set; }
        public string ImageUrl { get; set; }
        public string OfferType { get; set; }
        public long DiscountValue { get; set; }
        public float Price { get; set; }
        public float OfferPrice { get; set; }
        public int OfferMasterId { get; set; }
    }
}
