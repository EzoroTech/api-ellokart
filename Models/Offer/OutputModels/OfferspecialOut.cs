﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EllowCart.DTO.Offer.OutputModels
{
  public  class OfferspecialOut
    {
        public long ProductID { get; set; }
        public string ImageUrl { get; set; }
        public string OfferType { get; set; }
        public int DiscountValue { get; set; }
        public float Price { get; set; }
        public float OfferPrice { get; set; }
        public string OfferName { get; set; }
        public long OfferSpecialID { get; set; }
        public string ProductName { get; set; }
        public DateTime ValidFrom { get; set; }
        public DateTime ValidTo { get; set; }
        public int StockInHand { get; set; }

    }
}
