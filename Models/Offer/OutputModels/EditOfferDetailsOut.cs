﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EllowCart.DTO.Offer.OutputModels
{
  public  class EditOfferDetailsOut
    {
        public long ProductId { get; set; }
        public string ImageUrl { get; set; }
        public string OfferType { get; set; }
        public int DiscountValue { get; set; }
        public float Price { get; set; }
        public float OfferPrice { get; set; }
        public long OfferMasterId { get; set; }
    }
}
