﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EllowCart.DTO.BusinessCategory.InputModels
{
   public class GetBusinessCategoryAdminInput
    {
        public long BussinessCategoryId { get; set; }
    }
}
