﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EllowCart.DTO.BusinessCategory.InputModels
{
  public  class ShopByBusinessCategory
    {
        public string Latitude { get; set; }
        public string Logitude { get; set; }
        public long BusinessCategoryId { get; set; }
    }
}
