﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EllowCart.DTO.BusinessCategory.InputModels
{
    public class AddBusinesscategory
    {
        public string BussinessCategoryName { get; set; }
        public string BussinessCategoryDecr { get; set; }
        public long BussinessTypeId { get; set; }
        public DateTime CreatedOn { get; set; }
        public string CreatedBy { get; set; }
        public DateTime UpdatedOn { get; set; }
        public string UpdatedBy { get; set; }
        public bool IsActive { get; set; }
    }
    public class BusinesscategInputforUpdation
    {
        public int BussinessCategoryId { get; set; }
        public string BussinessCategoryName { get; set; }
        public string BussinessCategoryDecr { get; set; }
        public long BussinessTypeId { get; set; }
        public DateTime CreatedOn { get; set; }
        public string CreatedBy { get; set; }
        public DateTime UpdatedOn { get; set; }
        public string UpdatedBy { get; set; }
        public bool IsActive { get; set; }
    }
}
