﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EllowCart.DTO.BusinessCategory.OutputModels
{
  public  class GetBusinessCategoryAdmin
    {
        public long BussinessCategoryId { get; set; }
        public string BussinessCategoryName { get; set; }
        public string BussinessCategoryDecr { get; set; }
        public DateTime CreatedOn { get; set; }
        public string Active { get; set; }
    }
}
