﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EllowCart.DTO.PaymentMethods.InputModels
{
  public  class UserPaidOrNot
    {
        public long UserId { get; set; }
        public long PlanId { get; set; }
        public string TransId { get; set; }
        public string GatewayId { get; set; }
        public string Flag { get; set; } ="";
    }
}
