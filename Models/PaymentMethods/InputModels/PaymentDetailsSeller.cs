﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EllowCart.DTO.PaymentMethods.InputModels
{
  public  class PaymentDetailsSeller
    {
        public long BranchId { get; set; }
        public long PaymentTypeId { get; set; }
        public string PaymentOptionNoOrId { get; set; }
    }
}
