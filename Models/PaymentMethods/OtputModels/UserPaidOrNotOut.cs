﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EllowCart.DTO.PaymentMethods.OtputModels
{
  public  class UserPaidOrNotOut
    {
        public string Status { get; set; }
        public string msg { get; set; }
        public string TransId { get; set; }
    }
}
