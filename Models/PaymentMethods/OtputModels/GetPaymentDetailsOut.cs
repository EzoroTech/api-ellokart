﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EllowCart.DTO.PaymentMethods.OtputModels
{
  public  class GetPaymentDetailsOut
    {
        public long PaymentTypeId { get; set; }
        public string PaymentName { get; set; }
        public string PaymentOptionNoOrId { get; set; }
    }
}
