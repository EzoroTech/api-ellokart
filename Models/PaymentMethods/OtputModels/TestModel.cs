﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EllowCart.DTO.PaymentMethods.OtputModels
{
   public class TestModel
    {
        public string id { get; set; }
        public string entity { get; set; }
        public long amount { get; set; }
        public long amount_paid { get; set; }
        public long amount_due { get; set; }
        public string currency { get; set; }
        public string receipt { get; set; }
        public string status { get; set; }
        public int attempts { get; set; }
       // public int notes { get; set; }
        public long created_at { get; set; }
    }
}
