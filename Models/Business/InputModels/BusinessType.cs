﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EllowCart.DTO.Business.InputModels
{
    public class BusinessType
    {

        public string BussinessTypeName { get; set; }
        public string BussinessTypeDecr { get; set; }
        public DateTime CreatedOn { get; set; }
        public string CreatedBy { get; set; }
        public DateTime UpdatedOn { get; set; }
        public string UpdatedBy { get; set; }
        public bool IsActive { get; set; }
    }
    public class BusinessTypeForUpdation
    {
        public int BussinessTypeId { get; set; }
        public string BussinessTypeName { get; set; }
        public string BussinessTypeDecr { get; set; }
        public DateTime CreatedOn { get; set; }
        public string CreatedBy { get; set; }
        public DateTime UpdatedOn { get; set; }
        public string UpdatedBy { get; set; }
        public bool IsActive { get; set; }
    }
}
