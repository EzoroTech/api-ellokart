﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace EllowCart.DTO.Business.InputModels
{
   public class CreateBusiness
    {
        public long UserId { get; set; }
        public string role { get; set; }
        public long BussinessTypeId { get; set; }
        public long SellercategoryId { get; set; }
        public long BussinessCategoryId { get; set; }
       // public long ImageId { get; set; }
        public string BussinessName { get; set; }
        public string BusinessDisplayName { get; set; }
        public long BussinesSystmId { get; set; }
        public string Lattittude { get; set; }
        public string Longittude { get; set; }
        public string LocationName { get; set; }

        
    }
    
}
