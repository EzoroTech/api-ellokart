﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EllowCart.DTO.Business.InputModels
{
   public class GetBusinessListInput
    {
        public long UserId { get; set; }
        public string role { get; set; }
    }
}
