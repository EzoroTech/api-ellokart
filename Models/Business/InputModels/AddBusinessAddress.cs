﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EllowCart.DTO.Business.InputModels
{
   public class AddBusinessAddress
    {
        public long UserId { get; set; }
        public string AddressLineOne { get; set; }
        public string AddressLineTwo { get; set; }
        public string AddressLIneThree { get; set; }
        public string Description { get; set; }
        public string Pincode { get; set; }
        public string Lattitude { get; set; }
        public string Longittude { get; set; }
        public string LocationName { get; set; }
        public string AccountType { get; set; }
        public int Flag { get; set; }
        public long BranchId { get; set; }
        public int StateId { get; set; }

        public string GST { get; set; }

        public string WhatsappNo { get; set; }

        public string FranchiseCode { get; set; }
    }
}
