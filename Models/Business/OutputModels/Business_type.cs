﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EllowCart.DTO.Business.OutputModels
{
  public  class Business_type
    {
        public long BussinessTypeId { get; set; }
        public string Name { get; set; }
        public string Decr { get; set; }
    }
}
