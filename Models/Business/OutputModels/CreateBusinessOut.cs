﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EllowCart.DTO.Business.OutputModels
{
   public class CreateBusinessOut
    {
        public long UserBussinessId { get; set; }
        public string BussinessName { get; set; }
        public string BusinessDisplayName { get; set; }
        //public long ImageId { get; set; }
       // public string BranchDisplayName { get; set; }
        public long BranchId { get; set; }
        public long UserId { get; set; }
    }
}
