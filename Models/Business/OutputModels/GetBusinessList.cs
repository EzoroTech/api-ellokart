﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EllowCart.DTO.Business.OutputModels
{
  public  class GetBusinessList
    {
        public long UserBussinessId { get; set; }
        public string BusinessDisplayName { get; set; }
        public string ImageUrl { get; set; }
        public long BranchId { get; set; }
    }
}
