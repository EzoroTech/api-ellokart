﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EllowCart.DTO.Business.OutputModels
{
   public class LoadBusiness
    {
        public long UserBussinessId { get; set; }
        public string BussinessName { get; set; }
        public string BusinessDisplayName { get; set; }
        public long BussinessTypeId { get; set; }
        public string Name { get; set; }
        public string Decr { get; set; }
        public long SellerCatId { get; set; }
        public string SellerCatName { get; set; }
        public long BussinesSystmId { get; set; }
        public string BussinesSystmName { get; set; }
        public string BussinesSystmDescr { get; set; }
        public long BussinessCategoryId { get; set; }
        public string BussinessCategoryName { get; set; }
        public string BussinessCategoryDecr { get; set; }
        public long BussinessLogoId { get; set; }
        public string ImagePath { get; set; }
        public string ImageName { get; set; }
    }
}
