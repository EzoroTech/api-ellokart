﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EllowCart.DTO.District.InputModels
{
   public class GetDistrictListInput
    {
        public long StateId { get; set; }
    }
}
