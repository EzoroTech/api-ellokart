﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EllowCart.DTO.District.OutputModels
{
  public  class GetDistrictListOut
    {
       
        public long DistrictId { get; set; }
        public string DistrictName { get; set; }
    }
}
