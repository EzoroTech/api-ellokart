﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace EllowCart.DTO.General.InputModels
{
  public  class Checkemailorphno
    {
        public string MobileNo { get; set; }
        public string EmailID { get; set; }
        public long UsrCustmrFlg { get; set; }
        
        
    }
}
