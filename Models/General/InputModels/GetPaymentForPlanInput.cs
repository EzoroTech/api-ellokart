﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EllowCart.DTO.General.InputModels
{
  public  class GetPaymentForPlanInput
    {
        public long UserId { get; set; }
        public long Others { get; set; }
        public string Flag { get; set; }
    }
    
}
