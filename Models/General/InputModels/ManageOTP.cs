﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EllowCart.DTO.General.InputModels
{
    public class ManageOTP
    {
        public string mobile { get; set; }
     
        public string message { get; set; }
        
    }
}
