﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EllowCart.DTO.General.InputModels
{
   public class AllProductList
    {
        public int Index { get; set; }
        public int PageSize { get; set; }
    }
}
