﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EllowCart.DTO.General.InputModels
{
  public  class OTPInput
    {
        public string MobileNumber { get; set; }
        public long OTP { get; set; }
        public long OTPId { get; set; }
    }
}
