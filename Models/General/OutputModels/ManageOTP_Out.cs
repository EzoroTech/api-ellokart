﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EllowCart.DTO.General.OutputModels
{
  public  class ManageOTP_Out
    {
        public string OTP { get; set; }
        public long OTPId { get; set; }

        public int Status { get; set; }
    }
}
