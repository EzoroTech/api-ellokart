﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EllowCart.DTO.General.OutputModels
{
  public  class RazorPayInputs
    {
        public string RZR { get; set; } = "rzp_live_vgm6h3ICl2iA7V";
        public float Amount { get; set; }
        public string OrderId { get; set; }
        public string CSH { get; set; } 

    }
}
