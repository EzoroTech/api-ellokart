﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EllowCart.DTO.General.OutputModels
{
   public class GetTaxOut
    {
        public long TaxId { get; set; }
        public string TaxName { get; set; }
        public string TaxPercentage { get; set; }
    }
   
}
