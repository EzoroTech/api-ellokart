﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EllowCart.DTO.General.OutputModels
{
  public  class GetBrandOut
    {
        public long BrandId { get; set; }
        public string BrandName { get; set; }
    }
}
