﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EllowCart.DTO.General.OutputModels
{
   public class GetSecurityQuestion
    {
        public long QuestionId { get; set; }
        public string Question { get; set; }
    }

    public class SecurityQustnAnsCheck
    {
        public string MobileNumber { get; set; }
    }
    public class SecurityQustnAnsCheckOut
    {
        public bool IsSecurityAnsGiven { get; set; }
    }
}
