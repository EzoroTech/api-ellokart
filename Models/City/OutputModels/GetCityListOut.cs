﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EllowCart.DTO.City.OutputModels
{
  public  class GetCityListOut
    {
       
        public long CityId { get; set; }
        public string CityName { get; set; }
    }
}
