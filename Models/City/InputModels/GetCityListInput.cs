﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EllowCart.DTO.City.InputModels
{
   public class GetCityListInput
    {
        public long DistrictId { get; set; }
    }
}
