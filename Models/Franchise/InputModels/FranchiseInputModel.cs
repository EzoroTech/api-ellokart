﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EllowCart.DTO.Franchise.InputModels
{
  public  class FranchiseInputModel
    {
        public string PhoneNo { get; set; }
        public string EmailID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime DOB { get; set; }
        public string Gender { get; set; }
        public bool IsActive { get; set; }
        public string Password { get; set; }
       
    }
    public class FranchiseUpdateModel
    {
        public int FranchiseId { get; set; }
        public string FranchiseCode { get; set; }
        public string PhoneNumber { get; set; }
        public string EmailId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime DOB { get; set; }
        public string Gender { get; set; }
        public bool IsActive { get; set; }
        public string Password { get; set; }
       
        public DateTime UpdatedOn { get; set; }
        public string UpdatedBy { get; set; }
    }
}
