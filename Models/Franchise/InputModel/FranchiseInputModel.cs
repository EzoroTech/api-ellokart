﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EllowCart.DTO.Franchise.InputModel
{
    public class FranchiseInputModel
    {
        public string FranchiseCode { get; set; }
        public string FranchiseName { get; set; }
        public string LocationName { get; set; }
        public string PhoneNo { get; set; }
        public string EmailId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime DOB { get; set; }
        public string Gender { get; set; }
        public bool IsActive { get; set; }
        public string Password { get; set; }
        public string CreatedBy { get; set; }


    }
}
