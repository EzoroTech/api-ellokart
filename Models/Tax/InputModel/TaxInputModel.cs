﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EllowCart.DTO.Tax.InputModel
{
    public class TaxInputModel
    {
        public string TaxName { get; set; }
        public string TaxPercentage { get; set; }
        public DateTime CreatedOn { get; set; }
        public string CreatedBy { get; set; }
        public DateTime UpdatedOn { get; set; }
        public string UpdatedBy { get; set; }
        public bool IsActive { get; set; }
        public string TaxType { get; set; }

    }
}
