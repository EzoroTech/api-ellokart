﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EllowCart.DTO.DeliveryPoint.Input_Model
{
    public class DeliveryPointInput
    {
        public int DeliveryPointId { get; set; }
   }

    public class DeliveryPointCreateInput
    {
        public string DeliveryPointName { get; set; }
        public string Lattitude { get; set; }
        public string Longitude { get; set; }
        public string BuildingName { get; set; }
        public string StreetName { get; set; }
        public string Pincode { get; set; }
        public string City { get; set; }
        public int StateId { get; set; }
        public string PhoneNumber { get; set; }
        public string EmailId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime DOB { get; set; }
        public string Gender { get; set; }
        public string Password { get; set; }
        public bool IsActive { get; set; }
        public string CreatedBy { get; set; }

    }

    public class DeliveryPointUpdate
    {
        public int DeiveryPointId { get; set; }
        public string DeliveryPointName { get; set; }
        public string Lattitude { get; set; }
        public string Longitude { get; set; }
        public string BuildingName { get; set; }
        public string StreetName { get; set; }
        public string Pincode { get; set; }
        public string City { get; set; }
        public int StateId { get; set; }
        public string PhoneNumber { get; set; }
        public string EmailId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime DOB { get; set; }
        public string Gender { get; set; }
        public string Password { get; set; }
        public bool IsActive { get; set; }
        public string UpdatedBy { get; set; }

    }



}
