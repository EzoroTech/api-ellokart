﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EllowCart.DTO.State.OutputModels
{
   public class GetStateListOut
    {
        
        public long StateId { get; set; }
        public string StateName { get; set; }
       // public string StateCode { get; set; }
    }
}
