﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EllowCart.DTO.Order.InputModels
{
  public  class CartListout
    {
        // public long CartId { get; set; }
        public long ProductId { get; set; }
        public string ProductCode { get; set; }
        public string ProductName { get; set; }
        public string CategoryName { get; set; }
        public long ProductCategoryId { get; set; }
        public long BranchId { get; set; }
        public string BranchName { get; set; }
        public long CustomerId { get; set; }
        public float OrderItemQty { get; set; }
        public float UnitPrice { get; set; }
        public float NetPrice { get; set; }
        public string ImageUrl { get; set; }
       
    }
}
