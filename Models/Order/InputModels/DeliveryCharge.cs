﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EllowCart.DTO.Order.InputModels
{
   public class DeliveryCharge
    {
        public long ProductId { get; set; }
        public long BranchId { get; set; }
        public long CustomerId { get; set; }
    }
}
