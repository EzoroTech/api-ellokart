﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EllowCart.DTO.Order.InputModels
{
  public  class PlaceOrderInput
    {
        public long CustomerId { get; set; }
        public float OrderAmt { get; set; }
        public int SlNo { get; set; }
        public long BranchId { get; set; }
        public long AddressId { get; set; }
        public int DeliveryMode { get; set; }
        public List<OrderItems> DtOrderItems { get; set; }
    }
    public class OrderItems
    {
        public long ProductId { get; set; }
        public int OrderItemQty { get; set; }
        public float OrderItemPrice { get; set; }
        public long BranchId { get; set; }
        public float TotalPayableAmount { get; set; }
        public float DeliveryCharge { get; set; }

    }
}
