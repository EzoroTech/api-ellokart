﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EllowCart.DTO.Order.InputModels
{
   public class PickupRequestinput
    {
        public long OrderId { get; set; }
        public long BranchId { get; set; }
        public long ProductId { get; set; }
    }
}
