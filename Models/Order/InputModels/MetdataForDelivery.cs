﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EllowCart.DTO.Order.InputModels
{
   public class MetdataForDelivery
    {
        public string PAYMENT_TYPE { get; set; } = "Cash on delivery";
        public long PURCHASE_AMOUNT { get; set; }
        public long DELIVERY_AMOUNT { get; set; }
        public float GRAND_TOTAL { get; set; }
    }

    public class PickupMetdataForDelivery
    {
        public string DELIVERY_MODE { get; set; }
        public string ITEMS { get; set; }
        public string QTY { get; set; }
    }

}
