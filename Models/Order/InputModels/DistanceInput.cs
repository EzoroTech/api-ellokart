﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EllowCart.DTO.Order.InputModels
{
  public  class DistanceInput
    {
        public string SellerLattitude { get; set; }
        public string SellerLongitude { get; set; }
        public string CustomerLattitude { get; set; }
        public string CustomerLongitude { get; set; }
    }
}
