﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EllowCart.DTO.Order.InputModels
{
   public class DataForPickup
    {
        public string label { get; set; }
        public string data { get; set; }
    }
}
