﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EllowCart.DTO.Order.InputModels
{
  public  class InputToApplication
    {
        public string api_key { get; set; } = "5064638df2470906414166615c1425431becc7f22eda7d38591a04";
        public string order_id { get; set; }
        public string team_id { get; set; } 
        public string auto_assignment { get; set; } = "0";
        public string job_description { get; set; }
        public string job_pickup_phone { get; set; }
        public string job_pickup_name { get; set; }
        public string job_pickup_email { get; set; }
        public string job_pickup_address { get; set; }
        public string job_pickup_latitude { get; set; }
        public string job_pickup_longitude { get; set; }
        public string job_pickup_datetime { get; set; }
        public string customer_email { get; set; }
        public string customer_username { get; set; }
        public string customer_phone { get; set; }
        public string customer_address { get; set; }
        public string latitude { get; set; }
        public string longitude { get; set; }
        public string job_delivery_datetime { get; set; }
        public string has_pickup { get; set; } = "1";
        public string has_delivery { get; set; } = "1";
        public string layout_type { get; set; } = "0";
        public int tracking_link { get; set; } = 1;
        public string timezone { get; set; } = "+530";
        public string custom_field_template { get; set; } = "DELIVERY";
        public List<metadatafordelivery> meta_data { get; set; }
        public string pickup_custom_field_template { get; set; } = "PICKUP";
        public List<DataForPickup> pickup_meta_data { get; set; }
        public string fleet_id { get; set; }
        public string p_ref_images { get; set; }
        public string ref_images { get; set; }
        public int notify { get; set; } = 1;
        public string tags { get; set; }
        public int geofence { get; set; } = 0;
        public int ride_type { get; set; } = 0;
    }
}
