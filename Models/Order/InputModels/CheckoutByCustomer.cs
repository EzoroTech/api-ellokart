﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EllowCart.DTO.Order.InputModels
{
   public class CheckoutByCustomer
    {
        public long CustomerId { get; set; }
        public long AddressId { get; set; }
        public long SlNo { get; set; }

    }
}
