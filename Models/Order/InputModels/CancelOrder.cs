﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EllowCart.DTO.Order.InputModels
{
  public  class CancelOrder
    {
        public long OrderId { get; set; }
        public long CustomerId { get; set; }
    }
}
