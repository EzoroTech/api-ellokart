﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EllowCart.DTO.Order.InputModels
{
    public class OrderDetailInput
    {
        public long OrderId { get; set; }
        public long BranchId { get; set; }
        //  public int Flag { get; set; }
    }
}
