﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EllowCart.DTO.Order.OutputModels
{
   public class RequestOutPut
    {
        public string ProductName { get; set; }
        public string CategoryName { get; set; }
        public float TotalPayableAmount { get; set; }
        public float DeliveryCharge { get; set; }
        public long ProductId { get; set; }
        public long OrderId { get; set; }
        public string SellerName { get; set; }
        public string SellerMobileNumber { get; set; }
        public string SellerLattitude { get; set; }
        public string SellerLongittude { get; set; }
        public string SellerAddress { get; set; }
        public string CustomerName { get; set; }
        public string CustomerEmailId { get; set; }
        public string CustomerAlternateMobNo { get; set; }
        public string CustomerAddress { get; set; }
        public string CustomerLattitude { get; set; }
        public string CustomerLongittude { get; set; }
        public int DeliveryMode { get; set; }

    }
}
