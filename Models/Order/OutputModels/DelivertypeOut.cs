﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EllowCart.DTO.Order.OutputModels
{
   public class DelivertypeOut
    {
        public long MRP { get; set; }      
        public long ProductID { get; set; }
        public int OrderItemQty { get; set; }
        public long OfferPrice { get; set; }
        public long Specid { get; set; }
        public String VarientValue { get; set; }
        public int MinimumOrderValue { get; set; }
        public int MaxOrderValue { get; set; }
        public String imageurl { get; set; }
        public string ProductName { get; set; }
        public long Stock { get; set; }
    }
}
