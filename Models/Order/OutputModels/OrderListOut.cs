﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EllowCart.DTO.Order.OutputModels
{
  public  class OrderListOut
    {
        public long OrderId { get; set; }
        // public List<GetOderBySellerOut> OrderedItems { get; set; }
        public long OrderAmt { get; set; }
        public long OrderItemQty { get; set; }
        public DateTime OrderDate { get; set; }
        public string CustomerName { get; set; }
        public string AlternateMobileNumber { get; set; }
        public long BranchId { get; set; }
    }
}
