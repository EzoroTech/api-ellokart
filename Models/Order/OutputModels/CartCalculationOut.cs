﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EllowCart.DTO.Order.OutputModels
{
   public class CartCalculationOut
    {
        public float Price { get; set; }
        public int DeliveryCharge { get; set; }
        public int DeliveryChargeExpress { get; set; }
        public float PayableAmount { get; set; }
        public int DeliveryType { get; set; }
        public string Reason { get; set; }
    }
}
