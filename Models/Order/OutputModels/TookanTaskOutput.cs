﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EllowCart.DTO.Order.OutputModels
{
   public class TookanTaskOutput
    {
        public string message { get; set; }
        public string status { get; set; }
        public TookanData data { get; set; }
    }
    public class TookanData
    {
        public string job_id { get; set; }
        public string pickup_job_id { get; set; }
        public string delivery_job_id { get; set; }
        public string order_id { get; set; }
    }
}
