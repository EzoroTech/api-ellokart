﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EllowCart.DTO.Order.OutputModels
{
    public class OrderDetailsOutput
    {
        public long OrderId { get; set; }
        public DateTime OrderDate { get; set; }
        public string OrderStatus { get; set; }
        public string Status { get; set; }
    }
    public class DetailsSupport
    {
        public List<OrderDetailsOutput> OrderDetails { get; set; }
        public List<OrderProductListSupport> Products { get; set; }
        public List<ShippingAddressSupport> ShippingAddress { get; set; }
        public List<OrderSummerySupport> OrderSummery { get; set; }
    }

    public class ShippingAddressSupport
    {
        public string AddressLineOne { get; set; }
        public string AddressLineTwo { get; set; }
        public string AddressLineThree { get; set; }
        public string PinCode { get; set; }
        public string Customername { get; set; }
        public string MobileNumber { get; set; }
        public string StateName { get; set; }
        public string DistrictName { get; set; }
        public string CityName { get; set; }
    }

    public class OrderProductListSupport
    {
        public long ProductId { get; set; }
        public string ProductName { get; set; }
        public string BranchName { get; set; }
        public long BranchId { get; set; }
        public int OrderItemQty { get; set; }
        public long OrderItemPrice { get; set; }
        public string ImageUrl { get; set; }
    }
    public class OrderSummerySupport
    {
        public long Price { get; set; }
        public long DeliveryCharge { get; set; }
        public long GrandTotal { get; set; }
    }
}
