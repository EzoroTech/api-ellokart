﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EllowCart.DTO.Order.OutputModels
{
  public  class TotalPaymentOut
    {
        public long BranchId { get; set; }
        public string BranchName { get; set; }
        public float PayableAmount { get; set; }
        public float Price { get; set; }
        public long DeliveryCharge { get; set; }

        public float PayableAmountExpress { get; set; }
        public float PriceExpress { get; set; }
        public long DeliveryChargeExpress { get; set; }

        public int DeliveryMode { get; set; }
        public bool HasDelivery { get; set; } = true;
        public string Reason { get; set; }
        public List<DelivertypeOut> Products { get; set; }
    }
}
