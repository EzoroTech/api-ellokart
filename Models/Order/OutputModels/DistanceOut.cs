﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EllowCart.DTO.Order.OutputModels
{
   public class DistanceOut
    {
        public float DistanceFromSellerAndCustomer { get; set; }
    }
}
