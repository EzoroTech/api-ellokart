﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EllowCart.DTO.Order.OutputModels
{
  public  class PlaceOrderOut
    {
        public int STATUS { get; set; }
        public long OrderNo { get; set; }
    }
}
