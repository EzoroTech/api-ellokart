﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EllowCart.DTO.Order.OutputModels
{
  public  class AcceptorRejectOrderOut
    {
        public int Status { get; set; }
    }
}
