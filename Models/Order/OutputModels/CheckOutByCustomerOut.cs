﻿using EllowCart.DTO.CustomerAddress.OutputModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace EllowCart.DTO.Order.OutputModels
{
   public class CheckOutByCustomerOut
    {
        public List<TotalPaymentOut> Branch { get; set; }
        public List<GetAddressesOut> Address { get; set; }
    }
}
