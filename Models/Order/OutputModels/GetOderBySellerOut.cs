﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EllowCart.DTO.Order.OutputModels
{
  public  class GetOderBySellerOut
    {
        public long OrderId { get; set; }
        public long ProductId { get; set; }
        public int OrderItemQty { get; set; }
        public float OrderItemPrice { get; set; }
        public string ProductName { get; set; }
        public string ProductCode { get; set; }
        public string CustomerName { get; set; }
        public string MobileNumber { get; set; }
        public string LocationName { get; set; }
        public float TotalPayableAmount { get; set; }
        public DateTime CreatedOn { get; set; }
        public string ImageUrl { get; set; }
        public int DeliveryCharge { get; set; }
    }
}
