﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EllowCart.DTO.Order.OutputModels
{
   public class PriceCalculation
    {
        public float Price { get; set; }
        public long DeliveryCharge { get; set; }
    }
}
