﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EllowCart.DTO.Module.InputModels
{
   public class ModulesInputModel
    {
        public string ModuleName { get; set; }
        public bool IsActive { get; set; }
        public DateTime CreatedOn { get; set; }
        public int CreatedBy { get; set; }
        public DateTime UpdatedOn { get; set; }
        public int UpdatedBy { get; set; }
    }
}
