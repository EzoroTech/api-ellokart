﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EllowCart.DTO.Module.InputModels
{
  public  class ModulesOutputModel
    {
        public int ModuleId { get; set; }
        public int Flag { get; set; }
    }
}
