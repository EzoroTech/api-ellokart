﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EllowCart.DTO.Module.InputModels
{
   public class ModuleInputModel
    {
        public int ModuleId { get; set; }
        public string ModuleName { get; set; }
        public bool IsActive { get; set; }
        public DateTime CreatedOn { get; set; }
        public string CreatedBy { get; set; }
        public DateTime UpdatedOn { get; set; }
        public string UpdatedBy { get; set; }
    }
}
