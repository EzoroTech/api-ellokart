﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EllowCart.DTO.Module.InputModels
{
    public class ModuleAttributeModel
    {

        public long moduleId { get; set; }
        public string Operation { get; set; }
        public bool IsActive { get; set; }

        public DateTime CreatedOn { get; set; }
        public string CreatedBy { get; set; }
        public DateTime UpdatedOn { get; set; }
        public string UpdatedBy { get; set; }
    }
    public class ModuleAttributeModelForUpdation
    {
        public int AttributeId { get; set; }

        public int moduleId { get; set; }
        public string Operation { get; set; }
        public bool IsActive { get; set; }

        public DateTime CreatedOn { get; set; }
        public string CreatedBy { get; set; }
        public DateTime UpdatedOn { get; set; }
        public string UpdatedBy { get; set; }
    }
}
