﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EllowCart.DTO.Staff.InputModels
{
   public class StaffInputModel
    {
        public int StaffId { get; set; }
        public string StaffCode { get; set; }
        public string StaffName { get; set; }
        public string CreatedBy { get; set; }
        public string UpdatedBy { get; set; }
        public bool IsActive { get; set; }
        public int flag { get; set; }
        public string username { get; set; }
        public string Password { get; set; }
    }
}
