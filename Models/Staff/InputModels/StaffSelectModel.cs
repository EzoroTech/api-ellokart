﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EllowCart.DTO.Staff.InputModels
{
   public class StaffSelectModel
    {
        public int StaffId { get; set; }
        public int Flag { get; set; }
    }
}
