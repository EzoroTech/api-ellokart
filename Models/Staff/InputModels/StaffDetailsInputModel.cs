﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EllowCart.DTO.Staff.InputModels
{
  public  class StaffDetailsInputModel
    {
        public int StaffDetailsId { get; set; }
        public string MobileNo { get; set; }
        public string EmailID { get; set; }
        public string Addresslineone { get; set; }
        public string Addresslinetwo { get; set; }
        public string Addresslinethree { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Pincode { get; set; }
        public string CreatedBy { get; set; }
        public string UpdatedBy { get; set; }
        public int StaffId { get; set; }
        public int Flag { get; set; }
    }
}
