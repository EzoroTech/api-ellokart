﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EllowCart.DTO.Staff.InputModels
{
  public  class StaffCodeSelectModel
    {
        public string username { get; set; }
        public int Flag { get; set; }
        public string Password { get; set; }
        public string StaffCode { get; set; }
        public string CODE { get; set; }
    }
}
