﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EllowCart.DTO.Staff.InputModels
{
  public  class GetStaffCode
    {
        public int admin_User_flag { get; set; }

        public string username { get; set; }

        public string Password { get; set; }

        public string Code { get; set; }
    }
}
