﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EllowCart.DTO.Staff.InputModels
{
  public  class UpdateEzoroStaff
    {
        public long EmployeeId { get; set; }
        public string EmployeeCode { get; set; }
        public string Name { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public bool IsActive { get; set; }      
        public string UpdatedBy { get; set; }
        public string AccountType { get; set; }
    }
}
