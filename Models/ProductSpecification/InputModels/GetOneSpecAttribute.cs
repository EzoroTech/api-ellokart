﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EllowCart.DTO.ProductSpecification.InputModels
{
   public class GetOneSpecAttribute
    {
        public long ProductCategoryId { get; set; }
    }
}
