﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EllowCart.DTO.ProductSpecification.InputModels
{
  public  class GetSpecificationAttribute
    {
        public long userid { get; set; }
        public string role { get; set; }
        public long Productcategory_id { get; set; }
    }
}
