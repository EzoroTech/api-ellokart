﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EllowCart.DTO.ProductSpecification.InputModels
{
   public class ProductSpecificationGet
    {
        public long ProductSpecId { get; set; }
        public int flag { get; set; }
    }
}
