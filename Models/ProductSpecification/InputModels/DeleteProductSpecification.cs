﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EllowCart.DTO.ProductSpecification.InputModels
{
   public class DeleteProductSpecification
    {
        public long ProductSpecId { get; set; }
    }
}
