﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EllowCart.DTO.ProductSpecification.InputModels
{
  public  class AddSpecificationAttributeInput
    {
        public long ProductSubCategoryId { get; set; }
        public List<Subspec> DtProductSubCatSpec { get; set; }
    }
    public class Subspec
    {
        public long ProductSpecId { get; set; }
    }
}
