﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EllowCart.DTO.ProductSpecification.InputModels
{
  public  class ProductSpecificationInput
    {
       
        public string SpecificationName { get; set; }
        public string Description { get; set; }
        public DateTime CreatedOn { get; set; } = DateTime.Now;
        public string CreatedBy { get; set; }       
        public bool IsActive { get; set; }
    }
}
