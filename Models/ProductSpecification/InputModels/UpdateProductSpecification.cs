﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EllowCart.DTO.ProductSpecification.InputModels
{
   public class UpdateProductSpecification
    {
        public long ProductSpecificationId { get; set; }
        public string SpecificationName { get; set; }
        public string Description { get; set; }
        public bool IsActive { get; set; }
        public string UpdatedBy { get; set; }
    }
}
