﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EllowCart.DTO.ProductSpecification.InputModels
{
   public class AddProductSpecificationByCategory
    {
        public long ProductCategoryId { get; set; }
        public List<ProductSpec> DtProductSpec { get; set; }
    }
    public class ProductSpec
    {
        public long ProductSpecId { get; set; }
    }
        
}
