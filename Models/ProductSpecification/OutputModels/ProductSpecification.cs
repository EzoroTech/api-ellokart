﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EllowCart.DTO.ProductSpecification.OutputModels
{
   public class ProductSpecification
    {
        public long ProductSpecId { get; set; }
        public string SpecificationName { get; set; }
        public string Description { get; set; }
        public bool IsActive { get; set; }
    }
}
