﻿using EllowCart.DTO.Product.OutputModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace EllowCart.DTO.Search.OutputModels
{
   public class ProductSearchForSupportout
    {
        public string ProductName { get; set; }
        public string BranchName { get; set; }
        public string FranchiseName { get; set; }
        public long ProductID { get; set; }
        public long BranchId { get; set; }
        public long UserId { get; set; }
        public List<GetProductImages> Images { get; set; }
      
    }
}
