﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EllowCart.DTO.Search.OutputModels
{
  public  class OrderStatusSearch
    {
        public long OrderId { get; set; }
        public string OrderStatus { get; set; }
        public DateTime CreatedOn { get; set; }
        public string DeliveryMode { get; set; }
    }
}
