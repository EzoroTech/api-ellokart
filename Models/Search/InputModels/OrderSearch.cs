﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EllowCart.DTO.Search.InputModels
{
    public class OrderSearch
    {
        public string CreatedOn { get; set; }
    }

    public class OrderByStatus
    {
        public int Status { get; set; }


    }

    public class OrderDetailsOut
    {

        public int MyProperty { get; set; }

    }
}
