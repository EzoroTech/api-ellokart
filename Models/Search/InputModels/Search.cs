﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EllowCart.DTO.Search.InputModels
{
   public class Search
    {
        public string MobileNo { get; set; }
    }

    public class SearchByDate
    {

        public string CreatedOn { get; set; }
    }
    public class GetSellerOut
    {
        public string MobileNo { get; set; }
        public string BussinessName { get; set; }
        public DateTime CreatedOn { get; set; }
        public string FranchiseName { get; set; }
        public int UserId { get; set; }

    }


}
