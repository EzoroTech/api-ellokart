﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EllowCart.DTO.AddImages.Inputmodels
{
   public class AddImagesInput
    {
        public int SlNo { get; set; }
        public string ImageUrl { get; set; }
        public string ImageName { get; set; }
        public bool IsActive { get; set; }
        public long AccountId { get; set; }
        public long BranchId { get; set; }
        public string AccountType { get; set; }
        public bool IsDocument { get; set; }
        public string DocumentType { get; set; }
        public string ThumbUrl { get; set; }

    }
}
