﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EllowCart.DTO.AddImages.InputModels
{
  public  class updateMultipleImages
    {
        public int SlNo { get; set; }
        public List<Images> Images { get; set; }
        public long AccountId { get; set; }
        public long BranchId { get; set; }
        public string AccountType { get; set; }

    }
    public class Images
    {
        public long ImageId { get; set; }
        public string Imagefile { get; set; }
    }
}
