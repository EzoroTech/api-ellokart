﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EllowCart.DTO.AddImages.InputModels
{
   public class AddMultipleImages
    {
        public int SlNo { get; set; }
        public List<Image> Images { get; set; }
        public bool IsActive { get; set; }
        public long AccountId { get; set; }
        public long BranchId { get; set; }
        public string AccountType { get; set; }
        public bool IsDocument { get; set; }
        public string DocumentType { get; set; }
    }
    public class Image
    {
        public string Imagefile { get; set; }
    }
}

