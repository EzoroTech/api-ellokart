﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EllowCart.DTO.AddImages.InputModels
{
   public class UpdateBranchPhoto
    {
        public long UserId { get; set; }
        public string Role { get; set; }
        public long BranchId { get; set; }
        public List<BranchPhotoUpdate> ImageFile { get; set; }
    }
    public class BranchPhotoUpdate
    {
        public long ImageId { get; set; }
        public string ImageUrl { get; set; }
        public string AccountType { get; set; }
    }
}
