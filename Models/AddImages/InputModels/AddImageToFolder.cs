﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Text;

namespace EllowCart.DTO.AddImages.Inputmodels
{
   public class AddImageToFolder
    {
        public int SlNo { get; set; }
        public string Imagefile { get; set; }
        public bool IsActive { get; set; }
        public long AccountId { get; set; }
        public long BranchId { get; set; }
        public string AccountType { get; set; }
        public bool IsDocument { get; set; }
        public string DocumentType { get; set; }
    }
   
}
