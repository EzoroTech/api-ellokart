﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EllowCart.DTO.AddImages.InputModels
{
   public class UpdateImages
    {
        public int SlNo { get; set; }
        public string Imagefile { get; set; }
        public long AccountId { get; set; }
        public long BranchId { get; set; }
        public string AccountType { get; set; }
        
    }

}
