﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EllowCart.DTO.AddImages.InputModels
{
  public  class AddBranchPhoto
    {
        public long UserId { get; set; }
        public string Role { get; set; }
        public long BranchId { get; set; }       
        public List<BranchPhoto> ImageFile { get; set; }
    }
    public class BranchPhoto
    {
        public string ImageUrl { get; set; }
        public string AccountType { get; set; }
    }
}
