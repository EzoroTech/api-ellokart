﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EllowCart.DTO.AddImages.OutputModels
{
  public  class UpdateBranchPhotoOut
    {
        public long ImageId { get; set; }
        public string ImageUrl { get; set; }
    }
}
