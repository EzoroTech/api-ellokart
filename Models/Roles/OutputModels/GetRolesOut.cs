﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EllowCart.DTO.Roles.OutputModels
{
  public  class GetRolesOut
    {
        public long RoleId { get; set; }
        public string RoleName { get; set; }
        public string Active { get; set; }
    }
}
