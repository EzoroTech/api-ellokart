﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EllowCart.DTO.Roles.InputModels
{
    public class RoleInputModel
    {

        public string RoleName { get; set; }
        public bool IsActive { get; set; }
        public DateTime CreatedOn { get; set; }
        public int CreatedBy { get; set; }
        public DateTime UpdatedOn { get; set; }
        public int UpdatedBy { get; set; }
    }

    public class RoleAttributeInputModel
    {
        public int RoleId { get; set; }
        public int ModuleId { get; set; }
        public int ModuleAttributeId { get; set; }
        public bool IsActive { get; set; }
        public DateTime CreatedOn { get; set; }
        public string CreatedBy { get; set; }
        public DateTime UpdatedOn { get; set; }
        public string UpdatedBy { get; set; }

    }
    public class RoleInputForUpdation
    {
        public int RoleId { get; set; }
        public string RoleName { get; set; }
        public bool IsActive { get; set; }
        public DateTime CreatedOn { get; set; }
        public string CreatedBy { get; set; }
        public DateTime UpdatedOn { get; set; }
        public string UpdatedBy { get; set; }
    }
    public class RoleAttributeInputForUpdation
    {
        public int RoleAttributeID { get; set; }
        public int RoleId { get; set; }
        public int ModuleId { get; set; }
        public int ModuleAttributeId { get; set; }
        public bool IsActive { get; set; }
        public DateTime CreatedOn { get; set; }
        public string CreatedBy { get; set; }
        public DateTime UpdatedOn { get; set; }
        public string UpdatedBy { get; set; }

    }
}
