﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EllowCart.DTO.Roles.InputModels
{
   public class RoleAttribute
    {
        public long RoleAtributeID { get; set; }
        public string CreatedBy { get; set; }
        public string UpdatedBy { get; set; }
        public int Flag { get; set; }
        public List<attributes> dtRoleAttributes { get; set; }
    }
    public class attributes
    {
        public long RoleId { get; set; }
        public long ModuleId { get; set; }
        public long ModuleAttributeId { get; set; }
        public bool IsActive { get; set; }
    }
}
