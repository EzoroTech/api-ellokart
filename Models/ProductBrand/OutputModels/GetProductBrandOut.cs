﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EllowCart.DTO.ProductBrand.OutputModels
{
  public  class GetProductBrandOut
    {
        public string BrandCode { get; set; }
        public string BrandName { get; set; }
        public DateTime CreatedOn { get; set; }
        public long BrandId { get; set; }
    }

    public class GetProductBrand
    {
       
        public long BrandId { get; set; }
        public string BrandName { get; set; }
    }
}
