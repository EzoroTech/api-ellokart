﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EllowCart.DTO.ProductBrand.InputModels
{
  public  class GetProductBrandInput
    {
        public long BrandId { get; set; }
       // public int Flag { get; set; }
    }
    public class GetProductCategForParent
    {

        public int ProductCategoryId { get; set; }

    }


    public class ProductBrandInputForUpdation
    {
        public long BrandId { get; set; }
        public string BrandName { get; set; }
        public string BrandCode { get; set; }
        public string BrandDescription { get; set; }
        public bool IsActive { get; set; }
        public string UpdatedBy { get; set; }
        public DateTime UpdatedOn { get; set; }
        public string ManufactureName { get; set; }
        public int ProductCategoryId { get; set; }


    }


}
