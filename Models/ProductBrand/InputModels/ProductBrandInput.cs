﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EllowCart.DTO.ProductBrand.InputModels
{
  public  class ProductBrandInput
    {
      //  public long BrandId { get; set; }
        public string BrandCode { get; set; }
        public string BrandName { get; set; }
        public bool IsActive { get; set; } = true;
        public string Description { get; set; }
        public int ProductCategoryId { get; set; }
        public string ManufactureName { get; set; }
        public DateTime CreatedOn { get; set; } = DateTime.Now;
       public DateTime UpdatedOn { get; set; } = DateTime.Now;
        public string CreatedBy { get; set; }
       // public string UpdatedBy { get; set; }
     //   public string Type { get; set; }
       // public int Flag { get; set; }
    }
}
