﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EllowCart.DTO.Delivery.InputModels
{
   public class AllDeliveryMode
    {
        public long DeliveryModeId { get; set; }
        public string DeliveryMode { get; set; }
    }
}
