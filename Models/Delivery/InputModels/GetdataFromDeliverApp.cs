﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EllowCart.DTO.Delivery.InputModels
{
  public  class GetdataFromDeliverApp
    {
        public string job_latitude { get; set; }
        public string fleet_email { get; set; }
        public string is_routed { get; set; }
        public string job_id { get; set; }
        public string job_state { get; set; }
        public string has_delivery { get; set; }
        public string job_status { get; set; }
        public string order_id { get; set; }
        public string customer_id  { get; set; }
    }
}
