﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EllowCart.DTO.Branch.OutputModels
{
  public  class GetBranchLogoOut
    {
        public string ImageUrl { get; set; }
    }
}
