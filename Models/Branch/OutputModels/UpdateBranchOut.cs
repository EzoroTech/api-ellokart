﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EllowCart.DTO.Branch.OutputModels
{
   public class UpdateBranchOut
    {
        public long BranchId { get; set; }
    }
}
