﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EllowCart.DTO.Branch.OutputModels
{
  public  class BranchDetailsOut
    {
        public string BranchLogo { get; set; }
        public string ContactPerson { get; set; }
        public long BranchId { get; set; }
        public string MobileNumber { get; set; }
        public string BranchName { get; set; }
        public string Pincode { get; set; }
        public string EmailId { get; set; }
        public string Lattittude { get; set; }
        public string Longittude { get; set; }
        public string Addresslineone { get; set; }
        public string Addresslinetwo { get; set; }
        public string Description { get; set; }
        public string Addresslinethree { get; set; }
        public string LocationName { get; set; }
        public string GSTNO { get; set; }
        public string State { get; set; }
        public long ProPicId { get; set; }
        public string ProPic { get; set; }
        public long CvrPicId { get; set; }
        public string CvrPic { get; set; }

        public int StateId { get; set; }
        public Boolean  IsProfileCompleted { get; set; }

        public string WhatsappNo { get; set; }

        public String FranchiseCode { get; set; }
        public string UserBusinessCode { get; set; }

    }
}
