﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EllowCart.DTO.Branch.OutputModels
{
  public  class GetBranchDetails_CustomerOut
    {
        public string BranchName { get; set; }
        public string MobileNumber { get; set; }
        public string Lattittude { get; set; }
        public string Longittude { get; set; }
        public string BranchLogo { get; set; }
        public string LocationName { get; set; }
        public string Followers { get; set; }
        public string Ratings { get; set; }
    }
}
