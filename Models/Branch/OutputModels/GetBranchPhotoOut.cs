﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EllowCart.DTO.Branch.OutputModels
{
  public  class GetBranchPhotoOut
    {
        public long ImageId { get; set; }
        public string ImageUrl { get; set; }
    }
}
