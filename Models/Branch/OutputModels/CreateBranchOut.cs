﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EllowCart.DTO.Branch.OutputModels
{
   public class CreateBranchOut
    {
        public long BranchId { get; set; }
        public string DisplayName { get; set; }
        public string CityName { get; set; }
        public string Pincode { get; set; }
        public int STATUS { get; set; }
    }
}
