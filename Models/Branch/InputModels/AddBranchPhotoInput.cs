﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace EllowCart.DTO.Branch.InputModels
{
   public class AddBranchPhotoInput
    {
        [Required]
        public long UserId { get; set; }
        public string role { get; set; }
        public long BranchId { get; set; }
        //public List<Image> DtAddImage { get; set; }
    }
    public class Image
    {
        public long ImageId { get; set; }
    }
}
