﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EllowCart.DTO.Branch.InputModels
{
   public class GetBranchesInput
    {
        public long UserId { get; set; }
        public long UserBussinessId { get; set; }
    }
}
