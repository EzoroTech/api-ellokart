﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EllowCart.DTO.Branch.InputModels
{
   public class UpdateBranch
    {
        public long UserId { get; set; }
        public string role { get; set; }
        public long BranchId { get; set; }
        public long UserBussinessId { get; set; }
        public string BranchName { get; set; }
        public string DisplayName { get; set; }
        public string ContactPerson { get; set; }
    }
}
