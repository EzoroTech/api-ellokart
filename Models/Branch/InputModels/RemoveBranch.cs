﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EllowCart.DTO.Branch.InputModels
{
   public class RemoveBranch
    {
        public long UserId { get; set; }
        public string role { get; set; }
        public long BranchId { get; set; }
    }
}
