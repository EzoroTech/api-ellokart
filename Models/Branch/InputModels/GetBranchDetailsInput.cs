﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EllowCart.DTO.Branch.InputModels
{
   public class GetBranchDetailsInput
    {
        public long UserId { get; set; }
        public long BranchId { get; set; }
    }
}
