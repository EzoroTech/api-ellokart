﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EllowCart.DTO.Branch.InputModels
{
  public  class RemoveBranchPhoto
    {
        public long UserId { get; set; }
        public string role { get; set; }
        public long BranchId { get; set; }
        public long ImageId { get; set; }
    }
}
