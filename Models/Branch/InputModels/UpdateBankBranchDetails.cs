﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EllowCart.DTO.Branch.InputModels
{
   public class UpdateBankBranchDetails
    {
        public long UserId { get; set; }
        public string role { get; set; }
        public long BranchId { get; set; }
        public string AccntHolder { get; set; }
        public string AccntNumber { get; set; }
        public string IFSC_Code { get; set; }
        public string BankName { get; set; }
        public string Branch { get; set; }
        public int Flag { get; set; }
        public List<UserDocuments> dTUserDocuments { get; set; }
        public string GstNumber { get; set; }
        public string AadharNumber { get; set; }
        public string PanNumber { get; set; }
    }
    public class UserDocuments
    {
        public string ImageUrl { get; set; }
        public string ImageName { get; set; }
        public string Type { get; set; }
    }
}
