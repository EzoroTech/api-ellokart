﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EllowCart.DTO.Branch.InputModels
{
  public  class GetBranchPhotoInput
    {
        public long BranchId { get; set; }
    }
}
