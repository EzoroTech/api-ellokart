﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EllowCart.DTO.AddToCart.OutputModels
{
  public  class UpdateCartOut
    {
        public int Status { get; set; }
        public int Cartcount { get; set; }
    }
}
