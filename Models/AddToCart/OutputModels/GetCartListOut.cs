﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EllowCart.DTO.AddToCart.OutputModels
{
   public class GetCartListOut
    {
        //public long CartId { get; set; }
        public long ProductId { get; set; }
        public string ProductName { get; set; }
        public string CategoryName { get; set; }
        public string DisplayName { get; set; }
        public float Price { get; set; }
        public float MRP { get; set; }
        public float OrderItemQty { get; set; }
        public long ImageId { get; set; }
        public string ImageUrl { get; set; }
    }
}
