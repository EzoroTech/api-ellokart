﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EllowCart.DTO.AddToCart.OutputModels
{
  public  class SelectItemsFromcart
    {
       // public long CartId { get; set; }
        public long ProductId { get; set; }
        public string ProductCode { get; set; }
        public string ProductName { get; set; }
        public string CategoryName { get; set; }
        public long ProductCategoryId { get; set; }
        public long BranchId { get; set; }
        public string BranchName { get; set; }
        public long CustomerId { get; set; }
        public long OrderItemQty { get; set; } 
        public float UnitPrice { get; set; }
        public float NetPrice { get; set; }
        public string ImageUrl { get; set; }
        public long Stock { get; set; }
        public DateTime ValidTo { get; set; }
        public long MaxOrderValue { get; set; }
        public long MinimumOrderValue { get; set; }
        public float MRP { get; set; }
        public float OfferPrice { get; set; }
        public long DiscountValue { get; set; }

    }
}
