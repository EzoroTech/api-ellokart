﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EllowCart.DTO.AddToCart.OutputModels
{
   public class GetItemsFroCart
    {
        public long BranchId { get; set; }
        public string BranchName { get; set; }
        public int CartCount { get; set; }
        public List<SelectItemsFromcart> Items { get; set; }

    }
}
