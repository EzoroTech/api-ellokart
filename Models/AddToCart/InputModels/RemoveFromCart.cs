﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EllowCart.DTO.AddToCart.InputModels
{
  public  class RemoveFromCart
    {
        public long CustomerId { get; set; }
        public long ProductId { get; set; }
        
    }
}
