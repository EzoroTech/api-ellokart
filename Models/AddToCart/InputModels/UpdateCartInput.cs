﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EllowCart.DTO.AddToCart.InputModels
{
   public class UpdateCartInput
    {
        public long CustomerId { get; set; }
        public long ProductId { get; set; }
        public int OrderItemQty { get; set; }
        public float UnitPrice { get; set; }
        public float NetPrice { get; set; }
    }
}
