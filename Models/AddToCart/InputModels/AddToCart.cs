﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EllowCart.DTO.AddToCart.InputModels
{
   public class AddToCart
    {
        public long ProductId { get; set; }
        public long BranchId { get; set; }
        public long CustomerId { get; set; }
        public long OrderItemQty { get; set; }
        public float UnitPrice { get; set; }
        public float NetPrice { get; set; }
    }
}
