﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EllowCart.DTO.Stock.InputModels
{
     public class SKUinput
    {
        public string StockKeepingUnit { get; set; }
        public bool IsActive { get; set; }
        public DateTime CreatedOn { get; set; }
        public string CreatedBy { get; set; }
        public DateTime UpdatedOn { get; set; }
        public string UpdatedBy { get; set; }


    }

    public class UpdateSKUmdl
    {
        public int StockKeepingUnitId { get; set; }
        public string StockKeepingUnit { get; set; }
        public bool IsActive { get; set; }
        public DateTime CreatedOn { get; set; }
        public string CreatedBy { get; set; }
        public DateTime UpdatedOn { get; set; }
        public string UpdatedBy { get; set; }


    }
}
