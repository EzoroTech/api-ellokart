﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EllowCart.DTO.Stock.InputModels
{
    public class GetSKU
    {
        public int StockKeepingUnitId { get; set; }
    }
}
