﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EllowCart.DTO.Stock.OutputModels
{
  public  class GetStockKeepingunitOut
    {
       // public int STATUS { get; set; }
        public long StockKeepingUnitId { get; set; }
        public string StockKeepingUnitName { get; set; }
    }
}
