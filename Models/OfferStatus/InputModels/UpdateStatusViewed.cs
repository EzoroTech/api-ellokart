﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EllowCart.DTO.OfferStatus.InputModels
{
   public class UpdateStatusViewed
    {
        public long CustomerId { get; set; }
        public long StatusId { get; set; }
    }
}
