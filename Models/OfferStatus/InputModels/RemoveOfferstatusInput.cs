﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EllowCart.DTO.OfferStatus.InputModels
{
  public  class RemoveOfferstatusInput
    {
        public long UserId { get; set; }
        public string role { get; set; }
        public long BranchId { get; set; }
        public long StatusId { get; set; }
    }
}
