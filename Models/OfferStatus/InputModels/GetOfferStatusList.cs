﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EllowCart.DTO.OfferStatus.InputModels
{
  public  class GetOfferStatusList
    {
        public long UserId { get; set; }
        public string role { get; set; }
        public long BranchId { get; set; }
        public int IntialIndex { get; set; }
        public int PageSize { get; set; }
    }
}
