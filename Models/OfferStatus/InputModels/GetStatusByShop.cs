﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EllowCart.DTO.OfferStatus.InputModels
{
   public class GetStatusByShop
    {
        public long BranchId { get; set; }
        public int IntialIndex { get; set; }
        public int PageSize { get; set; }
    }
}
