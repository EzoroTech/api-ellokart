﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EllowCart.DTO.OfferStatus.InputModels
{
  public  class AddOfferStatus
    {
        public long UserId { get; set; }
        public string role { get; set; }
        public long BranchId { get; set; }
        public float MRP { get; set; }
        public float OfferValue { get; set; }
       
        public string StatusName { get; set; }
        public long ImageId { get; set; }
    }
   
}
