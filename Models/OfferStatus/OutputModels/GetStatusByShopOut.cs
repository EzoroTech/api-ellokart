﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EllowCart.DTO.OfferStatus.OutputModels
{
  public  class GetStatusByShopOut
    {
        public string ImageUrl { get; set; }
        public int StatusId { get; set; }
        public string StatusName { get; set; }
    }
}
