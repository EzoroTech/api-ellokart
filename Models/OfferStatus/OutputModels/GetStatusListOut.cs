﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EllowCart.DTO.OfferStatus.OutputModels
{
   public class GetStatusListOut
    {
        public string ImageUrl { get; set; }
        public long BranchId { get; set; }
        public string BranchName { get; set; }
        public string StatusName { get; set; }
    }
}
