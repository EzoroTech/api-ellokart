﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EllowCart.DTO.OfferStatus.OutputModels
{
   public class GetofferStatusListOut
    {
        public long StatusId { get; set; }
        public string StatusName { get; set; }
        public long AccountId { get; set; }
        public string ImageUrl { get; set; }
    }
}
