﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EllowCart.DTO.Verification.InputModels
{
    public class VerifiedBusiness
    {

        public int BranchId { get; set; }
    }
    public class BusinessUpdateModel
    {

        public int BranchId { get; set; }
        public string BusinessName { get; set; }
        public string BusinessDisplayName { get; set; }
        public string ContactPerson { get; set; }
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public string LocationName { get; set; }
        public string Pincode { get; set; }
        public string EmailId { get; set; }
        public string Description { get; set; }
        public string UpdatedBy { get; set; }
        public bool Visibility { get; set; }


    }

    public class DecryptPassword
    {
        public string Password { get; set; } 
    }


    public class BusinessStatusMdl
    {

        public int UserBusinessId { get; set; }
    }


    public class BusinessStatusOut
    {
       
        public int Status { get; set; }
        public string Message { get; set; }
        public string TransId { get; set; }
        public long PlanId { get; set; }
        public long PaymentStatus { get; set; }
    }


    public class UpgradeMdl
    {
        public int UserBusinessId { get; set; }
        public int BusinessStatus { get; set; }
    }
}

