﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EllowCart.DTO.Verification.InputModels
{
   public class VisibiltyOfBusiness
    {
        public long UserBussinessId { get; set; }
        public bool Visibility { get; set; }
    }
}
