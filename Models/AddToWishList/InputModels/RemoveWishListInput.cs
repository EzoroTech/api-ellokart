﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EllowCart.DTO.AddToWishList.InputModels
{
  public  class RemoveWishListInput
    {
        public long CustomerId { get; set; }
        public long WishListId { get; set; }
        public long ProductId { get; set; }
    }
}
