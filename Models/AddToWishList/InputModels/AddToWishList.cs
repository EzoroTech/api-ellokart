﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EllowCart.DTO.AddToWishList.InputModels
{
  public  class AddToWishList
    {
       
        public long CustomerId { get; set; }
        public long ProductId { get; set; }
        public long BranchId { get; set; }
    }
}
