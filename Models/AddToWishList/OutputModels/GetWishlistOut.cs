﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EllowCart.DTO.AddToWishList.OutputModels
{
   public class GetWishlistOut
    {
        public long WishListId { get; set; }
        public long ProductId { get; set; }
        public long BranchId { get; set; }
        public string BranchName { get; set; }
        public int OfferValue { get; set; }
        public int OfferUnit { get; set; }
        public string ProductName { get; set; }
        public string ImageUrl { get; set; }
        public float MRP { get; set; }
        public float OfferPrice { get; set; }
    }
}
