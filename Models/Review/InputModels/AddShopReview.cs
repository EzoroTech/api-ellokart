﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EllowCart.DTO.Review.InputModels
{
  public  class AddShopReview
    {
        public int RatingCount { get; set; }
        public string Comment { get; set; }
        public long CustomerId { get; set; }
        public long BranchId { get; set; }
    }
}
