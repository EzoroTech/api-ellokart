﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EllowCart.DTO.Review.InputModels
{
   public class FollowShop
    {
        public int BranchId { get; set; }
        public bool IsFollowing { get; set; }
        public int CustomerId { get; set; }
    }
    public class FollowStatusOut
    {
        public int FollowStatus { get; set; }
        public long Count { get; set; }

    }

    public class GetFollowersList
    {
        public int BranchId { get; set; }

    }


    public class FollowersListOut
    {
        public string Customer { get; set; }
        public int FollowersCount { get; set; }
    }

    public class GetFollowSummary
    {
        public int CustomerId { get; set; }

    }



    public class FollowSummaryOut
    {
        public string BranchName { get; set; }
        public string EmailId { get; set; }
        public string MobileNumber { get; set; }
        public string ContactPerson { get; set; }

    }
}
