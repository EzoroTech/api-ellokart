﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EllowCart.DTO.Review.InputModels
{
  public  class GetShopReview
    {
        public long CustomerId { get; set; }
        public long BranchId { get; set; }
    }
}
