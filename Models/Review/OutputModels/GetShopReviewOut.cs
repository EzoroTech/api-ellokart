﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EllowCart.DTO.Review.OutputModels
{
   public class GetShopReviewOut
    {
        public long RatingId { get; set; }
        public long RatingCount { get; set; }
        public string Comment { get; set; }
    }
}
