﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EllowCart.DTO.Product.InputModels
{
  public  class EditProductDetailsInput
    {
        public long UserId { get; set; }
        public string role { get; set; }
        public long BranchId { get; set; }
        public long ProductId { get; set; }
        public string ProductName { get; set; }
        public long ProductCategoryId { get; set; }
        public long ProductQty { get; set; }
        public int SKU { get; set; }
        public float MRP { get; set; }
        public long TaxId { get; set; }
        public float TaxablePrice { get; set; }
        public long BrandId { get; set; }
        public string HSNCode { get; set; }
        public string ProductDescr { get; set; }
        public bool IsNew { get; set; }
        public bool IsActive { get; set; }
      //  public float WeightInKg { get; set; }
        public List<Specification> dtProductSpec { get; set; }
       
    }

   public class Specification
    {
        public long ProductSpecId { get; set; }
        public string VarientValue { get; set; }
    }

    public class AddImage1
    {
        public long ImageId { get; set; }
    }
}
