﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace EllowCart.DTO.Product.InputModels
{
  public  class Product
    {
       
        public long UserId { get; set; }
        public string role { get; set; }
        public long BranchId { get; set; }
        public long ProductCategoryId { get; set; }
        public string ProductCode { get; set; }
        public string ProductName { get; set; }
        public long ProductQty { get; set; }
        public int SKU { get; set; }
        public float MRP { get; set; }
        public long TaxId { get; set; }
        public float TaxablePrice { get; set; }
        public long BrandId { get; set; }
        public string HSNCode { get; set; }
        public string ProductDescr { get; set; }
        public bool IsNew { get; set; }
        public long UserBussinessId { get; set; }
        public List<productspec> productSpecification { get; set; }
        
    }

    public class productspec
    {
        public long ProductSpecId { get; set; }
        public string VarientValue { get; set; }
    }

   
   
}
