﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EllowCart.DTO.Product.InputModels
{
  public  class GetProductSerachInput
    {
        public long UserId { get; set; }
        public string role { get; set; }
        public long BranchId { get; set; }
        public string SearchQuery { get; set; }
        public bool ListOrdering { get; set; }
        public long CategoryId { get; set; }
    }
}
