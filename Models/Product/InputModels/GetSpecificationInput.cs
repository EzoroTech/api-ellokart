﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EllowCart.DTO.Product.InputModels
{
   public class GetSpecificationInput
    {
        //public long UserId { get; set; }
        //public string role { get; set; }
        public long BranchId { get; set; }
        public long ProductId { get; set; }
    }
}
