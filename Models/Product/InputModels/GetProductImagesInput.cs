﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EllowCart.DTO.Product.InputModels
{
  public  class GetProductImagesInput
    {
        //public long UserId { get; set; }
        //public string role { get; set; }
        //public long BranchId { get; set; }
        public long ProductId { get; set; }
        public int Index { get; set; }
        public int PageSize { get; set; }
    }
}
