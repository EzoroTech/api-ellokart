﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EllowCart.DTO.Product.InputModels
{
   public class GetProductlistInput
    {
        public long UserId { get; set; }
        public long BranchId { get; set; }
        public int Index { get; set; }
        public int PageSize { get; set; }
    }
}
