﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EllowCart.DTO.Product.OutputModels
{
  public  class GetComboProductImages
    {
        public string ImageUrl { get; set; }
    }
}
