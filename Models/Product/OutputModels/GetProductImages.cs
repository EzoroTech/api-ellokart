﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EllowCart.DTO.Product.OutputModels
{
  public  class GetProductImages
    {
        public long ImageId { get; set; }
        public string ImageUrl { get; set; }
       
    }
}
