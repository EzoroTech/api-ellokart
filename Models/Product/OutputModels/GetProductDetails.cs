﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EllowCart.DTO.Product.OutputModels
{
   public class GetProductDetails
    {
       
        public long BranchId { get; set; }
        public long ProductID { get; set; }
        public string ProductCode { get; set; }
        public string ProductName { get; set; }
        public float STOCK { get; set; }
        public int SKU { get; set; }
        public string StockKeepingUnitName { get; set; }
        public int TaxId { get; set; }
        public string TaxName { get; set; }
        public float MRP { get; set; }
        public float TaxablePrice { get; set; }
        public long BrandId { get; set; }
        public string BrandName { get; set; }
        public string HSNCode { get; set; }
        public string Description { get; set; }
        public bool IsNew { get; set; }
        public string BranchName { get; set; }
        public float OfferPrice { get; set; }
        public long ProductCategoryId { get; set; }
        
        public long ProductSubCategoryId { get; set; }
        public string CategoryName { get; set; }
        public string SubCategoryName { get; set; }
        public string ImageUrl { get; set; }
        public float TaxPercentage { get; set; }
        public long DiscountValue { get; set; }
        public string OfferType { get; set; }
        public int MinimumOrderValue { get; set; }
        public int MaxOrderValue { get; set; }

        // public float WeightInKg { get; set; }

        public int Status { get; set; }
    }
}
