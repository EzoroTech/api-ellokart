﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EllowCart.DTO.Product.OutputModels
{
  public  class GetProductListOut
    {
        public string ImageUrl { get; set; }
        public long ProductID { get; set; }
        public float MRP { get; set; }
        public string ProductName { get; set; }
        public int StockInHand { get; set; }
        public float DiscountValue { get; set; }
        public string OfferName { get; set; }
        public long BranchId { get; set; }
        public long TaxId { get; set; }
        public string TaxName { get; set; }
        public string BrandName { get; set; }
        public long BrandId { get; set; }
        public long ProductSpecId { get; set; }
        public string SpecificationName { get; set; }
        public long ProductCategoryId { get; set; }
        public string CategoryName { get; set; }
    }
}
