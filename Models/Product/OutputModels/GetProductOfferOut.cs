﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EllowCart.DTO.Product.OutputModels
{
  public  class GetProductOfferOut
    {
        public string OfferName { get; set; }
        public int DiscountValue { get; set; }
        public string DiscountUnit { get; set; }
        public int MinimumOrderValue { get; set; }
        public int MaxOrderValue { get; set; }
        public int MaxDiscountAmt { get; set; }
        public DateTime ValidFrom { get; set; }
        public DateTime ValidTo { get; set; }
        public float OfferPrice { get; set; }
        public string OfferType { get; set; }
        public float MRP { get; set; }
        public long OfferSpecialID { get; set; }
    }
}
