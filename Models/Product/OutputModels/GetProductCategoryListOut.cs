﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EllowCart.DTO.Product.OutputModels
{
  public  class GetProductCategoryListOut
    {
        public long ProductCategoryId { get; set; }
        public string CategoryName { get; set; }
    }
}
