﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EllowCart.DTO.Product.OutputModels
{
   public class ProductDetails
    {
        public List<GetProductDetails> Details { get; set; }
        public List<GetProductSpecificationOut> Specification { get; set; }
        public List<GetProductImages> Images { get; set; }
    }
}
