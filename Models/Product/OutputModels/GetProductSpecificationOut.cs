﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EllowCart.DTO.Product.OutputModels
{
  public  class GetProductSpecificationOut
    {
        public long ProductSpecId { get; set; }
        public string VarientValue { get; set; }
        public string SpecificationName { get; set; }
    }
}
