﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EllowCart.DTO.Product.OutputModels
{
  public  class GetProductSearchResultOut
    {
        public long ProductID { get; set; }
        public string ImageUrl { get; set; }
        public float MRP { get; set; }
        public float ProductQty { get; set; }
        public string StockKeepingUnitName { get; set; }
    }
}
