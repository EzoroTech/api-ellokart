﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EllowCart.DTO.SellerCategory.InputModels
{
    public class SellerCategory
    {
        public string SellerCatName { get; set; }
        public int BussinessTypeId { get; set; }
        public bool IsActive { get; set; }
        public DateTime CreatedOn { get; set; }
        public string CreatedBy { get; set; }
        public DateTime UpdatedOn { get; set; }
        public string UpdatedBy { get; set; }
    }
    public class SellerCategInputForUpdation
    {
        public int SellerCatId { get; set; }
        public string SellerCatName { get; set; }
        public int BussinessTypeId { get; set; }
        public bool IsActive { get; set; }
        public DateTime CreatedOn { get; set; }
        public string CreatedBy { get; set; }
        public DateTime UpdatedOn { get; set; }
        public string UpdatedBy { get; set; }
    }
}
