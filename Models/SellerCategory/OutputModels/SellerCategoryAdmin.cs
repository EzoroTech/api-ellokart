﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EllowCart.DTO.SellerCategory.OutputModels
{
   public class SellerCategoryAdmin
    {
        public long SellerCatId { get; set; }
        public string SellerCatName { get; set; }
        public bool IsActive { get; set; }
        public long BussinessTypeId { get; set; }
    }
}
