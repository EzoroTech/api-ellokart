﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EllowCart.DTO.SellerCategory.OutputModels
{
   public class GetSellerCategoryCustomer
    {
        public long SellerCatId { get; set; }
        public string SellerCatName { get; set; }
    }
}
