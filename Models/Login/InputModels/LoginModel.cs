﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EllowCart.DTO.Login.InputModels
{
    public class UserModel
    {
        public string Username { get; set; }
        public string Password { get; set; }
        public string EmailAddress { get; set; }
        public DateTime DateOfJoing { get; set; }
    }
}
