﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EllowCart.DTO.Login.InputModels
{
    public class BuyerLogin
    {
        public string UserName { get; set; }
        public string Password { get; set; }
        public string Flag { get; set; }
        public string Action { get; set; } 
    }
}
