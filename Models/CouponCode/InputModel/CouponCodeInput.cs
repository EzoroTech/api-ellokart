﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EllowCart.DTO.CouponCode.InputModel
{
   public class CouponCodeInput
    {

        public string CouponCode { get; set; }
        public string Description { get; set; }
        public bool IsActive { get; set; }
        public DateTime CreatedOn { get; set; }
        public string CreatedBy { get; set; }
        public DateTime UpdatedOn { get; set; }
        public string UpdatedBy { get; set; }

    }
}
