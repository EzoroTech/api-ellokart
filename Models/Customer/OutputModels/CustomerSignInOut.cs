﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EllowCart.DTO.Customer.OutputModels
{
   public class CustomerSignInOut
    {
        public long CustomerId { get; set; }        
        public string Lattittude { get; set; }
        public string Longittude { get; set; }
        public string LocationName { get; set; }
    }
}
