﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EllowCart.DTO.Customer.OutputModels
{
  public  class GetCustomerDetailsOut
    {
        public long CustomerId { get; set; }
        public string MobileNumber { get; set; }
        public string EmailId { get; set; }
        public string CustomerName { get; set; }
        public DateTime DOB { get; set; }
        public string Gender { get; set; }
        public string Addresslineone { get; set; }
        public string Addresslinetwo { get; set; }
        public string Addresslinethree { get; set; }
        public string Lattittude { get; set; }
        public string Longittude { get; set; }
        public string LocationName { get; set; }
    }
}
