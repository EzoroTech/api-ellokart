﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EllowCart.DTO.Customer.OutputModels
{
   public class ShippingAddress
    {
        public string AddressLineOne { get; set; }
        public string AddressLineTwo { get; set; }
        public string AddressLineThree { get; set; }
        public string PinCode { get; set; }
        public string Customername { get; set; }
        public string MobileNumber { get; set; }
        public string StateName { get; set; }
        public string DistrictName { get; set; }
        public string CityName { get; set; }
    }
    public class OrderSummery
    {
        public long Price { get; set; }
        public long DeliveryCharge { get; set; }
        public long GrandTotal { get; set; }
    }
}
