﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EllowCart.DTO.Customer.OutputModels
{
  public  class GetMyOrdersOut
    {
        public long OrderId { get; set; }
        public long CustomerId { get; set; }
        public float OrderAmt { get; set; }
        public string OrderCode { get; set; }
        public string OrderStatus { get; set; }
        public DateTime Orderdate { get; set; }
        public string BranchName { get; set; }
        public string ImageUrl { get; set; }
    }
}
