﻿using EllowCart.DTO.Customer.InputModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace EllowCart.DTO.Customer.OutputModels
{
  public  class OrderDetails
    {
        public long OrderId { get; set; }
        public DateTime OrderDate { get; set; }
        public string OrderStatus { get; set; }
        public string Status { get; set; }
    }

    public class DetailsOrder
    {
        public List<OrderDetails> OrderDetails { get; set; }
        public List<OrderProductList> Products { get; set; }
        public List<ShippingAddress> ShippingAddress { get; set; }
        public List<OrderSummery> OrderSummery { get; set; }
    }
}
