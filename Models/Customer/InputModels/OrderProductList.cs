﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EllowCart.DTO.Customer.InputModels
{
 public   class OrderProductList
    {
        public long ProductId { get; set; }
        public string ProductName { get; set; }
        public string BranchName { get; set; }
        public long BranchId { get; set; }
        public int OrderItemQty { get; set; }
        public long OrderItemPrice { get; set; }
        public string ImageUrl { get; set; }
    }
}
