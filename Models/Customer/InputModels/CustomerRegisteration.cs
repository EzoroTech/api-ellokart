﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace EllowCart.DTO.Customer.InputModels
{
   public class CustomerRegisteration
    {
        [RegularExpression(@"^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$", ErrorMessage = "Not a valid phone number")]
        public string MobileNumber { get; set; }
        public string EmailId { get; set; }
        [Required]
        [StringLength(30, MinimumLength = 8, ErrorMessage = "Invalid")]
        public string Password { get; set; }
        public string Name { get; set; }
        public string Lattittude { get; set; }
        public string Longittude { get; set; }
        public string LocationName { get; set; }
        public List<Security> DtSecurity { get; set; }

    }
    public class Security
    {
        public long QusetionId { get; set; }
        public string SequrityAns { get; set; }
    }

}
