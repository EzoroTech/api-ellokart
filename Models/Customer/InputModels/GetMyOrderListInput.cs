﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EllowCart.DTO.Customer.InputModels
{
  public  class GetMyOrderListInput
    {
        public long CustomerId { get; set; }
    }
}
