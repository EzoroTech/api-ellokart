﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EllowCart.DTO.Country.OutputModels
{


  public  class GetCountryListOut
    {
        public long CountryId { get; set; }
        public string CountryName { get; set; }

    }
   
  
}
