﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EllowCart.DTO.Country.InputModels
{
  public  class CreateCountryInput
    {
        public string CountryName { get; set; }
        public bool Isactive { get; set; }
        public string Description { get; set; }
        public string CreatedBy { get; set; }
        public string UpdatedBy { get; set; }
    }
}
