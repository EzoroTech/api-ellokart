﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using EllowCart.DTO.Home.InputModels;
using EllowCart.DTO.Home.OutputModels;
using EllowCart.DTO.Upgrade.InputModel;
using EllowCart.Shared;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;




namespace EllowCart.Controllers
{
    [Route("api/[controller]/[Action]")]
    [ApiController]
    public class HomeController : ControllerBase
    {
        EllowCart.Shared.APiResult _oResult = null;
        EllowCart.BusinessContracts.Roles.HomeServices _HomeServices = null;


        // [Authorize]
        [HttpPost]
        [Produces("Application/Json")]
        public APiResult GetNearByShop(GetNearByShopsInput _shopsInput)
        {
            APiResult _ApiResult = new APiResult();
            _HomeServices = new BusinessContracts.Roles.HomeServices();
            try
            {
                _ApiResult = _HomeServices.GetNearByShpos(_shopsInput);
            }
            catch (Exception Ex)
            {
                _ApiResult.iRet = -1;
              //  _ApiResult.HasWarning = true;
                _ApiResult.sMessage = Ex.Message;
                _HomeServices = null;
            }
            return _ApiResult;
        }

        //    [Authorize]
        [HttpPost]
        [Produces("Application/Json")]
        public ApiResult<List<GetShopDetailsout>> GetShopDetails(GetShopDetailsInput _shopsInput)
        {
            ApiResult<List<GetShopDetailsout>> _ApiResult = new ApiResult<List<GetShopDetailsout>>();
            _HomeServices = new BusinessContracts.Roles.HomeServices();
            try
            {
                _ApiResult = _HomeServices.GetShpoDetails(_shopsInput);
            }
            catch (Exception Ex)
            {
                _ApiResult.ErrorCode = -1;
                _ApiResult.HasWarning = true;
                _ApiResult.Message = Ex.Message;
                _HomeServices = null;
            }
            return _ApiResult;
        }

        //  [Authorize]
        [HttpPost]
        [Produces("Application/Json")]
        public APiResult GetNearByOffers(GetNearByOffers _nearByOffers)
        {
            APiResult _ApiResult = new APiResult();
            _HomeServices = new BusinessContracts.Roles.HomeServices();
            try
            {
                _ApiResult = _HomeServices.GetNearByOffers(_nearByOffers);
            }
            catch (Exception Ex)
            {
                _ApiResult.iRet = -1;
                _ApiResult.sError = Ex.Message;
               // _ApiResult.Message = Ex.Message;
                _HomeServices = null;
            }
            return _ApiResult;
        }

        //   [Authorize]
        [HttpPost]
        [Produces("Application/Json")]
        public ApiResult<List<GetNearByStatusOut>> GetNearByStatus(GetNearByStatus _nearByStatus)
        {
            ApiResult<List<GetNearByStatusOut>> _ApiResult = new ApiResult<List<GetNearByStatusOut>>();
            _HomeServices = new BusinessContracts.Roles.HomeServices();
            try
            {
                _ApiResult = _HomeServices.GetNearByStatus(_nearByStatus);
            }
            catch (Exception Ex)
            {
                _ApiResult.ErrorCode = -1;
                _ApiResult.HasWarning = true;
                _ApiResult.Message = Ex.Message;
                _HomeServices = null;
            }
            return _ApiResult;
        }

        //  [Authorize]
        [HttpPost]
        [Produces("Application/Json")]
        public ApiResult<List<ProductListByShopOut>> ProductListByShop(ProductListByShop _shop)
        {
            ApiResult<List<ProductListByShopOut>> _ApiResult = new ApiResult<List<ProductListByShopOut>>();
            _HomeServices = new BusinessContracts.Roles.HomeServices();
            try
            {
                _ApiResult = _HomeServices.ProductListByShop(_shop);
            }
            catch (Exception Ex)
            {
                _ApiResult.ErrorCode = -1;
                _ApiResult.HasWarning = true;
                _ApiResult.Message = Ex.Message;
                _HomeServices = null;
            }
            return _ApiResult;
        }


        //  [Authorize]
        [HttpPost]
        public APiResult GetFeeds(FeedPostListInput _feed)
        {
            _oResult = new APiResult();
            _HomeServices = new BusinessContracts.Roles.HomeServices();

            try
            {
                _oResult = _HomeServices.GetFeeds(_feed);
            }
            catch (Exception eX)
            {
                _oResult.iRet = -1;
                _oResult.sError = eX.Message;
                _HomeServices = null;
            }
            return _oResult;
        }

        //  [Authorize]
        [HttpPost]
        [Produces("Application/Json")]
        public ApiResult<List<ShopSearchOutput>> ShopSearch(ShopSearchInput _search)
        {
            ApiResult<List<ShopSearchOutput>> _ApiResult = new ApiResult<List<ShopSearchOutput>>();
            _HomeServices = new BusinessContracts.Roles.HomeServices();
            try
            {
                _ApiResult = _HomeServices.ShopSearch(_search);
            }
            catch (Exception Ex)
            {
                _ApiResult.ErrorCode = -1;
                _ApiResult.HasWarning = true;
                _ApiResult.Message = Ex.Message;
                _HomeServices = null;
            }
            return _ApiResult;
        }

        //  [Authorize]
        [HttpPost]
        [Produces("Application/Json")]
        public ApiResult<List<ShopSearchByCategory>> ShopSearchByCategory(ShopSearchByCategoryInput _search)
        {
            ApiResult<List<ShopSearchByCategory>> _ApiResult = new ApiResult<List<ShopSearchByCategory>>();
            _HomeServices = new BusinessContracts.Roles.HomeServices();
            try
            {
                _ApiResult = _HomeServices.ShopSearchByCategory(_search);
            }
            catch (Exception Ex)
            {
                _ApiResult.ErrorCode = -1;
                _ApiResult.HasWarning = true;
                _ApiResult.Message = Ex.Message;
                _HomeServices = null;
            }
            return _ApiResult;
        }

        // [Authorize]
        [HttpPost]
        [Produces("Application/Json")]
        public ApiResult<List<ProductSearchOut>> ProductSearch(SearchProduct _search)
        {
            ApiResult<List<ProductSearchOut>> _ApiResult = new ApiResult<List<ProductSearchOut>>();
            _HomeServices = new BusinessContracts.Roles.HomeServices();
            try
            {
                _ApiResult = _HomeServices.ProductSearch(_search);
            }
            catch (Exception Ex)
            {
                _ApiResult.ErrorCode = -1;
                _ApiResult.HasWarning = true;
                _ApiResult.Message = Ex.Message;
                _HomeServices = null;
            }
            return _ApiResult;
        }


        //  [Authorize]
        [HttpPost]
        [Produces("Application/Json")]
        public ApiResult<List<ProductSearchByCategoryOut>> ProductSearchByCategory(ProductSearchByCategory _search)
        {
            ApiResult<List<ProductSearchByCategoryOut>> _ApiResult = new ApiResult<List<ProductSearchByCategoryOut>>();
            _HomeServices = new BusinessContracts.Roles.HomeServices();
            try
            {
                _ApiResult = _HomeServices.ProductSearchByCategory(_search);
            }
            catch (Exception Ex)
            {
                _ApiResult.ErrorCode = -1;
                _ApiResult.HasWarning = true;
                _ApiResult.Message = Ex.Message;
                _HomeServices = null;
            }
            return _ApiResult;
        }

        //   [Authorize]
        [HttpPost]
        [Produces("Application/Json")]
        public ApiResult<List<GetAllProducts>> GetAllProducts()
        {
            ApiResult<List<GetAllProducts>> _ApiResult = new ApiResult<List<GetAllProducts>>();
            _HomeServices = new BusinessContracts.Roles.HomeServices();
            try
            {
                _ApiResult = _HomeServices.GetAllProducts();
            }
            catch (Exception Ex)
            {
                _ApiResult.ErrorCode = -1;
                _ApiResult.HasWarning = true;
                _ApiResult.Message = Ex.Message;
                _HomeServices = null;
            }
            return _ApiResult;
        }

        [Authorize]
        [HttpPost]
        [Produces("Application/Json")]
        public ApiResult<List<BusinessVerifiedOrNotOut>> BusinessVerifiedOrNot(BusinessVerifiedOrNot _verifiedOrNot)
        {
            ApiResult<List<BusinessVerifiedOrNotOut>> _ApiResult = new ApiResult<List<BusinessVerifiedOrNotOut>>();
            _HomeServices = new BusinessContracts.Roles.HomeServices();
            try
            {
                _ApiResult = _HomeServices.BusinessVerifiedOrNot(_verifiedOrNot);
            }
            catch (Exception Ex)
            {
                _ApiResult.ErrorCode = -1;
                _ApiResult.HasWarning = true;
                _ApiResult.Message = Ex.Message;
                _HomeServices = null;
            }
            return _ApiResult;
        }

        [Authorize]
        [HttpPost]
        [Produces("Application/Json")]
        public ApiResult<List<UserBusinessDtls>> UpgradeToPremium(GetBusinessMdl _UpgradeUser)
        {
            ApiResult<List<UserBusinessDtls>> _ApiResult = new ApiResult<List<UserBusinessDtls>>();
            _HomeServices = new BusinessContracts.Roles.HomeServices();
            try
            {
                _ApiResult = _HomeServices.BusinessDataToAdmin(_UpgradeUser);
            }
            catch (Exception Ex)
            {
                _ApiResult.ErrorCode = -1;
                _ApiResult.HasWarning = true;
                _ApiResult.Message = Ex.Message;
                _HomeServices = null;
            }
            return _ApiResult;
        }


        [HttpPost]
        [Produces("Application/Json")]
        public APiResult AddCustomerSupportData(CustomerSupportDataMdl _input)
        {
            _oResult = new APiResult();
            _HomeServices = new BusinessContracts.Roles.HomeServices();

            try
            {
                _oResult = _HomeServices.CustomerSupportData(_input);
            }
            catch (Exception eX)
            {
                _oResult.iRet = -1;
                _oResult.sError = eX.Message;
                _HomeServices = null;
            }
            return _oResult;
        }


        [HttpPost]
        [Produces("Application/Json")]
        public ApiResult<List<GetPremiumSellersOut>> GetPremiumSellers(GetPremiumSellersInput _seller)
        {
            ApiResult<List<GetPremiumSellersOut>> _ApiResult = new ApiResult<List<GetPremiumSellersOut>>();
            _HomeServices = new BusinessContracts.Roles.HomeServices();
            try
            {
                _ApiResult = _HomeServices.GetPremiumSellers(_seller);
            }
            catch (Exception Ex)
            {
                _ApiResult.ErrorCode = -1;
                _ApiResult.HasWarning = true;
                _ApiResult.Message = Ex.Message;
                _HomeServices = null;
            }
            return _ApiResult;
        }


        [HttpPost]
        [Produces("Application/Json")]
        public ApiResult<List<GetNearByOffersByName>> GetPoductsByOfferName(GetOffersOfARegion _nearByOffers)
        {
            ApiResult<List<GetNearByOffersByName>> _ApiResult = new ApiResult<List<GetNearByOffersByName>>();
            _HomeServices = new BusinessContracts.Roles.HomeServices();
            try
            {
                _ApiResult = _HomeServices.GetOffersByName(_nearByOffers);
            }
            catch (Exception Ex)
            {
                _ApiResult.ErrorCode = -1;
                _ApiResult.HasWarning = true;
                _ApiResult.Message = Ex.Message;
                _HomeServices = null;
            }
            return _ApiResult;
        }





    }
}