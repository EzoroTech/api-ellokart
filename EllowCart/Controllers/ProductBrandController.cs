﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using EllowCart.DTO.ProductBrand.InputModels;
using EllowCart.DTO.ProductBrand.OutputModels;
using EllowCart.Shared;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace EllowCart.Controllers
{
    [Route("api/[controller]/[Action]")]
    [ApiController]
    public class ProductBrandController : ControllerBase
    {
        EllowCart.BusinessContracts.Roles.ProductBrandServices _ProductBrand = null;
        EllowCart.Shared.APiResult _ApiResult = null;

        [HttpPost]
        [Produces("Application/Json")]
        public APiResult CreateProductBrand(ProductBrandInput _brand)
        {
            APiResult _ApiResult = new APiResult();
            _ProductBrand = new BusinessContracts.Roles.ProductBrandServices();
            try
            {
                _ApiResult = _ProductBrand.CreateProductBrand(_brand);
            }
            catch (Exception eX)
            {
                _ApiResult.iRet = -1;
                _ApiResult.sError = eX.Message;
                _ProductBrand = null;
            }
            return _ApiResult;
        }

        [HttpPost]
        [Produces("Application/Json")]
        public APiResult GetAllProductBrand( )
        {
            APiResult _ApiResult = new APiResult();
            _ProductBrand = new BusinessContracts.Roles.ProductBrandServices();
            try
            {
                _ApiResult = _ProductBrand.ProductBrandGetAll();
            }
            catch (Exception eX)
            {
                _ApiResult.iRet = -1;
                _ApiResult.sError = eX.Message;
                _ProductBrand = null;
            }
            return _ApiResult;
        }

        [HttpPost]
        [Produces("Application/Json")]
        public ApiResult<List<GetProductBrandOut>> GetProductBrandAdmin(GetProductBrandInput _input)
        {
            ApiResult<List<GetProductBrandOut>> _ApiResult = new ApiResult<List<GetProductBrandOut>>();
            _ProductBrand = new BusinessContracts.Roles.ProductBrandServices();
            try
            {
                _ApiResult = _ProductBrand.GetProductBrandAdmin(_input);
            }
            catch (Exception eX)
            {
                _ApiResult.ErrorCode = -1;
                _ApiResult.HasWarning = true;
                _ApiResult.Message = eX.Message;
                _ProductBrand = null;
            }
            return _ApiResult;
        }

        [HttpPost]
        [Produces("Application/Json")]
        public ApiResult<List<GetProductBrand>> ProductBrandByProductCateg(GetProductCategForParent _input)
        {
            ApiResult<List<GetProductBrand>> _ApiResult = new ApiResult<List<GetProductBrand>>();
            _ProductBrand = new BusinessContracts.Roles.ProductBrandServices();
            try
            {
                _ApiResult = _ProductBrand.GetProductBrandForProductCateg(_input);
            }
            catch (Exception eX)
            {
                _ApiResult.ErrorCode = -1;
                _ApiResult.HasWarning = true;
                _ApiResult.Message = eX.Message;
                _ProductBrand = null;
            }
            return _ApiResult;
        }

        [HttpPost]
        public APiResult DeleteProductBrand(GetProductBrandInput _input)
        {
            _ApiResult = new APiResult();
            _ProductBrand = new BusinessContracts.Roles.ProductBrandServices();
            try
            {
                _ApiResult = _ProductBrand.DeleteProductBrand(_input);
            }
            catch (Exception eX)
            {
                _ApiResult.iRet = -1;
                _ApiResult.sError = eX.Message;
                _ProductBrand = null;
            }
            return _ApiResult;
        }


        [HttpPost]
        public APiResult UpdateProductBrand(ProductBrandInputForUpdation _input)
        {
            _ApiResult = new APiResult();
            _ProductBrand = new BusinessContracts.Roles.ProductBrandServices();
            try
            {
                _ApiResult = _ProductBrand.UpdateProductBrand(_input);
            }
            catch (Exception eX)
            {
                _ApiResult.iRet = -1;
                _ApiResult.sError = eX.Message;
                _ProductBrand = null;
            }
            return _ApiResult;
        }

        //getting productBrand querybank
        [HttpPost]
        public APiResult GetProductBrandById(GetProductBrandInput _input)
        {
            
          _ApiResult = new APiResult();
            _ProductBrand = new BusinessContracts.Roles.ProductBrandServices();
            try
            {
                _ApiResult = _ProductBrand.GetProductBrandById(_input);
            }
            catch (Exception eX)
            {
                _ApiResult.iRet = -1;
                _ApiResult.sError = eX.Message;
                _ProductBrand = null;
            }
            return _ApiResult;

        }



    }
}