﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using EllowCart.DTO.Stock.InputModels;
using EllowCart.DTO.Stock.OutputModels;
using EllowCart.Shared;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace EllowCart.Controllers
{
    [Route("api/[controller]/[Action]")]
    [ApiController]
    public class StockController : ControllerBase
    {
        EllowCart.BusinessContracts.Roles.StockServices _stockServices = null;


        EllowCart.Shared.APiResult _ApiResult = null;



        [HttpPost]
        public APiResult CreateSKU(SKUinput _input)
        {
            _ApiResult = new APiResult();
            _stockServices = new EllowCart.BusinessContracts.Roles.StockServices();
            try
            {
                _ApiResult = _stockServices.AddNewSKU(_input);

            }
            catch (Exception eX)
            {

                _ApiResult.iRet = -1;
                _ApiResult.sError = eX.Message;
                _input = null;

            }
            return _ApiResult;

        }


        [HttpPost]
        public APiResult DeleteSKU(GetSKU _SkId)
        {
            _ApiResult = new APiResult();
            _stockServices = new BusinessContracts.Roles.StockServices();
            try
            {
                _ApiResult = _stockServices.DeleteStockKeepingUnit(_SkId);

            }
            catch (Exception eX)
            {

                _ApiResult.iRet = -1;
                _ApiResult.sError = eX.Message;
                _stockServices = null;


            }
            return _ApiResult;

        }



        [HttpPost]
        [Produces("Application/Json")]
        public ApiResult<List<GetStockKeepingunitOut>> GetStockKeepingUnit()
        {
            ApiResult<List<GetStockKeepingunitOut>> _ApiResult = new ApiResult<List<GetStockKeepingunitOut>>();
            _stockServices = new BusinessContracts.Roles.StockServices();
            try
            {
                _ApiResult = _stockServices.GetStockKeepingUnit();
            }
            catch (Exception eX)
            {
                _ApiResult.ErrorCode = -1;
                _ApiResult.HasWarning = true;
                _ApiResult.Message = eX.Message;
                _stockServices = null;
            }
            return _ApiResult;
        }
        //get one sku 
        [HttpPost]
        [Produces("Application/Json")]
        public APiResult GetOneSKU(GetSKU _skuId)
        {
            _ApiResult = new APiResult();
            _stockServices = new BusinessContracts.Roles.StockServices();
            try
            {
                _ApiResult = _stockServices.GetSKUById(_skuId);
            }
            catch (Exception eX)
            {
                _ApiResult.iRet = -1;
                _ApiResult.sError = eX.Message;
                _stockServices = null;
            }
            return _ApiResult;
        }

        [HttpPost]
        public APiResult UpdateSKU( UpdateSKUmdl _input)
        {
            _ApiResult = new APiResult();
            _stockServices = new BusinessContracts.Roles.StockServices();
           


            try
            {
                _ApiResult = _stockServices.UpdateStockKeepingUnit(_input);

            }
            catch (Exception eX)
            {

                _ApiResult.iRet = -1;
                _ApiResult.sError = eX.Message;
                _stockServices = null;


            }
            return _ApiResult;

        }
    }
}