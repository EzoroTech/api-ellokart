﻿using System;
using System.Collections.Generic;
using EllowCart.DTO.Country.InputModels;
using EllowCart.DTO.Country.OutputModels;
using EllowCart.Shared;
using Microsoft.AspNetCore.Mvc;

namespace EllowCart.Controllers
{
    [Route("api/[controller]/[Action]")]
    [ApiController]
    public class CountryController : ControllerBase
    {
        EllowCart.BusinessContracts.Roles.CountryServices _countryServices = null;
        private APiResult _ApiResult;

        [HttpPost]
        public APiResult CreateCountry(CreateCountryInput _input)
        {
            _ApiResult = new APiResult();
            _countryServices = new BusinessContracts.Roles.CountryServices();
            try
            {
                _ApiResult = _countryServices.AddCountry(_input);

            }
            catch (Exception eX)
            {

                _ApiResult.iRet = -1;
                _ApiResult.sError = eX.Message;
                _countryServices = null;

            }
            return _ApiResult;

        }


        [HttpPost]
        [Produces("Application/Json")]
        public ApiResult<List<GetCountryListOut>> GetCountryList()
        {
            ApiResult<List<GetCountryListOut>> _ApiResult = new ApiResult<List<GetCountryListOut>>();
            _countryServices = new BusinessContracts.Roles.CountryServices();
            try
            {
                _ApiResult = _countryServices.GetCountryList();
            }
            catch (Exception eX)
            {
                _ApiResult.ErrorCode = -1;
                _ApiResult.HasWarning = true;
                _ApiResult.Message = eX.Message;
                _countryServices = null;
            }
            return _ApiResult;
        }
    }
}