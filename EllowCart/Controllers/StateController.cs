﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using EllowCart.DTO.State.InputModels;
using EllowCart.DTO.State.OutputModels;
using EllowCart.Shared;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace EllowCart.Controllers
{
    [Route("api/[controller]/[Action]")]
    [ApiController]
    public class StateController : ControllerBase
    {
        EllowCart.BusinessContracts.Roles.StateServices _StateServices = null;

        [HttpPost]
        [Produces("Application/Json")]
        public ApiResult<List<GetStateListOut>> GetStateList(GetStateListInput _getStateList)
        {
            ApiResult<List<GetStateListOut>> _ApiResult = new ApiResult<List<GetStateListOut>>();
            _StateServices = new BusinessContracts.Roles.StateServices();
            try
            {
                _ApiResult = _StateServices.GetstateList(_getStateList);
            }
            catch (Exception eX)
            {
                _ApiResult.ErrorCode = -1;
                _ApiResult.HasWarning = true;
                _ApiResult.Message = eX.Message;
                _StateServices = null;
            }
            return _ApiResult;
        }

        //[HttpPost]
        //[Produces("Application/Json")]
        //public APiResult GetStateList(GetStateListInput _getStateList)
        //{
        //    APiResult _ApiResult = new APiResult();
        //    _StateServices = new BusinessContracts.Roles.StateServices();
        //    try
        //    {
        //        _ApiResult = _StateServices.GetstateList(_getStateList);
        //    }
        //    catch (Exception eX)
        //    {
        //        _ApiResult.iRet = -1;
        //        _ApiResult.sError = eX.Message;
        //        _StateServices = null;
        //    }
        //    return _ApiResult;
        //}



    }



}