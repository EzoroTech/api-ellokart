﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using EllowCart.DTO.City.InputModels;
using EllowCart.DTO.City.OutputModels;
using EllowCart.Shared;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace EllowCart.Controllers
{
    [Route("api/[controller]/[Action]")]
    [ApiController]
    public class CityController : ControllerBase
    {
        EllowCart.BusinessContracts.Roles.CityServices _CityServices = null;
        [HttpPost]
        [Produces("Application/Json")]
        public ApiResult<List<GetCityListOut>> GetCityList(GetCityListInput _getCityList)
        {
            ApiResult<List<GetCityListOut>> _ApiResult = new ApiResult<List<GetCityListOut>>();
            _CityServices = new BusinessContracts.Roles.CityServices();
            try
            {
                _ApiResult = _CityServices.GetCityList(_getCityList);
            }
            catch (Exception eX)
            {
                _ApiResult.ErrorCode = -1;
                _ApiResult.HasWarning = true;
                _ApiResult.Message = eX.Message;
                _CityServices = null;
            }
            return _ApiResult;
        }
    }
}