﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using EllowCart.DTO.BusinessSystem.InputModels;
using EllowCart.DTO.BusinessSystem.OutputModels;
using EllowCart.Shared;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace EllowCart.Controllers
{
    [Route("api/[controller]/[Action]")]
    [ApiController]
    public class BusinessSystemController : ControllerBase
    {
        EllowCart.Shared.APiResult _ApiResult = null;
        EllowCart.BusinessContracts.Roles.BusinessSystemServices _businesssystem = null;

        [HttpPost]
        [Produces("Application/Json")]
        public ApiResult<List<BusinessSystemOut>> BusinessSystem(Businesssystem _businesssyst)
        {

            ApiResult<List<BusinessSystemOut>> _ApiResult = new ApiResult<List<BusinessSystemOut>>();
            _businesssystem = new BusinessContracts.Roles.BusinessSystemServices();
            try
            {
                _ApiResult = _businesssystem.BusinessSystem(_businesssyst);
            }
            catch (Exception eX)
            {
                _ApiResult.ErrorCode = -1;
                _ApiResult.HasWarning = true;
                _ApiResult.Message = eX.Message;
                _businesssystem = null;
            }
            return _ApiResult;
        }


        [HttpPost]
        [Produces("Application/Json")]
        public ApiResult<List<BusinesssystemDetails>> GetBusinessSystemAdmin([FromBody]BusinessSystemSelect _getbusinesssyst)
        {
            ApiResult<List<BusinesssystemDetails>> _ApiResult = new ApiResult<List<BusinesssystemDetails>>();
            _businesssystem = new BusinessContracts.Roles.BusinessSystemServices();
            try
            {
                _ApiResult = _businesssystem.GetBusinessSystemAdmin(_getbusinesssyst);
            }
            catch (Exception eX)
            {
                _ApiResult.ErrorCode = -1;
                _ApiResult.HasWarning = true;
                _ApiResult.Message = eX.Message;
                _businesssystem = null;
            }
            return _ApiResult;
        }

        [HttpPost]
        [Produces("Application/Json")]
        public ApiResult<List<BusinessSystemUser>> GetBusinessSystem()
        {
            ApiResult<List<BusinessSystemUser>> _ApiResult = new ApiResult<List<BusinessSystemUser>>();
            _businesssystem = new BusinessContracts.Roles.BusinessSystemServices();
            try
            {
                _ApiResult = _businesssystem.GetBusinessSystem();
            }
            catch (Exception eX)
            {
                _ApiResult.ErrorCode = -1;
                _ApiResult.HasWarning = true;
                _ApiResult.Message = eX.Message;
                _businesssystem = null;
            }
            return _ApiResult;
        }

    }
}