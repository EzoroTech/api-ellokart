﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using EllowCart.DTO.District.InputModels;
using EllowCart.DTO.District.OutputModels;
using EllowCart.Shared;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace EllowCart.Controllers
{
    [Route("api/[controller]/[Action]")]
    [ApiController]
    public class DistrictController : ControllerBase
    {
        EllowCart.BusinessContracts.Roles.DistrictServices _DistrictServices = null;

        [HttpPost]
        [Produces("Application/Json")]
        public ApiResult<List<GetDistrictListOut>> GetDistrictList(GetDistrictListInput _getDistrict)
        {
            ApiResult<List<GetDistrictListOut>> _ApiResult = new ApiResult<List<GetDistrictListOut>>();
            _DistrictServices = new BusinessContracts.Roles.DistrictServices();
            try
            {
               _ApiResult = _DistrictServices.GetDistrictlist(_getDistrict);
            }
            catch (Exception eX)
            {
                _ApiResult.ErrorCode = -1;
                _ApiResult.HasWarning = true;
                _ApiResult.Message = eX.Message;
                _DistrictServices = null;
            }
            return _ApiResult;
        }
    }
}