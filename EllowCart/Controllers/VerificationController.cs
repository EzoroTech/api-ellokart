﻿using System;
using EllowCart.Shared;
using Microsoft.AspNetCore.Mvc;
using System.Data;
using EllowCart.DTO.Verification.InputModels;
using System.Collections.Generic;

namespace EllowCart.Controllers
{
    [Route("api/[controller]/[Action]")]
    [ApiController]
    public class VerificationController : ControllerBase
    {
        EllowCart.BusinessContracts.Roles.VerificationServices _verification = null;
        EllowCart.Shared.APiResult _ApiResult = null;

        [HttpPost]
        [Produces("Application/Json")]
        public APiResult GetAllBusinesses()
        {
            _ApiResult = new APiResult();
            _verification = new BusinessContracts.Roles.VerificationServices();
            try
            {
                _ApiResult = _verification.GetAllBusinessForVerification();
            }
            catch (Exception eX)
            {
                _ApiResult.iRet = -1;
                _ApiResult.sError = eX.Message;
                _verification = null;
            }
            return _ApiResult;
        }


        //get one Business 
        [HttpPost]
        [Produces("Application/Json")]
        public APiResult GetOneBusiness(VerifiedBusinessModel _BusinessOut)
        {
            _ApiResult = new APiResult();
            _verification = new BusinessContracts.Roles.VerificationServices();
            try
            {
                _ApiResult = _verification.GetBusinessById(_BusinessOut);
            }
            catch (Exception eX)
            {
                _ApiResult.iRet = -1;
                _ApiResult.sError = eX.Message;
                _verification = null;
            }
            return _ApiResult;
        }



        [HttpPost]
        [Produces("Application/Json")]
        public APiResult BusinessRejection(BusinessVerificationModel _BusinessOut)
        {
            _ApiResult = new APiResult();
            _verification = new BusinessContracts.Roles.VerificationServices();
            try
            {
                _ApiResult = _verification.RejectABusiness(_BusinessOut);
            }
            catch (Exception eX)
            {
                _ApiResult.iRet = -1;
                _ApiResult.sError = eX.Message;
                _verification = null;
            }
            return _ApiResult;
        }

        [HttpPost]
        [Produces("Application/Json")]
        public APiResult GetAllVerifiedBusinesses()
        {
            _ApiResult = new APiResult();
            _verification = new BusinessContracts.Roles.VerificationServices();
            try
            {
                _ApiResult = _verification.GetVerifiedBusinesses();
            }
            catch (Exception eX)
            {
                _ApiResult.iRet = -1;
                _ApiResult.sError = eX.Message;
                _verification = null;
            }
            return _ApiResult;
        }

        //update a  verified Business
        [HttpPost]
        public APiResult UpdateVerifiedBusiness(BusinessUpdateModel Obj)
        {
            _ApiResult = new APiResult();
            _verification = new BusinessContracts.Roles.VerificationServices();
            DataTable dt = new DataTable();


            try
            {
                _ApiResult = _verification.UpdateAVerifiedBusiness(Obj);

            }
            catch (Exception eX)
            {

                _ApiResult.iRet = -1;
                _ApiResult.sError = eX.Message;
                _verification = null;


            }
            return _ApiResult;

        }

        //get one Verified Seller 
        [HttpPost]
        [Produces("Application/Json")]
        public APiResult GetOneVerifiedSeller(VerifiedBusiness _Branch)
        {
            _ApiResult = new APiResult();
            _verification = new BusinessContracts.Roles.VerificationServices();
            try
            {
                _ApiResult = _verification.GetVerifiedSellerById(_Branch);
            }
            catch (Exception eX)
            {
                _ApiResult.iRet = -1;
                _ApiResult.sError = eX.Message;
                _verification = null;
            }
            return _ApiResult;
        }


        //[HttpPost]
        //[Produces("Application/Json")]
        //public APiResult DecryptPassword(DecryptPassword _Branch)
        //{
        //    _ApiResult = new APiResult();
        //    _verification = new BusinessContracts.Roles.VerificationServices();
        //    try
        //    {
        //        _ApiResult = _verification.DecryptPassword(_Branch);
        //    }
        //    catch (Exception eX)
        //    {
        //        _ApiResult.iRet = -1;
        //        _ApiResult.sError = eX.Message;
        //        _verification = null;
        //    }
        //    return _ApiResult;
        //}




        [HttpPost]
        [Produces("Application/Json")]
        public APiResult GetBusinessToUpgrade()
        {
            _ApiResult = new APiResult();
            _verification = new BusinessContracts.Roles.VerificationServices();
            try
            {
                _ApiResult = _verification.UserToUpgradeList();
            }
            catch (Exception eX)
            {
                _ApiResult.iRet = -1;
                _ApiResult.sError = eX.Message;
                _verification = null;
            }
            return _ApiResult;
        }




        [HttpPost]
        public APiResult UpdateStatusOfBusiness(UpgradeMdl Obj)
        {
            _ApiResult = new APiResult();
            _verification = new BusinessContracts.Roles.VerificationServices();
            DataTable dt = new DataTable();


            try
            {
                _ApiResult = _verification.UpdateBusinessStatus(Obj);

            }
            catch (Exception eX)
            {

                _ApiResult.iRet = -1;
                _ApiResult.sError = eX.Message;
                _verification = null;


            }
            return _ApiResult;

        }

        [HttpPost]
        [Produces("Application/Json")]
        public ApiResult<List<BusinessStatusOut>> GetVerificationStatus(BusinessStatusMdl Obj)
  
        {
            ApiResult<List<BusinessStatusOut>> _ApiResult = new ApiResult<List<BusinessStatusOut>>();
            _verification = new BusinessContracts.Roles.VerificationServices();
            try
            {
                _ApiResult = _verification.VerificationStatus(Obj);
            }

            catch (Exception eX)
            {
                _ApiResult.ErrorCode = -1;
                _ApiResult.HasWarning = true;
                _ApiResult.Message = eX.Message;
                _verification = null;
            }
            return _ApiResult;
        }


        [HttpPost]
        [Produces("Application/Json")]
        public APiResult GetAllUpgradedSeller()
        {
            _ApiResult = new APiResult();
            _verification = new BusinessContracts.Roles.VerificationServices();
            try
            {
                _ApiResult = _verification.GetUpgradedBusinesses();
            }
            catch (Exception eX)
            {
                _ApiResult.iRet = -1;
                _ApiResult.sError = eX.Message;
                _verification = null;
            }
            return _ApiResult;
        }


        [HttpPost]
        [Produces("Application/Json")]
        public APiResult SetVisibiltyForBusiness(VisibiltyOfBusiness _Visibility)
        {
            _ApiResult = new APiResult();
            _verification = new BusinessContracts.Roles.VerificationServices();
            try
            {
                _ApiResult = _verification.SetVisibilityForBusiness(_Visibility);
            }
            catch (Exception eX)
            {
                _ApiResult.iRet = -1;
                _ApiResult.sError = eX.Message;
                _verification = null;
            }
            return _ApiResult;
        }

    }
}