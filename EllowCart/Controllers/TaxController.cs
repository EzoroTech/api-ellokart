﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using EllowCart.DTO.Tax.InputModels;
using EllowCart.Shared;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace EllowCart.Controllers
{
    [Route("api/[controller]/[Action]")]
    [ApiController]
    public class TaxController : ControllerBase
    {

        EllowCart.BusinessContracts.Roles.TaxServices _taxService = null;
        EllowCart.Shared.APiResult _ApiResult = null;

        [HttpPost]
        public APiResult CreateTax(TaxInputModel _tax)
        {
            _ApiResult = new APiResult();
            _taxService = new BusinessContracts.Roles.TaxServices();
            try
            {
                _ApiResult = _taxService.AddNewTax(_tax);

            }
            catch (Exception eX)
            {

                _ApiResult.iRet = -1;
                _ApiResult.sError = eX.Message;
                _taxService = null;

            }
            return _ApiResult;

        }
        [HttpPost]
        public APiResult DeleteATax(TaxModel _input)
        {
            _ApiResult = new APiResult();
            _taxService = new BusinessContracts.Roles.TaxServices();
            try
            {
                _ApiResult = _taxService.DeleteTax(_input);

            }
            catch (Exception eX)
            {

                _ApiResult.iRet = -1;
                _ApiResult.sError = eX.Message;
                _taxService = null;


            }
            return _ApiResult;

        }
        [HttpPost]
        public APiResult UpdateTax(TaxInputForUpdation Obj)
        {
            _ApiResult = new APiResult();
            _taxService = new BusinessContracts.Roles.TaxServices();
            DataTable dt = new DataTable();


            try
            {
                _ApiResult = _taxService.UpdateTax(Obj);

            }
            catch (Exception eX)
            {

                _ApiResult.iRet = -1;
                _ApiResult.sError = eX.Message;
                _taxService = null;


            }
            return _ApiResult;

        }
        [HttpPost]
        [Produces("Application/Json")]
        public APiResult GetAllTaxes()
        {
            _ApiResult = new APiResult();
            _taxService = new BusinessContracts.Roles.TaxServices();
            try
            {
                _ApiResult = _taxService.GetAllTaxes();
            }
            catch (Exception eX)
            {
                _ApiResult.iRet = -1;
                _ApiResult.sError = eX.Message;
                _taxService = null;
            }
            return _ApiResult;
        }

        //get one tax 
        [HttpPost]
        [Produces("Application/Json")]
        public APiResult GetOneTax(TaxModel _input)
        {
            _ApiResult = new APiResult();
            _taxService = new BusinessContracts.Roles.TaxServices();
            try
            {
                _ApiResult = _taxService.GetTaxById(_input);
            }
            catch (Exception eX)
            {
                _ApiResult.iRet = -1;
                _ApiResult.sError = eX.Message;
                _taxService = null;
            }
            return _ApiResult;
        }


    }
}