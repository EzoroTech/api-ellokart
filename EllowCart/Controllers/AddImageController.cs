﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Dapper;
using EllowCart.DTO.AddImages.Inputmodels;
using EllowCart.DTO.AddImages.InputModels;
using EllowCart.DTO.AddImages.OutputModels;

using EllowCart.Shared;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace EllowCart.Controllers
{
    [Route("api/[controller]/[Action]")]
    [ApiController]
    public class AddImageController : ControllerBase
    {
        EllowCart.BusinessContracts.Roles.AddImagesServices _addImagesServices = null;
        private IDbConnection DBContext;
        DAL.DBSettings _DBSettings = null;

      //  [Authorize]
        [HttpPost]
        [Produces("Application/Json")]
        public ApiResult<List<AddImagesOut>> AddImagesToFolder(AddImageToFolder _addImage)
        {
            _DBSettings = new DAL.DBSettings();  //-- New change
            DBContext = _DBSettings.GetCartDB();
            ApiResult<List<AddImagesOut>> _ApiResult = new ApiResult<List<AddImagesOut>>();
            _addImagesServices = new BusinessContracts.Roles.AddImagesServices();
            try
            {
                    var base64image = _addImage.Imagefile;
                    var bytes = Convert.FromBase64String(base64image);// a.base64image 
                                                                      //or full path to file in temp location
                                                                      //var filePath = Path.GetTempFileName();

                    // full path to file in current project location
                    string filedir = Path.Combine(Directory.GetCurrentDirectory(), "Images");
                    Debug.WriteLine(filedir);
                    Debug.WriteLine(Directory.Exists(filedir));
                    if (!Directory.Exists(filedir))
                    { //check if the folder exists;
                        Directory.CreateDirectory(filedir);
                    }
                    string fileName = _addImagesServices.ThumbUrl();
                    string file = Path.Combine(filedir, fileName + ".jpg");
                    Debug.WriteLine(file);

                    //Debug.WriteLine(File.Exists(file));

                    if (bytes.Length > 0)
                    {
                        using (var stream = new FileStream(file, FileMode.Create))
                        {
                            stream.Write(bytes, 0, bytes.Length);
                            stream.Flush();
                        }
                    }


                    if (DBContext.State == ConnectionState.Closed)
                    {
                        DBContext.Open();
                    }

                    var result = DBContext.Query<AddImagesOut>("AddImages",
                    new
                    {
                        @SlNo = _addImage.SlNo,
                        @ImageUrl = file,
                        @ImageName = fileName,
                        @IsActive = _addImage.IsActive,
                        @AccountId = _addImage.AccountId,
                        @BranchId = _addImage.BranchId,
                        @AccountType = _addImage.AccountType,
                        @IsDocument = _addImage.IsDocument,
                        @DocumentType = _addImage.DocumentType,
                        @ThumbUrl = file
                    }, commandType: CommandType.StoredProcedure) as List<AddImagesOut>;

                    DBContext.Close();
                    _ApiResult.Count = result.Count;
                    _ApiResult.HasWarning = false;
                    _ApiResult.Message = "Success";
                    _ApiResult.Value = result;

                
            }
            catch (Exception Ex)
            {
                _ApiResult.ErrorCode = -1;
                _ApiResult.HasWarning = true;
                _ApiResult.Message = Ex.Message;
                _addImagesServices = null;
            }
            return _ApiResult;
        }

      //  [Authorize]
        [HttpPost]
        [Produces("Application/Json")]
        public ApiResult<List<AddImagesOut>> AddMultipleImages(AddMultipleImages _addImage)
        {
            _DBSettings = new DAL.DBSettings();  //-- New change
            DBContext = _DBSettings.GetCartDB();
            ApiResult<List<AddImagesOut>> _ApiResult = new ApiResult<List<AddImagesOut>>();
            _addImagesServices = new BusinessContracts.Roles.AddImagesServices();
            try
            {
                var img = _addImage.Images;
                for (int i = 0; i < img.Count; i++)
                {
                    

                    var base64image = img[i].Imagefile;
                    var bytes = Convert.FromBase64String(base64image);// a.base64image 
                                                                      //or full path to file in temp location
                                                                      //var filePath = Path.GetTempFileName();

                    // full path to file in current project location
                    string filedir = Path.Combine(Directory.GetCurrentDirectory(), "Images");
                    Debug.WriteLine(filedir);
                    Debug.WriteLine(Directory.Exists(filedir));
                    if (!Directory.Exists(filedir))
                    { //check if the folder exists;
                        Directory.CreateDirectory(filedir);
                    }
                    string fileName = _addImagesServices.ThumbUrl();
                    string file = Path.Combine(filedir, fileName + ".jpg");
                    Debug.WriteLine(file);

                    //Debug.WriteLine(File.Exists(file));

                    if (bytes.Length > 0)
                    {
                        using (var stream = new FileStream(file, FileMode.Create))
                        {
                            stream.Write(bytes, 0, bytes.Length);
                            stream.Flush();
                        }
                    }


                    if (DBContext.State == ConnectionState.Closed)
                    {
                        DBContext.Open();
                    }

                    var result = DBContext.Query<AddImagesOut>("AddMultipleImages",
                    new
                    {
                        @SlNo = i,
                        @ImageUrl = file,
                        @ImageName = fileName,
                        @IsActive = _addImage.IsActive,
                        @AccountId = _addImage.AccountId,
                        @BranchId = _addImage.BranchId,
                        @AccountType = _addImage.AccountType,
                        @IsDocument = _addImage.IsDocument,
                        @DocumentType = _addImage.DocumentType,
                        @ThumbUrl = file
                    }, commandType: CommandType.StoredProcedure) as List<AddImagesOut>;

                    DBContext.Close();
                    _ApiResult.Count = result.Count;
                    _ApiResult.HasWarning = false;
                    _ApiResult.Message = "Success";
                    _ApiResult.Value = result;

                }
            }
            catch (Exception Ex)
            {
                _ApiResult.ErrorCode = -1;
                _ApiResult.HasWarning = true;
                _ApiResult.Message = Ex.Message;
                _addImagesServices = null;
            }
            return _ApiResult;
        }


     //   [Authorize]
        [HttpPost]
        [Produces("Application/Json")]
        public ApiResult<List<UpdateImagesOut>> UpdateImage(UpdateImages _addImage)
        {
            _DBSettings = new DAL.DBSettings();  //-- New change
            DBContext = _DBSettings.GetCartDB();
            ApiResult<List<UpdateImagesOut>> _ApiResult = new ApiResult<List<UpdateImagesOut>>();
            _addImagesServices = new BusinessContracts.Roles.AddImagesServices();
            try
            {
                var base64image = _addImage.Imagefile;
                var bytes = Convert.FromBase64String(base64image);// a.base64image 
                                                                  //or full path to file in temp location
                                                                  //var filePath = Path.GetTempFileName();

                // full path to file in current project location
                string filedir = Path.Combine(Directory.GetCurrentDirectory(), "Images");
                Debug.WriteLine(filedir);
                Debug.WriteLine(Directory.Exists(filedir));
                if (!Directory.Exists(filedir))
                { //check if the folder exists;
                    Directory.CreateDirectory(filedir);
                }
                string fileName = _addImagesServices.ThumbUrl();
                string file = Path.Combine(filedir, fileName + ".jpg");
                Debug.WriteLine(file);

                //Debug.WriteLine(File.Exists(file));

                if (bytes.Length > 0)
                {
                    using (var stream = new FileStream(file, FileMode.Create))
                    {
                        stream.Write(bytes, 0, bytes.Length);
                        stream.Flush();
                    }
                }


                if (DBContext.State == ConnectionState.Closed)
                {
                    DBContext.Open();
                }

                var result = DBContext.Query<UpdateImagesOut>("UpdateImages",
                   new
                   {
                       @SlNo = _addImage.SlNo,
                       @ImageUrl = file,
                       @ImageName = fileName,                    
                       @AccountId = _addImage.AccountId,
                       @BranchId = _addImage.BranchId,
                       @AccountType = _addImage.AccountType,
                       @ThumbUrl = file
                   }, commandType: CommandType.StoredProcedure) as List<UpdateImagesOut>;

                DBContext.Close();
                _ApiResult.Count = result.Count;
                _ApiResult.HasWarning = false;
                _ApiResult.Message = "Success";
                _ApiResult.Value = result;

            }
            catch (Exception Ex)
            {
                _ApiResult.ErrorCode = -1;
                _ApiResult.HasWarning = true;
                _ApiResult.Message = Ex.Message;
                _addImagesServices = null;
            }
            return _ApiResult;
        }

      //  [Authorize]
        [HttpPost]
        [Produces("Application/Json")]
        public ApiResult<List<UpdateImagesOut>> UpdateMultipleImage(updateMultipleImages _addImage)
        {
            _DBSettings = new DAL.DBSettings();  //-- New change
            DBContext = _DBSettings.GetCartDB();
            ApiResult<List<UpdateImagesOut>> _ApiResult = new ApiResult<List<UpdateImagesOut>>();
            _addImagesServices = new BusinessContracts.Roles.AddImagesServices();
            try
            {
                var img = _addImage.Images;
                for (int i = 0; i < img.Count; i++)
                {
                    if (img[i].ImageId == 0)
                    {
                        var base64image = img[i].Imagefile;
                        var bytes = Convert.FromBase64String(base64image);// a.base64image 
                                                                          //or full path to file in temp location
                                                                          //var filePath = Path.GetTempFileName();

                        // full path to file in current project location
                        string filedir = Path.Combine(Directory.GetCurrentDirectory(), "Images");
                        Debug.WriteLine(filedir);
                        Debug.WriteLine(Directory.Exists(filedir));
                        if (!Directory.Exists(filedir))
                        { //check if the folder exists;
                            Directory.CreateDirectory(filedir);
                        }
                        string fileName = _addImagesServices.ThumbUrl();
                        string file = Path.Combine(filedir, fileName + ".jpg");
                        Debug.WriteLine(file);

                        //Debug.WriteLine(File.Exists(file));

                        if (bytes.Length > 0)
                        {
                            using (var stream = new FileStream(file, FileMode.Create))
                            {
                                stream.Write(bytes, 0, bytes.Length);
                                stream.Flush();
                            }
                        }


                        if (DBContext.State == ConnectionState.Closed)
                        {
                            DBContext.Open();
                        }

                        var result = DBContext.Query<UpdateImagesOut>("AddMultipleImages",
                        new
                        {
                            @SlNo = _addImage.SlNo,                          
                            @ImageUrl = file,
                            @ImageName = fileName,
                            @AccountId = _addImage.AccountId,
                            @BranchId = _addImage.BranchId,
                            @AccountType = _addImage.AccountType,
                            @ThumbUrl = file

                        }, commandType: CommandType.StoredProcedure) as List<UpdateImagesOut>;

                       
                    }
                    else
                    {
                        if (DBContext.State == ConnectionState.Closed)
                        {
                            DBContext.Open();
                        }

                        var result = DBContext.Query<UpdateImagesOut>("RemoveImages",
                        new
                        {
                            @ImageId = img[i].ImageId

                        }, commandType: CommandType.StoredProcedure) as List<UpdateImagesOut>;

                       
                    }

                    DBContext.Close();
                    _ApiResult.Count = 1;
                    _ApiResult.HasWarning = false;
                    _ApiResult.Message = "Success";
                    //_ApiResult.Value = result;
                }

            }
            catch (Exception Ex)
            {
                _ApiResult.ErrorCode = -1;
                _ApiResult.HasWarning = true;
                _ApiResult.Message = Ex.Message;
                _addImagesServices = null;
            }
            return _ApiResult;
        }

      //  [Authorize]
        [HttpPost]
        [Produces("Application/Json")]
        public ApiResult<List<AddImagesOut>> AddBranchPhoto(AddBranchPhoto _addImage)
        {
            _DBSettings = new DAL.DBSettings();  //-- New change
            DBContext = _DBSettings.GetCartDB();
            ApiResult<List<AddImagesOut>> _ApiResult = new ApiResult<List<AddImagesOut>>();
            _addImagesServices = new BusinessContracts.Roles.AddImagesServices();
            try
            {
                var img = _addImage.ImageFile;
                for (int i = 0; i <img.Count; i++)
                {


                    var base64image = img[i].ImageUrl;
                    var bytes = Convert.FromBase64String(base64image);// a.base64image 
                                                                      //or full path to file in temp location
                                                                      //var filePath = Path.GetTempFileName();

                    // full path to file in current project location
                    string filedir = Path.Combine(Directory.GetCurrentDirectory(), "Images");
                    Debug.WriteLine(filedir);
                    Debug.WriteLine(Directory.Exists(filedir));
                    if (!Directory.Exists(filedir))
                    { //check if the folder exists;
                        Directory.CreateDirectory(filedir);
                    }
                    string fileName = _addImagesServices.ThumbUrl();
                    string file = Path.Combine(filedir, fileName + ".jpg");
                    Debug.WriteLine(file);

                    //Debug.WriteLine(File.Exists(file));

                    if (bytes.Length > 0)
                    {
                        using (var stream = new FileStream(file, FileMode.Create))
                        {
                            stream.Write(bytes, 0, bytes.Length);
                            stream.Flush();
                        }
                    }


                    if (DBContext.State == ConnectionState.Closed)
                    {
                        DBContext.Open();
                    }

                    var result = DBContext.Query<AddImagesOut>("AddBranchPhoto",
                    new
                    {
                        @SlNo = i,
                        @ImageUrl = file,
                        @ImageName = fileName,                       
                        @BranchId = _addImage.BranchId,                       
                       @AccountType=img[i].AccountType
                    }, commandType: CommandType.StoredProcedure) as List<AddImagesOut>;

                   

                    DBContext.Close();
                    _ApiResult.Count = result.Count;
                    _ApiResult.HasWarning = false;
                    _ApiResult.Message = "Success";
                    _ApiResult.Value = result ;

                }
            }
            catch (Exception Ex)
            {
                _ApiResult.ErrorCode = -1;
                _ApiResult.HasWarning = true;
                _ApiResult.Message = Ex.Message;
                _addImagesServices = null;
            }
            return _ApiResult;
        }

     //   [Authorize]
        [HttpPost]
        [Produces("Application/Json")]
        public ApiResult<List<UpdateBranchPhotoOut>> UpdateBranchPhoto(UpdateBranchPhoto _addImage)
        {
            _DBSettings = new DAL.DBSettings();  //-- New change
            DBContext = _DBSettings.GetCartDB();
            ApiResult<List<UpdateBranchPhotoOut>> _ApiResult = new ApiResult<List<UpdateBranchPhotoOut>>();
            _addImagesServices = new BusinessContracts.Roles.AddImagesServices();
            try
            {
                var img = _addImage.ImageFile;
                for (int i = 0; i < img.Count; i++)
                {
                    if (img[i].ImageId == 0)
                    {

                        var base64image = img[i].ImageUrl;
                        var bytes = Convert.FromBase64String(base64image);// a.base64image 
                                                                          //or full path to file in temp location
                                                                          //var filePath = Path.GetTempFileName();

                        // full path to file in current project location
                        string filedir = Path.Combine(Directory.GetCurrentDirectory(), "Images");
                        Debug.WriteLine(filedir);
                        Debug.WriteLine(Directory.Exists(filedir));
                        if (!Directory.Exists(filedir))
                        { //check if the folder exists;
                            Directory.CreateDirectory(filedir);
                        }
                        string fileName = _addImagesServices.ThumbUrl();
                        string file = Path.Combine(filedir, fileName + ".jpg");
                        Debug.WriteLine(file);

                        //Debug.WriteLine(File.Exists(file));

                        if (bytes.Length > 0)
                        {
                            using (var stream = new FileStream(file, FileMode.Create))
                            {
                                stream.Write(bytes, 0, bytes.Length);
                                stream.Flush();
                            }
                        }


                        if (DBContext.State == ConnectionState.Closed)
                        {
                            DBContext.Open();
                        }

                        var result = DBContext.Query<UpdateBranchPhotoOut>("AddBranchPhoto",
                        new
                        {
                            @SlNo = i,
                            @ImageUrl = file,
                            @ImageName = fileName,
                            @BranchId = _addImage.BranchId,
                            @AccountType=img[i].AccountType

                        }, commandType: CommandType.StoredProcedure) as List<UpdateBranchPhotoOut>;

                        DBContext.Close();
                        _ApiResult.Count = result.Count;
                        _ApiResult.HasWarning = false;
                        _ApiResult.Message = "Success";
                        _ApiResult.Value = result;

                    }
                    else
                    {
                        var base64image = img[i].ImageUrl;
                        var bytes = Convert.FromBase64String(base64image);// a.base64image 
                                                                          //or full path to file in temp location
                                                                          //var filePath = Path.GetTempFileName();

                        // full path to file in current project location
                        string filedir = Path.Combine(Directory.GetCurrentDirectory(), "Images");
                        Debug.WriteLine(filedir);
                        Debug.WriteLine(Directory.Exists(filedir));
                        if (!Directory.Exists(filedir))
                        { //check if the folder exists;
                            Directory.CreateDirectory(filedir);
                        }
                        string fileName = _addImagesServices.ThumbUrl();
                        string file = Path.Combine(filedir, fileName + ".jpg");
                        Debug.WriteLine(file);

                        //Debug.WriteLine(File.Exists(file));

                        if (bytes.Length > 0)
                        {
                            using (var stream = new FileStream(file, FileMode.Create))
                            {
                                stream.Write(bytes, 0, bytes.Length);
                                stream.Flush();
                            }
                        }


                        if (DBContext.State == ConnectionState.Closed)
                        {
                            DBContext.Open();
                        }

                        var result = DBContext.Query<UpdateBranchPhotoOut>("UpdateBranchPhoto",
                        new
                        {
                           @ImageId=img[i].ImageId,
                            @ImageName = fileName,
                            @AccountId = _addImage.BranchId,
                            @AccountType = img[i].AccountType

                        }, commandType: CommandType.StoredProcedure) as List<UpdateBranchPhotoOut>;

                        DBContext.Close();
                        _ApiResult.Count = 1;
                        _ApiResult.HasWarning = false;
                        _ApiResult.Message = "Success";
                        _ApiResult.Value = result;

                    }
                }
                UpdateBranchPhotoOut update =new UpdateBranchPhotoOut();


                _ApiResult.Count = 1;
                _ApiResult.HasWarning = false;
                _ApiResult.Message = "Success";
               // _ApiResult.Value = update;

            }
            catch (Exception Ex)
            {
                _ApiResult.ErrorCode = -1;
                _ApiResult.HasWarning = true;
                _ApiResult.Message = Ex.Message;
                _addImagesServices = null;
            }
            return _ApiResult;
        }


    }
}