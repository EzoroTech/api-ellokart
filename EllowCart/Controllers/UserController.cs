﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EllowCart.DTO.Login.InputModels;
using EllowCart.DTO.User.InputModels;
using EllowCart.DTO.User.OutputModels;
using EllowCart.Shared;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;

namespace EllowCart.Controllers
{
    [Route("api/[controller]/[Action]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        EllowCart.BusinessContracts.Roles.UserServices _UserServices = null;
        EllowCart.Shared.APiResult _ApiResult = null;
        private IConfiguration _config;
        public UserController(IConfiguration config)
        {
            _config = config;
        }

       // [Authorize]
        [HttpPost]
        [Produces("Application/Json")]
        public ApiResult<List<UserRegistrationout>> UserRegisteration([FromBody]UserRegisteration _User)
        {
            ApiResult<List<UserRegistrationout>> _ApiResult = new ApiResult<List<UserRegistrationout>>();
            _UserServices = new BusinessContracts.Roles.UserServices();
            try
            {
                _ApiResult = _UserServices.UserRegisteration(_User);


                if (_ApiResult.Message == "Success")
                {
                    GetUserCode _getUser = new GetUserCode();
                    _getUser.MobileNumber = _User.MobileNo;
                    _getUser.EmailId = _User.EmailID;


                    IActionResult response = Unauthorized();
                    var tokenString = GenerateJSONWebToken(_getUser);
                    response = Ok(new { token = tokenString });
                    if (tokenString != null)
                    {
                        //SignIn sigin = new SignIn();
                        //sigin.JwtToken = tokenString;
                        _ApiResult.Value[0].JwtToken = tokenString;
                    }

                }
            }
            catch (Exception Ex)
            {
                _ApiResult.ErrorCode = -1;
                _ApiResult.HasWarning = true;
                _ApiResult.Message = Ex.Message;
                _UserServices = null;
            }
            return _ApiResult;
        }

        [Authorize]
        [HttpPost]
        [Produces("Application/Json")]
        public ApiResult<List<UserDetails>> GetUser([FromBody]GetUser _getUser)
        {
            ApiResult<List<UserDetails>> _ApiResult = new ApiResult<List<UserDetails>>();
            _UserServices = new BusinessContracts.Roles.UserServices();
            try
            {
                _ApiResult = _UserServices.GetUser(_getUser);
            }
            catch (Exception Ex)
            {
                _ApiResult.ErrorCode = -1;
                _ApiResult.HasWarning = true;
                _ApiResult.Message = Ex.Message;
                _UserServices = null;
            }
            return _ApiResult;
        }

        [Authorize]
        [HttpPost]
        public ApiResult<List<SecurityAnsOut>> CheckingForSecurityQustn(SecurityQustnCheck _getUser)
        {
            ApiResult<List<SecurityAnsOut>> _ApiResult = new ApiResult<List<SecurityAnsOut>>();
            _UserServices = new BusinessContracts.Roles.UserServices();
            try
            {
                _ApiResult = _UserServices.CheckSecurityQuestion(_getUser);
            }
            catch (Exception Ex)
            {
                _ApiResult.ErrorCode = -1;
                _ApiResult.HasWarning = true;
                _ApiResult.Message = Ex.Message;
                _UserServices = null;
            }
            return _ApiResult;
        }


       // [Authorize]
        [HttpPost]
        public ApiResult<List<ForgotpasswordOut>> ForgotPassword(UserCodeSelect _getUser)
        {
            ApiResult<List<ForgotpasswordOut>> _ApiResult = new ApiResult<List<ForgotpasswordOut>>();
            _UserServices = new BusinessContracts.Roles.UserServices();
            try
            {
                _ApiResult = _UserServices.GetUserCodePasswordUpdate(_getUser);
            }
            catch (Exception Ex)
            {
                _ApiResult.ErrorCode = -1;
                _ApiResult.HasWarning = true;
                _ApiResult.Message = Ex.Message;
                _UserServices = null;
            }
            return _ApiResult;
        }



        [HttpPost]
        public ApiResult<List<SignIn>> UserSignIn(GetUserCode _getUser)
        {
            ApiResult<List<SignIn>> _ApiResult = new ApiResult<List<SignIn>>();
            // UserModel model = new UserModel();
            _UserServices = new BusinessContracts.Roles.UserServices();
            try
            {

                _ApiResult = _UserServices.UserSignIn(_getUser);
                if (_ApiResult.Message == "Success")
                {
                    IActionResult response = Unauthorized();
                    var tokenString = GenerateJSONWebToken(_getUser);
                    response = Ok(new { token = tokenString });
                    if (tokenString != null)
                    {
                        //SignIn sigin = new SignIn();
                        //sigin.JwtToken = tokenString;
                        _ApiResult.Value[0].JwtToken = tokenString;
                    }

                }
            }
            catch (Exception Ex)
            {
                _ApiResult.ErrorCode = -1;
                _ApiResult.HasWarning = true;
                _ApiResult.Message = Ex.Message;
                _UserServices = null;
            }
            return _ApiResult;
        }

        private string GenerateJSONWebToken(GetUserCode userInfo)
        {
            var securityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_config["Jwt:Key"]));
            var credentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256);

            var token = new JwtSecurityToken(_config["Jwt:Issuer"],
              _config["Jwt:Issuer"],
              null,
              expires: DateTime.Now.AddMinutes(120),
              signingCredentials: credentials);

            return new JwtSecurityTokenHandler().WriteToken(token);
        }

        private UserModel AuthenticateUser(UserModel login)
        {
            UserModel user = null;

            //Validate the User Credentials  
            //Demo Purpose, I have Passed HardCoded User Information  
            if (login.Username == "Siju")
            {
                user = new UserModel { Username = "Siju P Sukumar", EmailAddress = "test.btest@gmail.com" };
            }
            return user;
        }


        [Authorize]
        [HttpPost]
        public ApiResult<List<AddGSTOut>> Validation(AddGST _gst)
        {
            ApiResult<List<AddGSTOut>> _ApiResult = new ApiResult<List<AddGSTOut>>();
            _UserServices = new BusinessContracts.Roles.UserServices();
            try
            {
               // _ApiResult = _UserServices.AddGST(_gst);
                AddGSTOut gst = new AddGSTOut();
                var Oreturn = gst;
                _ApiResult.Count = 1;
                _ApiResult.HasWarning = false;
                _ApiResult.Message = "Success";
               
            }
            catch (Exception Ex)
            {
                _ApiResult.ErrorCode = -1;
                _ApiResult.HasWarning = true;
                _ApiResult.Message = Ex.Message;
                _UserServices = null;
            }
            return _ApiResult;
        }


       
    }
}