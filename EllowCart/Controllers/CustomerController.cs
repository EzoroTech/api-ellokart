﻿using System;
using Microsoft.AspNetCore.Mvc;
using EllowCart.Shared;
using EllowCart.DTO.Customer.InputModels;
using EllowCart.DTO.Customer.OutputModels;
using System.Collections.Generic;
using Microsoft.AspNetCore.Authorization;

namespace EllowCart.Controllers
{
    [Route("api/[controller]/[Action]")]
    [ApiController]
    public class CustomerController : ControllerBase
    {
        EllowCart.Shared.APiResult _ApiResult = null;
        EllowCart.BusinessContracts.Roles.CustomerService _customerservice = null;

       // [Authorize]
        [HttpPost]
        public APiResult ForgotPassword(CustomerCodeSelect _input)
        {

            _ApiResult = new APiResult();
            _customerservice = new BusinessContracts.Roles.CustomerService();
            try
            {
                _ApiResult = _customerservice.GetCustomerCodeUpdatePassword(_input);
            }
            catch (Exception eX)
            {
                _ApiResult.iRet = -1;
                _ApiResult.sError = eX.Message;
                _customerservice = null;
            }
            return _ApiResult;
        }


        [HttpPost]
        [Produces("Application/Json")]
        public ApiResult<List<CustomerSignInOut>> CustomerSignIn(GetCustomerCodeModel _customerRegisteration)
        {
            ApiResult<List<CustomerSignInOut>> _ApiResult = new ApiResult<List<CustomerSignInOut>>();
            _customerservice = new BusinessContracts.Roles.CustomerService();
            try
            {
                _ApiResult = _customerservice.CustomerSignIn(_customerRegisteration);
            }
            catch (Exception Ex)
            {
                _ApiResult.ErrorCode = -1;
                _ApiResult.HasWarning = true;
                _ApiResult.Message = Ex.Message;
                _customerservice = null;
            }
            return _ApiResult;
        }

      //  [Authorize]
        [HttpPost]
        [Produces("Application/Json")]
        public ApiResult<List<CustomerRegisterationOut>> CustomerRegisteration(CustomerRegisteration _customerRegisteration)
        {
            ApiResult<List<CustomerRegisterationOut>> _ApiResult = new ApiResult<List<CustomerRegisterationOut>>();
            _customerservice = new BusinessContracts.Roles.CustomerService();
            try
            {
                _ApiResult = _customerservice.CustomerRegisteration(_customerRegisteration);
            }
            catch (Exception Ex)
            {
                _ApiResult.ErrorCode = -1;
                _ApiResult.HasWarning = true;
                _ApiResult.Message = Ex.Message;
                _customerservice = null;
            }
            return _ApiResult;
        }

      //  [Authorize]
        [HttpPost]
        [Produces("Application/Json")]
        public ApiResult<List<CustomerProfilepdateOut>> CustomerProfileUpdate(CustomerProfileUpdate _customerProfileUpdate)
        {
            ApiResult<List<CustomerProfilepdateOut>> _ApiResult = new ApiResult<List<CustomerProfilepdateOut>>();
            _customerservice = new BusinessContracts.Roles.CustomerService();
            try
            {
                _ApiResult = _customerservice.CustomerProfileUpdate(_customerProfileUpdate);
            }
            catch (Exception Ex)
            {
                _ApiResult.ErrorCode = -1;
                _ApiResult.HasWarning = true;
                _ApiResult.Message = Ex.Message;
                _customerservice = null;
            }
            return _ApiResult;
        }


      //  [Authorize]
        [HttpPost]
        [Produces("Application/Json")]
        public ApiResult<List<GetCustomerDetailsOut>> GetCustomerDetails(GetCustomerDetailsinput _customerDetailsinput)
        {
            ApiResult<List<GetCustomerDetailsOut>> _ApiResult = new ApiResult<List<GetCustomerDetailsOut>>();
            _customerservice = new BusinessContracts.Roles.CustomerService();
            try
            {
                _ApiResult = _customerservice.GetCustomerDetails(_customerDetailsinput);
            }
            catch (Exception eX)
            {
                _ApiResult.ErrorCode = -1;
                _ApiResult.HasWarning = true;
                _ApiResult.Message = eX.Message;
                _customerservice = null;
            }
            return _ApiResult;
        }

      //  [Authorize]
        [HttpPost]
        [Produces("Application/Json")]
        public APiResult CustomerOrderDetails(CustomerOrder _details)
        {
            APiResult _ApiResult = new APiResult();
            _customerservice = new BusinessContracts.Roles.CustomerService();
            try
            {
                _ApiResult = _customerservice.CustomerOrderDetails(_details);
            }
            catch (Exception eX)
            {
                _ApiResult.iRet = -1;
                _ApiResult.sError = eX.Message;
               // _ApiResult.Message = eX.Message;
                _customerservice = null;
            }
            return _ApiResult;
        }

        //[Authorize]
        [HttpPost]
        [Produces("Application/Json")]
        public ApiResult<List<GetMyOrdersOut>> GetMyOrderList(GetMyOrderListInput _details)
        {
            ApiResult<List<GetMyOrdersOut>> _ApiResult = new ApiResult<List<GetMyOrdersOut>>();
            _customerservice = new BusinessContracts.Roles.CustomerService();
            try
            {
                _ApiResult = _customerservice.GetMyOrderList(_details);
            }
            catch (Exception eX)
            {
                _ApiResult.ErrorCode = -1;
                _ApiResult.HasWarning = true;
                _ApiResult.Message = eX.Message;
                _customerservice = null;
            }
            return _ApiResult;
        }
    }
}