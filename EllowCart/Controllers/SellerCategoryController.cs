﻿using System;
using System.Collections.Generic;
using System.Data;
using EllowCart.DTO.SellerCategory.InputModels;
using EllowCart.DTO.SellerCategory.OutputModels;
using EllowCart.Shared;
using Microsoft.AspNetCore.Mvc;

namespace EllowCart.Controllers
{
    [Route("api/[controller]/[Action]")]
    [ApiController]
    public class SellerCategoryController : ControllerBase
    {
        EllowCart.Shared.APiResult _ApiResult = null;
        EllowCart.BusinessContracts.Roles.SellerCategoryServices _SellerCategoryServices = null;


        [HttpPost]
        public APiResult CreateSellerCategory(SellerCategory _sellercateg)
        {
            _ApiResult = new APiResult();
            _SellerCategoryServices = new BusinessContracts.Roles.SellerCategoryServices();
            try
            {
                _ApiResult = _SellerCategoryServices.AddNewSellerCategory(_sellercateg);

            }
            catch (Exception eX)
            {

                _ApiResult.iRet = -1;
                _ApiResult.sError = eX.Message;
                _SellerCategoryServices = null;

            }
            return _ApiResult;

        }
        [HttpPost]
        public APiResult DeleteSellerCategory(SellerCategoryGetAdmin obj)
        {
            _ApiResult = new APiResult();
            _SellerCategoryServices = new BusinessContracts.Roles.SellerCategoryServices();
            try
            {
                _ApiResult = _SellerCategoryServices.DeleteSellerCateg(obj);

            }
            catch (Exception eX)
            {

                _ApiResult.iRet = -1;
                _ApiResult.sError = eX.Message;
                _SellerCategoryServices = null;


            }
            return _ApiResult;

        }
        [HttpPost]
        public APiResult UpdateSellerCategory(SellerCategInputForUpdation Obj)
        {
            _ApiResult = new APiResult();
            _SellerCategoryServices = new BusinessContracts.Roles.SellerCategoryServices();
            DataTable dt = new DataTable();


            try
            {
                _ApiResult = _SellerCategoryServices.UpdateSellerCategory(Obj);

            }
            catch (Exception eX)
            {

                _ApiResult.iRet = -1;
                _ApiResult.sError = eX.Message;
                _SellerCategoryServices = null;


            }
            return _ApiResult;

        }



        //get all seller categories
        [HttpPost]
        [Produces("Application/Json")]
        public APiResult GetAllSellerCategory()
        {
            _ApiResult = new APiResult();
            _SellerCategoryServices = new BusinessContracts.Roles.SellerCategoryServices();
            try
            {
                _ApiResult = _SellerCategoryServices.GetAllSellerCategory();
            }
            catch (Exception eX)
            {
                _ApiResult.iRet = -1;
                _ApiResult.sError = eX.Message;
                _SellerCategoryServices = null;
            }
            return _ApiResult;
        }


        //get one seller category 
        [HttpPost]
        [Produces("Application/Json")]
        public APiResult GetOneSellerCateg(SellerCategoryGetAdmin _obj)
        {
            _ApiResult = new APiResult();
            _SellerCategoryServices = new BusinessContracts.Roles.SellerCategoryServices();
            try
            {
                _ApiResult = _SellerCategoryServices.GetSellerCategById(_obj);
            }
            catch (Exception eX)
            {
                _ApiResult.iRet = -1;
                _ApiResult.sError = eX.Message;
                _SellerCategoryServices = null;
            }
            return _ApiResult;
        }


        [HttpPost]
        [Produces("Application/Json")]
        public ApiResult<List<SellerCategoryDetails>> GetSellerCategory(GetSellerCategory _Seller)
        {
            ApiResult<List<SellerCategoryDetails>> _ApiResult = new ApiResult<List<SellerCategoryDetails>>();
            _SellerCategoryServices = new BusinessContracts.Roles.SellerCategoryServices();
            try
            {
                _ApiResult = _SellerCategoryServices.GetSellerCategory(_Seller);
            }
            catch (Exception ex)
            {
                _ApiResult.ErrorCode = -1;
                _ApiResult.HasWarning = true;
                _ApiResult.Message = ex.Message;
                _SellerCategoryServices = null;
            }
            return _ApiResult;
        }


        [HttpPost]
        [Produces("Application/Json")]
        public ApiResult<List<GetSellerCategoryCustomer>> GetSellerCategoryCustomer()
        {
            ApiResult<List<GetSellerCategoryCustomer>> _ApiResult = new ApiResult<List<GetSellerCategoryCustomer>>();
            _SellerCategoryServices = new BusinessContracts.Roles.SellerCategoryServices();
            try
            {
                _ApiResult = _SellerCategoryServices.GetSellerCategoryCustomer();
            }
            catch (Exception ex)
            {
                _ApiResult.ErrorCode = -1;
                _ApiResult.HasWarning = true;
                _ApiResult.Message = ex.Message;
                _SellerCategoryServices = null;
            }
            return _ApiResult;
        }



    }
}