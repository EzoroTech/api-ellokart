﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using EllowCart.DTO.CustomerAddress.InputModels;
using EllowCart.DTO.CustomerAddress.OutputModels;
using EllowCart.Shared;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace EllowCart.Controllers
{
    [Route("api/[controller]/[Action]")]
    [ApiController]
    public class CustomerAddressController : ControllerBase
    {
        EllowCart.BusinessContracts.Roles.CustomerAddressServices _CustomerAddress = null;

       // [Authorize]
        [HttpPost]
        [Produces("Application/Json")]
        public ApiResult<List<AddCustomeraddressOut>> CustomerAddress(AddCustomerAddressInput _addressInput)
        {
            ApiResult<List<AddCustomeraddressOut>> _ApiResult = new ApiResult<List<AddCustomeraddressOut>>();
            _CustomerAddress = new BusinessContracts.Roles.CustomerAddressServices();
            try
            {
                _ApiResult = _CustomerAddress.CustomerAddress(_addressInput);
            }
            catch (Exception eX)
            {
                _ApiResult.ErrorCode = -1;
                _ApiResult.HasWarning = true;
                _ApiResult.Message = eX.Message;
                _CustomerAddress = null;
            }
            return _ApiResult;
        }

      //  [Authorize]
        [HttpPost]
        [Produces("Application/Json")]
        public ApiResult<List<GetAddressesOut>> GetAddresses(GetAddresses _addressInput)
        {
            ApiResult<List<GetAddressesOut>> _ApiResult = new ApiResult<List<GetAddressesOut>>();
            _CustomerAddress = new BusinessContracts.Roles.CustomerAddressServices();
            try
            {
                _ApiResult = _CustomerAddress.GetAddresses(_addressInput);
            }
            catch (Exception eX)
            {
                _ApiResult.ErrorCode = -1;
                _ApiResult.HasWarning = true;
                _ApiResult.Message = eX.Message;
                _CustomerAddress = null;
            }
            return _ApiResult;
        }

       // [Authorize]
        [HttpPost]
        [Produces("Application/Json")]
        public ApiResult<List<RemoveAddressOut>> RemoveCustomerAddress(RemoveCustomerAddress _addressInput)
        {
            ApiResult<List<RemoveAddressOut>> _ApiResult = new ApiResult<List<RemoveAddressOut>>();
            _CustomerAddress = new BusinessContracts.Roles.CustomerAddressServices();
            try
            {
                _ApiResult = _CustomerAddress.RemoveCustomerAddress(_addressInput);
            }
            catch (Exception eX)
            {
                _ApiResult.ErrorCode = -1;
                _ApiResult.HasWarning = true;
                _ApiResult.Message = eX.Message;
                _CustomerAddress = null;
            }
            return _ApiResult;
        }

       // [Authorize]
        [HttpPost]
        [Produces("Application/Json")]
        public ApiResult<List<EditaddressOut>> EditCustomerAdress(EditCustomerAddress _addressInput)
        {
            ApiResult<List<EditaddressOut>> _ApiResult = new ApiResult<List<EditaddressOut>>();
            _CustomerAddress = new BusinessContracts.Roles.CustomerAddressServices();
            try
            {
                _ApiResult = _CustomerAddress.EditCustomerAddress(_addressInput);
            }
            catch (Exception eX)
            {
                _ApiResult.ErrorCode = -1;
                _ApiResult.HasWarning = true;
                _ApiResult.Message = eX.Message;
                _CustomerAddress = null;
            }
            return _ApiResult;
        }

        [HttpPost]
        [Produces("Application/Json")]
        public ApiResult<List<EditaddressOut>> SetDefaultAddress(SetDefaultAddress _addressInput)
        {
            ApiResult<List<EditaddressOut>> _ApiResult = new ApiResult<List<EditaddressOut>>();
            _CustomerAddress = new BusinessContracts.Roles.CustomerAddressServices();
            try
            {
                _ApiResult = _CustomerAddress.SetDefaultAddress(_addressInput);
            }
            catch (Exception eX)
            {
                _ApiResult.ErrorCode = -1;
                _ApiResult.HasWarning = true;
                _ApiResult.Message = eX.Message;
                _CustomerAddress = null;
            }
            return _ApiResult;
        }


    }
}