﻿using System;
using EllowCart.Shared;
using Microsoft.AspNetCore.Mvc;
using System.Data;
using EllowCart.DTO.Module.InputModels;

namespace EllowCart.Controllers

{
    [Route("api/[controller]/[Action]")]
    [ApiController]
    public class ModuleController : ControllerBase
    {
        EllowCart.BusinessContracts.Roless.ModuleServices _ModuleServices = null;
        EllowCart.Shared.APiResult _ApiResult = null;

        //adding module
        [HttpPost]
        public APiResult CreateModule(ModulesInputModel _Module)
        {
            _ApiResult = new APiResult();
            _ModuleServices = new BusinessContracts.Roless.ModuleServices();
            try
            {
                _ApiResult = _ModuleServices.AddNewModule(_Module);

            }
            catch (Exception eX)
            {

                _ApiResult.iRet = -1;
                _ApiResult.sError = eX.Message;
                _ModuleServices = null;

            }
            return _ApiResult;

        }


        //delete a module
        [HttpPost]
        public APiResult DeleteAmodule(ModuleOuput _moduleout)
        {
            _ApiResult = new APiResult();
            _ModuleServices = new BusinessContracts.Roless.ModuleServices();
            try
            {
                _ApiResult = _ModuleServices.DeleteAmodule(_moduleout);

            }
            catch (Exception eX)
            {

                _ApiResult.iRet = -1;
                _ApiResult.sError = eX.Message;
                _ModuleServices = null;


            }
            return _ApiResult;

        }

        //update a  module
        [HttpPost]
        public APiResult UpdateModule(ModuleInputModel Obj)
        {
            _ApiResult = new APiResult();
            _ModuleServices = new BusinessContracts.Roless.ModuleServices();
            DataTable dt = new DataTable();


            try
            {
                _ApiResult = _ModuleServices.UpdateModule(Obj);

            }
            catch (Exception eX)
            {

                _ApiResult.iRet = -1;
                _ApiResult.sError = eX.Message;
                _ModuleServices = null;


            }
            return _ApiResult;

        }

        //get all modules
        [HttpPost]
        [Produces("Application/Json")]
        public APiResult GetAllModule()
        {
            _ApiResult = new APiResult();
            _ModuleServices = new BusinessContracts.Roless.ModuleServices();
            try
            {
                _ApiResult = _ModuleServices.GetAllModules();
            }
            catch (Exception eX)
            {
                _ApiResult.iRet = -1;
                _ApiResult.sError = eX.Message;
                _ModuleServices = null;
            }
            return _ApiResult;
        }


        //get one module 
        [HttpPost]
        [Produces("Application/Json")]
        public APiResult GetOneModule(ModuleOuput _moduleout)
        {
            _ApiResult = new APiResult();
            _ModuleServices = new BusinessContracts.Roless.ModuleServices();
            try
            {
                _ApiResult = _ModuleServices.GetModuleById(_moduleout);
            }
            catch (Exception eX)
            {
                _ApiResult.iRet = -1;
                _ApiResult.sError = eX.Message;
                _ModuleServices = null;
            }
            return _ApiResult;
        }


        [HttpPost]
        public APiResult CreateModuleAttribute(ModuleAttributeModel _ModuleAttr)
        {
            _ApiResult = new APiResult();
            _ModuleServices = new BusinessContracts.Roless.ModuleServices();
            try
            {
                _ApiResult = _ModuleServices.AddNewModuleAttribute(_ModuleAttr);

            }
            catch (Exception eX)
            {

                _ApiResult.iRet = -1;
                _ApiResult.sError = eX.Message;
                _ModuleServices = null;

            }
            return _ApiResult;

        }


        //delete a ModuleAttribute
        [HttpPost]
        public APiResult DeleteAmoduleAttribute(ModuleAttributeOutputModel _mAttrbt)
        {
            _ApiResult = new APiResult();
            _ModuleServices = new BusinessContracts.Roless.ModuleServices();
            try
            {
                _ApiResult = _ModuleServices.DeleteAmoduleAttribute(_mAttrbt);

            }
            catch (Exception eX)
            {

                _ApiResult.iRet = -1;
                _ApiResult.sError = eX.Message;
                _ModuleServices = null;


            }
            return _ApiResult;

        }

        //update a  ModuleAttribute
        [HttpPost]
        public APiResult UpdateModuleAttribute(ModuleAttributeModelForUpdation Obj)
        {
            _ApiResult = new APiResult();
            _ModuleServices = new BusinessContracts.Roless.ModuleServices();
            DataTable dt = new DataTable();


            try
            {
                _ApiResult = _ModuleServices.UpdateModuleAttribute(Obj);

            }
            catch (Exception eX)
            {

                _ApiResult.iRet = -1;
                _ApiResult.sError = eX.Message;
                _ModuleServices = null;


            }
            return _ApiResult;

        }

        //get all ModuleAttribute
        [HttpPost]
        [Produces("Application/Json")]
        public APiResult GetAllModuleAttribute()
        {
            _ApiResult = new APiResult();
            _ModuleServices = new BusinessContracts.Roless.ModuleServices();
            try
            {
                _ApiResult = _ModuleServices.GetAllModuleAttributes();
            }
            catch (Exception eX)
            {
                _ApiResult.iRet = -1;
                _ApiResult.sError = eX.Message;
                _ModuleServices = null;
            }
            return _ApiResult;
        }


        //get one ModuleAttribute 
        [HttpPost]
        [Produces("Application/Json")]
        public APiResult GetOneModuleAttr(ModuleAttributeOutputModel _mattributeOut)
        {
            _ApiResult = new APiResult();
            _ModuleServices = new BusinessContracts.Roless.ModuleServices();
            try
            {
                _ApiResult = _ModuleServices.GetModuleAttributeById(_mattributeOut);
            }
            catch (Exception eX)
            {
                _ApiResult.iRet = -1;
                _ApiResult.sError = eX.Message;
                _ModuleServices = null;
            }
            return _ApiResult;
        }

        //get one ModuleAttribute 
        [HttpPost]
        [Produces("Application/Json")]
        public APiResult GeModuleAttributeForModule(ModuleOuput _mattributeOut)
        {
            _ApiResult = new APiResult();
            _ModuleServices = new BusinessContracts.Roless.ModuleServices();
            try
            {
                _ApiResult = _ModuleServices.ModuleAttributeByModuleId(_mattributeOut);
            }
            catch (Exception eX)
            {
                _ApiResult.iRet = -1;
                _ApiResult.sError = eX.Message;
                _ModuleServices = null;
            }
            return _ApiResult;
        }
        }
}