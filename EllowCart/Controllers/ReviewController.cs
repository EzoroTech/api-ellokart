﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using EllowCart.DTO.Review.InputModels;
using EllowCart.DTO.Review.OutputModels;
using EllowCart.Shared;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace EllowCart.Controllers
{
    [Route("api/[controller]/[Action]")]
    [ApiController]
    public class ReviewController : ControllerBase
    {
        EllowCart.BusinessContracts.Roles.ReviewServices _ReviewServices = null;

        [Authorize]
        [HttpPost]
        [Produces("Application/Json")]
        public ApiResult<List<AddShopReviewout>> AddShopReviews(AddShopReview _shopReview)
        {
            ApiResult<List<AddShopReviewout>> _ApiResult = new ApiResult<List<AddShopReviewout>>();
            _ReviewServices = new BusinessContracts.Roles.ReviewServices();
            try
            {
                _ApiResult = _ReviewServices.AddShopReview(_shopReview);
            }
            catch (Exception eX)
            {
                _ApiResult.ErrorCode = -1;
                _ApiResult.HasWarning = true;
                _ApiResult.Message = eX.Message;
                _ReviewServices = null;
            }
            return _ApiResult;
        }

        [Authorize]
        [HttpPost]
        [Produces("Application/Json")]
        public ApiResult<List<GetShopReviewOut>> GetShopReviews(GetShopReview _shopReview)
        {
            ApiResult<List<GetShopReviewOut>> _ApiResult = new ApiResult<List<GetShopReviewOut>>();
            _ReviewServices = new BusinessContracts.Roles.ReviewServices();
            try
            {
                _ApiResult = _ReviewServices.GetShopReview(_shopReview);
            }
            catch (Exception eX)
            {
                _ApiResult.ErrorCode = -1;
                _ApiResult.HasWarning = true;
                _ApiResult.Message = eX.Message;
                _ReviewServices = null;
            }
            return _ApiResult;
        }

        [Authorize]
        [HttpPost]
        [Produces("Application/Json")]
        public ApiResult<List<AddProductReviewOut>> AddProductReviews(AddProductReviewInput _ProductReview)
        {
            ApiResult<List<AddProductReviewOut>> _ApiResult = new ApiResult<List<AddProductReviewOut>>();
            _ReviewServices = new BusinessContracts.Roles.ReviewServices();
            try
            {
                _ApiResult = _ReviewServices.AddProductReview(_ProductReview);
            }
            catch (Exception eX)
            {
                _ApiResult.ErrorCode = -1;
                _ApiResult.HasWarning = true;
                _ApiResult.Message = eX.Message;
                _ReviewServices = null;
            }
            return _ApiResult;
        }

       // [Authorize]
        [HttpPost]
        [Produces("Application/Json")]
        public ApiResult<List<FollowStatusOut>> UpdateFollowStatus(FollowShop Obj)

        {
            ApiResult<List<FollowStatusOut>> _ApiResult = new ApiResult<List<FollowStatusOut>>();
            _ReviewServices = new BusinessContracts.Roles.ReviewServices();
            try
            {
                _ApiResult = _ReviewServices.FollowOrUnfollow(Obj);
            }


            catch (Exception eX)
            {
                _ApiResult.ErrorCode = -1;
                _ApiResult.HasWarning = true;
                _ApiResult.Message = eX.Message;
                _ReviewServices = null;
            }
            return _ApiResult;
        }

        [Authorize]
        [HttpPost]
        [Produces("Application/Json")]
        public ApiResult<List<FollowersListOut>> FollowersList(GetFollowersList Obj)

        {
            ApiResult<List<FollowersListOut>> _ApiResult = new ApiResult<List<FollowersListOut>>();
            _ReviewServices = new BusinessContracts.Roles.ReviewServices();
            try
            {
                _ApiResult = _ReviewServices.GetFollowers(Obj);
            }


            catch (Exception eX)
            {
                _ApiResult.ErrorCode = -1;
                _ApiResult.HasWarning = true;
                _ApiResult.Message = eX.Message;
                _ReviewServices = null;
            }
            return _ApiResult;
        }

        [Authorize]
        [HttpPost]
        [Produces("Application/Json")]
        public ApiResult<List<FollowSummaryOut>> CustomersFollowSummary(GetFollowSummary Obj)

        {
            ApiResult<List<FollowSummaryOut>> _ApiResult = new ApiResult<List<FollowSummaryOut>>();
            _ReviewServices = new BusinessContracts.Roles.ReviewServices();
            try
            {
                _ApiResult = _ReviewServices.FollowSummary(Obj);
            }


            catch (Exception eX)
            {
                _ApiResult.ErrorCode = -1;
                _ApiResult.HasWarning = true;
                _ApiResult.Message = eX.Message;
                _ReviewServices = null;
            }
            return _ApiResult;
        }




    }
}