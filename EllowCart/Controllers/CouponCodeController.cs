﻿using System;
using EllowCart.Shared;
using Microsoft.AspNetCore.Mvc;
using System.Data;
using EllowCart.DTO.CouponCode.InputModel;

namespace EllowCart.Controllers
{
    [Route("api/[controller]/[Action]")]
    [ApiController]
    public class CouponCodeController : ControllerBase
    {


        EllowCart.BusinessContracts.Roles.CouponCodeServices _CouponCodeServices = null;
        EllowCart.Shared.APiResult _ApiResult = null;



        //adding Coupon
        [HttpPost]
        public APiResult CreateCouponCode(CouponCodeInput _CouponCode)
        {
            _ApiResult = new APiResult();
            _CouponCodeServices = new BusinessContracts.Roles.CouponCodeServices();
            try
            {
                _ApiResult = _CouponCodeServices.AddNewCouponCode(_CouponCode);

            }
            catch (Exception eX)
            {

                _ApiResult.iRet = -1;
                _ApiResult.sError = eX.Message;
                _CouponCodeServices = null;

            }
            return _ApiResult;

        }


        //delete a Coupon
        [HttpPost("{CouponId}")]
        public APiResult DeleteAmodule(int CouponId)
        {
            _ApiResult = new APiResult();
            _CouponCodeServices = new BusinessContracts.Roles.CouponCodeServices();
            try
            {
                _ApiResult = _CouponCodeServices.DeleteACouponCode(CouponId);

            }
            catch (Exception eX)
            {

                _ApiResult.iRet = -1;
                _ApiResult.sError = eX.Message;
                _CouponCodeServices = null;


            }
            return _ApiResult;

        }

        //update a  CouponCode
        [HttpPost("{CouponId}")]
        public APiResult UpdateCouponCode(CouponCodeInput Obj, int CouponId)
        {
            _ApiResult = new APiResult();
            _CouponCodeServices = new BusinessContracts.Roles.CouponCodeServices();
            DataTable dt = new DataTable();


            try
            {
                _ApiResult = _CouponCodeServices.UpdateCouponCode(Obj, CouponId);

            }
            catch (Exception eX)
            {

                _ApiResult.iRet = -1;
                _ApiResult.sError = eX.Message;
                _CouponCodeServices = null;


            }
            return _ApiResult;

        }

        //get all CouponCode
        [HttpPost]
        [Produces("Application/Json")]
        public APiResult GetAllModule()
        {
            _ApiResult = new APiResult();
            _CouponCodeServices = new BusinessContracts.Roles.CouponCodeServices();
            try
            {
                _ApiResult = _CouponCodeServices.GetAllCouponCodes();
            }
            catch (Exception eX)
            {
                _ApiResult.iRet = -1;
                _ApiResult.sError = eX.Message;
                _CouponCodeServices = null;
            }
            return _ApiResult;
        }


        //get one CouponCode 
        [HttpPost("{CouponId}")]
        [Produces("Application/Json")]
        public APiResult GetOneModule(int CouponId)
        {
            _ApiResult = new APiResult();
            _CouponCodeServices = new BusinessContracts.Roles.CouponCodeServices();
            try
            {
                _ApiResult = _CouponCodeServices.GetCouponCodeById(CouponId);
            }
            catch (Exception eX)
            {
                _ApiResult.iRet = -1;
                _ApiResult.sError = eX.Message;
                _CouponCodeServices = null;
            }
            return _ApiResult;
        }


     
        
    }
}