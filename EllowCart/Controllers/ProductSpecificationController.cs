﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using EllowCart.DTO.ProductSpecification.InputModels;
using EllowCart.DTO.ProductSpecification.OutputModels;
using EllowCart.Shared;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace EllowCart.Controllers
{
    [Route("api/[controller]/[Action]")]
    [ApiController]
    public class ProductSpecificationController : ControllerBase
    {
        EllowCart.Shared.APiResult _ApiResult = null;
        EllowCart.BusinessContracts.Roles.ProductSpecificationServices _productSpec = null;

        [HttpPost]
        public APiResult ProductSpecification(ProductSpecificationInput _prospec)
        {
            _ApiResult = new APiResult();
            _productSpec = new BusinessContracts.Roles.ProductSpecificationServices();
            try
            {
                _ApiResult = _productSpec.ProductSpecification(_prospec);
            }
            catch (Exception eX)
            {
                _ApiResult.iRet = -1;
                _ApiResult.sError = eX.Message;
                _productSpec = null;
            }
            return _ApiResult;
        }

        [HttpPost]
        public APiResult UpdateProductSpecification(UpdateProductSpecification _prospec)
        {
            _ApiResult = new APiResult();
            _productSpec = new BusinessContracts.Roles.ProductSpecificationServices();
            try
            {
                _ApiResult = _productSpec.UpdateProductSpecification(_prospec);
            }
            catch (Exception eX)
            {
                _ApiResult.iRet = -1;
                _ApiResult.sError = eX.Message;
                _productSpec = null;
            }
            return _ApiResult;
        }

        [HttpPost]
        public APiResult DeleteProductSpecification(DeleteProductSpecification _prospec)
        {
            _ApiResult = new APiResult();
            _productSpec = new BusinessContracts.Roles.ProductSpecificationServices();
            try
            {
                _ApiResult = _productSpec.DeleteProductSpecification(_prospec);
            }
            catch (Exception eX)
            {
                _ApiResult.iRet = -1;
                _ApiResult.sError = eX.Message;
                _productSpec = null;
            }
            return _ApiResult;
        }

        [HttpPost]
        public APiResult GetAllProductSpecification()
        {
            _ApiResult = new APiResult();
            _productSpec = new BusinessContracts.Roles.ProductSpecificationServices();
            try
            {
                _ApiResult = _productSpec.GetAllProductSpecification();
            }
            catch (Exception eX)
            {
                _ApiResult.iRet = -1;
                _ApiResult.sError = eX.Message;
                _productSpec = null;
            }
            return _ApiResult;
        }

        [HttpPost]
        public APiResult GetOneProductSpecification(DeleteProductSpecification _specification)
        {
            _ApiResult = new APiResult();
            _productSpec = new BusinessContracts.Roles.ProductSpecificationServices();
            try
            {
                _ApiResult = _productSpec.GetoneProductSpecification(_specification);
            }
            catch (Exception eX)
            {
                _ApiResult.iRet = -1;
                _ApiResult.sError = eX.Message;
                _productSpec = null;
            }
            return _ApiResult;
        }

        [HttpPost]
        [Produces("Application/Json")]
        public ApiResult<List<ProductSpecification>> GetProductSpecification(ProductSpecificationGet _product)
        {
            ApiResult<List<ProductSpecification>> _ApiResult = new ApiResult<List<ProductSpecification>>();
            _productSpec = new BusinessContracts.Roles.ProductSpecificationServices();
            try
            {
                _ApiResult = _productSpec.GetProductSpecification(_product);
            }
            catch (Exception eX)
            {
                _ApiResult.ErrorCode = -1;
                _ApiResult.HasWarning = true;
                _ApiResult.Message = eX.Message;
                _productSpec = null;
            }
            return _ApiResult;
        }


        [HttpPost]
        [Produces("Application/Json")]
        public APiResult AddSpecificationAttribute(AddSpecificationAttributeInput _attribute)
        {
            APiResult _ApiResult = new APiResult();
            _productSpec = new BusinessContracts.Roles.ProductSpecificationServices();
            try
            {
                _ApiResult = _productSpec.AddSpecificationAttributes(_attribute);
            }
            catch (Exception eX)
            {
                _ApiResult.iRet = -1;
                _ApiResult.sError = eX.Message;
                _productSpec = null;
            }
            return _ApiResult;
        }

        [HttpPost]
        [Produces("Application/Json")]
        public APiResult UpdateSpecificationAttribute(AddSpecificationAttributeInput _attribute)
        {
            APiResult _ApiResult = new APiResult();
            _productSpec = new BusinessContracts.Roles.ProductSpecificationServices();
            try
            {
                _ApiResult = _productSpec.UpdateSpecificationAttributes(_attribute);
            }
            catch (Exception eX)
            {
                _ApiResult.iRet = -1;
                _ApiResult.sError = eX.Message;
                _productSpec = null;
            }
            return _ApiResult;
        }

        [HttpPost]
        [Produces("Application/Json")]
        public APiResult DeleteSpecificationAttribute(DeleteProductSpecificationAttribute _attribute)
        {
            APiResult _ApiResult = new APiResult();
            _productSpec = new BusinessContracts.Roles.ProductSpecificationServices();
            try
            {
                _ApiResult = _productSpec.DeleteProductSpecificationAttribute(_attribute);
            }
            catch (Exception eX)
            {
                _ApiResult.iRet = -1;
                _ApiResult.sError = eX.Message;
                _productSpec = null;
            }
            return _ApiResult;
        }


        [HttpPost]
        [Produces("Application/Json")]
        public APiResult GetAllSpecificationAttributeAdmin()
        {
            APiResult _ApiResult = new APiResult();
            _productSpec = new BusinessContracts.Roles.ProductSpecificationServices();
            try
            {
                _ApiResult = _productSpec.GetAllProductSpecificationAttribute_Admin();
            }
            catch (Exception eX)
            {
                _ApiResult.iRet = -1;
                _ApiResult.sError = eX.Message;
                _productSpec = null;
            }
            return _ApiResult;
        }

        [HttpPost]
        [Produces("Application/Json")]
        public APiResult GetOneSpecificationAttributeAdmin(GetOneSpecAttribute _getOne)
        {
            APiResult _ApiResult = new APiResult();
            _productSpec = new BusinessContracts.Roles.ProductSpecificationServices();
            try
            {
                _ApiResult = _productSpec.GetOneProductSpecificationAttribute(_getOne);
            }
            catch (Exception eX)
            {
                _ApiResult.iRet = -1;
                _ApiResult.sError = eX.Message;
                _productSpec = null;
            }
            return _ApiResult;
        }

        [HttpPost]
        [Produces("Application/Json")]
        public ApiResult<List<SpecificationattributeOut>> GetSpecificationAttribute(GetSpecificationAttribute _attribute)
        {
            ApiResult<List<SpecificationattributeOut>> _ApiResult = new ApiResult<List<SpecificationattributeOut>>();
            _productSpec = new BusinessContracts.Roles.ProductSpecificationServices();
            try
            {
                _ApiResult = _productSpec.GetSpecificationAttributes(_attribute);
            }
            catch (Exception eX)
            {
                _ApiResult.ErrorCode = -1;
                _ApiResult.HasWarning = true;
                _ApiResult.Message = eX.Message;
                _productSpec = null;
            }
            return _ApiResult;
        }

   

    }
}