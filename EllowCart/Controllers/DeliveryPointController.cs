﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using EllowCart.DTO.DeliveryPoint.Input_Model;
using EllowCart.Shared;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace EllowCart.Controllers
{
    [Route("api/[controller]/[Action]")]
    [ApiController]
    public class DeliveryPointController : ControllerBase
    {
        EllowCart.BusinessContracts.Roles.DeliveryPointServices _deliveryPointServices = null;
        EllowCart.Shared.APiResult _ApiResult = null;

        //adding deliveryPoint
        [HttpPost]
        public APiResult CreateFranchise(DTO.DeliveryPoint.Input_Model.DeliveryPointCreateInput _deliveryPoint)
        {
            _ApiResult = new APiResult();
            _deliveryPointServices = new EllowCart.BusinessContracts.Roles.DeliveryPointServices();
            try
            {
                _ApiResult = _deliveryPointServices.AddNewDeliveryPoint(_deliveryPoint);

            }
            catch (Exception eX)
            {

                _ApiResult.iRet = -1;
                _ApiResult.sError = eX.Message;
                _deliveryPointServices = null;

            }
            return _ApiResult;

        }


        //deleting deliveryPoints
        [HttpPost]
        public APiResult DeleteDeliveryPoint(DeliveryPointInput _input)
        {
            _ApiResult = new APiResult();
            _deliveryPointServices = new BusinessContracts.Roles.DeliveryPointServices();
            try
            {
                _ApiResult = _deliveryPointServices.DeleteDeliveryPoint(_input);

            }
            catch (Exception eX)
            {

                _ApiResult.iRet = -1;
                _ApiResult.sError = eX.Message;
                _deliveryPointServices = null;


            }
            return _ApiResult;

        }
        //updating delivberyPoints 
        [HttpPost]
        public APiResult UpdateDeliveryPoints(DeliveryPointUpdate Obj)
        {
            _ApiResult = new APiResult();
            _deliveryPointServices = new BusinessContracts.Roles.DeliveryPointServices();
            DataTable dt = new DataTable();


            try
            {
                _ApiResult = _deliveryPointServices.UpdateDeliveryPoints(Obj);

            }
            catch (Exception eX)
            {

                _ApiResult.iRet = -1;
                _ApiResult.sError = eX.Message;
                _deliveryPointServices = null;


            }
            return _ApiResult;

        }


        //getting deliveryPoints 
        [HttpPost]
        [Produces("Application/Json")]
        public APiResult GetAllDeliveryPoint()
        {
            _ApiResult = new APiResult();
            _deliveryPointServices = new BusinessContracts.Roles.DeliveryPointServices();
            try
            {
                _ApiResult = _deliveryPointServices.GetAllDeliveryPoints();
            }
            catch (Exception eX)
            {
                _ApiResult.iRet = -1;
                _ApiResult.sError = eX.Message;
                _deliveryPointServices = null;
            }
            return _ApiResult;
        }


        //getting one deliveryPoint 
        [HttpPost]
        [Produces("Application/Json")]
        public APiResult GetOneDeliveryPoint(DeliveryPointInput _input)
        {
            _ApiResult = new APiResult();
            _deliveryPointServices = new BusinessContracts.Roles.DeliveryPointServices();
            try
            {
                _ApiResult = _deliveryPointServices.GetDeliveryPointById(_input);
            }
            catch (Exception eX)
            {
                _ApiResult.iRet = -1;
                _ApiResult.sError = eX.Message;
                _deliveryPointServices = null;
            }
            return _ApiResult;
        }
    }
}