﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using EllowCart.DTO.AddToWishList.InputModels;
using EllowCart.DTO.AddToWishList.OutputModels;
using EllowCart.Shared;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace EllowCart.Controllers
{
    [Route("api/[controller]/[Action]")]
    [ApiController]
    public class AddToWishListController : ControllerBase
    {
        EllowCart.Shared.APiResult _ApiResult = null;
        EllowCart.BusinessContracts.Roles.AddToWishListServices _WishlistService = null;

       // [Authorize]
        [HttpPost]
        [Produces("Application/Json")]
        public ApiResult<List<AddToWishListout>> AddtoWishList(AddToWishList _wishList)
        {
            ApiResult<List<AddToWishListout>> _ApiResult = new ApiResult<List<AddToWishListout>>();
            _WishlistService = new BusinessContracts.Roles.AddToWishListServices();
            try
            {
                _ApiResult = _WishlistService.AddToWishlist(_wishList);
            }
            catch (Exception eX)
            {
                _ApiResult.ErrorCode = -1;
                _ApiResult.HasWarning = true;
                _ApiResult.Message = eX.Message;
                _WishlistService = null;
            }
            return _ApiResult;
        }

      //  [Authorize]
        [HttpPost]
        [Produces("Application/Json")]
        public ApiResult<List<GetWishlistOut>> GetWishlist(SelectWishList _wishList)
        {
            ApiResult<List<GetWishlistOut>> _ApiResult = new ApiResult<List<GetWishlistOut>>();
            _WishlistService = new BusinessContracts.Roles.AddToWishListServices();
            try
            {
                _ApiResult = _WishlistService.GetWishlist(_wishList);
            }
            catch (Exception eX)
            {
                _ApiResult.ErrorCode = -1;
                _ApiResult.HasWarning = true;
                _ApiResult.Message = eX.Message;
                _WishlistService = null;
            }
            return _ApiResult;
        }

     //   [Authorize]
        [HttpPost]
        [Produces("Application/Json")]
        public ApiResult<List<RemoveWishlistout>> RemoveWishList(RemoveWishListInput _wishList)
        {
            ApiResult<List<RemoveWishlistout>> _ApiResult = new ApiResult<List<RemoveWishlistout>>();
            _WishlistService = new BusinessContracts.Roles.AddToWishListServices();
            try
            {
                _ApiResult = _WishlistService.RemoveWishlist(_wishList);
            }
            catch (Exception eX)
            {
                _ApiResult.ErrorCode = -1;
                _ApiResult.HasWarning = true;
                _ApiResult.Message = eX.Message;
                _WishlistService = null;
            }
            return _ApiResult;
        }

    }
}