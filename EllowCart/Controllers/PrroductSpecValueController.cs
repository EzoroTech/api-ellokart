﻿using System;
using EllowCart.Shared;
using Microsoft.AspNetCore.Mvc;
using System.Data;
using EllowCart.DTO.ProductSpecValue.InputModel;

namespace EllowCart.Controllers
{
    [Route("api/[controller]/[Action]")]
    [ApiController]
    public class PrroductSpecValueController : ControllerBase
    {

        EllowCart.BusinessContracts.Roles.ProductSpecValueServices _ProductSpecValue = null;
        EllowCart.Shared.APiResult _ApiResult = null;

        [HttpPost]
        public APiResult CreateProductSpecValue(ProductSpecValueInput _input)
        {
            _ApiResult = new APiResult();
            _ProductSpecValue = new BusinessContracts.Roles.ProductSpecValueServices();
            try
            {
                _ApiResult = _ProductSpecValue.AddNewProductSpecValue(_input);

            }
            catch (Exception eX)
            {

                _ApiResult.iRet = -1;
                _ApiResult.sError = eX.Message;
                _ProductSpecValue = null;

            }
            return _ApiResult;

        }
        [HttpPost("{ProductSpecValueId}")]
        public APiResult DeleteAProductSpecValue(int ProductSpecValueId)
        {
            _ApiResult = new APiResult();
            _ProductSpecValue = new BusinessContracts.Roles.ProductSpecValueServices();
            try
            {
                _ApiResult = _ProductSpecValue.DeleteProductSpecValue(ProductSpecValueId);

            }
            catch (Exception eX)
            {

                _ApiResult.iRet = -1;
                _ApiResult.sError = eX.Message;
                _ProductSpecValue = null;


            }
            return _ApiResult;

        }
        [HttpPost("{ProductSpecValueId}")]
        public APiResult UpdateProductSpecValue(ProductSpecValueInput Obj, int ProductSpecValueId)
        {
            _ApiResult = new APiResult();
            _ProductSpecValue = new BusinessContracts.Roles.ProductSpecValueServices();
            DataTable dt = new DataTable();


            try
            {
                _ApiResult = _ProductSpecValue.UpdateProductSpecValue(Obj, ProductSpecValueId);

            }
            catch (Exception eX)
            {

                _ApiResult.iRet = -1;
                _ApiResult.sError = eX.Message;
                _ProductSpecValue = null;


            }
            return _ApiResult;

        }
        [HttpPost]
        [Produces("Application/Json")]
        public APiResult GetAllProductSpecValues( )
        {
            _ApiResult = new APiResult();
            _ProductSpecValue = new BusinessContracts.Roles.ProductSpecValueServices();
            try
            {
                _ApiResult = _ProductSpecValue.GetAllProductSpecValue();
            }
            catch (Exception eX)
            {
                _ApiResult.iRet = -1;
                _ApiResult.sError = eX.Message;
                _ProductSpecValue = null;
            }
            return _ApiResult;
        }

        //get one ProductSpecValue 
        [HttpPost("{ProductSpecValueId}")]
        [Produces("Application/Json")]
        public APiResult GetOneProductSpecValue(int ProductSpecValueId)
        {
            _ApiResult = new APiResult();
            _ProductSpecValue = new BusinessContracts.Roles.ProductSpecValueServices();
            try
            {
                _ApiResult = _ProductSpecValue.GetProductSpecValueById(ProductSpecValueId);
            }
            catch (Exception eX)
            {
                _ApiResult.iRet = -1;
                _ApiResult.sError = eX.Message;
                _ProductSpecValue = null;
            }
            return _ApiResult;
        }

    }
}