﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using EllowCart.Shared;
using EllowCart.BusinessContracts;
using EllowCart.BusinessPersistantContracts;
using EllowCart.DTO.Roles.InputModels;

namespace EllowCart.Controllers
{
    [Route("api/[controller]/[Action]")]
    [ApiController]
    public class ValuesController : ControllerBase
    {
        EllowCart.BusinessContracts.Roless.ModuleServices _ModuleServices = null;
        EllowCart.Shared.APiResult _ApiResult = null;

        // GET api/values
        [HttpGet]
        public ActionResult<IEnumerable<string>> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public ActionResult<string> Get(int id)
        {
            return "value";
        }
        
        //[HttpPost]
        //public APiResult CreateModule(ModuleInputModel _Module)
        //{
        //    _ApiResult = new APiResult();
        //    _ModuleServices = new BusinessContracts.Roless.ModuleServices();
        //    try
        //    {
        //        _ApiResult = _ModuleServices.AddNewModule(_Module);
                
        //    }
        //    catch(Exception eX)
        //    {
        //        _ApiResult.iRet = -1;
        //        _ApiResult.sError = eX.Message;
        //        _ModuleServices = null;
        //    }
        //    return _ApiResult;

        //}
        [HttpPost]
        public APiResult GetAllModules()
        {
            _ApiResult = new APiResult();
            _ModuleServices = new BusinessContracts.Roless.ModuleServices();
            try
            {
                _ApiResult = _ModuleServices.GetAllModules();
            }
            catch(Exception eX)
            {
                _ApiResult.iRet = -1;
                _ApiResult.sError = eX.Message;
                _ModuleServices = null;
            }
            return _ApiResult;
        }




        // POST api/values
        [HttpPost]
        public void Post([FromBody] string value)
        {
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
