﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using EllowCart.DTO.Service.InputModels;
using EllowCart.DTO.Service.OutputModels;
using EllowCart.Shared;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace EllowCart.Controllers
{
    [Route("api/[controller]/[Action]")]
    [Produces("Application/Json")]
    [ApiController]
    public class ServiceController : ControllerBase
    {
        EllowCart.BusinessContracts.Roles.ServiceServices _ServiceServices = null;

        //  [Authorize]
        [HttpPost]
        [Produces("Application/Json")]
        public ApiResult<List<AddservicesOut>> AddServices(AddServices _addServices)
        {
            ApiResult<List<AddservicesOut>> _ApiResult = new ApiResult<List<AddservicesOut>>();
            _ServiceServices = new BusinessContracts.Roles.ServiceServices();

           try
            {
                _ApiResult = _ServiceServices.AddServices(_addServices);
            }
            catch (Exception eX)
            {
                _ApiResult.ErrorCode = -1;
                _ApiResult.HasWarning = true;   
                _ApiResult.Message = eX.Message;
                
            }
            return _ApiResult;
        }

        [HttpPost]
        [Produces("Application/Json")]
        public ApiResult<List<GetServicesOfABranchOut>> GetServicesOfABranch(GetServiceByBranch _getService)
        {
            ApiResult<List<GetServicesOfABranchOut>> _ApiResult = new ApiResult<List<GetServicesOfABranchOut>>();
            _ServiceServices = new BusinessContracts.Roles.ServiceServices();

            try
            {
                _ApiResult = _ServiceServices.GetServicesOfABranch(_getService);
            }
            catch (Exception eX)
            {
                _ApiResult.ErrorCode = -1;
                _ApiResult.HasWarning = true;
                _ApiResult.Message = eX.Message;

            }
            return _ApiResult;
        }

        [HttpPost]
        [Produces("Application/Json")]
        public APiResult ServiceDetails(ServiceDetailsInput _Input)
        {
            APiResult _ApiResult = new APiResult();
            _ServiceServices = new BusinessContracts.Roles.ServiceServices();

            try
            {
                _ApiResult = _ServiceServices.ServiceDetails(_Input);
            }
            catch (Exception eX)
            {
                _ApiResult.iRet = -1;
                _ApiResult.sError = eX.Message;
               // _ApiResult.Message = eX.Message;

            }
            return _ApiResult;
        }

    }
}