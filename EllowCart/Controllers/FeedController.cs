﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using EllowCart.DTO.Feed.InputModels;
using EllowCart.DTO.Feed.OutputModels;
using EllowCart.Shared;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace EllowCart.Controllers
{
    [Route("api/[controller]/[Action]")]
    [ApiController]
    public class FeedController : ControllerBase
    {
        EllowCart.BusinessContracts.Roles.FeedServices _feedServices = null;

        [Authorize]
        [HttpPost]
        [Produces("Application/Json")]
        public ApiResult<List<AddFeedPostOut>> AddFeedPost(AddFeedPostInput _feedPostInput)
        {
            ApiResult<List<AddFeedPostOut>> _ApiResult = new ApiResult<List<AddFeedPostOut>>();
            _feedServices = new BusinessContracts.Roles.FeedServices();
            try
            {
                _ApiResult = _feedServices.AddFeedPost(_feedPostInput);
            }
            catch (Exception eX)
            {
                _ApiResult.ErrorCode = -1;
                _ApiResult.HasWarning = true;
                _ApiResult.Message = eX.Message;
                _feedServices = null;
            }
            return _ApiResult;
        }

       // [Authorize]
        [HttpPost]
        [Produces("Application/Json")]
        public ApiResult<List<GetFeedPostOut>> GetFeedpostListForPost(GetFeedPostInput _feedPostInput)
        {
            ApiResult<List<GetFeedPostOut>> _ApiResult = new ApiResult<List<GetFeedPostOut>>();
            _feedServices = new BusinessContracts.Roles.FeedServices();
            try
            {
                _ApiResult = _feedServices.GetFeedPostForPost(_feedPostInput);
            }
            catch (Exception eX)
            {
                _ApiResult.ErrorCode = -1;
                _ApiResult.HasWarning = true;
                _ApiResult.Message = eX.Message;
                _feedServices = null;
            }
            return _ApiResult;
        }

      //  [Authorize]
        [HttpPost]
        [Produces("Application/Json")]
        public ApiResult<List<GetFeedPostOut>> GetFeedpostListForEvent(GetFeedPostInput _feedPostInput)
        {
            ApiResult<List<GetFeedPostOut>> _ApiResult = new ApiResult<List<GetFeedPostOut>>();
            _feedServices = new BusinessContracts.Roles.FeedServices();
            try
            {
                _ApiResult = _feedServices.GetFeedPostForEvent(_feedPostInput);
            }
            catch (Exception eX)
            {
                _ApiResult.ErrorCode = -1;
                _ApiResult.HasWarning = true;
                _ApiResult.Message = eX.Message;
                _feedServices = null;
            }
            return _ApiResult;
        }

      //  [Authorize]
        [HttpPost]
        [Produces("Application/Json")]
        public ApiResult<List<GetFeedPostListForOfferImage>> GetFeedpostListForOfferImage(GetFeedPostListForOfferImageInput _feedPostInput)
        {
            ApiResult<List<GetFeedPostListForOfferImage>> _ApiResult = new ApiResult<List<GetFeedPostListForOfferImage>>();
            _feedServices = new BusinessContracts.Roles.FeedServices();
            try
            {
                _ApiResult = _feedServices.GetFeedPostForOfferImages(_feedPostInput);
            }
            catch (Exception eX)
            {
                _ApiResult.ErrorCode = -1;
                _ApiResult.HasWarning = true;
                _ApiResult.Message = eX.Message;
                _feedServices = null;
            }
            return _ApiResult;
        }

      //  [Authorize]
        [HttpPost]
        [Produces("Application/Json")]
        public ApiResult<List<GetfeedPostListForOffer>> GetFeedpostListForOffer(GetFeedPostOfferInput _feedPostInput)
        {
            ApiResult<List<GetfeedPostListForOffer>> _ApiResult = new ApiResult<List<GetfeedPostListForOffer>>();
            _feedServices = new BusinessContracts.Roles.FeedServices();
            try
            {
                _ApiResult = _feedServices.GetFeedPostForOffer(_feedPostInput);
            }
            catch (Exception eX)
            {
                _ApiResult.ErrorCode = -1;
                _ApiResult.HasWarning = true;

                _ApiResult.Message = eX.Message;
                _feedServices = null;
            }
            return _ApiResult;
        }

      //  [Authorize]
        [HttpPost]
        [Produces("Application/Json")]
        public ApiResult<List<updateFeedLikedout>> FeedResponse(UpdateFeedLikedInput _feedInput)
        {
            ApiResult<List<updateFeedLikedout>> _ApiResult = new ApiResult<List<updateFeedLikedout>>();
            _feedServices = new BusinessContracts.Roles.FeedServices();
            try
            {
                _ApiResult = _feedServices.FeedResponse(_feedInput);
            }
            catch (Exception eX)
            {
                _ApiResult.ErrorCode = -1;
                _ApiResult.HasWarning = true;
                _ApiResult.Message = eX.Message;
                _feedServices = null;
            }
            return _ApiResult;
        }

      //  [Authorize]
        [HttpPost]
        [Produces("Application/Json")]
        public ApiResult<List<RemoveFeedPostOut>> RemoveFeedPost(RemoveFeedPost _remove)
        {
            ApiResult<List<RemoveFeedPostOut>> _ApiResult = new ApiResult<List<RemoveFeedPostOut>>();
            _feedServices = new BusinessContracts.Roles.FeedServices();
            try
            {
                _ApiResult = _feedServices.RemoveFeedPost(_remove);
            }
            catch (Exception eX)
            {
                _ApiResult.ErrorCode = -1;
                _ApiResult.HasWarning = true;
                _ApiResult.Message = eX.Message;
                _feedServices = null;
            }
            return _ApiResult;
        }


     //   [Authorize]
        [HttpPost]
        [Produces("Application/Json")]
        public ApiResult<List<GetFeedsCustomerout>> GetFeedsCustomer(GetFeedsCustomer _feeds)
        {
            ApiResult<List<GetFeedsCustomerout>> _ApiResult = new ApiResult<List<GetFeedsCustomerout>>();
            _feedServices = new BusinessContracts.Roles.FeedServices();
            try
            {
                _ApiResult = _feedServices.GetFeedsCustomer(_feeds);
            }
            catch (Exception eX)
            {
                _ApiResult.ErrorCode = -1;
                _ApiResult.HasWarning = true;
                _ApiResult.Message = eX.Message;
                _feedServices = null;
            }
            return _ApiResult;
        }

       // [Authorize]
        [HttpPost]
        [Produces("Application/Json")]
        public APiResult TestFeed(GetFeedPostInput _feedPostInput)
        {
            APiResult _ApiResult = new APiResult();
            _feedServices = new BusinessContracts.Roles.FeedServices();
            try
            {
                _ApiResult = _feedServices.TestFeed(_feedPostInput);
            }
            catch (Exception eX)
            {
                _ApiResult.iRet = -1;
                _ApiResult.sError = eX.Message;
                _ApiResult.sMessage = "Success";
            }
            return _ApiResult;
        }

        [Authorize]
        [HttpPost]
        [Produces("Application/Json")]
        public ApiResult<List<UpdateFeedOut>> UpdateFeeds(UpdateFeed _feeds)
        {
            ApiResult<List<UpdateFeedOut>> _ApiResult = new ApiResult<List<UpdateFeedOut>>();
            _feedServices = new BusinessContracts.Roles.FeedServices();
            try
            {
                _ApiResult = _feedServices.UpdateFeeds(_feeds);
            }
            catch (Exception eX)
            {
                _ApiResult.ErrorCode = -1;
                _ApiResult.HasWarning = true;
                _ApiResult.Message = eX.Message;
                _feedServices = null;
            }
            return _ApiResult;
        }


      //  [Authorize]
        [HttpPost]
        [Produces("Application/Json")]
        public APiResult FeedsBySeller(FeedsBySeller _feedPostInput)
        {
            APiResult _ApiResult = new APiResult();
            _feedServices = new BusinessContracts.Roles.FeedServices();
            try
            {
                _ApiResult = _feedServices.FeedsBySeller(_feedPostInput);
            }
            catch (Exception eX)
            {
                _ApiResult.iRet = -1;
                _ApiResult.sError = eX.Message;
                _ApiResult.sMessage = "Success";
            }
            return _ApiResult;
        }

    }
}