﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using EllowCart.DTO.Franchise.InputModel;
using EllowCart.DTO.Franchise.InputModels;
using EllowCart.Shared;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace EllowCart.Controllers
{
    [Route("api/[controller]/[Action]")]
    [ApiController]
    public class FranchiseController : ControllerBase
    {
        EllowCart.BusinessContracts.Roles.FranchiseMasterServices _FranchiseServices = null;
        EllowCart.Shared.APiResult _ApiResult = null;


        //adding franchise
        [HttpPost]
        public APiResult CreateFranchise(DTO.Franchise.InputModel.FranchiseInputModel _Franchise)
        {
            _ApiResult = new APiResult();
            _FranchiseServices = new EllowCart.BusinessContracts.Roles.FranchiseMasterServices();
            try
            {
                _ApiResult = _FranchiseServices.AddNewFranchiseMaster(_Franchise);

            }
            catch (Exception eX)
            {

                _ApiResult.iRet = -1;
                _ApiResult.sError = eX.Message;
                _FranchiseServices = null;

            }
            return _ApiResult;

        }


        //deleting Franchise
        [HttpPost]
        public APiResult DeleteFranchise(FranchiseModel _frnchise)
        {
            _ApiResult = new APiResult();
            _FranchiseServices = new BusinessContracts.Roles.FranchiseMasterServices();
            try
            {
                _ApiResult = _FranchiseServices.DeleteFranchiseMaster(_frnchise);

            }
            catch (Exception eX)
            {

                _ApiResult.iRet = -1;
                _ApiResult.sError = eX.Message;
                _FranchiseServices = null;


            }
            return _ApiResult;

        }

        //updating Franchise 
        [HttpPost]
        public APiResult UpdateFranchise(FranchiseUpdateModel Obj)
        {
            _ApiResult = new APiResult();
            _FranchiseServices = new BusinessContracts.Roles.FranchiseMasterServices();
            DataTable dt = new DataTable();


            try
            {
                _ApiResult = _FranchiseServices.UpdateFranchiseMaster(Obj);

            }
            catch (Exception eX)
            {

                _ApiResult.iRet = -1;
                _ApiResult.sError = eX.Message;
                _FranchiseServices = null;


            }
            return _ApiResult;

        }


        //getting Franchise 
        [HttpPost]
        [Produces("Application/Json")]
        public APiResult GetAllFranchises()
        {
            _ApiResult = new APiResult();
            _FranchiseServices = new BusinessContracts.Roles.FranchiseMasterServices();
            try
            {
                _ApiResult = _FranchiseServices.GetAllFranchise();
            }
            catch (Exception eX)
            {
                _ApiResult.iRet = -1;
                _ApiResult.sError = eX.Message;
                _FranchiseServices = null;
            }
            return _ApiResult;
        }



        //getting one Franchise 
        [HttpPost]
        [Produces("Application/Json")]
        public APiResult GetOneFranchise(FranchiseModel _input)
        {
            _ApiResult = new APiResult();
            _FranchiseServices = new BusinessContracts.Roles.FranchiseMasterServices();
            try
            {
                _ApiResult = _FranchiseServices.GetFranchiseById(_input);
            }
            catch (Exception eX)
            {
                _ApiResult.iRet = -1;
                _ApiResult.sError = eX.Message;
                _FranchiseServices = null;
            }
            return _ApiResult;
        }

        [HttpPost]
        [Produces("Application/Json")]
        public APiResult GeneratePaymentCode(GeneratePaymentCode _input)
        {
            _ApiResult = new APiResult();
            _FranchiseServices = new BusinessContracts.Roles.FranchiseMasterServices();
            try
            {
                _ApiResult = _FranchiseServices.GeneratePaymentCode(_input);
            }
            catch (Exception eX)
            {
                _ApiResult.iRet = -1;
                _ApiResult.sError = eX.Message;
                _FranchiseServices = null;
            }
            return _ApiResult;
        }


        [HttpPost]
        [Produces("Application/Json")]
        public APiResult SellerDetailsByFranchise(SellerDetailsByFranchise _input)
        {
            _ApiResult = new APiResult();
            _FranchiseServices = new BusinessContracts.Roles.FranchiseMasterServices();
            try
            {
                _ApiResult = _FranchiseServices.SellerDetailsByFranchise(_input);
            }
            catch (Exception eX)
            {
                _ApiResult.iRet = -1;
                _ApiResult.sError = eX.Message;
                _FranchiseServices = null;
            }
            return _ApiResult;
        }


    }
}