﻿using System;
using System.Collections.Generic;
using EllowCart.DTO.ProductCategory.InputModels;
using EllowCart.DTO.ProductCategory.OutputModels;
using EllowCart.Shared;
using Microsoft.AspNetCore.Mvc;

namespace EllowCart.Controllers
{
    [Route("api/[controller]/[Action]")]
    [ApiController]
    public class ProductCategoryController : ControllerBase
    {
        EllowCart.Shared.APiResult _ApiResult = null;
        EllowCart.BusinessContracts.Roles.ProductCategoryServices _productCategoryServices = null;
        [HttpPost]
        public APiResult ProductCategory(Productcategory _Product)
        {
            _ApiResult = new APiResult();
            _productCategoryServices = new BusinessContracts.Roles.ProductCategoryServices();
            try
            {
                _ApiResult = _productCategoryServices.ProductCategory(_Product);
            }
            catch (Exception eX)
            {
                _ApiResult.iRet = -1;
                _ApiResult.sError = eX.Message;
                _productCategoryServices = null;
            }
            return _ApiResult;
        }

        [HttpPost]
        public APiResult GetParentCategory()
        {
            _ApiResult = new APiResult();
            _productCategoryServices = new BusinessContracts.Roles.ProductCategoryServices();
            try
            {
                _ApiResult = _productCategoryServices.GetAllParentCategory();
            }
            catch (Exception eX)
            {
                _ApiResult.iRet = -1;
                _ApiResult.sError = eX.Message;
                _productCategoryServices = null;
            }
            return _ApiResult;
        }
        [HttpPost]
        public APiResult GetAllProductCategory()
        {
            _ApiResult = new APiResult();
            _productCategoryServices = new BusinessContracts.Roles.ProductCategoryServices();
            try
            {
                _ApiResult = _productCategoryServices.GetAllProductCategory();
            }
            catch (Exception eX)
            {
                _ApiResult.iRet = -1;
                _ApiResult.sError = eX.Message;
                _productCategoryServices = null;
            }
            return _ApiResult;
        }

        [HttpPost]
        public APiResult GetAProductCategory(GetOneProductCategory _category)
        {
            _ApiResult = new APiResult();
            _productCategoryServices = new BusinessContracts.Roles.ProductCategoryServices();
            try
            {
                _ApiResult = _productCategoryServices.GetProductCategoryById(_category);
            }
            catch (Exception eX)
            {
                _ApiResult.iRet = -1;
                _ApiResult.sError = eX.Message;
                _productCategoryServices = null;
            }
            return _ApiResult;
        }

        [HttpPost]
        public APiResult DeleteProductCategory(GetOneProductCategory _category)
        {
            _ApiResult = new APiResult();
            _productCategoryServices = new BusinessContracts.Roles.ProductCategoryServices();
            try
            {
                _ApiResult = _productCategoryServices.DeleteAProductCategory(_category);
            }
            catch (Exception eX)
            {
                _ApiResult.iRet = -1;
                _ApiResult.sError = eX.Message;
                _productCategoryServices = null;
            }
            return _ApiResult;
        }


        [HttpPost]
        public APiResult UpdateProductCategory(ProductCategoryInputForAdmin _category)
        {
            _ApiResult = new APiResult();
            _productCategoryServices = new BusinessContracts.Roles.ProductCategoryServices();
            try
            {
                _ApiResult = _productCategoryServices.UpdateProductCategory(_category);
            }
            catch (Exception eX)
            {
                _ApiResult.iRet = -1;
                _ApiResult.sError = eX.Message;
                _productCategoryServices = null;
            }
            return _ApiResult;
        }


        [HttpPost]
        [Produces("Application/Json")]
        public ApiResult<List<ProductCategoryDetails>> GetProductCategory([FromBody]GetProductCategory _getProductCategory )
        {
            ApiResult<List<ProductCategoryDetails>> _ApiResult = new ApiResult<List<ProductCategoryDetails>>();
            _productCategoryServices = new BusinessContracts.Roles.ProductCategoryServices();
            try
            {
                _ApiResult = _productCategoryServices.GetProductCategory(_getProductCategory);
            }
            catch (Exception eX)
            {
                _ApiResult.ErrorCode = -1;
                _ApiResult.HasWarning = true;
                _ApiResult.Message = eX.Message;
                _productCategoryServices = null;
            }
            return _ApiResult;
        }


        [HttpPost]
        [Produces("Application/Json")]
        public ApiResult<List<ChildToNodeCategoryOut>> ChildToNodeCategory(ChildToNodeCategoryinput _childtonode)
        {
            ApiResult<List<ChildToNodeCategoryOut>> _ApiResult = new ApiResult<List<ChildToNodeCategoryOut>>();
            _productCategoryServices = new BusinessContracts.Roles.ProductCategoryServices();
            try
            {
                _ApiResult = _productCategoryServices.ChildToNodeCategory(_childtonode);
            }
            catch (Exception eX)
            {
                _ApiResult.ErrorCode = -1;
                _ApiResult.HasWarning = true;
                _ApiResult.Message = eX.Message;
                _productCategoryServices = null;
            }
            return _ApiResult;
        }



        [HttpPost]
        [Produces("Application/Json")]
        public ApiResult<List<GetProductCategoryList>> GetProductCategoryList()
        {
            ApiResult<List<GetProductCategoryList>> _ApiResult = new ApiResult<List<GetProductCategoryList>>();
            _productCategoryServices = new BusinessContracts.Roles.ProductCategoryServices();
            try
            {
                _ApiResult = _productCategoryServices.GetProductCategoryList();
            }
            catch (Exception eX)
            {
                _ApiResult.ErrorCode = -1;
                _ApiResult.HasWarning = true;
                _ApiResult.Message = eX.Message;
                _productCategoryServices = null;
            }
            return _ApiResult;
        }


        [HttpPost]
        [Produces("Application/Json")]
        public ApiResult<List<GetProductCategoryList>> GetProductCategoryListByBranchId(ProductCategoryLisyByBranch _byBranch)
        {
            ApiResult<List<GetProductCategoryList>> _ApiResult = new ApiResult<List<GetProductCategoryList>>();
            _productCategoryServices = new BusinessContracts.Roles.ProductCategoryServices();
            try
            {
                _ApiResult = _productCategoryServices.GetProductCategoryListByBranchId(_byBranch);
            }
            catch (Exception eX)
            {
                _ApiResult.ErrorCode = -1;
                _ApiResult.HasWarning = true;
                _ApiResult.Message = eX.Message;
                _productCategoryServices = null;
            }
            return _ApiResult;
        }



        [HttpPost]
        [Produces("Application/Json")]
        public ApiResult<List<GetProductCategoryList>> GetProductCategoryListForParent(ProductSubCategForParent _byParent)
        {
            ApiResult<List<GetProductCategoryList>> _ApiResult = new ApiResult<List<GetProductCategoryList>>();
            _productCategoryServices = new BusinessContracts.Roles.ProductCategoryServices();
            try
            {
                _ApiResult = _productCategoryServices.GetProductSubCategForParentCateg(_byParent);
            }
            catch (Exception eX)
            {
                _ApiResult.ErrorCode = -1;
                _ApiResult.HasWarning = true;
                _ApiResult.Message = eX.Message;
                _productCategoryServices = null;
            }
            return _ApiResult;
        }




    }
}