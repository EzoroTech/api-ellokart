﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using EllowCart.DTO.MembershipPlan.InputModels;
using EllowCart.DTO.MembershipPlan.OutputModels;
using EllowCart.Shared;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace EllowCart.Controllers
{
    [Route("api/[controller]/[Action]")]
    [ApiController]
    public class MembershipPlanController : ControllerBase
    {

        EllowCart.BusinessContracts.Roles.MembershipPlanServices _membershipPlanService = null;
        EllowCart.Shared.APiResult _ApiResult = null;

        [HttpPost]
        public APiResult CreateMembershipPlan(MembershipPlanInputModel _membershipPlan)
        {
            _ApiResult = new APiResult();
            _membershipPlanService = new EllowCart.BusinessContracts.Roles.MembershipPlanServices();
            try
            {
                _ApiResult = _membershipPlanService.AddNewMembershipPlan(_membershipPlan);

            }
            catch (Exception eX)
            {

                _ApiResult.iRet = -1;
                _ApiResult.sError = eX.Message;
                _membershipPlan = null;

            }
            return _ApiResult;

        }

        [HttpPost("{MembershipPlanId}")]
        public APiResult DeleteAmembershipPlan(int MembershipPlanId)
        {
            _ApiResult = new APiResult();
            _membershipPlanService = new BusinessContracts.Roles.MembershipPlanServices();
            try
            {
                _ApiResult = _membershipPlanService.DeleteMembershipPlan(MembershipPlanId);

            }
            catch (Exception eX)
            {

                _ApiResult.iRet = -1;
                _ApiResult.sError = eX.Message;
                _membershipPlanService = null;


            }
            return _ApiResult;

        }
        [HttpPost("{MembershipPlanId}")]
        public APiResult UpdateMembershipPlan(MembershipPlanInputModel Obj, int MembershipPlanId)
        {
            _ApiResult = new APiResult();
            _membershipPlanService = new BusinessContracts.Roles.MembershipPlanServices();
            DataTable dt = new DataTable();


            try
            {
                _ApiResult = _membershipPlanService.UpdateMembershipPlan(Obj, MembershipPlanId);

            }
            catch (Exception eX)
            {

                _ApiResult.iRet = -1;
                _ApiResult.sError = eX.Message;
                _membershipPlanService = null;


            }
            return _ApiResult;

        }


        [HttpPost]
        [Produces("Application/Json")]
        public APiResult AllMembershipPlans()
        {
            _ApiResult = new APiResult();
            _membershipPlanService = new BusinessContracts.Roles.MembershipPlanServices();
            try
            {
                _ApiResult = _membershipPlanService.MembershipPlanBySp();
            }
            catch (Exception eX)
            {
                _ApiResult.iRet = -1;
                _ApiResult.sError = eX.Message;
                _membershipPlanService = null;
            }
            return _ApiResult;
        }


        [HttpPost]
        [Produces("Application/Json")]
        public APiResult GetAllMembershipPlan(MembershipPlanOutputModel _obj)
        {
            _ApiResult = new APiResult();
            _membershipPlanService = new BusinessContracts.Roles.MembershipPlanServices();
            try
            {
                _ApiResult = _membershipPlanService.GetAllMembershipPlanBySp(_obj);
            }
            catch (Exception eX)
            {
                _ApiResult.iRet = -1;
                _ApiResult.sError = eX.Message;
                _membershipPlanService = null;
            }
            return _ApiResult;
        }


        [HttpPost]
        [Produces("Application/Json")]
        public ApiResult<List<CheckForMembershipPlanOut>> CheckMembershipPlan(CheckForMembershipPlan _membershipPlan)
        {
            ApiResult<List<CheckForMembershipPlanOut>> _ApiResult = new ApiResult<List<CheckForMembershipPlanOut>>();
            _membershipPlanService = new BusinessContracts.Roles.MembershipPlanServices();
            try
            {
                _ApiResult = _membershipPlanService.CheckMembershipPlan(_membershipPlan);
            }
            catch (Exception eX)
            {
                _ApiResult.ErrorCode = -1;
                _ApiResult.HasWarning = true;
                _ApiResult.Message = eX.Message;
                _membershipPlanService = null;
            }
            return _ApiResult;
        }

        [HttpPost]
        [Produces("Application/Json")]
        public ApiResult<List<GetPlanAddingStatus>> PurchaseMembershipPlan(PurchaseMembershipPlan _input)
        {
            ApiResult<List<GetPlanAddingStatus>> _ApiResult = new ApiResult<List<GetPlanAddingStatus>>();
            _membershipPlanService = new BusinessContracts.Roles.MembershipPlanServices();
            try
            {
                _ApiResult = _membershipPlanService.BuyMembershipPlan(_input);
            }
            catch (Exception Ex)
            {
                _ApiResult.ErrorCode = -1;
                _ApiResult.HasWarning = true;
                _ApiResult.Message = Ex.Message;
                _membershipPlanService = null;
            }
            return _ApiResult;
        }

        [HttpPost]
        [Produces("Application/Json")]
        public ApiResult<List<CustomerPlanOut>> GetAllCustomerMembershipPlan()
        {
            ApiResult<List<CustomerPlanOut>> _ApiResult = new ApiResult<List<CustomerPlanOut>>();
            _membershipPlanService = new BusinessContracts.Roles.MembershipPlanServices();
            try
            {
                _ApiResult = _membershipPlanService.GetAllCustomerMembershipPlan();
            }
            catch (Exception Ex)
            {
                _ApiResult.ErrorCode = -1;
                _ApiResult.HasWarning = true;
                _ApiResult.Message = Ex.Message;
                _membershipPlanService = null;
            }
            return _ApiResult;
        }

    }
}