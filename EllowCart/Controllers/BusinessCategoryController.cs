﻿using System;
using System.Collections.Generic;
using System.Data;
using EllowCart.DTO.BusinessCategory.InputModels;
using EllowCart.DTO.BusinessCategory.OutputModels;
using EllowCart.Shared;
using Microsoft.AspNetCore.Mvc;

namespace EllowCart.Controllers
{
    [Route("api/[controller]/[Action]")]
    [ApiController]
    public class BusinessCategoryController : ControllerBase
    {
        EllowCart.Shared.APiResult _ApiResult = null;
        EllowCart.BusinessContracts.Roles.BusinessCategoryServices _BusinessCategoryServices = null;
        //BusnessCateg get by querybank
        [HttpPost]
        public APiResult CreateBusinessCategory(AddBusinesscategory _businesscategory)
        {
            _ApiResult = new APiResult();
            _BusinessCategoryServices = new BusinessContracts.Roles.BusinessCategoryServices();
            try
            {
                _ApiResult = _BusinessCategoryServices.AddBusinesscategory(_businesscategory);
            }
            catch (Exception eX)
            {
                _ApiResult.iRet = -1;
                _ApiResult.sError = eX.Message;
                _BusinessCategoryServices = null;
            }
            return _ApiResult;
        }

        //BusnessCateg delete by querybank
        [HttpPost]
        public APiResult DeleteBusinessCategory(GetBusinessCategoryAdminInput _input)
        {
            _ApiResult = new APiResult();
            _BusinessCategoryServices = new BusinessContracts.Roles.BusinessCategoryServices();
            try
            {
                _ApiResult = _BusinessCategoryServices.DeleteBusinessCategory(_input);

            }
            catch (Exception eX)
            {

                _ApiResult.iRet = -1;
                _ApiResult.sError = eX.Message;
                _BusinessCategoryServices = null;


            }
            return _ApiResult;

        }

        //BusnessCateg update by querybank
        [HttpPost]
        public APiResult UpdateBusinessCategory(BusinesscategInputforUpdation Obj)
        {
            _ApiResult = new APiResult();
            _BusinessCategoryServices = new BusinessContracts.Roles.BusinessCategoryServices();
            DataTable dt = new DataTable();

            try
            {
                _ApiResult = _BusinessCategoryServices.UpdateBusinessCategory(Obj);

            }
            catch (Exception eX)
            {

                _ApiResult.iRet = -1;
                _ApiResult.sError = eX.Message;
                _BusinessCategoryServices = null;


            }
            return _ApiResult;

        }

        //get all BusinessCategory
        [HttpPost]
        [Produces("Application/Json")]
        public APiResult GetAllBusinessCategoryAdmin()
        {
            _ApiResult = new APiResult();
            _BusinessCategoryServices = new BusinessContracts.Roles.BusinessCategoryServices();
            try
            {
                _ApiResult = _BusinessCategoryServices.GetAllBusinessCategory();
            }
            catch (Exception eX)
            {
                _ApiResult.iRet = -1;
                _ApiResult.sError = eX.Message;
                _BusinessCategoryServices = null;
            }
            return _ApiResult;
        }


        //get one business category 
        [HttpPost]
        [Produces("Application/Json")]
        public APiResult GetOneBusinessCateg(GetBusinessCategoryAdminInput Obj)
        {
            _ApiResult = new APiResult();
            _BusinessCategoryServices = new BusinessContracts.Roles.BusinessCategoryServices();
            try
            {
                _ApiResult = _BusinessCategoryServices.GetBusinessCategoryById(Obj);
            }
            catch (Exception eX)
            {
                _ApiResult.iRet = -1;
                _ApiResult.sError = eX.Message;
                _BusinessCategoryServices = null;
            }
            return _ApiResult;
        }




        [HttpPost]
        [Produces("Application/Json")]
        public ApiResult<List<BusinessCategory>> GetBusinessCategory([FromBody]GetBusinessCategory _getBusiness)
        {
            ApiResult<List<BusinessCategory>> _ApiResult = new ApiResult<List<BusinessCategory>>();
            _BusinessCategoryServices = new BusinessContracts.Roles.BusinessCategoryServices();
            try
            {
                _ApiResult = _BusinessCategoryServices.GetBusinesscategory(_getBusiness);
            }
            catch (Exception eX)
            {
                _ApiResult.ErrorCode = -1;
                _ApiResult.HasWarning = true;
                _ApiResult.Message = eX.Message;
                _BusinessCategoryServices = null;
            }
            return _ApiResult;
        }


        [HttpPost]
        [Produces("Application/Json")]
        public ApiResult<List<ShopByBusinessCategoryOut>> ShopByBusinessCategory(ShopByBusinessCategory _shop)
        {
            ApiResult<List<ShopByBusinessCategoryOut>> _ApiResult = new ApiResult<List<ShopByBusinessCategoryOut>>();
            _BusinessCategoryServices = new BusinessContracts.Roles.BusinessCategoryServices();
            try
            {
                _ApiResult = _BusinessCategoryServices.ShopByBusinessCategory(_shop);
            }
            catch (Exception eX)
            {
                _ApiResult.ErrorCode = -1;
                _ApiResult.HasWarning = true;
                _ApiResult.Message = eX.Message;
                _BusinessCategoryServices = null;
            }
            return _ApiResult;
        }

    }
}