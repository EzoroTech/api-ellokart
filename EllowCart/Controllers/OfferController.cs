﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using EllowCart.DTO.Offer.InputModels;
using EllowCart.DTO.Offer.OutputModels;
using EllowCart.Shared;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace EllowCart.Controllers
{
    [Route("api/[controller]/[Action]")]
    [ApiController]
    public class OfferController : ControllerBase
    {
        EllowCart.BusinessContracts.Roles.OfferServices _OfferServices = null;

        [Authorize]
        [HttpPost]
        [Produces("Application/Json")]
        public ApiResult<List<AddProductOfferout>> AddProductOffer(AddProductOfferInput _offer)
        {
            ApiResult<List<AddProductOfferout>> _ApiResult = new ApiResult<List<AddProductOfferout>>();
            _OfferServices = new BusinessContracts.Roles.OfferServices();
            try
            {
                _ApiResult = _OfferServices.AddProductOffer(_offer);
            }
            catch (Exception eX)
            {
                _ApiResult.ErrorCode = -1;
                _ApiResult.HasWarning = true;
                _ApiResult.Message = eX.Message;
                _OfferServices = null;
            }
            return _ApiResult;
        }

        [Authorize]
        [HttpPost]
        [Produces("Application/Json")]
        public ApiResult<List<AddProductOfferout>> AddProductOfferSpecial(AddProductOfferSpecial _offer)
        {
            ApiResult<List<AddProductOfferout>> _ApiResult = new ApiResult<List<AddProductOfferout>>();
            _OfferServices = new BusinessContracts.Roles.OfferServices();
            try
            {
                _ApiResult = _OfferServices.AddProductOfferSpecial(_offer);
            }
            catch (Exception eX)
            {
                _ApiResult.ErrorCode = -1;
                _ApiResult.HasWarning = true;
                _ApiResult.Message = eX.Message;
                _OfferServices = null;
            }
            return _ApiResult;
        }

        [Authorize]
        [HttpPost]
        [Produces("Application/Json")]
        public ApiResult<List<GetOfferListout>> GetOfferFreeList(GetOfferListInput _offer)
        {
            ApiResult<List<GetOfferListout>> _ApiResult = new ApiResult<List<GetOfferListout>>();
            _OfferServices = new BusinessContracts.Roles.OfferServices();
            try
            {
                _ApiResult = _OfferServices.GetOfferfreeList(_offer);
            }
            catch (Exception eX)
            {
                _ApiResult.ErrorCode = -1;
                _ApiResult.HasWarning = true;
                _ApiResult.Message = eX.Message;
                _OfferServices = null;
            }
            return _ApiResult;
        }

        [Authorize]
        [HttpPost]
        [Produces("Application/Json")]
        public ApiResult<List<OfferspecialOut>> GetOfferSpecialList(GetOfferSpecialInput _offer)
        {
            ApiResult<List<OfferspecialOut>> _ApiResult = new ApiResult<List<OfferspecialOut>>();
            _OfferServices = new BusinessContracts.Roles.OfferServices();
            try
            {
                _ApiResult = _OfferServices.GetOfferSpecialList(_offer);
            }
            catch (Exception eX)
            {
                _ApiResult.ErrorCode = -1;
                _ApiResult.HasWarning = true;
                _ApiResult.Message = eX.Message;
                _OfferServices = null;
            }
            return _ApiResult;
        }
        [Authorize]
        [HttpPost]
        [Produces("Application/Json")]
        public ApiResult<List<GetOffermaster>> GetOfferMaster()
        {
            ApiResult<List<GetOffermaster>> _ApiResult = new ApiResult<List<GetOffermaster>>();
            _OfferServices = new BusinessContracts.Roles.OfferServices();
            try
            {
                _ApiResult = _OfferServices.GetOfferMaster();
            }
            catch (Exception eX)
            {
                _ApiResult.ErrorCode = -1;
                _ApiResult.HasWarning = true;
                _ApiResult.Message = eX.Message;
                _OfferServices = null;
            }
            return _ApiResult;
        }
        [Authorize]
        [HttpPost]
        [Produces("Application/Json")]
        public ApiResult<List<EditOfferDetailsOut>> UpadteOfferSpecialDetails(EditOfferdetailsInput _editOfferdetails)
        {
            ApiResult<List<EditOfferDetailsOut>> _ApiResult = new ApiResult<List<EditOfferDetailsOut>>();
            _OfferServices = new BusinessContracts.Roles.OfferServices();
            try
            {
                _ApiResult = _OfferServices.EditOfferDetails(_editOfferdetails);
            }
            catch (Exception eX)
            {
                _ApiResult.ErrorCode = -1;
                _ApiResult.HasWarning = true;
                _ApiResult.Message = eX.Message;
                _OfferServices = null;
            }
            return _ApiResult;
        }
        [Authorize]
        [HttpPost]
        [Produces("Application/Json")]
        public ApiResult<List<UpdateOfferFreeDetailsout>> UpadteOfferFreeDetails(UpdateOfferFreeDetailsInput _editOfferdetails)
        {
            ApiResult<List<UpdateOfferFreeDetailsout>> _ApiResult = new ApiResult<List<UpdateOfferFreeDetailsout>>();
            _OfferServices = new BusinessContracts.Roles.OfferServices();
            try
            {
                _ApiResult = _OfferServices.UpdateofferFreeDetails(_editOfferdetails);
            }
            catch (Exception eX)
            {
                _ApiResult.ErrorCode = -1;
                _ApiResult.HasWarning = true;
                _ApiResult.Message = eX.Message;
                _OfferServices = null;
            }
            return _ApiResult;
        }

        [Authorize]
        [HttpPost]
        [Produces("Application/Json")]
        public ApiResult<List<RemoveOfferOut>> RemoveOffer(RemoveOffer _remove)
        {
            ApiResult<List<RemoveOfferOut>> _ApiResult = new ApiResult<List<RemoveOfferOut>>();
            _OfferServices = new BusinessContracts.Roles.OfferServices();
            try
            {
                _ApiResult = _OfferServices.RemoveOffer(_remove);
            }
            catch (Exception eX)
            {
                _ApiResult.ErrorCode = -1;
                _ApiResult.HasWarning = true;
                _ApiResult.Message = eX.Message;
                _OfferServices = null;
            }
            return _ApiResult;
        }
      //  [Authorize]
        [HttpPost]
        [Produces("Application/Json")]
        public ApiResult<List<GetOffersOutput>> GetOffers(GetOffersInput _getoffer)
        {
            ApiResult<List<GetOffersOutput>> _ApiResult = new ApiResult<List<GetOffersOutput>>();
            _OfferServices = new BusinessContracts.Roles.OfferServices();
            try
            {
                _ApiResult = _OfferServices.GetOffers(_getoffer);
            }
            catch (Exception eX)
            {
                _ApiResult.ErrorCode = -1;
                _ApiResult.HasWarning = true;
                _ApiResult.Message = eX.Message;
                _OfferServices = null;
            }
            return _ApiResult;
        }

        [Authorize]
        [HttpPost]
        [Produces("Application/Json")]
        public APiResult GetMultipleOffersForTimeline(GetShopByLocation _input)
        {
            APiResult _ApiResult = new APiResult();
            _OfferServices = new BusinessContracts.Roles.OfferServices();
            try
            {
                _ApiResult = _OfferServices.MultipleOffersOfAshop(_input);
            }
            catch (Exception eX)
            {
                _ApiResult.iRet = -1;
                _ApiResult.sError = eX.Message;
                _ApiResult.sMessage = "Success";
            }
            return _ApiResult;
        }

        [Authorize]
        [HttpPost]
        [Produces("Application/Json")]
        public ApiResult<List<OfferTypeSearchOut>> OfferTypeSearch(OfferTypeSearch _search)
        {
            ApiResult<List<OfferTypeSearchOut>> _ApiResult = new ApiResult<List<OfferTypeSearchOut>>();
            _OfferServices = new BusinessContracts.Roles.OfferServices();
            try
            {
                _ApiResult = _OfferServices.OfferTypeBasedSearch(_search);
            }
            catch (Exception Ex)
            {
                _ApiResult.ErrorCode = -1;
                _ApiResult.HasWarning = true;
                _ApiResult.Message = Ex.Message;
                _OfferServices = null;
            }
            return _ApiResult;
        }


       // [Authorize]
        [HttpPost]
        [Produces("Application/Json")]
        public ApiResult<List<GetAllOfferNames>> GetAllOfferNames()
        {
            ApiResult<List<GetAllOfferNames>> _ApiResult = new ApiResult<List<GetAllOfferNames>>();
            _OfferServices = new BusinessContracts.Roles.OfferServices();
            try
            {
                _ApiResult = _OfferServices.GetAllOfferNames();
            }
            catch (Exception Ex)
            {
                _ApiResult.ErrorCode = -1;
                _ApiResult.HasWarning = true;
                _ApiResult.Message = Ex.Message;
                _OfferServices = null;
            }
            return _ApiResult;
        }


    }
}