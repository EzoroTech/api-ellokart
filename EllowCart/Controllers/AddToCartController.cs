﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using EllowCart.DTO.AddToCart.InputModels;
using EllowCart.DTO.AddToCart.OutputModels;
using EllowCart.Shared;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace EllowCart.Controllers
{
    [Route("api/[controller]/[Action]")]
    [ApiController]
    public class AddToCartController : ControllerBase
    {
        EllowCart.Shared.APiResult _ApiResult = null;
        EllowCart.BusinessContracts.Roles.AddToCartServices _AddToCart = null;

       // [Authorize]
        [HttpPost]
        [Produces("Application/Json")]
        public ApiResult<List<AddToCartOut>> AddToCart(AddToCart _addToCart)
        {
            ApiResult<List<AddToCartOut>> _ApiResult = new ApiResult<List<AddToCartOut>>();
            _AddToCart = new BusinessContracts.Roles.AddToCartServices();
            try
            {
                _ApiResult = _AddToCart.AddToCart(_addToCart);
            }
            catch (Exception eX)
            {
                _ApiResult.ErrorCode = -1;
                _ApiResult.HasWarning = true;
                _ApiResult.Message = eX.Message;
                _AddToCart = null;
            }
            return _ApiResult;
        }

     //   [Authorize]
        [HttpPost]
        [Produces("Application/Json")]
        public ApiResult<List<UpdateCartOut>> UpdateCart(UpdateCartInput _Cart)
        {
            ApiResult<List<UpdateCartOut>> _ApiResult = new ApiResult<List<UpdateCartOut>>();
            _AddToCart = new BusinessContracts.Roles.AddToCartServices();
            try
            {
                _ApiResult = _AddToCart.UpdateCart(_Cart);
            }
            catch (Exception eX)
            {
                _ApiResult.ErrorCode = -1;
                _ApiResult.HasWarning = true;
                _ApiResult.Message = eX.Message;
                _AddToCart = null;
            }
            return _ApiResult;
        }

      //  [Authorize]
        [HttpPost]
      [Produces("Application/Json")]
        public ApiResult<List<GetItemsFroCart>> GetItemsFromCart(SelectItems _items)
        {
            ApiResult<List<GetItemsFroCart>> _ApiResult = new ApiResult<List<GetItemsFroCart>>();
            _AddToCart = new BusinessContracts.Roles.AddToCartServices();
            try
            {
                _ApiResult = _AddToCart.GetItemsFromCart(_items);
            }
            catch (Exception eX)
            {
                _ApiResult.ErrorCode = -1;
                _ApiResult.HasWarning = true;
                _ApiResult.Message = eX.Message;
               
            }
            return _ApiResult;
        }

       // [Authorize]
        [HttpPost]
        [Produces("Application/Json")]
        public ApiResult<List<GetCartListOut>> GetCartList(GetCartListInput _cartListInput)
        {
            ApiResult<List<GetCartListOut>> _ApiResult = new ApiResult<List<GetCartListOut>>();
            _AddToCart = new BusinessContracts.Roles.AddToCartServices();
            try
            {
                _ApiResult = _AddToCart.GetCartList(_cartListInput);
            }
            catch (Exception eX)
            {
                _ApiResult.ErrorCode = -1;
                _ApiResult.HasWarning = true;
                _ApiResult.Message = eX.Message;
                _AddToCart = null;
            }
            return _ApiResult;
        }

       // [Authorize]
        [HttpPost]
        [Produces("Application/Json")]
        public ApiResult<List<GetCartcountout>> GetCartCount(GetCartCountInput _cartInput)
        {
            ApiResult<List<GetCartcountout>> _ApiResult = new ApiResult<List<GetCartcountout>>();
            _AddToCart = new BusinessContracts.Roles.AddToCartServices();
            try
            {
                _ApiResult = _AddToCart.GetCartCount(_cartInput);
            }
            catch (Exception eX)
            {
                _ApiResult.ErrorCode = -1;
                _ApiResult.HasWarning = true;
                _ApiResult.Message = eX.Message;
                _AddToCart = null;
            }
            return _ApiResult;
        }

       // [Authorize]
        [HttpPost]
        [Produces("Application/Json")]
        public ApiResult<List<RemovefromCartOut>> RemoveFromCart(RemoveFromCart _cartInput)
        {
            ApiResult<List<RemovefromCartOut>> _ApiResult = new ApiResult<List<RemovefromCartOut>>();
            _AddToCart = new BusinessContracts.Roles.AddToCartServices();
            try
            {
                _ApiResult = _AddToCart.RemoveFromCart(_cartInput);
            }
            catch (Exception eX)
            {
                _ApiResult.ErrorCode = -1;
                _ApiResult.HasWarning = true;
                _ApiResult.Message = eX.Message;
                _AddToCart = null;
            }
            return _ApiResult;
        }
    }
}