﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using EllowCart.DTO.PaymentMethods.InputModels;
using EllowCart.DTO.PaymentMethods.OtputModels;
using EllowCart.Shared;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Razorpay.Api;

namespace EllowCart.Controllers
{
    [Route("api/[controller]/[Action]")]
    [ApiController]
    public class PaymentMethodsController : ControllerBase
    {
        EllowCart.Shared.APiResult _ApiResult = null;
        EllowCart.BusinessContracts.Roles.PaymentMethodServices _paymentservices = null;

        [HttpPost]
        [Produces("Application/Json")]
        public APiResult GetAllPaymentMethods()
        {
            _ApiResult = new APiResult();
            _paymentservices = new BusinessContracts.Roles.PaymentMethodServices();
            try
            {
                _ApiResult = _paymentservices.GetAllPaymentMethods();
            }
            catch (Exception eX)
            {
                _ApiResult.iRet = -1;
                _ApiResult.sError = eX.Message;
                _paymentservices = null;
            }
            return _ApiResult;
        }

        [Authorize]
        [HttpPost]
        [Produces("Application/Json")]
        public ApiResult<List<PaymentDetailsSellerOut>> AddPaymentDetailsBySeller(PaymentDetailsSeller _payment)
        {
            ApiResult<List<PaymentDetailsSellerOut>> _ApiResult = new ApiResult<List<PaymentDetailsSellerOut>>();
            _paymentservices = new BusinessContracts.Roles.PaymentMethodServices();
            try
            {
                _ApiResult = _paymentservices.AddPaymentDetailsBySeller(_payment);
            }
            catch (Exception eX)
            {
                _ApiResult.ErrorCode = -1;
                _ApiResult.HasWarning = true;
                _ApiResult.Message = eX.Message;
                _paymentservices = null;
            }
            return _ApiResult;
        }

        [Authorize]
        [HttpPost]
        [Produces("Application/Json")]
        public ApiResult<List<GetPaymentDetailsOut>> GetPaymentDetailsBySeller(GetPaymentDetailsSeller _payment)
        {
            ApiResult<List<GetPaymentDetailsOut>> _ApiResult = new ApiResult<List<GetPaymentDetailsOut>>();
            _paymentservices = new BusinessContracts.Roles.PaymentMethodServices();
            try
            {
                _ApiResult = _paymentservices.GetPaymentDetailsBySeller(_payment);
            }
            catch (Exception eX)
            {
                _ApiResult.ErrorCode = -1;
                _ApiResult.HasWarning = true;
                _ApiResult.Message = eX.Message;
                _paymentservices = null;
            }
            return _ApiResult;
        }

       // [Authorize]
        [HttpPost]
        [Produces("Application/Json")]
        public ApiResult<List<UserPaidOrNotOut>> CheckUserPaidOrNot(UserPaidOrNot _input)
        {
            ApiResult<List<UserPaidOrNotOut>> _ApiResult = new ApiResult<List<UserPaidOrNotOut>>();
            ApiResult<List<UserPaidOrNot>> _paid = new ApiResult<List<UserPaidOrNot>>();
            _paymentservices = new BusinessContracts.Roles.PaymentMethodServices();
            try
            {
                _paid = _paymentservices.CheckPaymentStatus(_input);

                if (_paid.Count > 0)
                {
                    // RazorpayClient client = new RazorpayClient("rzp_test_H3xJZmKH7FpO2d", "oasRtI7O2TxQhn9zGTLHgwqi");

                    RazorpayClient client = new RazorpayClient("rzp_live_vgm6h3ICl2iA7V", "I4XMrWMsopPTlgJ7qyd0uyXy");
                    Order order = client.Order.Fetch(_paid.Value[0].TransId);
                    var data = order.Attributes;
                    string jsondata = data.ToString();

                    var result = JsonConvert.DeserializeObject<TestModel>(jsondata);

                    _input.TransId = result.status;
                }

                _ApiResult = _paymentservices.CheckUserPaidOrNot(_input);
            }
            catch (Exception eX)
            {
                _ApiResult.ErrorCode = -1;
                _ApiResult.HasWarning = true;
                _ApiResult.Message = eX.Message;
                _paymentservices = null;
            }
            return _ApiResult;
        }

       // [Authorize]
        [HttpPost]
        [Produces("Application/Json")]
        public APiResult RazorPayOrderCreation()
        {
            APiResult _ApiResult = new APiResult();
            _paymentservices = new BusinessContracts.Roles.PaymentMethodServices();
            try
            {
                IList<GetPaymentDetailsOut> _payment = new List<GetPaymentDetailsOut>();

                RazorpayClient client = new RazorpayClient("rzp_test_H3xJZmKH7FpO2d", "oasRtI7O2TxQhn9zGTLHgwqi");
                Dictionary<string, object> options = new Dictionary<string, object>();
                options.Add("amount", 50000); // amount in the smallest currency unit
                options.Add("receipt", "order_rcptid_11");
                options.Add("currency", "INR");
                options.Add("payment_capture", "1");
                Order order = client.Order.Create(options);

                var data = order.Attributes;

                string jsondata = data.ToString();

                var result = JsonConvert.DeserializeObject<TestModel>(jsondata);

                _payment.Add(new GetPaymentDetailsOut { PaymentOptionNoOrId = result.id }) ;

                _ApiResult.sResult = _payment;

            }
            catch (Exception eX)
            {
                _ApiResult.iRet = -1;
                _ApiResult.sError = eX.Message;
                _ApiResult.sMessage = eX.Message;
                _paymentservices = null;
            }
            return _ApiResult;
        }


    }
}