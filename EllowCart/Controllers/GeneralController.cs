﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using Dapper;
using EllowCart.DTO.General.InputModels;
using EllowCart.DTO.General.OutputModels;
using EllowCart.DTO.BusinessSystem.InputModels;
using EllowCart.Shared;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using Razorpay.Api;
using Newtonsoft.Json;
using EllowCart.DTO.PaymentMethods.OtputModels;
using EllowCart.DTO.PaymentMethods.InputModels;

namespace EllowCart.Controllers
{
    [Route("api/[controller]/[Action]")]
    [ApiController]
    public class GeneralController : ControllerBase
    {
        EllowCart.Shared.APiResult _ApiResult = null;
        EllowCart.BusinessContracts.Roles.GeneralServices _GeneralServices = null;
        private IDbConnection DBContext;
        DAL.DBSettings _DBSettings = null;

        // [Authorize]
        [HttpPost]
        [Produces("Application/Json")]
        public ApiResult<List<CheckExisting>> CheckEmailorMobilenumber(Checkemailorphno _check)
        {
            ApiResult<List<CheckExisting>> _ApiResult = new ApiResult<List<CheckExisting>>();
            _GeneralServices = new BusinessContracts.Roles.GeneralServices();
            try
            {
                _ApiResult = _GeneralServices.Checkemailorph(_check);
            }
            catch (Exception eX)
            {
                _ApiResult.Count = 1;
                // _ApiResult.ErrorCode = -1;
                _ApiResult.HasWarning = true;
                _ApiResult.Message = eX.Message;
                _GeneralServices = null;
            }
            return _ApiResult;
        }


        [HttpPost]
        public async Task<APiResult> ManageOTPAsync(ManageOTP _otp)
        {
            _ApiResult = new APiResult();
            _GeneralServices = new BusinessContracts.Roles.GeneralServices();
            try
            {

                //  _ApiResult = _GeneralServices.ManageOTPAsync(_otp);

                OTPMaster appMethods = new OTPMaster();

                return await appMethods.SendSMS(_otp);

            }
            catch (Exception eX)
            {
                _ApiResult.iRet = -1;
                _ApiResult.sError = eX.Message;
                _GeneralServices = null;
            }

            return _ApiResult;
        }

        [HttpPost]
        public async Task<APiResult> OTPMangement(OTPInput _otp)
        {
            _ApiResult = new APiResult();
            _GeneralServices = new BusinessContracts.Roles.GeneralServices();
            try
            {

                OTPMaster appMethods = new OTPMaster();

                return await appMethods.OTPGenerate(_otp);

            }
            catch (Exception eX)
            {
                _ApiResult.iRet = -1;
                _ApiResult.sError = eX.Message;
                _GeneralServices = null;
            }

            return _ApiResult;
        }


        [Authorize]
        [HttpPost]
        [Produces("Application/Json")]
        public ApiResult<List<GetTaxOut>> GetTax_Seller()
        {
            ApiResult<List<GetTaxOut>> _ApiResult = new ApiResult<List<GetTaxOut>>();
            _GeneralServices = new BusinessContracts.Roles.GeneralServices();
            try
            {
                _ApiResult = _GeneralServices.GetTax();
            }
            catch (Exception eX)
            {
                _ApiResult.ErrorCode = -1;
                _ApiResult.HasWarning = true;
                _ApiResult.Message = eX.Message;
                _GeneralServices = null;
            }
            return _ApiResult;
        }

        [Authorize]
        [HttpPost]
        [Produces("Application/Json")]
        public ApiResult<List<GetBrandOut>> GetBrand()
        {
            ApiResult<List<GetBrandOut>> _ApiResult = new ApiResult<List<GetBrandOut>>();
            _GeneralServices = new BusinessContracts.Roles.GeneralServices();
            try
            {
                _ApiResult = _GeneralServices.GetBrand();
            }
            catch (Exception eX)
            {
                _ApiResult.ErrorCode = -1;
                _ApiResult.HasWarning = true;
                _ApiResult.Message = eX.Message;
                _GeneralServices = null;
            }
            return _ApiResult;
        }

        // [Authorize]
        [HttpPost]
        [Produces("Application/Json")]
        public ApiResult<List<GetSecurityQuestion>> GetSecurityQuestions()
        {
            ApiResult<List<GetSecurityQuestion>> _ApiResult = new ApiResult<List<GetSecurityQuestion>>();
            _GeneralServices = new BusinessContracts.Roles.GeneralServices();
            try
            {
                _ApiResult = _GeneralServices.GetSecurityQuestion();
            }
            catch (Exception eX)
            {
                _ApiResult.ErrorCode = -1;
                _ApiResult.HasWarning = true;
                _ApiResult.Message = eX.Message;
                _GeneralServices = null;
            }
            return _ApiResult;
        }

        //  [Authorize]
        [HttpPost]
        [Produces("Application/Json")]
        public ApiResult<List<SecurityQustnAnsCheckOut>> CheckForSecurityQustnAns(SecurityQustnAnsCheck _input)
        {
            ApiResult<List<SecurityQustnAnsCheckOut>> _ApiResult = new ApiResult<List<SecurityQustnAnsCheckOut>>();
            _GeneralServices = new BusinessContracts.Roles.GeneralServices();
            try
            {
                _ApiResult = _GeneralServices.SecurityQustnAnsCheck(_input);
            }
            catch (Exception eX)
            {
                _ApiResult.ErrorCode = -1;
                _ApiResult.HasWarning = true;
                _ApiResult.Message = eX.Message;
                _GeneralServices = null;
            }
            return _ApiResult;
        }

        // [Authorize]
        [HttpPost]
        [Produces("Application/Json")]
        public APiResult AccountDeactivate(UserBusiness _BusinessOut)
        {
            _ApiResult = new APiResult();
            _GeneralServices = new BusinessContracts.Roles.GeneralServices();
            try
            {
                _ApiResult = _GeneralServices.DeactivateABusiness(_BusinessOut);
            }
            catch (Exception eX)
            {
                _ApiResult.iRet = -1;
                _ApiResult.sError = eX.Message;
                _GeneralServices = null;
            }
            return _ApiResult;
        }

        [HttpPost]
        [Produces("Application/Json")]
        public ApiResult<List<GetAllproductList>> GetAllProductsList(AllProductList _input)
        {
            ApiResult<List<GetAllproductList>> _ApiResult = new ApiResult<List<GetAllproductList>>();
            _GeneralServices = new BusinessContracts.Roles.GeneralServices();
            try
            {
                _ApiResult = _GeneralServices.GetAllProductsList(_input);
            }
            catch (Exception eX)
            {
                _ApiResult.Count = -1;
                _ApiResult.HasWarning = true;
                _ApiResult.Message = eX.Message;
                _GeneralServices = null;
            }
            return _ApiResult;
        }

        [HttpPost]
        [Produces("Application/Json")]
        public ApiResult<List<RazorPayInputs>> GetPaymentGateways(GetPaymentForPlanInput _input)
        {
            ApiResult<List<RazorPayInputs>> _ApiResult = new ApiResult<List<RazorPayInputs>>();
            APiResult _api = new APiResult();
            _GeneralServices = new BusinessContracts.Roles.GeneralServices();

            try
            {
                _ApiResult = _GeneralServices.GetPaymentGateways(_input);

                  RazorpayClient client = new RazorpayClient("rzp_live_vgm6h3ICl2iA7V", "I4XMrWMsopPTlgJ7qyd0uyXy");

                //  RazorpayClient client = new RazorpayClient("rzp_test_H3xJZmKH7FpO2d", "oasRtI7O2TxQhn9zGTLHgwqi");
                float Amount = ((_ApiResult.Value[0].Amount) * 100);

                Dictionary<string, object> options = new Dictionary<string, object>();
                options.Add("amount", Amount); // amount in the smallest currency unit
                options.Add("receipt", "order_rcptid_11");
                options.Add("currency", "INR");
                options.Add("payment_capture", "1");
                Order order = client.Order.Create(options);

                var data = order.Attributes;

                string jsondata = data.ToString();

                var result = JsonConvert.DeserializeObject<TestModel>(jsondata);

                UserPaidOrNot _paid = new UserPaidOrNot();
                _paid.UserId = _input.UserId;
                _paid.Flag = "UPGRADE_PLAN";
                _paid.PlanId = _input.Others;
                _paid.TransId = result.id;
                _paid.GatewayId = "RZR";

                _api = _GeneralServices.InsertGatewayIdToTable(_paid);

                _ApiResult.Value[0].OrderId = result.id;
            }
            catch (Exception eX)
            {
                _ApiResult.Count = -1;
                _ApiResult.HasWarning = true;
                _ApiResult.Message = eX.Message;
                _GeneralServices = null;
            }
            return _ApiResult;
        }

    }
}