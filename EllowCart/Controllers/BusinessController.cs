﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using EllowCart.DTO.Business.InputModels;
using EllowCart.DTO.Business.OutputModels;
using EllowCart.Shared;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Data;
using Microsoft.AspNetCore.Authorization;

namespace EllowCart.Controllers
{
    [Route("api/[controller]/[Action]")]
    [Produces("Application/Json")]
    [ApiController]
    public class BusinessController : ControllerBase
    {
        EllowCart.Shared.APiResult _ApiResult = null;
        EllowCart.BusinessContracts.Roles.BusinessServices _businessServices = null;

      
        [HttpPost]
        public APiResult CreateBusinessType(BusinessType _BussinessType)
        {
            _ApiResult = new APiResult();
            _businessServices = new BusinessContracts.Roles.BusinessServices();

            try
            {
                _ApiResult = _businessServices.AddBusinessType(_BussinessType);
            }
            catch (Exception eX)
            {
                _ApiResult.iRet = -1;
                _ApiResult.sError = eX.Message;
                _businessServices = null;
            }
            return _ApiResult;
        }
        [HttpPost]
        public APiResult DeleteBusinessType(GetBusinessType _businessOut)
        {
            _ApiResult = new APiResult();
            _businessServices = new BusinessContracts.Roles.BusinessServices();
            try
            {
                _ApiResult = _businessServices.DeleteBusinessType(_businessOut);

            }
            catch (Exception eX)
            {

                _ApiResult.iRet = -1;
                _ApiResult.sError = eX.Message;
                _businessServices = null;


            }
            return _ApiResult;

        }

        [HttpPost]
        public APiResult UpdateBusinessType(BusinessTypeForUpdation _BusinessUpd)
        {
            _ApiResult = new APiResult();
            _businessServices = new BusinessContracts.Roles.BusinessServices();
            DataTable dt = new DataTable();


            try
            {
                _ApiResult = _businessServices.UpdateBusinessType(_BusinessUpd);

            }
            catch (Exception eX)
            {

                _ApiResult.iRet = -1;
                _ApiResult.sError = eX.Message;
                _businessServices = null;


            }
            return _ApiResult;

        }
        //get all businessType
        [HttpPost]
        [Produces("Application/Json")]
        public APiResult GetAllBusinessType()
        {
            _ApiResult = new APiResult();
            _businessServices = new BusinessContracts.Roles.BusinessServices();
            try
            {
                _ApiResult = _businessServices.GetAllBusinessType();
            }
            catch (Exception eX)
            {
                _ApiResult.iRet = -1;
                _ApiResult.sError = eX.Message;
                _businessServices = null;
            }
            return _ApiResult;
        }


        //get one businessType 
        [HttpPost]
        [Produces("Application/Json")]
        public APiResult GetOneBusinessType(GetBusinessType _businessOut)
        {
            _ApiResult = new APiResult();
            _businessServices = new BusinessContracts.Roles.BusinessServices();
            try
            {
                _ApiResult = _businessServices.GetBusinessTypeById(_businessOut);
            }
            catch (Exception eX)
            {
                _ApiResult.iRet = -1;
                _ApiResult.sError = eX.Message;
                _businessServices = null;
            }
            return _ApiResult;
        }

        [Authorize]
        [HttpPost]
        [Produces("Application/Json")]
        public ApiResult<List<Business_type>> GetBusinessType([FromBody]GetBusinessType _BusinessType)
        {
            ApiResult<List<Business_type>> _ApiResult = new ApiResult<List<Business_type>>();
            _businessServices = new BusinessContracts.Roles.BusinessServices();
            try
            {
                _ApiResult = _businessServices.GetBusinessType(_BusinessType);
            }
            catch (Exception eX)
            {
                _ApiResult.ErrorCode = -1;
                _ApiResult.HasWarning = true;
                _ApiResult.Message = eX.Message;
                _businessServices = null;
            }
            return _ApiResult;
        }

        [Authorize]
        [HttpPost]
        [Produces("Application/Json")]
        public APiResult GetBusinessType_Admin(GetBusinessType _businessType)
        {
            _ApiResult = new APiResult();
            _businessServices = new BusinessContracts.Roles.BusinessServices();
            try
            {
                _ApiResult = _businessServices.GetBusinessType_Admin(_businessType);
            }
            catch (Exception eX)
            {
                _ApiResult.iRet = -1;
                _ApiResult.sError = eX.Message;
                _businessServices = null;
            }
            return _ApiResult;
        }

       // [Authorize]
        [HttpPost]
        [Produces("Application/Json")]
        public ApiResult<List<CreateBusinessOut>> CreateBusiness(CreateBusiness _business)
        {
            ApiResult<List<CreateBusinessOut>> _ApiResult = new ApiResult<List<CreateBusinessOut>>();
            _businessServices = new BusinessContracts.Roles.BusinessServices();
            try
            {
                _ApiResult = _businessServices.CreateBusiness(_business);

            }
            catch (Exception eX)
            {
                _ApiResult.ErrorCode = -1;
                _ApiResult.HasWarning = true;
                _ApiResult.Message = eX.Message;
                _businessServices = null;
            }
            return _ApiResult;

        }

        [Authorize]
        [HttpPost]
        [Produces("Application/Json")]
        public ApiResult<List<LoadBusiness>> GetBusinessDetails(DTO.Business.InputModels.BusinessDetails _details)
        {
            ApiResult<List<LoadBusiness>> _ApiResult = new ApiResult<List<LoadBusiness>>();
            _businessServices = new BusinessContracts.Roles.BusinessServices();
            try
            {
                _ApiResult = _businessServices.LoadBusinessDetails(_details);

            }
            catch (Exception eX)
            {
                _ApiResult.ErrorCode = -1;
                _ApiResult.HasWarning = true;
                _ApiResult.Message = eX.Message;
                _businessServices = null;
            }
            return _ApiResult;
        }

        [Authorize]
        [HttpPost]
        [Produces("Application/Json")]
        public ApiResult<List<GetBusinessList>> GetBusinessList(GetBusinessListInput _getBusiness)
        {
            ApiResult<List<GetBusinessList>> _ApiResult = new ApiResult<List<GetBusinessList>>();
            _businessServices = new BusinessContracts.Roles.BusinessServices();
            try
            {
                _ApiResult = _businessServices.GetBusinesslist(_getBusiness);

            }
            catch (Exception eX)
            {
                _ApiResult.ErrorCode = -1;
                _ApiResult.HasWarning = true;
                _ApiResult.Message = eX.Message;
                _businessServices = null;
            }
            return _ApiResult;
        }

       // [Authorize]
        [HttpPost]
        [Produces("Application/Json")]
        public ApiResult<List<AddBusinessAddressOut>> AddBusinessAddress(AddBusinessAddress _getBusiness)
        {
            ApiResult<List<AddBusinessAddressOut>> _ApiResult = new ApiResult<List<AddBusinessAddressOut>>();
            _businessServices = new BusinessContracts.Roles.BusinessServices();
            try
            {
                _ApiResult = _businessServices.AddBusinessAddress(_getBusiness);

            }
            catch (Exception eX)
            {
                _ApiResult.ErrorCode = -1;
                _ApiResult.HasWarning = true;
                _ApiResult.Message = eX.Message;
                _businessServices = null;
            }
            return _ApiResult;
        }

    } 
}