﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using EllowCart.DTO.Branch.InputModels;
using EllowCart.DTO.Branch.OutputModels;
using EllowCart.Shared;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace EllowCart.Controllers
{
    [Route("api/[controller]/[Action]")]
    [ApiController]
    public class BranchController : ControllerBase
    {
        
        EllowCart.BusinessContracts.Roles.BranchServices _BranchServices = null;


        [Authorize]
        [HttpPost]
        [Produces("Application/Json")]
        public ApiResult<List<CreateBranchOut>> CreateBranch(BranchRegisteration _branch)
        {
            ApiResult<List<CreateBranchOut>> _ApiResult = new ApiResult<List<CreateBranchOut>>();
            _BranchServices = new BusinessContracts.Roles.BranchServices();
            try
            {
                _ApiResult = _BranchServices.BranchRegisteration(_branch);
            }
            catch (Exception eX)
            {
                _ApiResult.ErrorCode = -1;
                _ApiResult.HasWarning = true;
                _ApiResult.Message = eX.Message;
                _BranchServices = null;
            }
            return _ApiResult;
        }

        [Authorize]
        [HttpPost]
        [Produces("Application/Json")]
        public ApiResult<List<UpdateBranchOut>> UpdateBranch(UpdateBranch _update)
        {
            ApiResult<List<UpdateBranchOut>> _ApiResult = new ApiResult<List<UpdateBranchOut>>();
            _BranchServices = new BusinessContracts.Roles.BranchServices();
            try
            {
                _ApiResult = _BranchServices.UpdateBranch(_update);
            }
            catch (Exception eX)
            {
                _ApiResult.ErrorCode = -1;
                _ApiResult.HasWarning = true;
                _ApiResult.Message = eX.Message;
                _BranchServices = null;
            }
            return _ApiResult;
        }

        [Authorize]
        [HttpPost]
        [Produces("Application/Json")]
        public ApiResult<List<UpdateBranchOut>> UpdateBranchAddress(UpdateBranchAddress _address)
        {
            ApiResult<List<UpdateBranchOut>> _ApiResult = new ApiResult<List<UpdateBranchOut>>();
            _BranchServices = new BusinessContracts.Roles.BranchServices();
            try
            {
                _ApiResult = _BranchServices.UpdateBranchAddress(_address);
            }
            catch (Exception eX)
            {
                _ApiResult.ErrorCode = -1;
                _ApiResult.HasWarning = true;
                _ApiResult.Message = eX.Message;
                _BranchServices = null;
            }
            return _ApiResult;
        }

        [Authorize]
        [HttpPost]
        [Produces("Application/Json")]
        public ApiResult<List<UpdateBranchOut>> UpdateBranchBankDetails(UpdateBankBranchDetails _bank)
        {
            ApiResult<List<UpdateBranchOut>> _ApiResult = new ApiResult<List<UpdateBranchOut>>();
            _BranchServices = new BusinessContracts.Roles.BranchServices();
            try
            {
                _ApiResult = _BranchServices.UpdateBranchBankDetails(_bank);
            }
            catch (Exception eX)
            {
                _ApiResult.ErrorCode = -1;
                _ApiResult.HasWarning = true;
                _ApiResult.Message = eX.Message;
                _BranchServices = null;
            }
            return _ApiResult;
        }

        [Authorize]
        [HttpPost]
        [Produces("Application/Json")]
        public ApiResult<List<GetBranches>> GetBranches(GetBranchesInput _branch)
        {
            ApiResult<List<GetBranches>> _ApiResult = new ApiResult<List<GetBranches>>();
            _BranchServices = new BusinessContracts.Roles.BranchServices();
            try
            {
                _ApiResult = _BranchServices.GetBranches(_branch);
            }
            catch (Exception eX)
            {
                _ApiResult.ErrorCode = -1;
                _ApiResult.HasWarning = true;
                _ApiResult.Message = eX.Message;
                _BranchServices = null;
            }
            return _ApiResult;
        }

        
        [HttpPost]
        [Produces("Application/Json")]
        public ApiResult<List<BranchDetailsOut>> GetBrancheDetails(GetBranchDetailsInput _input)
        {
            ApiResult<List<BranchDetailsOut>> _ApiResult = new ApiResult<List<BranchDetailsOut>>();
            _BranchServices = new BusinessContracts.Roles.BranchServices();
            try
            {
                _ApiResult = _BranchServices.GetBrancheDetails(_input);
            }
            catch (Exception eX)
            {
                _ApiResult.ErrorCode = -1;
                _ApiResult.HasWarning = true;
                _ApiResult.Message = eX.Message;
                _BranchServices = null;
            }
            return _ApiResult;
        }

        [Authorize]
        [HttpPost]
        [Produces("Application/Json")]
        public ApiResult<List<addBranchPhotoOut>> AddBranchPhoto(AddBranchPhotoInput _input)
        {
            ApiResult<List<addBranchPhotoOut>> _ApiResult = new ApiResult<List<addBranchPhotoOut>>();
            _BranchServices = new BusinessContracts.Roles.BranchServices();
            try
            {
                _ApiResult = _BranchServices.AddBranchPhoto(_input);
            }
            catch (Exception eX)
            {
                _ApiResult.ErrorCode = -1;
                _ApiResult.HasWarning = true;
                _ApiResult.Message = eX.Message;
                _BranchServices = null;
            }
            return _ApiResult;
        }

        [Authorize]
        [HttpPost]
        [Produces("Application/Json")]
        public ApiResult<List<GetBranchPhotoOut>> GetBranchPhoto(GetBranchPhotoInput _input)
        {
            ApiResult<List<GetBranchPhotoOut>> _ApiResult = new ApiResult<List<GetBranchPhotoOut>>();
            _BranchServices = new BusinessContracts.Roles.BranchServices();
            try
            {
                _ApiResult = _BranchServices.GetBranchPhoto(_input);
            }
            catch (Exception eX)
            {
                _ApiResult.ErrorCode = -1;
                _ApiResult.HasWarning = true;
                _ApiResult.Message = eX.Message;
                _BranchServices = null;
            }
            return _ApiResult;
        }


        [Authorize]
        [HttpPost]
        [Produces("Application/Json")]
        public ApiResult<List<RemoveBranchPhotoOut>> RemoveBranchPhoto(RemoveBranchPhoto _input)
        {
            ApiResult<List<RemoveBranchPhotoOut>> _ApiResult = new ApiResult<List<RemoveBranchPhotoOut>>();
            _BranchServices = new BusinessContracts.Roles.BranchServices();
            try
            {
                _ApiResult = _BranchServices.RemoveBranchPhoto(_input);
            }
            catch (Exception eX)
            {
                _ApiResult.ErrorCode = -1;
                _ApiResult.HasWarning = true;
                _ApiResult.Message = eX.Message;
                _BranchServices = null;
            }
            return _ApiResult;
        }

        [Authorize]
        [HttpPost]
        [Produces("Application/Json")]
        public ApiResult<List<RemoveBranchOut>> RemoveBranch(RemoveBranch _input)
        {
            ApiResult<List<RemoveBranchOut>> _ApiResult = new ApiResult<List<RemoveBranchOut>>();
            _BranchServices = new BusinessContracts.Roles.BranchServices();
            try
            {
                _ApiResult = _BranchServices.RemoveBranch(_input);
            }
            catch (Exception eX)
            {
                _ApiResult.ErrorCode = -1;
                _ApiResult.HasWarning = true;
                _ApiResult.Message = eX.Message;
                _BranchServices = null;
            }
            return _ApiResult;
        }

        [Authorize]
        [HttpPost]
        [Produces("Application/Json")]
        public ApiResult<List<SetDeafaultBranchOut>> SetDeafultBranch(SetDeafultBranchInput _setDeafult)
        {
            ApiResult<List<SetDeafaultBranchOut>> _ApiResult = new ApiResult<List<SetDeafaultBranchOut>>();
            _BranchServices = new BusinessContracts.Roles.BranchServices();
            try
            {
                _ApiResult = _BranchServices.DefaultBranch(_setDeafult);
            }
            catch (Exception eX)
            {
                _ApiResult.ErrorCode = -1;
                _ApiResult.HasWarning = true;
                _ApiResult.Message = eX.Message;
                _BranchServices = null;
            }
            return _ApiResult;
        }


        [Authorize]
        [HttpPost]
        [Produces("Application/Json")]
        public ApiResult<List<GetBranchLogoOut>> GetBranchLogo(GetBranchLogo _logo)
        {
            ApiResult<List<GetBranchLogoOut>> _ApiResult = new ApiResult<List<GetBranchLogoOut>>();
            _BranchServices = new BusinessContracts.Roles.BranchServices();
            try
            {
                _ApiResult = _BranchServices.GetBranchLogo(_logo);
            }
            catch (Exception eX)
            {
                _ApiResult.ErrorCode = -1;
                _ApiResult.HasWarning = true;
                _ApiResult.Message = eX.Message;
                _BranchServices = null;
            }
            return _ApiResult;
        }

       

    }
}