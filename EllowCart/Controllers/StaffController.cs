﻿using System;
using EllowCart.DTO.Staff.InputModels;
using EllowCart.Shared;
using Microsoft.AspNetCore.Mvc;

namespace EllowCart.Controllers
{
    [Route("api/[controller]/[Action]")]
    [ApiController]
    public class StaffController : ControllerBase
    {
        EllowCart.Shared.APiResult _ApiResult = null;
        EllowCart.BusinessContracts.Roles.StaffServices _staffservices = null;

        [HttpPost]
        public APiResult AddStaff(StaffInputModel _staff)
        {

            _ApiResult = new APiResult();
            _staffservices = new BusinessContracts.Roles.StaffServices();
            try
            {
                _ApiResult = _staffservices.StaffProcess(_staff);
            }
            catch (Exception eX)
            {
                _ApiResult.iRet = -1;
                _ApiResult.sError = eX.Message;
                _staffservices = null;
            }

            return _ApiResult;
        }

        [HttpPost]
        public APiResult GetAllStaff(StaffSelectModel _input)
        {

            _ApiResult = new APiResult();
            _staffservices = new BusinessContracts.Roles.StaffServices();
            try
            {
                _ApiResult = _staffservices.GetStaff(_input);
            }
            catch (Exception eX)
            {
                _ApiResult.iRet = -1;
                _ApiResult.sError = eX.Message;
                _staffservices = null;
            }
            return _ApiResult;
        }



        [HttpPost]
        public APiResult AddStaffDetails(StaffDetailsInputModel _staffDetails)
        {

            _ApiResult = new APiResult();
            _staffservices = new BusinessContracts.Roles.StaffServices();
            try
            {
                _ApiResult = _staffservices.StaffDetailsProcess(_staffDetails);
            }
            catch (Exception eX)
            {
                _ApiResult.iRet = -1;
                _ApiResult.sError = eX.Message;
                _staffservices = null;
            }

            return _ApiResult;
        }

        [HttpPost]
        public APiResult GetAllStaffDetails(StaffSelectModel _input)
        {

            _ApiResult = new APiResult();
            _staffservices = new BusinessContracts.Roles.StaffServices();
            try
            {
                _ApiResult = _staffservices.GetStaffDetails(_input);
            }
            catch (Exception eX)
            {
                _ApiResult.iRet = -1;
                _ApiResult.sError = eX.Message;
                _staffservices = null;
            }
            return _ApiResult;
        }


        [HttpPost]
        public APiResult ForgotPassword(StaffCodeSelectModel _input)
        {

            _ApiResult = new APiResult();
            _staffservices = new BusinessContracts.Roles.StaffServices();
            try
            {
                _ApiResult = _staffservices.GetStaffCode(_input);
            }
            catch (Exception eX)
            {
                _ApiResult.iRet = -1;
                _ApiResult.sError = eX.Message;
                _staffservices = null;
            }
            return _ApiResult;
        }


        [HttpPost]
        public APiResult StaffSignIn(GetStaffCode _getUser)
        {
            _ApiResult = new APiResult();
            _staffservices = new BusinessContracts.Roles.StaffServices();
            try
            {
                _ApiResult = _staffservices.StaffSignIn(_getUser);
            }
            catch (Exception Ex)
            {
                _ApiResult.iRet = -1;
                _ApiResult.sError = Ex.Message;
                _staffservices = null;
            }
            return _ApiResult;
        }

        [HttpPost]
        public APiResult EzoroStaffRegisteration(EzoroStaffRegisteration _Staff)
        {
            _ApiResult = new APiResult();
            _staffservices = new BusinessContracts.Roles.StaffServices();
            try
            {
                _ApiResult = _staffservices.EzoroStaffRegisteration(_Staff);
            }
            catch (Exception Ex)
            {
                _ApiResult.iRet = -1;
                _ApiResult.sError = Ex.Message;
                _staffservices = null;
            }
            return _ApiResult;
        }

        [HttpPost]
        public APiResult UpdateEzoroStaff(UpdateEzoroStaff _Staff)
        {
            _ApiResult = new APiResult();
            _staffservices = new BusinessContracts.Roles.StaffServices();
            try
            {
                _ApiResult = _staffservices.UpdateEzoroStaff(_Staff);
            }
            catch (Exception Ex)
            {
                _ApiResult.iRet = -1;
                _ApiResult.sError = Ex.Message;
                _staffservices = null;
            }
            return _ApiResult;
        }


        [HttpPost]
        public APiResult DeleteEzoroStaff(GetOneEzorostaff _Staff)
        {
            _ApiResult = new APiResult();
            _staffservices = new BusinessContracts.Roles.StaffServices();
            try
            {
                _ApiResult = _staffservices.DeleteEzoroStaff(_Staff);
            }
            catch (Exception Ex)
            {
                _ApiResult.iRet = -1;
                _ApiResult.sError = Ex.Message;
                _staffservices = null;
            }
            return _ApiResult;
        }

        [HttpPost]
        public APiResult GetAllEzoroStaff()
        {
            _ApiResult = new APiResult();
            _staffservices = new BusinessContracts.Roles.StaffServices();
            try
            {
                _ApiResult = _staffservices.GetAllEzoroStaff();
            }
            catch (Exception Ex)
            {
                _ApiResult.iRet = -1;
                _ApiResult.sError = Ex.Message;
                _staffservices = null;
            }
            return _ApiResult;
        }

        [HttpPost]
        public APiResult GetOneEzoroStaff(GetOneEzorostaff _staff)
        {
            _ApiResult = new APiResult();
            _staffservices = new BusinessContracts.Roles.StaffServices();
            try
            {
                _ApiResult = _staffservices.GetoneEzoroStaff(_staff);
            }
            catch (Exception Ex)
            {
                _ApiResult.iRet = -1;
                _ApiResult.sError = Ex.Message;
                _staffservices = null;
            }
            return _ApiResult;
        }
    }
}