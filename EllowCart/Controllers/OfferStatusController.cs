﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using EllowCart.DTO.OfferStatus.InputModels;
using EllowCart.DTO.OfferStatus.OutputModels;
using EllowCart.Shared;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace EllowCart.Controllers
{
    [Route("api/[controller]/[Action]")]
    [ApiController]
    public class OfferStatusController : ControllerBase
    {
        EllowCart.BusinessContracts.Roles.OfferStatusServices _StatusServices = null;

        [Authorize]
        [HttpPost]
        [Produces("Application/Json")]
        public ApiResult<List<addOfferStatusOut>> AddOfferStatus(AddOfferStatus _offerstatus)
        {
            ApiResult<List<addOfferStatusOut>> _ApiResult = new ApiResult<List<addOfferStatusOut>>();
            _StatusServices = new BusinessContracts.Roles.OfferStatusServices();
            try
            {
                _ApiResult = _StatusServices.AddOfferstatus(_offerstatus);
            }
            catch (Exception eX)
            {
                _ApiResult.ErrorCode = -1;
                _ApiResult.HasWarning = true;
                _ApiResult.Message = eX.Message;
                _StatusServices = null;
            }
            return _ApiResult;
        }

      //  [Authorize]
        [HttpPost]
        [Produces("Application/Json")]
        public ApiResult<List<GetofferStatusListOut>> GetOfferStatusList(GetOfferStatusList _statusList)
        {
            ApiResult<List<GetofferStatusListOut>> _ApiResult = new ApiResult<List<GetofferStatusListOut>>();
            _StatusServices = new BusinessContracts.Roles.OfferStatusServices();
            try
            {
                _ApiResult = _StatusServices.GetStatusList(_statusList);
            }
            catch (Exception eX)
            {
                _ApiResult.ErrorCode = -1;
                _ApiResult.HasWarning = true;
                _ApiResult.Message = eX.Message;
                _StatusServices = null;
            }
            return _ApiResult;
        }

        [Authorize]
        [HttpPost]
        [Produces("Application/Json")]
        public ApiResult<List<RemoveOfferStatus>> RemoveOfferStatus(RemoveOfferstatusInput _remove)
        {
            ApiResult<List<RemoveOfferStatus>> _ApiResult = new ApiResult<List<RemoveOfferStatus>>();
            _StatusServices = new BusinessContracts.Roles.OfferStatusServices();
            try
            {
                _ApiResult = _StatusServices.RemoveOfferStatus(_remove);
            }
            catch (Exception eX)
            {
                _ApiResult.ErrorCode = -1;
                _ApiResult.HasWarning = true;
                _ApiResult.Message = eX.Message;
                _StatusServices = null;
            }
            return _ApiResult;
        }

        [Authorize]
        [HttpPost]
        [Produces("Application/Json")]
        public ApiResult<List<UpdateStatusViewedOUT>> UpdateofferstatusViewed(UpdateStatusViewed _update)
        {
            ApiResult<List<UpdateStatusViewedOUT>> _ApiResult = new ApiResult<List<UpdateStatusViewedOUT>>();
            _StatusServices = new BusinessContracts.Roles.OfferStatusServices();
            try
            {
                _ApiResult = _StatusServices.UpdateofferstatusViewed(_update);
            }
            catch (Exception eX)
            {
                _ApiResult.ErrorCode = -1;
                _ApiResult.HasWarning = true;
                _ApiResult.Message = eX.Message;
                _StatusServices = null;
            }
            return _ApiResult;
        }

        [Authorize]
        [HttpPost]
        [Produces("Application/Json")]
        public ApiResult<List<GetStatusListOut>> GetStatusList(GetStatusListInput _update)
        {
            ApiResult<List<GetStatusListOut>> _ApiResult = new ApiResult<List<GetStatusListOut>>();
            _StatusServices = new BusinessContracts.Roles.OfferStatusServices();
            try
            {
                _ApiResult = _StatusServices.GetStatusList(_update);
            }
            catch (Exception eX)
            {
                _ApiResult.ErrorCode = -1;
                _ApiResult.HasWarning = true;
                _ApiResult.Message = eX.Message;
                _StatusServices = null;
            }
            return _ApiResult;
        }

     //   [Authorize]
        [HttpPost]
        [Produces("Application/Json")]
        public ApiResult<List<GetStatusByShopOut>> GetStatusListByShop(GetStatusByShop _statusShop)
        {
            ApiResult<List<GetStatusByShopOut>> _ApiResult = new ApiResult<List<GetStatusByShopOut>>();
            _StatusServices = new BusinessContracts.Roles.OfferStatusServices();
            try
            {
                _ApiResult = _StatusServices.GetStatusListByShop(_statusShop);
            }
            catch (Exception eX)
            {
                _ApiResult.ErrorCode = -1;
                _ApiResult.HasWarning = true;
                _ApiResult.Message = eX.Message;
                _StatusServices = null;
            }
            return _ApiResult;
        }

    }
}