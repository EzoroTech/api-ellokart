﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Data;
using EllowCart.DTO.Search.InputModels;
using Dapper;
using EllowCart.DTO.General.InputModels;
using EllowCart.DTO.General.OutputModels;
using EllowCart.Shared;
using EllowCart.DTO.Search.OutputModels;

namespace EllowCart.Controllers
{
    [Route("api/[controller]/[Action]")]
    [ApiController]
    public class SearchController : ControllerBase
    {
        EllowCart.Shared.APiResult _ApiResult = null;
        EllowCart.BusinessContracts.Roles.SearchServicesForAdmin _SearchServices = null;
        private IDbConnection DBContext;
        DAL.DBSettings _DBSettings = null;


        //[HttpPost]
        //[Produces("Application/Json")]
        //public ApiResult<List<GetSellerOut>> GetSellerWithMobileNo(Search _obj)
        //{
        //    ApiResult<List<GetSellerOut>> _ApiResult = new ApiResult<List<GetSellerOut>>();
        //    _SearchServices = new BusinessContracts.Roles.SearchServicesForAdmin();
        //    try
        //    {
        //        _ApiResult = _SearchServices.GetSellerByMobileNo(_obj);
        //    }
        //    catch (Exception eX)
        //    {
        //        _ApiResult.ErrorCode = -1;
        //        _ApiResult.HasWarning = true;
        //        _ApiResult.Message = eX.Message;
        //        _SearchServices = null;
        //    }
        //    return _ApiResult;
        //}



        [HttpPost]
        [Produces("Application/Json")]
        public APiResult GetSellerWithMobileNo(Search _obj)
        {
            APiResult _ApiResult = new APiResult();
            _SearchServices = new BusinessContracts.Roles.SearchServicesForAdmin();
            try
            {
                _ApiResult = _SearchServices.GetSellerByMobileNo(_obj);
            }
            catch (Exception eX)
            {
                _ApiResult.iRet = -1;
                _ApiResult.sError = eX.Message;
                _ApiResult.sMessage = "Success";
            }
            return _ApiResult;
        }


        [HttpPost]
        [Produces("Application/Json")]
        public APiResult GetVerifiedSellerWithMobileNo(Search _obj)
        {
            APiResult _ApiResult = new APiResult();
            _SearchServices = new BusinessContracts.Roles.SearchServicesForAdmin();
            try
            {
                _ApiResult = _SearchServices.verifiedSellerByMobileNo(_obj);
            }
            catch (Exception eX)
            {
                _ApiResult.iRet = -1;
                _ApiResult.sError = eX.Message;
               // _ApiResult.sMessage = "Success";
            }
            return _ApiResult;
        }



        //////[HttpPost]
        //////[Produces("Application/Json")]
        //////public ApiResult<List<GetSellerOut>> GetSellerWithCreatedDate(SearchByDate _obj)
        //////{
        //////    ApiResult<List<GetSellerOut>> _ApiResult = new ApiResult<List<GetSellerOut>>();
        //////    _SearchServices = new BusinessContracts.Roles.SearchServicesForAdmin();
        //////    try
        //////    {
        //////        _ApiResult = _SearchServices.GetSellerByCreatedDate(_obj);
        //////    }
        //////    catch (Exception eX)
        //////    {
        //////        _ApiResult.ErrorCode = -1;
        //////        _ApiResult.HasWarning = true;
        //////        _ApiResult.Message = eX.Message;
        //////        _SearchServices = null;
        //////    }
        //////    return _ApiResult;
        //////}




        [HttpPost]
        [Produces("Application/Json")]
        public APiResult GetSellerWithCreatedDate(SearchByDate _obj)
        {
            APiResult _ApiResult = new APiResult();
            _SearchServices = new BusinessContracts.Roles.SearchServicesForAdmin();
            try
            {
                _ApiResult = _SearchServices.GetSellerByCreatedDate(_obj);
            }
            catch (Exception eX)
            {
                _ApiResult.iRet = -1;
                _ApiResult.sError = eX.Message;
              //  _ApiResult.sMessage = "Success";
            }
            return _ApiResult;
        }

        [HttpPost]
        [Produces("Application/Json")]
        public APiResult VeriifiedSellerWithCreatedDate(SearchByDate _obj)
        {
            APiResult _ApiResult = new APiResult();
            _SearchServices = new BusinessContracts.Roles.SearchServicesForAdmin();
            try
            {
                _ApiResult = _SearchServices.GetVerifiedSellerByCreatedDate(_obj);
            }
            catch (Exception eX)
            {
                _ApiResult.iRet = -1;
                _ApiResult.sError = eX.Message;
               // _ApiResult.sMessage = "Success";
            }
            return _ApiResult;
        }


        [HttpPost]
        [Produces("Application/Json")]
        public APiResult GetOrdersWithCreatedDate(OrderSearch _obj)
        {
            APiResult _ApiResult = new APiResult();
            _SearchServices = new BusinessContracts.Roles.SearchServicesForAdmin();
            try
            {
                _ApiResult = _SearchServices.GetOrdersByCreatedDate(_obj);
            }
            catch (Exception eX)
            {
                _ApiResult.iRet = -1;
                _ApiResult.sError = eX.Message;
               // _ApiResult.sMessage = "Success";
            }
            return _ApiResult;
        }



        [HttpPost]
        [Produces("Application/Json")]
        public ApiResult<List<OrderStatusSearch>> GetOrdersByOrderStatus(OrderByStatus _obj)
        {
            ApiResult<List<OrderStatusSearch>> _ApiResult = new ApiResult<List<OrderStatusSearch>>();
            _SearchServices = new BusinessContracts.Roles.SearchServicesForAdmin();
            try
            {
                _ApiResult = _SearchServices.OrdersByStatus(_obj);
            }
            catch (Exception eX)
            {
                _ApiResult.Count = -1;
                _ApiResult.Message = eX.Message;
                _ApiResult.HasWarning = true;
            }
            return _ApiResult;
        }

        [HttpPost]
        [Produces("Application/Json")]
        public ApiResult<List<ProductSearchForSupportout>> ProductSearchForSupport(ProductSearchSupport _Search)
        {
            ApiResult<List<ProductSearchForSupportout>> _ApiResult = new ApiResult<List<ProductSearchForSupportout>>();
            _SearchServices = new BusinessContracts.Roles.SearchServicesForAdmin();
            try
            {
                _ApiResult = _SearchServices.ProductSearchForSupport(_Search);
            }
            catch (Exception eX)
            {
                _ApiResult.Count = -1;
                _ApiResult.Message = eX.Message;
                _ApiResult.HasWarning = false;
            }
            return _ApiResult;
        }



    }
}