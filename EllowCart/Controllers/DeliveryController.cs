﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using Dapper;
using EllowCart.DTO.Delivery.InputModels;
using EllowCart.Shared;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace EllowCart.Controllers
{
    [Route("api/[controller]/[Action]")]
    [ApiController]
    public class DeliveryController : ControllerBase
    {
        EllowCart.BusinessContracts.Roles.DeliveryServices _deliveryservice = null;
        private IDbConnection DBContext;
        DAL.DBSettings _DBSettings = null;

        [HttpPost]
        [Produces("Application/Json")]
        public APiResult GetDeliveryStatus([FromBody]GetdataFromDeliverApp _input)
        {
            APiResult _ApiResult = new APiResult();
            _DBSettings = new DAL.DBSettings();
            DBContext = _DBSettings.GetCartDB();

            _deliveryservice = new EllowCart.BusinessContracts.Roles.DeliveryServices();
            try
            {


                _ApiResult = _deliveryservice.Delivery(_input);

                if (DBContext.State == ConnectionState.Closed)
                {
                    DBContext.Open();
                }

                var oReturn = DBContext.Query("DeliveryDetailsForCustomer", new
                {
                    @OrderId =Int32.Parse( _input.order_id),
                    @JobId = Int32.Parse(_input.job_id),
                    @DeliveryStatusId = Int32.Parse(_input.job_status),
                   

                }, commandType: CommandType.StoredProcedure);

                DBContext.Close();

              

            }
            catch (Exception Ex)
            {
                _ApiResult.iRet = -1;
                _ApiResult.sError = Ex.Message;
            }
            return _ApiResult;
        }

        [HttpPost]
        [Produces("Application/Json")]
        public ApiResult<List<AllDeliveryMode>> GetAllDeliveryMode()
        {
            ApiResult<List<AllDeliveryMode>> _ApiResult = new ApiResult<List<AllDeliveryMode>>();
            _deliveryservice = new BusinessContracts.Roles.DeliveryServices();
            try
            {
                _ApiResult = _deliveryservice.DeliveryMode();
            }
            catch (Exception eX)
            {
                _ApiResult.ErrorCode = -1;
                _ApiResult.HasWarning = true;
                _ApiResult.Message = eX.Message;
                _deliveryservice = null;
            }
            return _ApiResult;
        }


    }
}