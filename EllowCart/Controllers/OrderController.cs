﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using EllowCart.DTO.Order.InputModels;
using EllowCart.DTO.Order.OutputModels;
using EllowCart.Shared;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace EllowCart.Controllers
{
    [Route("api/[controller]/[Action]")]
    [ApiController]
    public class OrderController : ControllerBase
    {
        EllowCart.BusinessContracts.Roles.OrderServices _orderServices = null;

        // [Authorize]
        [HttpPost]
        [Produces("Application/Json")]
        public ApiResult<List<PlaceOrderOut>> PlaceOrder(PlaceOrderInput _placeOrder) 
        {
            ApiResult<List<PlaceOrderOut>> _ApiResult = new ApiResult<List<PlaceOrderOut>>();
            _orderServices = new BusinessContracts.Roles.OrderServices();
            try
            {
                _ApiResult = _orderServices.PlaceOrder(_placeOrder);
            }
            catch (Exception eX)
            {
                _ApiResult.ErrorCode = -1;
                _ApiResult.HasWarning = true;
                _ApiResult.Message = eX.Message;
                _orderServices = null;
            }
            return _ApiResult;
        }

      // [Authorize]
        [HttpPost]
        [Produces("Application/Json")]
        public ApiResult<List<OrderListOut>> GetOrderListBySeller(GetOrderBySeller _orderBySeller)
        {
            ApiResult<List<OrderListOut>> _ApiResult = new ApiResult<List<OrderListOut>>();
            _orderServices = new BusinessContracts.Roles.OrderServices();
            try
            {
                _ApiResult = _orderServices.GetOrderBySeller(_orderBySeller);
            }
            catch (Exception eX)
            {
                _ApiResult.ErrorCode = -1;
                _ApiResult.HasWarning = true;
                _ApiResult.Message = eX.Message;
                _orderServices = null;
            }
            return _ApiResult;
        }


        [HttpPost]
        [Produces("Application/Json")]
        public ApiResult<List<GetOderBySellerOut>> GetOrderDetailsSeller(OrderDetailInput _orderBySeller)
        {
            ApiResult<List<GetOderBySellerOut>> _ApiResult = new ApiResult<List<GetOderBySellerOut>>();
            _orderServices = new BusinessContracts.Roles.OrderServices();
            try
            {
                _ApiResult = _orderServices.GetOrderDetailSeller(_orderBySeller);
            }
            catch (Exception eX)
            {
                _ApiResult.ErrorCode = -1;
                _ApiResult.HasWarning = true;
                _ApiResult.Message = eX.Message;
                _orderServices = null;
            }
            return _ApiResult;
        }



        // [Authorize]
        [HttpPost]
        [Produces("Application/Json")]
        public ApiResult<List<AcceptorRejectOrderOut>> AcceptorRejectOrder(AcceptOrRejectOrder _orderBySeller)
        {
            ApiResult<List<AcceptorRejectOrderOut>> _ApiResult = new ApiResult<List<AcceptorRejectOrderOut>>();
            APiResult _api = new APiResult();
            _orderServices = new BusinessContracts.Roles.OrderServices();
            try
            {
                _api = _orderServices.AcceptorRejectOrder(_orderBySeller);

                if (_api.iRet == 2)
                {

                    string url = "https://api.tookanapp.com/v2/create_task";

                    HttpClient client1 = new HttpClient();
                    string token1 = string.Empty;
                    client1.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", token1);
                    HttpResponseMessage response1 = client1.PostAsJsonAsync(url, _api.sResult).Result;
                    response1.EnsureSuccessStatusCode();
                    var data1 = response1.Content.ReadAsStringAsync().Result;
                    var result = JsonConvert.DeserializeObject<TookanTaskOutput>(data1);

                    _ApiResult = _orderServices.AddDeliveryDetails(result);


                }
                else
                {
                    List<AcceptorRejectOrderOut> _res = new List<AcceptorRejectOrderOut>();
                    _res.Add(new AcceptorRejectOrderOut { Status = _api.iRet });
                    _ApiResult.Count = 1;
                    _ApiResult.HasWarning = false;
                    _ApiResult.Value= _res;
                }
                
            }
            catch (Exception eX)
            {
                _ApiResult.ErrorCode = -1;
                _ApiResult.HasWarning = true;
                _ApiResult.Message = eX.Message;
                _orderServices = null;
            }
            return _ApiResult;
        }

       // [Authorize]
        [HttpPost]
        [Produces("Application/Json")]
        public ApiResult<List<CancelOrderOut>> CancelOrder(CancelOrder _cancel)
        {
            ApiResult<List<CancelOrderOut>> _ApiResult = new ApiResult<List<CancelOrderOut>>();
            _orderServices = new BusinessContracts.Roles.OrderServices();
            try
            {
                _ApiResult = _orderServices.CancelOrder(_cancel);
            }
            catch (Exception eX)
            {
                _ApiResult.ErrorCode = -1;
                _ApiResult.HasWarning = true;
                _ApiResult.Message = eX.Message;
                _orderServices = null;
            }
            return _ApiResult;
        }

        //[Authorize]
        [HttpPost]
        [Produces("Application/Json")]
        public ApiResult<List<TotalPaymentOut>> DeliveryChargeByProduct(DeliveryCharge _charge)
        {
            ApiResult<List<TotalPaymentOut>> _ApiResult = new ApiResult<List<TotalPaymentOut>>();
            _orderServices = new BusinessContracts.Roles.OrderServices();
            try
            {
                _ApiResult = _orderServices.DeliveryChargeByProduct(_charge);
            }
            catch (Exception eX)
            {
                _ApiResult.ErrorCode = -1;
                _ApiResult.HasWarning = true;
                _ApiResult.Message = eX.Message;
                _orderServices = null;
            }
            return _ApiResult;
        }

        //  [Authorize]
        [HttpPost]
        [Produces("Application/Json")]
        public APiResult CheckoutByCustomer(CheckoutByCustomer _charge)
        {
            APiResult _ApiResult = new APiResult();
            _orderServices = new BusinessContracts.Roles.OrderServices();
            try
            {
                _ApiResult = _orderServices.CheckOutByCustomer(_charge);
            }
            catch (Exception eX)
            {
                _ApiResult.iRet = -1;
                _ApiResult.sError = eX.Message;
                _ApiResult.sMessage = "Success";
                _orderServices = null;
            }
            return _ApiResult;
        }

        // [Authorize]
        //[HttpPost]
        //[Produces("Application/Json")]
        //public ApiResult<List<RequestOutPut>> PickupRequest(PickupRequestinput _pickup)
        //{
        //    ApiResult<List<RequestOutPut>> _ApiResult = new ApiResult<List<RequestOutPut>>();
        //    _orderServices = new BusinessContracts.Roles.OrderServices();
        //    try
        //    {
        //        _ApiResult = _orderServices.PickupRequest(_pickup);


        //        if (_ApiResult.Message == "Success")
        //        {

        //            InputToApplication _input = new InputToApplication();

        //            /********To metadata*******/
        //            List<MetdataForDelivery> _metadata = new List<MetdataForDelivery>();
        //          //  _metadata[0].DELIVERY_AMOUNT=
        //            _input.meta_data = _metadata;

        //               /********End *******/


        //            /********Date Time Changes******/

        //               TimeSpan s = new TimeSpan(18, 0, 0);

        //            DateTime today = DateTime.Now;
        //            DateTime tomarrow= today.AddDays(1);                  
        //            today = today.Date + s;
        //            tomarrow = tomarrow.Date + s;

        //            string OrderId = _ApiResult.Value[0].OrderId.ToString();

        //            string today1 = today.ToString("yyyy-MM-dd HH':'mm':'ss");
        //            string tomarrow1= tomarrow.ToString("yyyy-MM-dd HH':'mm':'ss");
        //            /********Date Time Changes******/

        //            /******Feilds For Delivery Application*******/
        //            #region TookanInputs

        //            _input.order_id = OrderId;
        //            _input.job_description = _ApiResult.Value[0].CategoryName;
        //            _input.job_pickup_phone = _ApiResult.Value[0].SellerMobileNumber;
        //            _input.job_pickup_name = _ApiResult.Value[0].SellerName;
        //            _input.job_pickup_address = _ApiResult.Value[0].SellerAddress;
        //            _input.job_pickup_latitude = _ApiResult.Value[0].SellerLattitude;
        //            _input.job_pickup_longitude = _ApiResult.Value[0].SellerLongittude;
        //            _input.job_pickup_datetime = today1;
        //            _input.customer_email = _ApiResult.Value[0].CustomerEmailId;
        //            _input.customer_username = _ApiResult.Value[0].CustomerName;
        //            _input.customer_phone = _ApiResult.Value[0].CustomerAlternateMobNo;
        //            _input.customer_address = _ApiResult.Value[0].CustomerAddress;
        //            _input.latitude = _ApiResult.Value[0].CustomerLattitude;
        //            _input.longitude = _ApiResult.Value[0].CustomerLongittude;
        //            _input.job_delivery_datetime = tomarrow1;

        //            #endregion
        //            /******Feilds For Delivery Application*******/

        //            string url = "https://api.tookanapp.com/v2/create_task";

        //            HttpClient client1 = new HttpClient();
        //            string token1 = string.Empty;
        //            client1.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", token1);
        //            HttpResponseMessage response1 = client1.PostAsJsonAsync(url, _input).Result;
        //            response1.EnsureSuccessStatusCode();
        //            var data1 = response1.Content.ReadAsStringAsync().Result;
        //            var result= JsonConvert.DeserializeObject<TookanTaskOutput>(data1);


        //            _ApiResult.Count = 1;
        //        }



        //    }
        //    catch (Exception eX)
        //    {
        //        _ApiResult.ErrorCode = -1;
        //        _ApiResult.HasWarning = true;
        //        _ApiResult.Message = eX.Message;
        //        _orderServices = null;
        //    }
        //    return _ApiResult;
        //}

        [HttpPost]
        [Produces("Application/Json")]
        public APiResult OrderListForSupport()
        {
            APiResult _ApiResult = new APiResult();
            _orderServices = new BusinessContracts.Roles.OrderServices();
            try
            {
                _ApiResult = _orderServices.OrderListForSupport();
            }
            catch (Exception eX)
            {
                _ApiResult.iRet = -1;
                _ApiResult.sError = eX.Message;
                _ApiResult.sMessage = "Success";
                _orderServices = null;
            }
            return _ApiResult;
        }

        //  [Authorize]
        [HttpPost]
        [Produces("Application/Json")]
        public APiResult OrderDetailsSupport(OrderDetailSupport _details)
        {
            APiResult _ApiResult = new APiResult();
            _orderServices = new BusinessContracts.Roles.OrderServices();
            try
            {
                _ApiResult = _orderServices.OrderDetailsSupport(_details);
            }
            catch (Exception eX)
            {
                _ApiResult.iRet = -1;
                _ApiResult.sError = eX.Message;
                // _ApiResult.Message = eX.Message;
                _orderServices = null;
            }
            return _ApiResult;
        }

    }
}