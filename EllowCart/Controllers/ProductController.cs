﻿using System;
using System.Collections.Generic;
using EllowCart.DTO.Product.InputModels;
using EllowCart.DTO.Product.OutputModels;
using EllowCart.Shared;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace EllowCart.Controllers
{
    [Route("api/[controller]/[Action]")]
    [ApiController]
    public class ProductController : ControllerBase
    {
        EllowCart.Shared.APiResult _ApiResult = null;
        EllowCart.BusinessContracts.Roles.ProductServices _ProductServices = null;

      //  [Authorize]
        [HttpPost]
        [Produces("Application/Json")]
        public ApiResult<List<addproductout>> AddProduct(Product _product)
        {
            ApiResult<List<addproductout>> _ApiResult = new ApiResult<List<addproductout>>();
            _ProductServices = new BusinessContracts.Roles.ProductServices();
            try
            {
               _ApiResult = _ProductServices.AddProduct(_product);
            }
            catch (Exception eX)
            {
                _ApiResult.ErrorCode = -1;
                _ApiResult.HasWarning = true;
                _ApiResult.Message = eX.Message;
                _ProductServices = null;
            }
            return _ApiResult;
        }

        [Authorize]
        [HttpPost]
        [Produces("Application/Json")]
        public ApiResult<List<EditProductDetailsOut>> EditProductDetails(EditProductDetailsInput _product)
        {
            ApiResult<List<EditProductDetailsOut>> _ApiResult = new ApiResult<List<EditProductDetailsOut>>();
            _ProductServices = new BusinessContracts.Roles.ProductServices();
            try
            {
                _ApiResult = _ProductServices.EditProductDetails(_product);
            }
            catch (Exception eX)
            {
                _ApiResult.ErrorCode = -1;
                _ApiResult.HasWarning = true;
                _ApiResult.Message = eX.Message;
                _ProductServices = null;
            }
            return _ApiResult;
        }



     //   [Authorize]
        [HttpPost]
        [Produces("Application/Json")]
        public ApiResult<List<GetProductListOut>> GetProductList(GetProductlistInput _getProductlist)
        {
            ApiResult<List<GetProductListOut>> _ApiResult = new ApiResult<List<GetProductListOut>>();
            _ProductServices = new BusinessContracts.Roles.ProductServices();
            try
            {
                _ApiResult = _ProductServices.GetproductList(_getProductlist);
            }
            catch (Exception eX)
            {
                _ApiResult.ErrorCode = -1;
                _ApiResult.HasWarning = true;
                _ApiResult.Message = eX.Message;
                _ProductServices = null;
            }
            return _ApiResult;
        }

        [Authorize]
        [HttpPost]
        [Produces("Application/Json")]
        public ApiResult<List<RemoveProductout>> RemoveProduct(RemoveProductInput _removeProduct)
        {
            ApiResult<List<RemoveProductout>> _ApiResult = new ApiResult<List<RemoveProductout>>();
            _ProductServices = new BusinessContracts.Roles.ProductServices();
            try
            {
                _ApiResult = _ProductServices.RemoveProduct(_removeProduct);
            }
            catch (Exception eX)
            {
                _ApiResult.ErrorCode = -1;
                _ApiResult.HasWarning = true;
                _ApiResult.Message = eX.Message;
                _ProductServices = null;
            }
            return _ApiResult;
        }


        [Authorize]
        [HttpPost]
        [Produces("Application/Json")]
        public ApiResult<List<GetProductSearchResultOut>> SearchProduct(GetProductSerachInput _search)
        {
            ApiResult<List<GetProductSearchResultOut>> _ApiResult = new ApiResult<List<GetProductSearchResultOut>>();
            _ProductServices = new BusinessContracts.Roles.ProductServices();
            try
            {
                _ApiResult = _ProductServices.SearchProduct(_search);
            }
            catch (Exception eX)
            {
                _ApiResult.ErrorCode = -1;
                _ApiResult.HasWarning = true;
                _ApiResult.Message = eX.Message;
                _ProductServices = null;
            }
            return _ApiResult;
        }


       // [Authorize]
        [HttpPost]
        [Produces("Application/Json")]
        public APiResult GetProductDetails(GetproductDetailsInput _getproduct)
        {
            APiResult _ApiResult = new APiResult();
            _ProductServices = new BusinessContracts.Roles.ProductServices();
            try
            {
                _ApiResult = _ProductServices.GetproductDetails(_getproduct);
            }
            catch (Exception eX)
            {
                _ApiResult.iRet = 1;
                _ApiResult.sError = eX.Message;
                _ApiResult.sMessage = "Success";
            }
            return _ApiResult;
        }

       // [Authorize]
        [HttpPost]
        [Produces("Application/Json")]
        public APiResult GetProductDetailsCustomer(GetproductDetailsInput _getproduct)
        {
            APiResult _ApiResult = new APiResult();
            _ProductServices = new BusinessContracts.Roles.ProductServices();
            try
            {
                _ApiResult = _ProductServices.GetproductDetailsCustomer(_getproduct);
            }
            catch (Exception eX)
            {
                _ApiResult.iRet = 1;
                _ApiResult.sError = eX.Message;
                _ApiResult.sMessage = "Success";
            }
            return _ApiResult;
        }


        [Authorize]
        [HttpPost]
        [Produces("Application/Json")]
        public ApiResult<List<GetProductCategoryListOut>> GetProductCategoryList(GetProductCategoryListInput _categoryListInput)
        {
            ApiResult<List<GetProductCategoryListOut>> _ApiResult = new ApiResult<List<GetProductCategoryListOut>>();
            _ProductServices = new BusinessContracts.Roles.ProductServices();
            try
            {
                _ApiResult = _ProductServices.GetProductCategoryList(_categoryListInput);
            }
            catch (Exception eX)
            {
                _ApiResult.ErrorCode = -1;
                _ApiResult.HasWarning = true;
                _ApiResult.Message = eX.Message;
                _ProductServices = null;
            }
            return _ApiResult;
        }

        [Authorize]
        [HttpPost]
        [Produces("Application/Json")]
        public ApiResult<List<GetProductImages>> GetProductImagesList(GetProductImagesInput _imagesInput)
        {
            ApiResult<List<GetProductImages>> _ApiResult = new ApiResult<List<GetProductImages>>();
            _ProductServices = new BusinessContracts.Roles.ProductServices();
            try
            {
                _ApiResult = _ProductServices.GetProductImages(_imagesInput);
            }
            catch (Exception eX)
            {
                _ApiResult.ErrorCode = -1;
                _ApiResult.HasWarning = true;
                _ApiResult.Message = eX.Message;
                _ProductServices = null;
            }
            return _ApiResult;
        }
      //  [Authorize]
        [HttpPost]
        [Produces("Application/Json")]
        public ApiResult<List<GetProductOfferOut>> GetProductOfferList(GetProductOfferInput _offerInput)
        {
            ApiResult<List<GetProductOfferOut>> _ApiResult = new ApiResult<List<GetProductOfferOut>>();
            _ProductServices = new BusinessContracts.Roles.ProductServices();
            try
            {
                _ApiResult = _ProductServices.GetProductOfferList(_offerInput);
            }
            catch (Exception eX)
            {
                _ApiResult.ErrorCode = -1;
                _ApiResult.HasWarning = true;
                _ApiResult.Message = eX.Message;
                _ProductServices = null;
            }
            return _ApiResult;
        }
        [Authorize]
        [HttpPost]
        [Produces("Application/Json")]
        public ApiResult<List<GetProductSpecificationOut>> GetProductSpecificationList(GetSpecificationInput _specificationInput)
        {
            ApiResult<List<GetProductSpecificationOut>> _ApiResult = new ApiResult<List<GetProductSpecificationOut>>();
            _ProductServices = new BusinessContracts.Roles.ProductServices();
            try
            {
                _ApiResult = _ProductServices.GetSpecificationList(_specificationInput);
            }
            catch (Exception eX)
            {
                _ApiResult.ErrorCode = -1;
                _ApiResult.HasWarning = true;
                _ApiResult.Message = eX.Message;
                _ProductServices = null;
            }
            return _ApiResult;
        }

        [Authorize]
        [HttpPost]
        [Produces("Application/Json")]
        public ApiResult<List<GetComboProductImages>> GetOfferProductImages(GetComboProductImagesInput _comboinput)
        {
            ApiResult<List<GetComboProductImages>> _ApiResult = new ApiResult<List<GetComboProductImages>>();
            _ProductServices = new BusinessContracts.Roles.ProductServices();
            try
            {
                _ApiResult = _ProductServices.GetComboProductimages(_comboinput);
            }
            catch (Exception eX)
            {
                _ApiResult.ErrorCode = -1;
                _ApiResult.HasWarning = true;
                _ApiResult.Message = eX.Message;
                _ProductServices = null;
            }
            return _ApiResult;
        }

        [HttpPost]
        [Produces("Application/Json")]
        public ApiResult<List<GetProductOfferOut>> GetProductOfferListForCustomer(GetProductOfferInput _offerInput)
        {
            ApiResult<List<GetProductOfferOut>> _ApiResult = new ApiResult<List<GetProductOfferOut>>();
            _ProductServices = new BusinessContracts.Roles.ProductServices();
            try
            {
                _ApiResult = _ProductServices.ProductOfferListForCustomer(_offerInput);
            }
            catch (Exception eX)
            {
                _ApiResult.ErrorCode = -1;
                _ApiResult.HasWarning = true;
                _ApiResult.Message = eX.Message;
                _ProductServices = null;
            }
            return _ApiResult;
        }

    }
}