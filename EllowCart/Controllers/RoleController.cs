﻿using System;
using Microsoft.AspNetCore.Mvc;
using EllowCart.Shared;
using EllowCart.DTO.Roles.InputModels;
using System.Data;
using EllowCart.DTO.Roles.OutputModels;
using System.Collections.Generic;

namespace EllowCart.Controllers
{
    [Route("api/[controller]/[Action]")]
    [ApiController]
    public class RoleController : ControllerBase
    {


        EllowCart.BusinessContracts.Roles.RoleServices _roleservices = null;
        EllowCart.Shared.APiResult _ApiResult = null;

        //adding role
        [HttpPost]
        public APiResult CreateRole(RoleInputModel _role)
        {
            _ApiResult = new APiResult();
            _roleservices = new BusinessContracts.Roles.RoleServices();
            try
            {
                _ApiResult = _roleservices.AddNewRole(_role);

            }
            catch (Exception eX)
            {

                _ApiResult.iRet = -1;
                _ApiResult.sError = eX.Message;
                _roleservices = null;


            }
            return _ApiResult;

        }

        //deleting a role
        [HttpPost]
        public APiResult DeleteRole(RoleSelectModel _role)
        {
            _ApiResult = new APiResult();
            _roleservices = new BusinessContracts.Roles.RoleServices();
            try
            {
                _ApiResult = _roleservices.DeleteRole(_role);

            }
            catch (Exception eX)
            {

                _ApiResult.iRet = -1;
                _ApiResult.sError = eX.Message;
                _roleservices = null;


            }
            return _ApiResult;

        }

        //updating a role
        [HttpPost]
        public APiResult UpdateRoles(RoleInputForUpdation _rOut)
        {
            _ApiResult = new APiResult();
            _roleservices = new BusinessContracts.Roles.RoleServices();
            DataTable dt = new DataTable();


            try
            {
                _ApiResult = _roleservices.UpdateRole(_rOut);

            }
            catch (Exception eX)
            {

                _ApiResult.iRet = -1;
                _ApiResult.sError = eX.Message;
                _roleservices = null;


            }
            return _ApiResult;

        }
        //get one role 
        [HttpPost]
        [Produces("Application/Json")]
        public APiResult GetOneRole(RoleSelectModel _role)
        {
            _ApiResult = new APiResult();
            _roleservices = new BusinessContracts.Roles.RoleServices();
            try
            {
                _ApiResult = _roleservices.GetRoleById(_role);
            }
            catch (Exception eX)
            {
                _ApiResult.iRet = -1;
                _ApiResult.sError = eX.Message;
                _roleservices = null;
            }
            return _ApiResult;
        }

        //getting all roles
        [HttpPost]
        [Produces("Application/Json")]
        public APiResult GetAllRoles()
        {
            APiResult _ApiResult = new APiResult();
            _roleservices = new BusinessContracts.Roles.RoleServices();
            try
            {
                _ApiResult = _roleservices.GetAllRoles();
            }
            catch (Exception eX)
            {
                _ApiResult.iRet = -1;
                _ApiResult.sError = eX.Message;
                _roleservices = null;
            }
            return _ApiResult;
        }

     

        [HttpPost]
        public APiResult CreateRoleAttribute(RoleAttributeInputModel _RoleAttr)
        {
            _ApiResult = new APiResult();
            _roleservices = new BusinessContracts.Roles.RoleServices();
            try
            {
                _ApiResult = _roleservices.AddNewRoleAttribute(_RoleAttr);

            }
            catch (Exception eX)
            {

                _ApiResult.iRet = -1;
                _ApiResult.sError = eX.Message;
                _roleservices = null;

            }
            return _ApiResult;

        }


        //delete a RoleAtribute
        [HttpPost]
        public APiResult DeleteARoleAttribute(RoleAttributeSelectModel _rAttrModel)
        {
            _ApiResult = new APiResult();
            _roleservices = new BusinessContracts.Roles.RoleServices();
            try
            {
                _ApiResult = _roleservices.DeleteARoleAttribute(_rAttrModel);

            }
            catch (Exception eX)
            {

                _ApiResult.iRet = -1;
                _ApiResult.sError = eX.Message;
                _roleservices = null;


            }
            return _ApiResult;

        }

        //update a  RoleAtribute
        [HttpPost]
        public APiResult UpdateRoleAttribute(RoleAttributeInputForUpdation RoleAttrObj)
        {
            _ApiResult = new APiResult();
            _roleservices = new BusinessContracts.Roles.RoleServices();
            DataTable dt = new DataTable();


            try
            {
                _ApiResult = _roleservices.UpdateRoleAttribute(RoleAttrObj);

            }
            catch (Exception eX)
            {

                _ApiResult.iRet = -1;
                _ApiResult.sError = eX.Message;
                _roleservices = null;


            }
            return _ApiResult;

        }

        //get all RoleAtribute
        [HttpPost]
        [Produces("Application/Json")]
        public APiResult GetAllRoleAttribute()
        {
            _ApiResult = new APiResult();
            _roleservices = new BusinessContracts.Roles.RoleServices();
            try
            {
                _ApiResult = _roleservices.GetAllRoleAttributes();
            }
            catch (Exception eX)
            {
                _ApiResult.iRet = -1;
                _ApiResult.sError = eX.Message;
                _roleservices = null;
            }
            return _ApiResult;
        }


        //get one RoleAtribute 
        [HttpPost]
        [Produces("Application/Json")]
        public APiResult GetOneRoleAttr(RoleAttributeSelectModel _roleAttrSelect)
        {
            _ApiResult = new APiResult();
            _roleservices = new BusinessContracts.Roles.RoleServices();
            try
            {
                _ApiResult = _roleservices.GetRoleAttributeById(_roleAttrSelect);
            }
            catch (Exception eX)
            {
                _ApiResult.iRet = -1;
                _ApiResult.sError = eX.Message;
                _roleservices = null;
            }
            return _ApiResult;
        }

    }
}