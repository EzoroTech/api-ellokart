﻿using Dapper;
using EllowCart.DTO.Offer.InputModels;
using EllowCart.DTO.Offer.OutputModels;
using EllowCart.Shared;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace EllowCart.BusinessContracts.Roles
{
    public class OfferServices
    {
        private IDbConnection DBContext;
        DAL.DBSettings _DBSettings = null;

        public OfferServices(IDbConnection _DBContext)
        {
            this.DBContext = _DBContext;

        }
        public OfferServices()
        {
            _DBSettings = new DAL.DBSettings();  //-- New change
            DBContext = _DBSettings.GetCartDB();
        }

        public ApiResult<List<AddProductOfferout>> AddProductOffer(AddProductOfferInput _offerInput)
        {
            ApiResult<List<AddProductOfferout>> oResult = new ApiResult<List<AddProductOfferout>>();

            if (DBContext.State == ConnectionState.Closed)
            {
                DBContext.Open();
            }

            
                var dt = new DataTable("OfferFreeComboProductType");
                dt.Columns.Add("ComboProductID", typeof(long));
                dt.Columns.Add("OfferQty", typeof(long));

                var combo = _offerInput.dtOfferComboProductType;
                for (int i = 0; i < combo.Count; i++)
                {
                    dt.Rows.Add(combo[i].ComboProductID, combo[i].OfferQty);
                }

                var oReturn = DBContext.Query<AddProductOfferout>("AddProductOffer", new
                {
                    @UserId = _offerInput.UserId,
                    @role = _offerInput.role,
                    @BranchId = _offerInput.BranchId,
                    @ProductId = _offerInput.ProductId,
                    @OfferMasterId = _offerInput.OfferMasterId,
                    @DiscountValue = _offerInput.DiscountValue,
                    @DiscountUnit = _offerInput.DiscountUnit,
                    @MinimumOrderValue = _offerInput.MinimumOrderValue,
                    @MaxOrderValue = _offerInput.MaxOrderValue,
                    @MaxDiscountAmt = _offerInput.MaxDiscountAmt,
                    @ValidFrom = _offerInput.ValidFrom,
                    @ValidTo = _offerInput.ValidTo,
                    @OfferName = _offerInput.OfferName,
                    @dtOfferComboProductType = dt.AsTableValuedParameter("OfferFreeComboProductType")
                }, commandType: CommandType.StoredProcedure) as List<AddProductOfferout>;
            DBContext.Close();

            oResult.Count = oReturn.Count;
                oResult.HasWarning = false;
                oResult.Message = "Success";
                oResult.Value = oReturn;
           
            
            return oResult;

        }


        public ApiResult<List<AddProductOfferout>> AddProductOfferSpecial(AddProductOfferSpecial _offerInput)
        {
            ApiResult<List<AddProductOfferout>> oResult = new ApiResult<List<AddProductOfferout>>();

            if (DBContext.State == ConnectionState.Closed)
            {
                DBContext.Open();
            }

            var oReturn = DBContext.Query<AddProductOfferout>("AddProductOfferSpecial", _offerInput, commandType: CommandType.StoredProcedure) as List<AddProductOfferout>;
            DBContext.Close();

            oResult.Count = oReturn.Count;
            oResult.HasWarning = false;
            oResult.Message = "Success";
            oResult.Value = oReturn;
            return oResult;

        }

        public ApiResult<List<GetOfferListout>> GetOfferfreeList(GetOfferListInput _offerInput)
        {
            ApiResult<List<GetOfferListout>> oResult = new ApiResult<List<GetOfferListout>>();

            if (DBContext.State == ConnectionState.Closed)
            {
                DBContext.Open();
            }

            var oReturn = DBContext.Query<GetOfferListout>("GetOfferFreeList", _offerInput, commandType: CommandType.StoredProcedure) as List<GetOfferListout>;
            DBContext.Close();

            oResult.Count = oReturn.Count;
            oResult.HasWarning = false;
            oResult.Message = "Success";
            oResult.Value = oReturn;
            return oResult;

        }

        public ApiResult<List<OfferspecialOut>> GetOfferSpecialList(GetOfferSpecialInput _offerInput)
        {
            ApiResult<List<OfferspecialOut>> oResult = new ApiResult<List<OfferspecialOut>>();

            if (DBContext.State == ConnectionState.Closed)
            {
                DBContext.Open();
            }

            var oReturn = DBContext.Query<OfferspecialOut>("GetOfferSpecialList", _offerInput, commandType: CommandType.StoredProcedure) as List<OfferspecialOut>;
            DBContext.Close();

            oResult.Count = oReturn.Count;
            oResult.HasWarning = false;
            oResult.Message = "Success";
            oResult.Value = oReturn;
            return oResult;

        }


        public ApiResult<List<GetOffermaster>> GetOfferMaster()
        {
            ApiResult<List<GetOffermaster>> oResult = new ApiResult<List<GetOffermaster>>();

            if (DBContext.State == ConnectionState.Closed)
            {
                DBContext.Open();
            }

            var oReturn = DBContext.Query<GetOffermaster>("GetOfferMaster", commandType: CommandType.StoredProcedure) as List<GetOffermaster>;
            DBContext.Close();

            oResult.Count = oReturn.Count;
            oResult.HasWarning = false;
            oResult.Message = "Success";
            oResult.Value = oReturn;
            return oResult;

        }


        public ApiResult<List<EditOfferDetailsOut>> EditOfferDetails(EditOfferdetailsInput _editOfferdetails)
        {
            ApiResult<List<EditOfferDetailsOut>> oResult = new ApiResult<List<EditOfferDetailsOut>>();

            if (DBContext.State == ConnectionState.Closed)
            {
                DBContext.Open();
            }

            if (_editOfferdetails.UserId != 0 || _editOfferdetails.branchId != 0 || _editOfferdetails.ProductId != 0)
            {

                var oReturn = DBContext.Query<EditOfferDetailsOut>("UpdateOfferSpecialDetails", _editOfferdetails, commandType: CommandType.StoredProcedure) as List<EditOfferDetailsOut>;
                DBContext.Close();

                oResult.Count = oReturn.Count;
                oResult.HasWarning = false;
                oResult.Message = "Success";
                oResult.Value = oReturn;
            }
            else
            {
                oResult.HasWarning = true;
                oResult.Message = "Must insert UserId,BranchId and ProductId";
            }
            return oResult;

        }


        public ApiResult<List<UpdateOfferFreeDetailsout>> UpdateofferFreeDetails(UpdateOfferFreeDetailsInput _editOfferdetails)
        {
            ApiResult<List<UpdateOfferFreeDetailsout>> oResult = new ApiResult<List<UpdateOfferFreeDetailsout>>();

            if (DBContext.State == ConnectionState.Closed)
            {
                DBContext.Open();
            }

            if (_editOfferdetails.UserId != 0 || _editOfferdetails.branchId != 0 || _editOfferdetails.ProductId != 0)
            {

                var dt = new DataTable("OfferFreeComboProductType");
                dt.Columns.Add("ComboProductID", typeof(long));
                dt.Columns.Add("OfferQty", typeof(long));

                var combo = _editOfferdetails.dtOfferComboProductType;
                for (int i = 0; i < combo.Count; i++)
                {
                    dt.Rows.Add(combo[i].ComboProductID, combo[i].OfferQty);
                }

                var oReturn = DBContext.Query<UpdateOfferFreeDetailsout>("UpdateOfferFreeDetails", new
                {
                    @UserId=_editOfferdetails.UserId,
                    @role=_editOfferdetails.role,
                    @branchId=_editOfferdetails.branchId,
                    @ProductId=_editOfferdetails.ProductId,
                    @OfferMasterId=_editOfferdetails.OfferMasterId,
                    @OfferName=_editOfferdetails.OfferName,
                    @DiscountValue=_editOfferdetails.DiscountValue,
                    @DiscountUnit=_editOfferdetails.DiscountUnit,
                    @MinimumOrderValue=_editOfferdetails.MinimumOrderValue,
                    @MaxOrderValue=_editOfferdetails.MaxOrderValue,
                    @MaxDiscountAmt=_editOfferdetails.MaxDiscountAmt,
                    @ValidFrom=_editOfferdetails.ValidFrom,
                    @ValidTo=_editOfferdetails.ValidTo,
                    @OfferSpecialId=_editOfferdetails.OfferSpecialId,
                    @dtOfferComboProductType=dt.AsTableValuedParameter("OfferFreeComboProductType")
                }, commandType: CommandType.StoredProcedure) as List<UpdateOfferFreeDetailsout>;
                DBContext.Close();

                oResult.Count = oReturn.Count;
                oResult.HasWarning = false;
                oResult.Message = "Success";
                oResult.Value = oReturn;
            }
            else
            {
                oResult.HasWarning = true;
                oResult.Message = "Must insert UserId,BranchId and ProductId";
            }
            return oResult;

        }

        public ApiResult<List<RemoveOfferOut>> RemoveOffer(RemoveOffer _removeOffer)
        {
            ApiResult<List<RemoveOfferOut>> oResult = new ApiResult<List<RemoveOfferOut>>();

            if (DBContext.State == ConnectionState.Closed)
            {
                DBContext.Open();
            }

            var oReturn = DBContext.Query<RemoveOfferOut>("RemoveOffer", _removeOffer, commandType: CommandType.StoredProcedure) as List<RemoveOfferOut>;
            DBContext.Close();

            oResult.Count = oReturn.Count;
            oResult.HasWarning = false;
            oResult.Message = "Success";
            oResult.Value = oReturn;
            return oResult;

        }

        public ApiResult<List<GetOffersOutput>> GetOffers(GetOffersInput _getOffers)
        {
            ApiResult<List<GetOffersOutput>> oResult = new ApiResult<List<GetOffersOutput>>();

            if (DBContext.State == ConnectionState.Closed)
            {
                DBContext.Open();
            }

            var oReturn = DBContext.Query<GetOffersOutput>("GetOffers", _getOffers, commandType: CommandType.StoredProcedure) as List<GetOffersOutput>;
            DBContext.Close();

            oResult.Count = oReturn.Count;
            oResult.HasWarning = false;
            oResult.Message = "Success";
            oResult.Value = oReturn;
            return oResult;

        }



        public APiResult MultipleOffersOfAshop(GetShopByLocation _obj)
        {
            APiResult oResult = new APiResult();


            if (DBContext.State == ConnectionState.Closed)
            {
                DBContext.Open();
            }

            var oReturn = DBContext.Query<GetShopList>("GetShopsOfaRegion", _obj, commandType: CommandType.StoredProcedure) as List<GetShopList>;


            int Rowcount = oReturn.Count;

         
            for (int i = 0; i <= Rowcount - 1; i++)
            {
                var Oreturn1 = DBContext.Query<NearByOffers>("GetMultipleOffersList", new { BranchId = oReturn[i].BranchId }, commandType: CommandType.StoredProcedure) as List<NearByOffers>;
                oReturn[i].offers = Oreturn1;                          
            }

            DBContext.Close();

            oResult.sResult = oReturn;
            oResult.iRet = 1;
            oResult.sMessage = "Success";
            return oResult;



        }
        public ApiResult<List<OfferTypeSearchOut>> OfferTypeBasedSearch(OfferTypeSearch _search)
        {
            ApiResult<List<OfferTypeSearchOut>> oResult = new ApiResult<List<OfferTypeSearchOut>>();

            if (DBContext.State == ConnectionState.Closed)
            {
                DBContext.Open();
            }

            var oReturn = DBContext.Query<OfferTypeSearchOut>("GetResultForOfferTypeSearch", _search, commandType: CommandType.StoredProcedure) as List<OfferTypeSearchOut>;
            DBContext.Close();

            oResult.Count = oReturn.Count;
            oResult.HasWarning = false;
            oResult.Message = "Success";
            oResult.Value = oReturn;
            return oResult;

        }

        public ApiResult<List<GetAllOfferNames>> GetAllOfferNames()
        {
            ApiResult<List<GetAllOfferNames>> oResult = new ApiResult<List<GetAllOfferNames>>();

            if (DBContext.State == ConnectionState.Closed)
            {
                DBContext.Open();
            }

            var oReturn = DBContext.Query<GetAllOfferNames>("GetAllOfferNames", commandType: CommandType.StoredProcedure) as List<GetAllOfferNames>;
            DBContext.Close();

            oResult.Count = oReturn.Count;
            oResult.HasWarning = false;
            oResult.Message = "Success";
            oResult.Value = oReturn;
            return oResult;

        }

    }
}
