﻿using Dapper;
using EllowCart.Shared;
using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using EllowCart.DTO.Customer.OutputModels;
using EllowCart.DTO.Customer.InputModels;

namespace EllowCart.BusinessContracts.Roles
{
    public class CustomerService
    {

        private IDbConnection DBContext;
        private APiResult oResult = null;
        DAL.DBSettings _DBSettings = null;
        //private QueryBank.Roles.Admin oQoeryAdmin = null;


        public CustomerService(IDbConnection _DBContext)
        {
            this.DBContext = _DBContext;

        }
        public CustomerService()
        {
            _DBSettings = new DAL.DBSettings();  //-- New change
            DBContext = _DBSettings.GetCartDB();
        }

        public APiResult GetCustomerCodeUpdatePassword(DTO.Customer.InputModels.CustomerCodeSelect _getcustomer)
        {

            var out1 = "";
            var out2 = "";
            var stat = EllowCart.Shared.Cryptography.Encrypt_String(_getcustomer.Password, ref out1, ref out2);

            oResult = new APiResult();
            if (DBContext.State == ConnectionState.Closed)
            {
                DBContext.Open();
            }



            var oReturn = DBContext.Query("ForgotPassword_Customer", new
            {
                @PhoneNumber = _getcustomer.PhoneNumber,
                @Password = out1
            }, commandType: CommandType.StoredProcedure); 
            DBContext.Close();

            oResult.iRet = 1;
            oResult.sMessage = "Success";
            oResult.sResult = oReturn;
            return oResult;
        }

        public ApiResult<List<CustomerSignInOut>> CustomerSignIn(GetCustomerCodeModel _customerProfileUpdate)
        {
            ApiResult<List<CustomerSignInOut>> oResult = new ApiResult<List<CustomerSignInOut>>();

            if (DBContext.State == ConnectionState.Closed)
            {
                DBContext.Open();
            }

            var out1 = "";
            var out2 = "";
            var stat = EllowCart.Shared.Cryptography.Encrypt_String(_customerProfileUpdate.Password, ref out1, ref out2);

            var oReturn = DBContext.Query<CustomerSignInOut>("CustomerSignIn", new
            {
                @MobileNumber = _customerProfileUpdate.MobileNumber,
                @Password = out1,
                @EmailId = _customerProfileUpdate.EmailId
            }, commandType: CommandType.StoredProcedure) as List<CustomerSignInOut>;

            DBContext.Close();

            oResult.Count = oReturn.Count;
            oResult.HasWarning = false;
            oResult.Message = "Success";
            oResult.Value = oReturn;
            return oResult;
        }


        public ApiResult<List<CustomerRegisterationOut>> CustomerRegisteration(DTO.Customer.InputModels.CustomerRegisteration _customerRegisteration)
        {
            ApiResult<List<CustomerRegisterationOut>> oResult = new ApiResult<List<CustomerRegisterationOut>>();
            if (DBContext.State == ConnectionState.Closed)
            {
                DBContext.Open();
            }

            var dt = new DataTable("SecurityAnswer_Type");
            dt.Columns.Add("QusetionId", typeof(long));
            dt.Columns.Add("SequrityAns", typeof(string));
            var secqty = _customerRegisteration.DtSecurity;

            for (int i = 0; i < secqty.Count; i++)
            {
                dt.Rows.Add(secqty[i].QusetionId, secqty[i].SequrityAns);
            }

            var out1 = "";
            var out2 = "";
            var stat = EllowCart.Shared.Cryptography.Encrypt_String(_customerRegisteration.Password, ref out1, ref out2);

            var oReturn = DBContext.Query<CustomerRegisterationOut>("CustomerRegistration", new
            {
                @MobileNumber = _customerRegisteration.MobileNumber,
                @Name = _customerRegisteration.Name,
                @EmailId = _customerRegisteration.EmailId,
                @Password = out1,
                @Lattittude = _customerRegisteration.Lattittude,
                @Longittude = _customerRegisteration.Longittude,
                @LocationName = _customerRegisteration.LocationName,
                @DtSecurityAnswers = dt.AsTableValuedParameter("SecurityAnswer_Type")

            }, commandType: CommandType.StoredProcedure) as List<CustomerRegisterationOut>;
            DBContext.Close();

            oResult.Count = oReturn.Count;
            oResult.HasWarning = false;
            oResult.Message = "Success";
            oResult.Value = oReturn;
            return oResult;
        }

        public ApiResult<List<CustomerProfilepdateOut>> CustomerProfileUpdate(CustomerProfileUpdate _customerProfileUpdate)
        {
            ApiResult<List<CustomerProfilepdateOut>> oResult = new ApiResult<List<CustomerProfilepdateOut>>();

            if (DBContext.State == ConnectionState.Closed)
            {
                DBContext.Open();
            }

            var oReturn = DBContext.Query<CustomerProfilepdateOut>("CustomerProfileUpdates", _customerProfileUpdate, commandType: CommandType.StoredProcedure) as List<CustomerProfilepdateOut>;
            DBContext.Close();

            oResult.Count = oReturn.Count;
            oResult.HasWarning = false;
            oResult.Message = "Success";
            oResult.Value = oReturn;
            return oResult;
        }


        public ApiResult<List<GetCustomerDetailsOut>> GetCustomerDetails(GetCustomerDetailsinput _detailsinput)
        {
            ApiResult<List<GetCustomerDetailsOut>> oResult = new ApiResult<List<GetCustomerDetailsOut>>();

            if (DBContext.State == ConnectionState.Closed)
            {
                DBContext.Open();
            }

            var oReturn = DBContext.Query<GetCustomerDetailsOut>("GetCustomerprofileDetails", _detailsinput, commandType: CommandType.StoredProcedure) as List<GetCustomerDetailsOut>;
            DBContext.Close();

            oResult.Count = oReturn.Count;
            oResult.HasWarning = false;
            oResult.Message = "Success";
            oResult.Value = oReturn;
            return oResult;

        }


        public APiResult CustomerOrderDetails(CustomerOrder _details)
        {
            APiResult oResult = new APiResult();

            if (DBContext.State == ConnectionState.Closed)
            {
                DBContext.Open();
            }


            DetailsOrder _orderdetails = new DetailsOrder();

            var oReturn = DBContext.Query<OrderDetails>("CustomerOrderDetails", _details, commandType: CommandType.StoredProcedure) as List<OrderDetails>;

            var oReturn1 = DBContext.Query<OrderProductList>("OrderProductDetails", _details, commandType: CommandType.StoredProcedure) as List<OrderProductList>;

            var oReturn2 = DBContext.Query<ShippingAddress>("GetShippingAddress", _details, commandType: CommandType.StoredProcedure) as List<ShippingAddress>;

            var oReturn3 = DBContext.Query<OrderSummery>("GetOrderSummary", _details, commandType: CommandType.StoredProcedure) as List<OrderSummery>;



            _orderdetails.OrderDetails = oReturn;
            _orderdetails.Products = oReturn1;
            _orderdetails.ShippingAddress = oReturn2;
            _orderdetails.OrderSummery = oReturn3;


            DBContext.Close();

            oResult.iRet = 1;
            oResult.sError = "false";
            oResult.sMessage = "Success";
            oResult.sResult = _orderdetails;
            return oResult;

        }

        public ApiResult<List<GetMyOrdersOut>> GetMyOrderList(GetMyOrderListInput _details)
        {
            ApiResult<List<GetMyOrdersOut>> oResult = new ApiResult<List<GetMyOrdersOut>>();

            if (DBContext.State == ConnectionState.Closed)
            {
                DBContext.Open();
            }

            var oReturn = DBContext.Query<GetMyOrdersOut>("GetMyOrders_Customer", _details, commandType: CommandType.StoredProcedure) as List<GetMyOrdersOut>;
            DBContext.Close();

            oResult.Count = oReturn.Count;
            oResult.HasWarning = false;
            oResult.Message = "Success";
            oResult.Value = oReturn;
            return oResult;

        }

    }
}
