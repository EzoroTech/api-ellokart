﻿using Dapper;
using EllowCart.DTO.Delivery.InputModels;
using EllowCart.Shared;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace EllowCart.BusinessContracts.Roles
{
    public class DeliveryServices
    {

        private IDbConnection DBContext;
        private APiResult oResult = null;
        DAL.DBSettings _DBSettings = null;
        public DeliveryServices(IDbConnection _DBContext)
        {
            this.DBContext = _DBContext;

        }
        public DeliveryServices()
        {
            _DBSettings = new DAL.DBSettings();  //-- New change
            DBContext = _DBSettings.GetCartDB();
        }

        public APiResult Delivery(GetdataFromDeliverApp _input)
        {
            APiResult oResult = new APiResult();

            if (DBContext.State == ConnectionState.Closed)
            {
                DBContext.Open();
            }



            var oReturn = DBContext.Query("DeliveryDetailsForCustomer", new
            {
                @OrderId = _input.order_id,
                @JobId = _input.job_id,
                @DeliveryStatusId = _input.job_status

            }, commandType: CommandType.StoredProcedure);

            DBContext.Close();

            // oResult.Count = oReturn.Count;
            oResult.iRet = 1;
            oResult.sMessage = "Success";
            oResult.sResult = oReturn;

            DBContext.Close();
            return oResult;

        }

        public ApiResult<List<AllDeliveryMode>> DeliveryMode()
        {
            ApiResult< List < AllDeliveryMode >> oResult = new ApiResult<List<AllDeliveryMode>>();

            if (DBContext.State == ConnectionState.Closed)
            {
                DBContext.Open();
            }

            var oReturn = DBContext.Query<AllDeliveryMode>("GetAllDeliveryModes", commandType: CommandType.StoredProcedure)as List<AllDeliveryMode>;


            DBContext.Close();

            oResult.Count = oReturn.Count;
            oResult.HasWarning = false;
            oResult.Message = "Success";
            oResult.Value = oReturn;

            DBContext.Close();
            return oResult;

        }


    }
}
