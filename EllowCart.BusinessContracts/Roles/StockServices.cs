﻿using Dapper;
using EllowCart.DTO.Stock.InputModels;
using EllowCart.DTO.Stock.OutputModels;
using EllowCart.Shared;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using EllowCart.QueryBank.Roles;

namespace EllowCart.BusinessContracts.Roles
{
    public class StockServices
    {
        private IDbConnection DBContext;
        private APiResult oResult = null;
        DAL.DBSettings _DBSettings = null;
        private QueryBank.Roles.StockKeepingUnitManage OQuerySKU = null;

        public StockServices(IDbConnection _DBContext)
        {
            this.DBContext = _DBContext;

        }
        public StockServices()
        {
            _DBSettings = new DAL.DBSettings();  //-- New change
            DBContext = _DBSettings.GetCartDB();
        }

        public ApiResult<List<GetStockKeepingunitOut>> GetStockKeepingUnit()
        {
            ApiResult<List<GetStockKeepingunitOut>> oResult = new ApiResult<List<GetStockKeepingunitOut>>();

            if (DBContext.State == ConnectionState.Closed)
            {
                DBContext.Open();
            }


            var oReturn = DBContext.Query<GetStockKeepingunitOut>("GetStockKeepingUnit", commandType: CommandType.StoredProcedure) as List<GetStockKeepingunitOut>;
            DBContext.Close();
            oResult.Count = oReturn.Count;
            oResult.HasWarning = false;
            oResult.Message = "Success";
            oResult.Value = oReturn;
            return oResult;
        }


        public APiResult AddNewSKU(SKUinput _input)
        {
            oResult = new APiResult();
            string sSQlAddSku= string.Empty;
            OQuerySKU = new StockKeepingUnitManage();

            sSQlAddSku = string.Format(OQuerySKU.CreateSku(),
                                            _input.StockKeepingUnit, _input.IsActive,
                                            _input.CreatedOn, _input.CreatedBy,
                                            _input.UpdatedOn, _input.UpdatedBy);

            if (DBContext.State == ConnectionState.Closed)
            {
                DBContext.Open();
            }
            int iCount = DBContext.Execute(sSQlAddSku);
            if (iCount > 0)
            {
                oResult = new APiResult();
                oResult.iRet = 1;
                oResult.sMessage = "Successfully added Stock keeping unit";
            }
            DBContext.Close();
            return oResult;
        }

        //SKU Deletion by querybank
        
        public APiResult DeleteStockKeepingUnit(GetSKU _input)
        {

            oResult = new APiResult();
            string sSQlDeleteSku = string.Empty;
            OQuerySKU = new QueryBank.Roles.StockKeepingUnitManage();

            sSQlDeleteSku = string.Format(OQuerySKU.DeleteSKU(_input));


            if (DBContext.State == ConnectionState.Closed)
            {
                DBContext.Open();
            }

            int iCount = DBContext.Execute(sSQlDeleteSku);
            if (iCount > 0)
            {
                oResult = new APiResult();
                oResult.iRet = 1;
                oResult.sMessage = "Success";
            }
            DBContext.Close();
            return oResult;
        }

        //getting SKU by querybank
        public APiResult GetSKUById(GetSKU _SKUId)
        {
            oResult = new APiResult();
            OQuerySKU = new QueryBank.Roles.StockKeepingUnitManage();
            string ssqlGetAllSKU = OQuerySKU.GetAstockKeepingUnit(_SKUId);
            if (DBContext.State == ConnectionState.Closed)
            {
                DBContext.Open();
            }

            var oReturn = DBContext.Query(ssqlGetAllSKU);
            DBContext.Close();

            oResult.iRet = 1;
            oResult.sMessage = "Successfully collected the SKU";
            oResult.sResult = oReturn;
            return oResult;

        }

        public APiResult UpdateStockKeepingUnit(UpdateSKUmdl Obj)
        {

            oResult = new APiResult();
            string sSQlUpdateSKU = string.Empty;
            OQuerySKU = new QueryBank.Roles.StockKeepingUnitManage();
            int stockKeepingUnitId = Obj.StockKeepingUnitId;
            string stockKeepingUnit = Obj.StockKeepingUnit;
            DateTime CreatedOn = Obj.CreatedOn;
            string CreatedBy = Obj.CreatedBy;
            DateTime UpdatedOn = Obj.UpdatedOn;
            string UpdatedBy = Obj.UpdatedBy;
            bool IsActive = Obj.IsActive;

            sSQlUpdateSKU = string.Format(OQuerySKU.UpdateSKU(stockKeepingUnitId, stockKeepingUnit,  CreatedOn, CreatedBy, UpdatedOn, UpdatedBy, IsActive));


            if (DBContext.State == ConnectionState.Closed)
            {
                DBContext.Open();
            }

            int iCount = DBContext.Execute(sSQlUpdateSKU);
            if (iCount > 0)
            {
                oResult = new APiResult();
                oResult.iRet = 1;
                oResult.sMessage = "Success";
            }
            DBContext.Close();
            return oResult;
        }


    }
}
