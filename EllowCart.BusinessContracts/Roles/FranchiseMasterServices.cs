﻿using Dapper;
using EllowCart.DTO.Franchise.InputModel;
using EllowCart.DTO.Franchise.InputModels;
using EllowCart.QueryBank.Roles;
using EllowCart.Shared;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace EllowCart.BusinessContracts.Roles
{
    public class FranchiseMasterServices
    {

        private IDbConnection DBContext;
        private APiResult oResult = null;
        DAL.DBSettings _DBSettings = null;
        private QueryBank.Roles.FranchiseMasterManage OQueryFranchiseMaster = null;

        public FranchiseMasterServices(IDbConnection _DBContext)
        {
            this.DBContext = _DBContext;

        }
        public FranchiseMasterServices()
        {
            _DBSettings = new DAL.DBSettings();  //-- New change
            DBContext = _DBSettings.GetCartDB();
        }


        public APiResult AddNewFranchiseMaster(DTO.Franchise.InputModel.FranchiseInputModel _input)
        {
            oResult = new APiResult();
            string sSQlAddFranchiseMaster = string.Empty;
            OQueryFranchiseMaster = new FranchiseMasterManage();


            if (DBContext.State == ConnectionState.Closed)
            {
                DBContext.Open();
            }

            var oReturn = DBContext.Query("FranchiseRegistration", _input, commandType: CommandType.StoredProcedure);


                oResult = new APiResult();
                oResult.iRet = 1;
                oResult.sMessage = "Success";
           
            DBContext.Close();
            return oResult;
        }
        //Franchise Deletion by querybank
        public APiResult DeleteFranchiseMaster(FranchiseModel _input)
        {

            oResult = new APiResult();
            string sSQlDeleteFranchiseMaster = string.Empty;
            OQueryFranchiseMaster = new FranchiseMasterManage();

            sSQlDeleteFranchiseMaster = string.Format(OQueryFranchiseMaster.DeleteFranchiseMaster(_input));


            if (DBContext.State == ConnectionState.Closed)
            {
                DBContext.Open();
            }

            int iCount = DBContext.Execute(sSQlDeleteFranchiseMaster);
            if (iCount > 0)
            {
                oResult = new APiResult();
                oResult.iRet = 1;
                oResult.sMessage = "Success";
            }
            DBContext.Close();
            return oResult;
        }
        //Franchise Updation by querybank
        public APiResult UpdateFranchiseMaster(FranchiseUpdateModel Obj)
        {

            oResult = new APiResult();
            string sSQlUpdateFranchiseMaster = string.Empty;
            OQueryFranchiseMaster = new FranchiseMasterManage();
            int FranchiseId = Obj.FranchiseId;
            string franchiseCode = Obj.FranchiseCode;
            string PhoneNumber = Obj.PhoneNumber;
            string EmailId = Obj.EmailId;
            string FirstName = Obj.FirstName;
            string LastName = Obj.LastName;
            DateTime DOB = Obj.DOB;
            string Gender = Obj.Gender;
            string Password = Obj.Password;
            bool IsActive = Obj.IsActive;
           
            DateTime UpdatedOn = Obj.UpdatedOn;
            string UpdatedBy = Obj.UpdatedBy;


            sSQlUpdateFranchiseMaster = string.Format(OQueryFranchiseMaster.UpdateFranchiseMaster( PhoneNumber, EmailId, FirstName, LastName, DOB, Gender, Password, IsActive,  UpdatedOn, UpdatedBy, FranchiseId,franchiseCode));


            if (DBContext.State == ConnectionState.Closed)
            {
                DBContext.Open();
            }

            int iCount = DBContext.Execute(sSQlUpdateFranchiseMaster);
            if (iCount > 0)
            {
                oResult = new APiResult();
                oResult.iRet = 1;
                oResult.sMessage = "Success";
            }
            DBContext.Close();
            return oResult;
        }
        //getting Franchise
        public APiResult GetAllFranchise()
        {
            oResult = new APiResult();
            OQueryFranchiseMaster = new FranchiseMasterManage();
            string ssqlGetFranchise = OQueryFranchiseMaster.GetAllFranchiseMaster();
            if (DBContext.State == ConnectionState.Closed)
            {
                DBContext.Open();
            }

            var oReturn = DBContext.Query(ssqlGetFranchise);
            DBContext.Close();
            oResult.iRet = 1;
            oResult.sMessage = "Success";
            oResult.sResult = oReturn;
            return oResult;

        }
        //getting Franchise querybank
        public APiResult GetFranchiseById(FranchiseModel _franchiseId)
        {
            oResult = new APiResult();
            OQueryFranchiseMaster = new FranchiseMasterManage();
            string ssqlGetAllModules = OQueryFranchiseMaster.GetAFranchise(_franchiseId);
            if (DBContext.State == ConnectionState.Closed)
            {
                DBContext.Open();
            }

            var oReturn = DBContext.Query(ssqlGetAllModules);
            DBContext.Close();
            oResult.iRet = 1;
            oResult.sMessage = "Success";
            oResult.sResult = oReturn;
            return oResult;

        }

        public APiResult GeneratePaymentCode(GeneratePaymentCode _input)
        {
            oResult = new APiResult();
            OQueryFranchiseMaster = new FranchiseMasterManage();
           
            if (DBContext.State == ConnectionState.Closed)
            {
                DBContext.Open(); 

            }


            var oReturn = DBContext.Query("GetSellerCode", _input, commandType: CommandType.StoredProcedure);
           
            
            DBContext.Close();
            oResult.iRet = 1;
            oResult.sMessage = "Success";
            oResult.sResult = oReturn;
            return oResult;

        }


        public APiResult SellerDetailsByFranchise(SellerDetailsByFranchise _input)
        {
            oResult = new APiResult();
            OQueryFranchiseMaster = new FranchiseMasterManage();

            if (DBContext.State == ConnectionState.Closed)
            {
                DBContext.Open();

            }

            var oReturn = DBContext.Query("SellerDetailsByFranchise", _input, commandType: CommandType.StoredProcedure);
            DBContext.Close();
            oResult.iRet = 1;
            oResult.sMessage = "Success";
            oResult.sResult = oReturn;
            return oResult;

        }

    }
}
