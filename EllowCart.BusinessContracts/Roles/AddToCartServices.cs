﻿using Dapper;
using EllowCart.DTO.AddToCart.InputModels;
using EllowCart.DTO.AddToCart.OutputModels;
using EllowCart.Shared;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace EllowCart.BusinessContracts.Roles
{
    public class AddToCartServices
    {
        private IDbConnection DBContext;
        private APiResult oResult = null;
        DAL.DBSettings _DBSettings = null;
        public AddToCartServices(IDbConnection _DBContext)
        {
            this.DBContext = _DBContext;

        }
        public AddToCartServices()
        {
            _DBSettings = new DAL.DBSettings();  //-- New change
            DBContext = _DBSettings.GetCartDB();
        }

        public ApiResult<List<AddToCartOut>> AddToCart(AddToCart _cart)
        {
            ApiResult<List<AddToCartOut>> oResult = new ApiResult<List<AddToCartOut>>();

            if (DBContext.State == ConnectionState.Closed)
            {
                DBContext.Open();
            }

            if (_cart.BranchId != 0 || _cart.CustomerId != 0 || _cart.ProductId != 0)
            {

                var oReturn = DBContext.Query<AddToCartOut>("AddToCart", _cart, commandType: CommandType.StoredProcedure) as List<AddToCartOut>;

                DBContext.Close();

                oResult.Count = oReturn.Count;
                oResult.HasWarning = false;
                oResult.Message = "Success";
                oResult.Value = oReturn;
            }
            else
            {
                oResult.Message = "Must insert BranchId,CustomerId and ProductId";
            }
            DBContext.Close();
            return oResult;

        }

        public ApiResult<List<UpdateCartOut>> UpdateCart(UpdateCartInput _Cart)
        {
            ApiResult<List<UpdateCartOut>> oResult = new ApiResult<List<UpdateCartOut>>();

            if (DBContext.State == ConnectionState.Closed)
            {
                DBContext.Open();
            }

            var oReturn = DBContext.Query<UpdateCartOut>("UpdateCart", _Cart, commandType: CommandType.StoredProcedure) as List<UpdateCartOut>;
            DBContext.Close();

            oResult.Count = oReturn.Count;
            oResult.HasWarning = false;
            oResult.Message = "Success";
            oResult.Value = oReturn;
            return oResult;

        }

        public ApiResult<List<GetItemsFroCart>> GetItemsFromCart(SelectItems _items)
        {
            ApiResult<List<GetItemsFroCart>> oResult = new ApiResult<List<GetItemsFroCart>>();

            if (DBContext.State == ConnectionState.Closed)
            {
                DBContext.Open();
            }


            var oReturn = DBContext.Query<GetItemsFroCart>("BranchDetailsForCart", _items, commandType: CommandType.StoredProcedure) as List<GetItemsFroCart>;


            if (oReturn.Count != 0)
            {

                oResult.Count = oReturn[0].CartCount;

                for (int i = 0; i < oReturn.Count; i++)
                {


                    var oReturn1 = DBContext.Query<SelectItemsFromcart>("SelectItemsFromCart", new
                    {
                        @CustomerId = _items.CustomerId,
                        @BranchId = oReturn[i].BranchId
                    }, commandType: CommandType.StoredProcedure) as List<SelectItemsFromcart>;

                    oReturn[i].Items = oReturn1;
                }

            }
                DBContext.Close();
            

            oResult.HasWarning = false;
            oResult.Message = "Success";
            oResult.Value = oReturn;


            return oResult;

        }
        public ApiResult<List<GetCartListOut>> GetCartList(GetCartListInput _cartListInput)
        {
            ApiResult<List<GetCartListOut>> oResult = new ApiResult<List<GetCartListOut>>();

            if (DBContext.State == ConnectionState.Closed)
            {
                DBContext.Open();
            }

            var oReturn = DBContext.Query<GetCartListOut>("GetCartList", _cartListInput, commandType: CommandType.StoredProcedure) as List<GetCartListOut>;

            DBContext.Close();

            oResult.Count = oReturn.Count;
            oResult.HasWarning = false;
            oResult.Message = "Success";
            oResult.Value = oReturn;
            return oResult;

        }

        public ApiResult<List<GetCartcountout>> GetCartCount(GetCartCountInput _cartInput)
        {
            ApiResult<List<GetCartcountout>> oResult = new ApiResult<List<GetCartcountout>>();

            if (DBContext.State == ConnectionState.Closed)
            {
                DBContext.Open();
            }

            var oReturn = DBContext.Query<GetCartcountout>("GetCartCount", _cartInput, commandType: CommandType.StoredProcedure) as List<GetCartcountout>;


            DBContext.Close();

            oResult.Count = oReturn.Count;
            oResult.HasWarning = false;
            oResult.Message = "Success";
            oResult.Value = oReturn;
            return oResult;

        }


        public ApiResult<List<RemovefromCartOut>> RemoveFromCart(RemoveFromCart _cartInput)
        {
            ApiResult<List<RemovefromCartOut>> oResult = new ApiResult<List<RemovefromCartOut>>();

            if (DBContext.State == ConnectionState.Closed)
            {
                DBContext.Open();
            }

            var oReturn = DBContext.Query<RemovefromCartOut>("RemoveFromCart", _cartInput, commandType: CommandType.StoredProcedure) as List<RemovefromCartOut>;

            DBContext.Close();

            oResult.Count = oReturn.Count;
            oResult.HasWarning = false;
            oResult.Message = "Success";
            oResult.Value = oReturn;
            return oResult;

        }
    }
}
