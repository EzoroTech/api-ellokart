﻿using Dapper;
using EllowCart.DTO.Review.InputModels;
using EllowCart.DTO.Review.OutputModels;
using EllowCart.Shared;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace EllowCart.BusinessContracts.Roles
{
  public  class ReviewServices
    {
        private IDbConnection DBContext;

        DAL.DBSettings _DBSettings = null;
        public ReviewServices(IDbConnection _DBContext)
        {
            this.DBContext = _DBContext;

        }
        public ReviewServices()
        {
            _DBSettings = new DAL.DBSettings();  //-- New change
            DBContext = _DBSettings.GetCartDB();
        }

        public ApiResult<List<AddShopReviewout>> AddShopReview(AddShopReview _shopReview)
        {
            ApiResult<List<AddShopReviewout>> oResult = new ApiResult<List<AddShopReviewout>>();

            if (DBContext.State == ConnectionState.Closed)
            {
                DBContext.Open();
            }

            var oReturn = DBContext.Query<AddShopReviewout>("AddShopReviews", _shopReview, commandType: CommandType.StoredProcedure) as List<AddShopReviewout>;

            DBContext.Close();
            oResult.Count = oReturn.Count;
            oResult.HasWarning = false;
            oResult.Message = "Success";
            oResult.Value = oReturn;
            return oResult;

        }


        public ApiResult<List<GetShopReviewOut>> GetShopReview(GetShopReview _shopReview)
        {
            ApiResult<List<GetShopReviewOut>> oResult = new ApiResult<List<GetShopReviewOut>>();

            if (DBContext.State == ConnectionState.Closed)
            {
                DBContext.Open();
            }

            var oReturn = DBContext.Query<GetShopReviewOut>("GetShopReviews", _shopReview, commandType: CommandType.StoredProcedure) as List<GetShopReviewOut>;
            DBContext.Close();
            oResult.Count = oReturn.Count;
            oResult.HasWarning = false;
            oResult.Message = "Success";
            oResult.Value = oReturn;
            return oResult;

        }


        public ApiResult<List<AddProductReviewOut>> AddProductReview(AddProductReviewInput _productReview)
        {
            ApiResult<List<AddProductReviewOut>> oResult = new ApiResult<List<AddProductReviewOut>>();

            if (DBContext.State == ConnectionState.Closed)
            {
                DBContext.Open();
            }

            var oReturn = DBContext.Query<AddProductReviewOut>("AddProductReviews", _productReview, commandType: CommandType.StoredProcedure) as List<AddProductReviewOut>;
            DBContext.Close();
            oResult.Count = oReturn.Count;
            oResult.HasWarning = false;
            oResult.Message = "Success";
            oResult.Value = oReturn;
            return oResult;

        }
        public ApiResult<List<FollowStatusOut>> FollowOrUnfollow(FollowShop Obj)
        {
            ApiResult<List<FollowStatusOut>> oResult = new ApiResult<List<FollowStatusOut>>();

            if (DBContext.State == ConnectionState.Closed)
            {
                DBContext.Open();
            }

            var oReturn = DBContext.Query<FollowStatusOut>("FollowOrUnfollowShop", Obj, commandType: CommandType.StoredProcedure) as List<FollowStatusOut>;

            DBContext.Close();


            oResult.Count = oReturn.Count;
            oResult.HasWarning = false;
            oResult.Message = "Success";
            oResult.Value = oReturn;
            return oResult;



        }
        public ApiResult<List<FollowersListOut>> GetFollowers(GetFollowersList Obj)
        {
            ApiResult<List<FollowersListOut>> oResult = new ApiResult<List<FollowersListOut>>();

            if (DBContext.State == ConnectionState.Closed)
            {
                DBContext.Open();
            }

            var oReturn = DBContext.Query<FollowersListOut>("GetFollowersList", Obj, commandType: CommandType.StoredProcedure) as List<FollowersListOut>;

            DBContext.Close();


            oResult.Count = oReturn.Count;
            oResult.HasWarning = false;
            oResult.Message = "Success";
            oResult.Value = oReturn;
            return oResult;
            
        }

        public ApiResult<List<FollowSummaryOut>> FollowSummary(GetFollowSummary Obj)
        {
            ApiResult<List<FollowSummaryOut>> oResult = new ApiResult<List<FollowSummaryOut>>();

            if (DBContext.State == ConnectionState.Closed)
            {
                DBContext.Open();
            }

            var oReturn = DBContext.Query<FollowSummaryOut>("GetShopFollowingListForCustomer", Obj, commandType: CommandType.StoredProcedure) as List<FollowSummaryOut>;

            DBContext.Close();


            oResult.Count = oReturn.Count;
            oResult.HasWarning = false;
            oResult.Message = "Success";
            oResult.Value = oReturn;
            return oResult;

        }

    }
}
