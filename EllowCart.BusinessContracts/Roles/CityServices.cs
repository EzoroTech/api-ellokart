﻿using Dapper;
using EllowCart.DTO.City.InputModels;
using EllowCart.DTO.City.OutputModels;
using EllowCart.Shared;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace EllowCart.BusinessContracts.Roles
{
  public  class CityServices
    {
        private IDbConnection DBContext;
        DAL.DBSettings _DBSettings = null;
        public CityServices(IDbConnection _DBContext)
        {
            this.DBContext = _DBContext;

        }
        public CityServices()
        {
            _DBSettings = new DAL.DBSettings();  //-- New change
            DBContext = _DBSettings.GetCartDB();
        }
        public ApiResult<List<GetCityListOut>> GetCityList(GetCityListInput _getCityList)
        {
            ApiResult<List<GetCityListOut>> oResult = new ApiResult<List<GetCityListOut>>();

            if (DBContext.State == ConnectionState.Closed)
            {
                DBContext.Open();
            }

            var oReturn = DBContext.Query<GetCityListOut>("GetCityList", _getCityList, commandType: CommandType.StoredProcedure) as List<GetCityListOut>;
            DBContext.Close();
            oResult.Count = oReturn.Count;
            oResult.HasWarning = false;
            oResult.Message = "Success";
            oResult.Value = oReturn;

            return oResult;

        }
    }
}
