﻿using Dapper;
using EllowCart.DTO.Business.InputModels;
using EllowCart.DTO.Business.OutputModels;
using EllowCart.Shared;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace EllowCart.BusinessContracts.Roles
{
    public class BusinessServices
    {
        private IDbConnection DBContext;
        private APiResult oResult = null;
        DAL.DBSettings _DBSettings = null;

        private QueryBank.Roles.BusinessTypeManage oQoeryBusinessType = null;

        public BusinessServices(IDbConnection _DBContext)
        {
            this.DBContext = _DBContext;

        }
        public BusinessServices()
        {
            _DBSettings = new DAL.DBSettings();  //-- New change
            DBContext = _DBSettings.GetCartDB();
        }
        public APiResult AddBusinessType(BusinessType _input)
        {
            oResult = new APiResult();
            string sSQlAddBusinessType = string.Empty;
            oQoeryBusinessType = new QueryBank.Roles.BusinessTypeManage();

            sSQlAddBusinessType = string.Format(oQoeryBusinessType.GetAddBusinessType(),
                                            _input.BussinessTypeName, _input.BussinessTypeDecr,
                                            _input.CreatedOn, _input.CreatedBy, _input.UpdatedOn,
                                            _input.UpdatedBy, _input.IsActive);

            if (DBContext.State == ConnectionState.Closed)
            {
                DBContext.Open();
            }
            int iCount = DBContext.Execute(sSQlAddBusinessType);
            if (iCount > 0)
            {
                oResult = new APiResult();
                oResult.iRet = 1;
                oResult.sMessage = "Success";
            }
            DBContext.Close();
            return oResult;
        }
        //BusinessType Deletion by querybank
        public APiResult DeleteBusinessType(GetBusinessType _businessType)
        {

            oResult = new APiResult();
            string sSQlDeleteBusinessType = string.Empty;
            oQoeryBusinessType = new QueryBank.Roles.BusinessTypeManage();

            sSQlDeleteBusinessType = string.Format(oQoeryBusinessType.DeleteBusinessTypes(_businessType));


            if (DBContext.State == ConnectionState.Closed)
            {
                DBContext.Open();
            }

            int iCount = DBContext.Execute(sSQlDeleteBusinessType);
            if (iCount > 0)
            {
                oResult = new APiResult();
                oResult.iRet = 1;
                oResult.sMessage = "Success";
            }
            DBContext.Close();
            return oResult;
        }

        //BusinessType Updation by querybank
        public APiResult UpdateBusinessType(BusinessTypeForUpdation Obj)
        {

            oResult = new APiResult();
            string sSQlUpdateBusinessType = string.Empty;
            oQoeryBusinessType = new QueryBank.Roles.BusinessTypeManage();
            int BusinessTypeId = Obj.BussinessTypeId;
            string Name = Obj.BussinessTypeName;
            string Descr = Obj.BussinessTypeDecr;
            DateTime CreatedOn = Obj.CreatedOn;
            string CreatedBy = Obj.CreatedBy;
            DateTime UpdatedOn = Obj.UpdatedOn;
            string UpdatedBy = Obj.UpdatedBy;
            bool IsActive = Obj.IsActive;

            sSQlUpdateBusinessType = string.Format(oQoeryBusinessType.UpdateBusinessType(BusinessTypeId, Name, Descr, CreatedOn, CreatedBy, UpdatedOn, UpdatedBy, IsActive));


            if (DBContext.State == ConnectionState.Closed)
            {
                DBContext.Open();
            }

            int iCount = DBContext.Execute(sSQlUpdateBusinessType);
            if (iCount > 0)
            {
                oResult = new APiResult();
                oResult.iRet = 1;
                oResult.sMessage = "Success";
            }
            DBContext.Close();
            return oResult;
        }


        //getting BusinessType querybank
        public APiResult GetAllBusinessType()
        {
            oResult = new APiResult();
            oQoeryBusinessType = new QueryBank.Roles.BusinessTypeManage();
            string ssqlGetAllBusinessType = oQoeryBusinessType.GetAllBusinessType();
            if (DBContext.State == ConnectionState.Closed)
            {
                DBContext.Open();
            }

            var oReturn = DBContext.Query(ssqlGetAllBusinessType);
            DBContext.Close();
            oResult.iRet = 1;
            oResult.sMessage = "Success";
            oResult.sResult = oReturn;
            return oResult;

        }
        //getting businessType by querybank
        public APiResult GetBusinessTypeById(GetBusinessType _businessinput)
        {
            oResult = new APiResult();
            oQoeryBusinessType = new QueryBank.Roles.BusinessTypeManage();
            string ssqlGetAllBusinessType = oQoeryBusinessType.GetABusinessType(_businessinput);
            if (DBContext.State == ConnectionState.Closed)
            {
                DBContext.Open();
            }

            var oReturn = DBContext.Query(ssqlGetAllBusinessType);
            DBContext.Close();

            oResult.iRet = 1;
            oResult.sMessage = "Successfully collected the BusinessType";
            oResult.sResult = oReturn;
            return oResult;

        }



        public ApiResult<List<Business_type>> GetBusinessType(DTO.Business.InputModels.GetBusinessType _businesstype)
        {
            ApiResult<List<Business_type>> oResult = new ApiResult<List<Business_type>>();

            if (DBContext.State == ConnectionState.Closed)
            {
                DBContext.Open();
            }

            var oReturn = DBContext.Query<Business_type>("BussinessTypeSelect", _businesstype, commandType: CommandType.StoredProcedure) as List<Business_type>;

            DBContext.Close();

            oResult.Count = oReturn.Count;
            oResult.HasWarning = false;
            oResult.Message = "Success";
            oResult.Value = oReturn;
            return oResult;

        }

        public APiResult GetBusinessType_Admin(GetBusinessType _businessType)
        {
            oResult = new APiResult();
            if (DBContext.State == ConnectionState.Closed)
            {
                DBContext.Open();
            }
            var oReturn = DBContext.Query("BussinessTypeSelect", _businessType, commandType: CommandType.StoredProcedure);

            DBContext.Close();

            oResult.iRet = 1;
            oResult.sMessage = "Success";
            oResult.sResult = oReturn;
            return oResult;
        }


        public ApiResult<List<CreateBusinessOut>> CreateBusiness(DTO.Business.InputModels.CreateBusiness _business)
        {

            ApiResult<List<CreateBusinessOut>> oResult = new ApiResult<List<CreateBusinessOut>>();

            if (DBContext.State == ConnectionState.Closed)
            {
                DBContext.Open();
            }
            if (_business.BussinessTypeId != 0 || _business.SellercategoryId != 0 || _business.BussinessCategoryId != 0 || _business.UserId != 0 || _business.BussinesSystmId != 0)
            {

                var oReturn = DBContext.Query<CreateBusinessOut>("UserBussinessRegistration",_business,
                commandType: CommandType.StoredProcedure) as List<CreateBusinessOut>;
                DBContext.Close();

                oResult.Count = oReturn.Count;
                oResult.HasWarning = false;
                oResult.Message = "Success";
                oResult.Value = oReturn;
            }
            else
            {
                oResult.HasWarning = true;
                oResult.Message = "Must Need BussinessTypeId,SellercategoryId,bussinessCategoryId,UserId,BussinesSystmId";
            }
            return oResult;

        }
        public ApiResult<List<LoadBusiness>> LoadBusinessDetails(DTO.Business.InputModels.BusinessDetails _details)
        {
            ApiResult<List<LoadBusiness>> oResult = new ApiResult<List<LoadBusiness>>();

            if (DBContext.State == ConnectionState.Closed)
            {
                DBContext.Open();
            }

            var oReturn = DBContext.Query<LoadBusiness>("LoadBussinessDetails", _details, commandType: CommandType.StoredProcedure) as List<LoadBusiness>;
            DBContext.Close();

            oResult.Count = oReturn.Count;
            oResult.HasWarning = false;
            oResult.Message = "Success";
            oResult.Value = oReturn;
            return oResult;

        }


        public ApiResult<List<GetBusinessList>> GetBusinesslist(GetBusinessListInput _getBusiness)
        {
            ApiResult<List<GetBusinessList>> oResult = new ApiResult<List<GetBusinessList>>();

            if (DBContext.State == ConnectionState.Closed)
            {
                DBContext.Open();
            }

            var oReturn = DBContext.Query<GetBusinessList>("GetBusinessList", _getBusiness, commandType: CommandType.StoredProcedure) as List<GetBusinessList>;
            DBContext.Close();

            oResult.Count = oReturn.Count;
            oResult.HasWarning = false;
            oResult.Message = "Success";
            oResult.Value = oReturn;
            return oResult;

        }

        public ApiResult<List<AddBusinessAddressOut>> AddBusinessAddress(AddBusinessAddress _getBusiness)
        {
            ApiResult<List<AddBusinessAddressOut>> oResult = new ApiResult<List<AddBusinessAddressOut>>();

            if (DBContext.State == ConnectionState.Closed)
            {
                DBContext.Open();
            }

            var oReturn = DBContext.Query<AddBusinessAddressOut>("AddAddress", _getBusiness, commandType: CommandType.StoredProcedure) as List<AddBusinessAddressOut>;
            DBContext.Close();

            oResult.Count = oReturn.Count;
            oResult.HasWarning = false;
            oResult.Message = "Success";
            oResult.Value = oReturn;
            return oResult;

        }

    }
}
