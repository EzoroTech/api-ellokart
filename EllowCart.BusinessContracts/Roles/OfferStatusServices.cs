﻿using Dapper;
using EllowCart.DTO.OfferStatus.InputModels;
using EllowCart.DTO.OfferStatus.OutputModels;
using EllowCart.Shared;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace EllowCart.BusinessContracts.Roles
{
  public  class OfferStatusServices
    {

        private IDbConnection DBContext;

        DAL.DBSettings _DBSettings = null;
        public OfferStatusServices(IDbConnection _DBContext)
        {
            this.DBContext = _DBContext;

        }
        public OfferStatusServices()
        {
            _DBSettings = new DAL.DBSettings();  //-- New change
            DBContext = _DBSettings.GetCartDB();
        }


        public ApiResult<List<addOfferStatusOut>> AddOfferstatus(AddOfferStatus _offerstatus)
        {
            ApiResult<List<addOfferStatusOut>> oResult = new ApiResult<List<addOfferStatusOut>>();

            if (DBContext.State == ConnectionState.Closed)
            {
                DBContext.Open();
            }

            var oReturn = DBContext.Query<addOfferStatusOut>("AddOfferStatus", new
            {
                @UserId = _offerstatus.UserId,
                @role = _offerstatus.role,
                @BranchId = _offerstatus.BranchId,
                @MRP = _offerstatus.MRP,
                @OfferValue = _offerstatus.OfferValue,               
                @StatusName = _offerstatus.StatusName,
                @ImageId = _offerstatus.ImageId
            }, commandType: CommandType.StoredProcedure) as List<addOfferStatusOut>;
            DBContext.Close();

            oResult.Count = oReturn.Count;
            oResult.HasWarning = false;
            oResult.Message = "Success";
            oResult.Value = oReturn;
            return oResult;

        }

        public ApiResult<List<GetofferStatusListOut>> GetStatusList(GetOfferStatusList _statusList)
        {
            ApiResult<List<GetofferStatusListOut>> oResult = new ApiResult<List<GetofferStatusListOut>>();

            if (DBContext.State == ConnectionState.Closed)
            {
                DBContext.Open();
            }

            var oReturn = DBContext.Query<GetofferStatusListOut>("GetOfferStatusList", _statusList, commandType: CommandType.StoredProcedure) as List<GetofferStatusListOut>;
            DBContext.Close();

            oResult.Count = oReturn.Count;
            oResult.HasWarning = false;
            oResult.Message = "Success";
            oResult.Value = oReturn;
            return oResult;

        }


        public ApiResult<List<RemoveOfferStatus>> RemoveOfferStatus(RemoveOfferstatusInput _remove)
        {
            ApiResult<List<RemoveOfferStatus>> oResult = new ApiResult<List<RemoveOfferStatus>>();

            if (DBContext.State == ConnectionState.Closed)
            {
                DBContext.Open();
            }

            var oReturn = DBContext.Query<RemoveOfferStatus>("RemoveOfferStatus", _remove,  commandType: CommandType.StoredProcedure) as List<RemoveOfferStatus>;
            DBContext.Close();

            oResult.Count = oReturn.Count;
            oResult.HasWarning = false;
            oResult.Message = "Success";
            oResult.Value = oReturn;
            return oResult;

        }

        public ApiResult<List<UpdateStatusViewedOUT>> UpdateofferstatusViewed(UpdateStatusViewed _update)
        {
            ApiResult<List<UpdateStatusViewedOUT>> oResult = new ApiResult<List<UpdateStatusViewedOUT>>();

            if (DBContext.State == ConnectionState.Closed)
            {
                DBContext.Open();
            }

            var oReturn = DBContext.Query<UpdateStatusViewedOUT>("SetStatusViewed", _update, commandType: CommandType.StoredProcedure) as List<UpdateStatusViewedOUT>;
            DBContext.Close();

            oResult.Count = oReturn.Count;
            oResult.HasWarning = false;
            oResult.Message = "Success";
            oResult.Value = oReturn;
            return oResult;

        }

        public ApiResult<List<GetStatusListOut>> GetStatusList(GetStatusListInput _update)
        {
            ApiResult<List<GetStatusListOut>> oResult = new ApiResult<List<GetStatusListOut>>();

            if (DBContext.State == ConnectionState.Closed)
            {
                DBContext.Open();
            }

            var oReturn = DBContext.Query<GetStatusListOut>("GetStatusList", _update, commandType: CommandType.StoredProcedure) as List<GetStatusListOut>;
            DBContext.Close();

            oResult.Count = oReturn.Count;
            oResult.HasWarning = false;
            oResult.Message = "Success";
            oResult.Value = oReturn;
            return oResult;

        }

        public ApiResult<List<GetStatusByShopOut>> GetStatusListByShop(GetStatusByShop _StatusShop)
        {
            ApiResult<List<GetStatusByShopOut>> oResult = new ApiResult<List<GetStatusByShopOut>>();

            if (DBContext.State == ConnectionState.Closed)
            {
                DBContext.Open();
            }

            var oReturn = DBContext.Query<GetStatusByShopOut>("GetOfferStatusListOfShop", _StatusShop, commandType: CommandType.StoredProcedure) as List<GetStatusByShopOut>;
            DBContext.Close();

            oResult.Count = oReturn.Count;
            oResult.HasWarning = false;
            oResult.Message = "Success";
            oResult.Value = oReturn;
            return oResult;

        }

    }
}
