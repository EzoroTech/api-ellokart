﻿using Dapper;
using EllowCart.DTO.User.InputModels;
using EllowCart.DTO.User.OutputModels;
using EllowCart.Shared;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;


namespace EllowCart.BusinessContracts.Roles
{
    public class UserServices
    {
        private IDbConnection DBContext;
        private APiResult oResult = null;
        DAL.DBSettings _DBSettings = null;
        public UserServices(IDbConnection _DBContext)
        {
            this.DBContext = _DBContext;

        }
        public UserServices()
        {
            _DBSettings = new DAL.DBSettings();  //-- New change
            DBContext = _DBSettings.GetCartDB();
        }
        public ApiResult<List<UserRegistrationout>> UserRegisteration(DTO.User.InputModels.UserRegisteration _user)
        {
            ApiResult<List<UserRegistrationout>> oResult = new ApiResult<List<UserRegistrationout>>();
            if (DBContext.State == ConnectionState.Closed)
            {
                DBContext.Open();
            }

            var out1 = "";
            var out2 = "";
            var stat = EllowCart.Shared.Cryptography.Encrypt_String(_user.Password, ref out1, ref out2);

            /* ------For Security Question-------*/

            //var dt = new DataTable("SecurityAnswer_Type");
            //dt.Columns.Add("QusetionId", typeof(long));
            //dt.Columns.Add("SequrityAns", typeof(string));
            //var secqty = _user.DtSecurity;

            //for (int i = 0; i < secqty.Count; i++)
            //{
            //    dt.Rows.Add(secqty[i].QusetionId, secqty[i].SequrityAns);
            //}
            /* ------For Security Question-------*/

            var oReturn = DBContext.Query<UserRegistrationout>("UserRegistration", new
            {
                @MobileNo = _user.MobileNo,
                @EmailID = _user.EmailID,
                @Password = out1
                //@FranchiseCode = _user.FranchiseCode,
                //@DtSecurityAnswers = dt.AsTableValuedParameter("SecurityAnswer_Type")
            }, commandType: CommandType.StoredProcedure) as List<UserRegistrationout>;

            DBContext.Close();

            oResult.Count = oReturn.Count;
            oResult.HasWarning = false;
            oResult.Message = "Success";
            oResult.Value = oReturn;
            return oResult;
        }

        public ApiResult<List<UserDetails>> GetUser(DTO.User.InputModels.GetUser _getuser)
        {
            ApiResult<List<UserDetails>> oResult = new ApiResult<List<UserDetails>>();
            if (DBContext.State == ConnectionState.Closed)
            {
                DBContext.Open();
            }

            var oReturn = DBContext.Query<UserDetails>("GetUser", _getuser, commandType: CommandType.StoredProcedure) as List<UserDetails>;

            string pass = oReturn[0].Password;
            var out1 = "";
            var out2 = "";
            var ps = EllowCart.Shared.Cryptography.Decrypt_String(pass, ref out1, ref out2);

            oReturn[0].Password = out1;
            DBContext.Close();

            oResult.Count = oReturn.Count;
            oResult.HasWarning = false;
            oResult.Message = "Success";
            oResult.Value = oReturn;
            return oResult;
        }

        public ApiResult<List<SecurityAnsOut>> CheckSecurityQuestion(DTO.User.InputModels.SecurityQustnCheck _getans)
        {
            ApiResult<List<SecurityAnsOut>> oResult = new ApiResult<List<SecurityAnsOut>>();
            if (DBContext.State == ConnectionState.Closed)
            {
                DBContext.Open();
            }

            var oReturn = DBContext.Query<SecurityAnsOut>("CheckSecurityQuestion", _getans, commandType: CommandType.StoredProcedure) as List<SecurityAnsOut>;
            DBContext.Close();

            oResult.Count = oReturn.Count;
            oResult.HasWarning = false;
            oResult.Message = "Success";
            oResult.Value = oReturn;
            return oResult;
        }




        public ApiResult<List<ForgotpasswordOut>> GetUserCodePasswordUpdate(DTO.User.InputModels.UserCodeSelect _getuser)
        {
            ApiResult<List<ForgotpasswordOut>> oResult = new ApiResult<List<ForgotpasswordOut>>();

            var out1 = "";
            var out2 = "";
            var stat = EllowCart.Shared.Cryptography.Encrypt_String(_getuser.Password, ref out1, ref out2);

            if (DBContext.State == ConnectionState.Closed)
            {
                DBContext.Open();
            }
             
            var oReturn = DBContext.Query<ForgotpasswordOut>("ForgotPassword_User", new {
                @MobileNumber = _getuser.MobileNumber,
                @Password = out1
            }, commandType: CommandType.StoredProcedure) as List<ForgotpasswordOut>;
            DBContext.Close();

            oResult.Count = oReturn.Count;
            oResult.HasWarning = false;
            oResult.Message = "Success";
            oResult.Value = oReturn;
            return oResult;
        }


        public ApiResult<List<SignIn>> UserSignIn(GetUserCode _getuser)
        {
            ApiResult<List<SignIn>> oResult = new ApiResult<List<SignIn>>();
            if (DBContext.State == ConnectionState.Closed)
            {
                DBContext.Open();
            }

            var out1 = "";
            var out2 = "";
            var stat = EllowCart.Shared.Cryptography.Encrypt_String(_getuser.Password, ref out1, ref out2);

            var oReturn = DBContext.Query<SignIn>("SellerSignIn", new
            {
                @MobileNumber = _getuser.MobileNumber,
                @Password = out1,
                @EmailId = _getuser.EmailId
            }, commandType: CommandType.StoredProcedure) as List<SignIn>;
            DBContext.Close();


            oResult.Count = oReturn.Count;
            oResult.HasWarning = false;
            oResult.Message = "Success";
            oResult.Value = oReturn;
            return oResult;
        }


        public ApiResult<List<AddGSTOut>> AddGST(AddGST _gst)
        {
            ApiResult<List<AddGSTOut>> oResult = new ApiResult<List<AddGSTOut>>();
            if (DBContext.State == ConnectionState.Closed)
            {
                DBContext.Open();
            }

            var oReturn = DBContext.Query<AddGSTOut>("AddGST",_gst, commandType: CommandType.StoredProcedure) as List<AddGSTOut>;

            DBContext.Close();

            oResult.Count = oReturn.Count;
            oResult.HasWarning = false;
            oResult.Message = "Success";
            oResult.Value = oReturn;
            return oResult;
        }
    }
}
