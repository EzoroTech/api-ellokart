﻿using Dapper;
using EllowCart.DTO.General.InputModels;
using EllowCart.DTO.General.OutputModels;
using EllowCart.DTO.BusinessSystem.InputModels;
using EllowCart.Shared;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using EllowCart.DTO.Product.OutputModels;
using EllowCart.DTO.PaymentMethods.InputModels;

namespace EllowCart.BusinessContracts.Roles
{
    public class GeneralServices
    {
        private IDbConnection DBContext;
        private APiResult oResult = null;
        DAL.DBSettings _DBSettings = null;
        public GeneralServices(IDbConnection _DBContext)
        {
            this.DBContext = _DBContext;

        }
        public GeneralServices()
        {
            _DBSettings = new DAL.DBSettings();  //-- New change
            DBContext = _DBSettings.GetCartDB();
        }

        public ApiResult<List<CheckExisting>> Checkemailorph(DTO.General.InputModels.Checkemailorphno _check)
        {
            ApiResult<List<CheckExisting>> oResult = new ApiResult<List<CheckExisting>>();

            if (DBContext.State == ConnectionState.Closed)
            {
                DBContext.Open();
            }

            var oReturn = DBContext.Query<CheckExisting>("CheckExistingNumberOrEmail", _check, commandType: CommandType.StoredProcedure) as List<CheckExisting>;

            DBContext.Close();

            oResult.Count = 0;
            oResult.HasWarning = false;
            oResult.Message = "Success";
            oResult.Value = oReturn;
            return oResult;
        }
        public APiResult ManageOTPAsync(DTO.General.InputModels.ManageOTP _otp)
        {
            APiResult oResult = new APiResult();
            _DBSettings = new DAL.DBSettings();  //-- New change
            DBContext = _DBSettings.GetCartDB();

            if (DBContext.State == ConnectionState.Closed)
            {
                DBContext.Open();
            }
            ManageOTP_Out otp = new ManageOTP_Out();

            var result = DBContext.Query<ManageOTP_Out>("Manage_OTP",
               new
               {
                   @MobileNumber = _otp.mobile,
                   @STATUS = 0,
                   @Flag = 0
               }, commandType: CommandType.StoredProcedure) as List<ManageOTP_Out>;


            oResult.sResult = result;
            DBContext.Close();

            return oResult;
        }

        public ApiResult<List<GetTaxOut>> GetTax()
        {
            ApiResult<List<GetTaxOut>> oResult = new ApiResult<List<GetTaxOut>>();

            if (DBContext.State == ConnectionState.Closed)
            {
                DBContext.Open();
            }

            var oReturn = DBContext.Query<GetTaxOut>("GetTax", commandType: CommandType.StoredProcedure) as List<GetTaxOut>;
            DBContext.Close();

            oResult.Count = oReturn.Count;
            oResult.HasWarning = false;
            oResult.Message = "Success";
            oResult.Value = oReturn;
            return oResult;
        }

        public ApiResult<List<GetBrandOut>> GetBrand()
        {
            ApiResult<List<GetBrandOut>> oResult = new ApiResult<List<GetBrandOut>>();

            if (DBContext.State == ConnectionState.Closed)
            {
                DBContext.Open();
            }

            var oReturn = DBContext.Query<GetBrandOut>("GetBrand", commandType: CommandType.StoredProcedure) as List<GetBrandOut>;
            DBContext.Close();

            oResult.Count = oReturn.Count;
            oResult.HasWarning = false;
            oResult.Message = "Success";
            oResult.Value = oReturn;
            return oResult;
        }

        public ApiResult<List<GetSecurityQuestion>> GetSecurityQuestion()
        {
            ApiResult<List<GetSecurityQuestion>> oResult = new ApiResult<List<GetSecurityQuestion>>();

            if (DBContext.State == ConnectionState.Closed)
            {
                DBContext.Open();
            }

            var oReturn = DBContext.Query<GetSecurityQuestion>("GetSecurityQuestions", commandType: CommandType.StoredProcedure) as List<GetSecurityQuestion>;
            DBContext.Close();

            oResult.Count = oReturn.Count;
            oResult.HasWarning = false;
            oResult.Message = "Success";
            oResult.Value = oReturn;
            return oResult;
        }




        public ApiResult<List<SecurityQustnAnsCheckOut>> SecurityQustnAnsCheck(SecurityQustnAnsCheck _input)
        {
            ApiResult<List<SecurityQustnAnsCheckOut>> oResult = new ApiResult<List<SecurityQustnAnsCheckOut>>();

            if (DBContext.State == ConnectionState.Closed)
            {
                DBContext.Open();
            }

            var oReturn = DBContext.Query<SecurityQustnAnsCheckOut>("CheckSecurityQustnAnsForUser", _input, commandType: CommandType.StoredProcedure) as List<SecurityQustnAnsCheckOut>;
            DBContext.Close();

            oResult.Count = oReturn.Count;
            oResult.HasWarning = false;
            oResult.Message = "Success";
            oResult.Value = oReturn;
            return oResult;
        }
        public APiResult DeactivateABusiness(UserBusiness _businessInput)
        {

            oResult = new APiResult();

            if (DBContext.State == ConnectionState.Closed)
            {
                DBContext.Open();
            }


            var oReturn = DBContext.Query("DeActivateVerifiedSeller", _businessInput, commandType: CommandType.StoredProcedure);


            DBContext.Close();
            oResult.iRet = 1;
            oResult.sMessage = "Success";
            oResult.sResult = oReturn;
            return oResult;

        }

        public ApiResult<List<GetAllproductList>> GetAllProductsList(AllProductList _input)
        {

            ApiResult<List<GetAllproductList>> oResult = new ApiResult<List<GetAllproductList>>();

            if (DBContext.State == ConnectionState.Closed)
            {
                DBContext.Open();
            }


            var oReturn = DBContext.Query<GetAllproductList>("GetAllProductList", _input, commandType: CommandType.StoredProcedure) as List<GetAllproductList>;

            for (int i = 0; i < oReturn.Count; i++)
            {
                var oReturn2 = DBContext.Query<GetProductImages>("GetProductImages",
               new
               {
                   @BranchId = oReturn[i].BranchId,
                   @ProductId = oReturn[i].ProductID
               }, commandType: CommandType.StoredProcedure) as List<GetProductImages>;
                oReturn[i].Images = oReturn2;
            }





            DBContext.Close();
            oResult.Count = oReturn.Count;
            oResult.HasWarning = false;
            oResult.Message = "Success";
            oResult.Value = oReturn;
            return oResult;

        }

        public ApiResult<List<RazorPayInputs>> GetPaymentGateways(GetPaymentForPlanInput _input)
        {

            ApiResult<List<RazorPayInputs>> oResult = new ApiResult<List<RazorPayInputs>>();

            if (DBContext.State == ConnectionState.Closed)
            {
                DBContext.Open();
            }

            long planid = 0;

            if (_input.Flag == "UPGRADE_PLAN")
            {
                planid = _input.Others;
            }
            
            var oReturn = DBContext.Query<RazorPayInputs>("GetPlanDetailsForPlan", new
            {
                @PlanId = planid

            }, commandType: CommandType.StoredProcedure) as List<RazorPayInputs>;



            DBContext.Close();

            oResult.Count = 1;
            oResult.HasWarning = false;
            oResult.Message = "Success";
            oResult.Value = oReturn;

            return oResult;

        }

        public APiResult InsertGatewayIdToTable(UserPaidOrNot _paid)
        {
            APiResult oResult = new APiResult();
            if (DBContext.State == ConnectionState.Closed)
            {
                DBContext.Open();
            }

            var oReturn = DBContext.Query("CheckUserPaidOrNot",_paid , commandType: CommandType.StoredProcedure);


            DBContext.Close();

            return oResult;
        }

    }
}
