﻿using Dapper;
using EllowCart.DTO.ProductBrand.InputModels;
using EllowCart.DTO.ProductBrand.OutputModels;
using EllowCart.QueryBank.Roles;
using EllowCart.Shared;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace EllowCart.BusinessContracts.Roles
{
    public class ProductBrandServices
    {
        private IDbConnection DBContext;

        DAL.DBSettings _DBSettings = null;
        private ProductBrandManage oQoeryProductBrandManage;
        private APiResult oResult;
        private ProductBrandManage pdtBrand;
        private ProductBrandManage oQoeryBrand;




        public ProductBrandServices(IDbConnection _DBContext)
        {
            this.DBContext = _DBContext;

        }
        public ProductBrandServices()
        {
            _DBSettings = new DAL.DBSettings();  //-- New change
            DBContext = _DBSettings.GetCartDB();
        }

        public APiResult CreateProductBrand(ProductBrandInput _input)
        {
          



            oResult = new APiResult();
            string sSQlAddProductBrand = string.Empty;
           oQoeryProductBrandManage = new QueryBank.Roles.ProductBrandManage();

            sSQlAddProductBrand = string.Format(oQoeryProductBrandManage.GetAddProductBrand(),
                                            _input.BrandCode, _input.BrandName,_input.IsActive,
                                            _input.Description,_input.ProductCategoryId,_input.ManufactureName,
                                            _input.CreatedOn, _input.CreatedBy, _input.UpdatedOn
                                            );

            if (DBContext.State == ConnectionState.Closed)
            {
                DBContext.Open();
            }
            int iCount = DBContext.Execute(sSQlAddProductBrand);
            if (iCount > 0)
            {
                oResult = new APiResult();
                oResult.iRet = 1;
                oResult.sMessage = "Success";
            }
            DBContext.Close();
            return oResult;
        }


        public APiResult ProductBrandGetAll()
        {
            oResult = new APiResult();
            string sSQlGetProductBrand = string.Empty;
            oQoeryProductBrandManage = new QueryBank.Roles.ProductBrandManage();

            sSQlGetProductBrand = string.Format(oQoeryProductBrandManage.GetAllProductBrands());


            if (DBContext.State == ConnectionState.Closed)
            {
                DBContext.Open();
            }

            var oReturn = DBContext.Query(sSQlGetProductBrand);

            oResult = new APiResult();
            oResult.iRet = 1;
            oResult.sMessage = "Success";
            oResult.sResult = oReturn;

            DBContext.Close();
            return oResult;

        }

        public ApiResult<List<GetProductBrandOut>> GetProductBrandAdmin(GetProductBrandInput _input)
        {
            ApiResult<List<GetProductBrandOut>> oResult = new ApiResult<List<GetProductBrandOut>>();

            if (DBContext.State == ConnectionState.Closed)
            {
                DBContext.Open();
            }

            var oReturn = DBContext.Query<GetProductBrandOut>("ProductBrandMasterSelect", _input, commandType: CommandType.StoredProcedure) as List<GetProductBrandOut>;
            DBContext.Close();

            oResult.Count = oReturn.Count;
            oResult.HasWarning = false;
            oResult.Message = "Success";
            oResult.Value = oReturn;
            return oResult;
        }

        public ApiResult<List<GetProductBrand>> GetProductBrandForProductCateg(GetProductCategForParent _input)
        {
            ApiResult<List<GetProductBrand>> oResult = new ApiResult<List<GetProductBrand>>();

            if (DBContext.State == ConnectionState.Closed)
            {
                DBContext.Open();
            }

            var oReturn = DBContext.Query<GetProductBrand>("ProductBrandByProductCateg", _input, commandType: CommandType.StoredProcedure) as List<GetProductBrand>;
            DBContext.Close();

            oResult.Count = oReturn.Count;
            oResult.HasWarning = false;
            oResult.Message = "Success";
            oResult.Value = oReturn;
            return oResult;
        }


        //ProductBrand Deletion by querybank
        public APiResult DeleteProductBrand(GetProductBrandInput _input)
        {

            oResult = new APiResult();
            string sSQlProductBrand = string.Empty;
            pdtBrand = new QueryBank.Roles.ProductBrandManage();

            sSQlProductBrand = string.Format(pdtBrand.DeleteProductBrand(_input.BrandId));


            if (DBContext.State == ConnectionState.Closed)
            {
                DBContext.Open();
            }

            int iCount = DBContext.Execute(sSQlProductBrand);
            if (iCount > 0)
            {
                oResult = new APiResult();
                oResult.iRet = 1;
                oResult.sMessage = "Success";
            }
            DBContext.Close();
            return oResult;
        }


        //ProductBrand Updation by querybank
        public APiResult UpdateProductBrand(ProductBrandInputForUpdation Obj)
        {

            oResult = new APiResult();
            string sSQlUpdateProductBrand = string.Empty;
            pdtBrand = new QueryBank.Roles.ProductBrandManage();
            long brandId = Obj.BrandId;
            string BrandName = Obj.BrandName;
            string brandCode = Obj.BrandCode;
            string BrandDescription = Obj.BrandDescription;
            bool IsActive = Obj.IsActive;
            DateTime UpdatedOn = Obj.UpdatedOn;
            string UpdatedBy = Obj.UpdatedBy;
            string manufactureName = Obj.ManufactureName;
            int ProductCategoryID = Obj.ProductCategoryId;

            sSQlUpdateProductBrand = string.Format(pdtBrand.UpdateProductBrand( brandCode, BrandName,  IsActive, UpdatedOn, UpdatedBy, BrandDescription, manufactureName, ProductCategoryID, Obj.BrandId));


            if (DBContext.State == ConnectionState.Closed)
            {
                DBContext.Open();
            }

            int iCount = DBContext.Execute(sSQlUpdateProductBrand);
            if (iCount > 0)
            {
                oResult = new APiResult();
                oResult.iRet = 1;
                oResult.sMessage = "Success";
            }
            DBContext.Close();
            return oResult;
        }


        //getting product brand querybank
        public APiResult GetProductBrandById(GetProductBrandInput _brand)
        {
            oResult = new APiResult();
            pdtBrand = new QueryBank.Roles.ProductBrandManage();
            string ssqlGetAProductBrand = pdtBrand.GetAProductBrand(_brand.BrandId);
            if (DBContext.State == ConnectionState.Closed)
            {
                DBContext.Open();
            }

            var oReturn = DBContext.Query(ssqlGetAProductBrand);
            DBContext.Close();
            oResult.iRet = 1;
            oResult.sMessage = "Success";
            oResult.sResult = oReturn;
            return oResult;

        }





    }
}
