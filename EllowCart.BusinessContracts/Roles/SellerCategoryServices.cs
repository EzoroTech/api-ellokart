﻿using Dapper;
using EllowCart.DTO.SellerCategory.InputModels;
using EllowCart.DTO.SellerCategory.OutputModels;
using EllowCart.Shared;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace EllowCart.BusinessContracts.Roles
{
   public class SellerCategoryServices
    {
        private IDbConnection DBContext;
        private APiResult oResult = null;
        private QueryBank.Roles.SellerCategoryManage oQuerySellerCateg = null;
        DAL.DBSettings _DBSettings = null;
        public SellerCategoryServices(IDbConnection _DBContext)
        {
            this.DBContext = _DBContext;

        }
        public SellerCategoryServices()
        {
            _DBSettings = new DAL.DBSettings();  //-- New change
            DBContext = _DBSettings.GetCartDB();
        }

        public APiResult AddNewSellerCategory(SellerCategory _input)
        {
            oResult = new APiResult();
            string sSQlAddSellerCateg = string.Empty;
            oQuerySellerCateg = new QueryBank.Roles.SellerCategoryManage();

            sSQlAddSellerCateg = string.Format(oQuerySellerCateg.GetAddSellerCategory(),
                                            _input.SellerCatName,
                                            _input.IsActive, _input.CreatedOn,
                                            _input.CreatedBy, _input.UpdatedOn,
                                            _input.UpdatedBy, _input.BussinessTypeId);

            if (DBContext.State == ConnectionState.Closed)
            {
                DBContext.Open();
            }
            int iCount = DBContext.Execute(sSQlAddSellerCateg);
            if (iCount > 0)
            {
                oResult = new APiResult();
                oResult.iRet = 1;
                oResult.sMessage = "Success";
            }
            DBContext.Close();
            return oResult;
        }

        //Module Deletion by querybank
        public APiResult DeleteSellerCateg(SellerCategoryGetAdmin _input)
        {

            oResult = new APiResult();
            string sSQlDeleteSellerCateg = string.Empty;
            oQuerySellerCateg = new QueryBank.Roles.SellerCategoryManage();

            sSQlDeleteSellerCateg = string.Format(oQuerySellerCateg.DeleteSellerCategory(_input));


            if (DBContext.State == ConnectionState.Closed)
            {
                DBContext.Open();
            }

            int iCount = DBContext.Execute(sSQlDeleteSellerCateg);
            if (iCount > 0)
            {
                oResult = new APiResult();
                oResult.iRet = 1;
                oResult.sMessage = "Success";
            }
            DBContext.Close();
            return oResult;
        }


        //SellerCateg Updation by querybank
        public APiResult UpdateSellerCategory(SellerCategInputForUpdation Obj)
        {

            oResult = new APiResult();
            string sSQlUpdateSellerCateg = string.Empty;
            oQuerySellerCateg = new QueryBank.Roles.SellerCategoryManage();
            int SellerCatId = Obj.SellerCatId;
            string sellerCategName = Obj.SellerCatName;
            bool IsActive = Obj.IsActive;
            DateTime CreatedOn = Obj.CreatedOn;
            string CreatedBy = Obj.CreatedBy;
            DateTime UpdatedOn = Obj.UpdatedOn;
            string UpdatedBy = Obj.UpdatedBy;
            int BusinessTypeId = Obj.BussinessTypeId;

            sSQlUpdateSellerCateg = string.Format(oQuerySellerCateg.UpdateSellerCategory(SellerCatId, sellerCategName, IsActive, CreatedOn, CreatedBy, UpdatedOn, UpdatedBy, BusinessTypeId));


            if (DBContext.State == ConnectionState.Closed)
            {
                DBContext.Open();
            }

            int iCount = DBContext.Execute(sSQlUpdateSellerCateg);
            if (iCount > 0)
            {
                oResult = new APiResult();
                oResult.iRet = 1;
                oResult.sMessage = "Success";
            }
            DBContext.Close();
            return oResult;
        }
        //getting SellerCategory querybank
        public APiResult GetAllSellerCategory()
        {
            oResult = new APiResult();
            oQuerySellerCateg = new QueryBank.Roles.SellerCategoryManage();
            string ssqlGetAllSellerCategory = oQuerySellerCateg.GetAllSellerCategory();
            if (DBContext.State == ConnectionState.Closed)
            {
                DBContext.Open();
            }

            //var oReturn = DBContext.Query<SellerCategInputForUpdation>(ssqlGetAllSellerCategory);
            var oReturn = DBContext.Query(ssqlGetAllSellerCategory);
            DBContext.Close();
            oResult.iRet = 1;
            oResult.sMessage = "Success";
            oResult.sResult = oReturn;
            return oResult;

        }
        //getting SellerCateg querybank
        public APiResult GetSellerCategById(SellerCategoryGetAdmin _sellerCategOut)
        {
            oResult = new APiResult();
            oQuerySellerCateg = new QueryBank.Roles.SellerCategoryManage();
            string ssqlGetAllSellerCateg = oQuerySellerCateg.GetASellerCateg(_sellerCategOut);
            if (DBContext.State == ConnectionState.Closed)
            {
                DBContext.Open();
            }

            var oReturn = DBContext.Query(ssqlGetAllSellerCateg);
            DBContext.Close();
            oResult.iRet = 1;
            oResult.sMessage = "Success";
            oResult.sResult = oReturn;
            return oResult;

        }



        public ApiResult<List<SellerCategoryDetails>> GetSellerCategory(DTO.SellerCategory.InputModels.GetSellerCategory _Seller)
        {
            ApiResult<List<SellerCategoryDetails>> oResult = new ApiResult<List<SellerCategoryDetails>>();

            if (DBContext.State == ConnectionState.Closed)
            {
                DBContext.Open();
            }

            var oReturn = DBContext.Query < SellerCategoryDetails >("SellerCategorySelect", _Seller, commandType: CommandType.StoredProcedure)as List<SellerCategoryDetails>;

            oResult.Count = oReturn.Count;
            oResult.HasWarning = false;
            oResult.Message = "Success";
            oResult.Value = oReturn;
            return oResult;

        }

        public ApiResult<List<GetSellerCategoryCustomer>> GetSellerCategoryCustomer()
        {
            ApiResult<List<GetSellerCategoryCustomer>> oResult = new ApiResult<List<GetSellerCategoryCustomer>>();

            if (DBContext.State == ConnectionState.Closed)
            {
                DBContext.Open();
            }

            var oReturn = DBContext.Query<GetSellerCategoryCustomer>("GetSellerCategory", commandType: CommandType.StoredProcedure) as List<GetSellerCategoryCustomer>;

            oResult.Count = oReturn.Count;
            oResult.HasWarning = false;
            oResult.Message = "Success";
            oResult.Value = oReturn;
            return oResult;

        }

    }
}
