﻿using System;
using Dapper;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using EllowCart.DTO.Search.InputModels;
using EllowCart.Shared;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using EllowCart.QueryBank.Roles;
using EllowCart.DTO.Search.OutputModels;
using EllowCart.DTO.Product.OutputModels;

namespace EllowCart.BusinessContracts.Roles


{
   public class SearchServicesForAdmin
    {
        private IDbConnection DBContext;
        private APiResult oResult = null;
        DAL.DBSettings _DBSettings = null;

        public SearchServicesForAdmin(IDbConnection _DBContext)
        {
            this.DBContext = _DBContext;

        }
        public SearchServicesForAdmin()
        {
            _DBSettings = new DAL.DBSettings();  //-- New change
            DBContext = _DBSettings.GetCartDB();
        }





        //////public ApiResult<List<GetSellerOut>> GetSellerByMobileNo(Search _obj)
        //////{
        //////    ApiResult<List<GetSellerOut>> oResult = new ApiResult<List<GetSellerOut>>();

        //////    if (DBContext.State == ConnectionState.Closed)
        //////    {
        //////        DBContext.Open();
        //////    }

        //////    var oReturn = DBContext.Query<GetSellerOut>("SellerSearchByMobNo", _obj, commandType: CommandType.StoredProcedure) as List<GetSellerOut>;
        //////    DBContext.Close();

        //////    oResult.Count = oReturn.Count;
        //////    oResult.HasWarning = false;
        //////    oResult.Message = "Success";
        //////    oResult.Value = oReturn;
        //////    return oResult;
        //////}

        public APiResult GetSellerByMobileNo(Search _obj)
        {
            APiResult oResult = new APiResult();


            if (DBContext.State == ConnectionState.Closed)
            {
                DBContext.Open();
            }

            var oReturn = DBContext.Query("SellerSearchByMobNo", _obj, commandType: CommandType.StoredProcedure);

            DBContext.Close();

            oResult.iRet = 1;
            oResult.sMessage = "Success";
            oResult.sResult = oReturn;

            return oResult;


        }


        public APiResult verifiedSellerByMobileNo(Search _obj)
        {
            APiResult oResult = new APiResult();


            if (DBContext.State == ConnectionState.Closed)
            {
                DBContext.Open();
            }

            var oReturn = DBContext.Query("VerifiedSellerSearchByMobNo", _obj, commandType: CommandType.StoredProcedure);

            DBContext.Close();

            oResult.iRet = 1;
            oResult.sMessage = "Success";
            oResult.sResult = oReturn;

            return oResult;


        }

        public APiResult GetSellerByCreatedDate(SearchByDate _obj)
        {
            APiResult oResult = new APiResult();
            //DateTime test = _obj.CreatedOn;
            //string a = test.ToString("o");

            //DateTime dtttt = DateTime.Parse(a).ToLocalTime();//{9/8/2017 2:51:28 AM}

            //string CreatedOn = dtttt.ToString("yyyy-MM-dd");
            // _obj.ToString("dd/MM/yyyy"
            if (DBContext.State == ConnectionState.Closed)
            {
                DBContext.Open();
            }

            var oReturn = DBContext.Query("SellerSearchByCreatedDate", _obj, commandType: CommandType.StoredProcedure);

            DBContext.Close();

            oResult.iRet = 1;
            oResult.sMessage = "Success";
            oResult.sResult = oReturn;

            return oResult;


        }


        public APiResult GetVerifiedSellerByCreatedDate(SearchByDate _obj)
        {
            APiResult oResult = new APiResult();
           
            if (DBContext.State == ConnectionState.Closed)
            {
                DBContext.Open();
            }

            var oReturn = DBContext.Query("VerifiedSellerSearchByCreatedDate", _obj, commandType: CommandType.StoredProcedure);

            DBContext.Close();

            oResult.iRet = 1;
            oResult.sMessage = "Success";
            oResult.sResult = oReturn;

            return oResult;


        }

        //////public ApiResult<List<GetSellerOut>> GetSellerByCreatedDate(SearchByDate _obj)
        //////{
        //////    ApiResult<List<GetSellerOut>> oResult = new ApiResult<List<GetSellerOut>>();

        //////    if (DBContext.State == ConnectionState.Closed)
        //////    {
        //////        DBContext.Open();
        //////    }

        //////    var oReturn = DBContext.Query<GetSellerOut>("SellerSearchByCreatedDate", commandType: CommandType.StoredProcedure) as List<GetSellerOut>;
        //////    DBContext.Close();

        //////    oResult.Count = oReturn.Count;
        //////    oResult.HasWarning = false;
        //////    oResult.Message = "Success";
        //////    oResult.Value = oReturn;
        //////    return oResult;
        //////}



        //public APiResult GetSellerByCreatedDate(SearchByDate _obj)
        //{
        //    APiResult oResult = new APiResult();


        //    if (DBContext.State == ConnectionState.Closed)
        //    {
        //        DBContext.Open();
        //    }

        //    var oReturn = DBContext.Query("SellerSearchByCreatedDate", _obj, commandType: CommandType.StoredProcedure);

        //    DBContext.Close();

        //    oResult.iRet = 1;
        //    oResult.sMessage = "Success";
        //    oResult.sResult = oReturn;

        //    return oResult;


        //}



        public APiResult GetOrdersByCreatedDate(OrderSearch _obj)
        {
            APiResult oResult = new APiResult();

            if (DBContext.State == ConnectionState.Closed)
            {
                DBContext.Open();
            }

            var oReturn = DBContext.Query("OrderSearchByDate", _obj, commandType: CommandType.StoredProcedure);

            DBContext.Close();

            oResult.iRet = 1;
            oResult.sMessage = "Success";
            oResult.sResult = oReturn;

            return oResult;


        }


        public ApiResult<List<OrderStatusSearch>> OrdersByStatus(OrderByStatus _obj)
        {
            ApiResult<List<OrderStatusSearch>> oResult = new ApiResult<List<OrderStatusSearch>>();


            if (DBContext.State == ConnectionState.Closed)
            {
                DBContext.Open();
            }

            var oReturn = DBContext.Query<OrderStatusSearch>("OrderSearchByStatus", _obj, commandType: CommandType.StoredProcedure)as List<OrderStatusSearch>;

            DBContext.Close();

            oResult.Count = oReturn.Count;
            oResult.HasWarning = false;
            oResult.Message = "Success";
            oResult.Value = oReturn;

            return oResult;


        }

        public ApiResult<List<ProductSearchForSupportout>> ProductSearchForSupport(ProductSearchSupport _Search)
        {
            ApiResult<List<ProductSearchForSupportout>> oResult = new ApiResult<List<ProductSearchForSupportout>>();


            if (DBContext.State == ConnectionState.Closed)
            {
                DBContext.Open();
            }

            var oReturn = DBContext.Query<ProductSearchForSupportout>("SearchByProductforSupport", _Search, commandType: CommandType.StoredProcedure)as List<ProductSearchForSupportout>;

            for (int i = 0; i < oReturn.Count; i++)
            {
                var oReturn2 = DBContext.Query<GetProductImages>("GetProductImages",
               new
               {
                   @BranchId = oReturn[i].BranchId,
                   @ProductId = oReturn[i].ProductID
               }, commandType: CommandType.StoredProcedure) as List<GetProductImages>;
                oReturn[i].Images = oReturn2;
            }


            DBContext.Close();

            oResult.Count = oReturn.Count;
            oResult.HasWarning = false;
            oResult.Message = "Success";
            oResult.Value = oReturn;

            return oResult;


        }


    }
}
