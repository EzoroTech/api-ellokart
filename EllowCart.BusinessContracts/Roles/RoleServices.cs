﻿using System;
using System.Collections.Generic;
using System.Data;
using Dapper;
using EllowCart.DTO.Roles.InputModels;
using EllowCart.DTO.Roles.OutputModels;
using EllowCart.QueryBank.Roles;
using EllowCart.Shared;

namespace EllowCart.BusinessContracts.Roles
{
    public class RoleServices
    {
        private IDbConnection DBContext;
        private APiResult oResult = null;
        DAL.DBSettings _DBSettings = null;
        private QueryBank.Roles.Role oQoeryRole = null;

        public RoleServices(IDbConnection _DBContext)
        {
            this.DBContext = _DBContext;

        }
        public RoleServices()
        {
            _DBSettings = new DAL.DBSettings();  //-- New change
            DBContext = _DBSettings.GetCartDB();
        }


        //Role insertion by querybank
        public APiResult AddNewRole(DTO.Roles.InputModels.RoleInputModel _input)
        {
            oResult = new APiResult();
            string sSQlAddRoles = string.Empty;
            oQoeryRole = new Role();

            sSQlAddRoles = string.Format(oQoeryRole.GetAddRoles(),
                                            _input.RoleName,
                                            _input.IsActive, _input.CreatedOn,
                                            _input.CreatedBy, _input.UpdatedOn, _input.UpdatedBy);

            if (DBContext.State == ConnectionState.Closed)
            {
                DBContext.Open();
            }
            int iCount = DBContext.Execute(sSQlAddRoles);
            if (iCount > 0)
            {
                oResult = new APiResult();
                oResult.iRet = 1;
                oResult.sMessage = "Success";
            }
            DBContext.Close();
            return oResult;
        }
        //Role Deletion by querybank
        public APiResult DeleteRole(RoleSelectModel _role)
        {

            oResult = new APiResult();
            string sSQlDeleteRoles = string.Empty;
            oQoeryRole = new Role();

            sSQlDeleteRoles = string.Format(oQoeryRole.DeleteRole(_role));


            if (DBContext.State == ConnectionState.Closed)
            {
                DBContext.Open();
            }

            int iCount = DBContext.Execute(sSQlDeleteRoles);
            if (iCount > 0)
            {
                oResult = new APiResult();
                oResult.iRet = 1;
                oResult.sMessage = "Success";
            }
            DBContext.Close();
            return oResult;
        }
        //Role Updation by querybank
        public APiResult UpdateRole(DTO.Roles.InputModels.RoleInputForUpdation Obj)
        {

            oResult = new APiResult();
            string sSQlUpdateRoles = string.Empty;
            oQoeryRole = new Role();
            int RoleId = Obj.RoleId;
            string RoleName = Obj.RoleName;
            bool IsActive = Obj.IsActive;
            DateTime CreatedOn = Obj.CreatedOn;
            string CreatedBy = Obj.CreatedBy;
            DateTime UpdatedOn = Obj.UpdatedOn;
            string UpdatedBy = Obj.UpdatedBy;

            sSQlUpdateRoles = string.Format(oQoeryRole.UpdateRoles(RoleId, RoleName, IsActive, CreatedOn, CreatedBy, UpdatedOn, UpdatedBy));


            if (DBContext.State == ConnectionState.Closed)
            {
                DBContext.Open();
            }

            int iCount = DBContext.Execute(sSQlUpdateRoles);
            if (iCount > 0)
            {
                oResult = new APiResult();
                oResult.iRet = 1;
                oResult.sMessage = "Success";
            }
            DBContext.Close();
            return oResult;
        }
        //getting role querybank
        public APiResult GetRoleById(RoleSelectModel _rModel)
        {
            oResult = new APiResult();
            oQoeryRole = new Role();
            string ssqlGetAllRoles = oQoeryRole.GetARole(_rModel);
            if (DBContext.State == ConnectionState.Closed)
            {
                DBContext.Open();
            }

            var oReturn = DBContext.Query(ssqlGetAllRoles);
            DBContext.Close();
            oResult.iRet = 1;
            oResult.sMessage = "Success";
            oResult.sResult = oReturn;
            return oResult;

        }
        //getting role querybank
        public APiResult GetAllRoles()
        {
            oResult = new APiResult();
            oQoeryRole = new Role();
            string ssqlGetAllRoles = oQoeryRole.GetAllRoles();
            if (DBContext.State == ConnectionState.Closed)
            {
                DBContext.Open();
            }

            var oReturn = DBContext.Query(ssqlGetAllRoles);
            DBContext.Close();
            oResult.iRet = 1;
            oResult.sMessage = "Success";
            oResult.sResult = oReturn;
            return oResult;

        }
        //public APiResult RoleAttribute(DTO.Roles.InputModels.RoleAttribute _roleAttribute)
        //{
        //    oResult = new APiResult();
        //    if (DBContext.State == ConnectionState.Closed)
        //    {
        //        DBContext.Open();
        //    }
        //    var dt = new DataTable("RoleAttribute_Type");
        //    dt.Columns.Add("RoleId", typeof(long));
        //    dt.Columns.Add("ModuleId", typeof(long));
        //    dt.Columns.Add("ModuleAttributeId", typeof(long));
        //    dt.Columns.Add("IsActive", typeof(bool));

        //    var attr = _roleAttribute.dtRoleAttributes;
        //    for (int i = 0; i < attr.Count; i++)
        //    {
        //        dt.Rows.Add(attr[i].RoleId, attr[i].ModuleId, attr[i].ModuleAttributeId, attr[i].IsActive);
        //    }

        //    var oReturn = DBContext.Query("RoleAttributes_INSRT", new
        //    {
        //        @RoleAtributeID = _roleAttribute.RoleAtributeID,
        //        @CreatedBy = _roleAttribute.CreatedBy,
        //        @UpdatedBy = _roleAttribute.UpdatedBy,
        //        @Flag = _roleAttribute.Flag,
        //        @dtRoleAttributes = dt.AsTableValuedParameter("RoleAttribute_Type")
        //    }, commandType: CommandType.StoredProcedure);

        //    oResult.iRet = 1;
        //    oResult.sMessage = "Success";
        //    oResult.sResult = oReturn;
        //    return oResult;
        //}

        //public APiResult GetRoleAttribute(GetRoleattribute _input)
        //{
        //    APiResult oResult = new APiResult();

        //    if (DBContext.State == ConnectionState.Closed)
        //    {
        //        DBContext.Open();
        //    }

        //    var oReturn = DBContext.Query("RoleAttributeGet", _input, commandType: CommandType.StoredProcedure);

        //    oResult.sResult = oReturn;
        //    oResult.sMessage = "Success";
        //    oResult.iRet = 1;
        //    return oResult;

        //}

        public APiResult AddNewRoleAttribute(RoleAttributeInputModel _input)
        {
            oResult = new APiResult();
            string sSQlAddRoleAttribute = string.Empty;
            oQoeryRole = new Role();

            sSQlAddRoleAttribute = string.Format(oQoeryRole.GetAddRoleAttribute(),
                                            _input.RoleId, _input.ModuleId,
                                            _input.ModuleAttributeId,
                                            _input.IsActive, _input.CreatedOn,
                                            _input.CreatedBy, _input.UpdatedOn, _input.UpdatedBy);

            if (DBContext.State == ConnectionState.Closed)
            {
                DBContext.Open();
            }
            int iCount = DBContext.Execute(sSQlAddRoleAttribute);
            if (iCount > 0)
            {
                oResult = new APiResult();
                oResult.iRet = 1;
                oResult.sMessage = "Success";
            }
            DBContext.Close();
            return oResult;
        }
        //RoleAttribute Deletion by querybank
        public APiResult DeleteARoleAttribute(RoleAttributeSelectModel _rAttrOut)
        {

            oResult = new APiResult();
            string sSQlDeleteRoleAttribute = string.Empty;
            oQoeryRole = new Role();

            sSQlDeleteRoleAttribute = string.Format(oQoeryRole.DeleteRoleAttribute(_rAttrOut));


            if (DBContext.State == ConnectionState.Closed)
            {
                DBContext.Open();
            }

            int iCount = DBContext.Execute(sSQlDeleteRoleAttribute);
            if (iCount > 0)
            {
                oResult = new APiResult();
                oResult.iRet = 1;
                oResult.sMessage = "Success";
            }
            DBContext.Close();
            return oResult;
        }
        //RoleAttribute Updation by querybank
        public APiResult UpdateRoleAttribute(RoleAttributeInputForUpdation Obj)
        {

            oResult = new APiResult();
            string sSQlUpdateRoleAttribute = string.Empty;
            oQoeryRole = new Role();
            int roleattributeId = Obj.RoleAttributeID;
            int roleId = Obj.RoleId;
            int moduleId = Obj.ModuleId;
            int moduleattributeId = Obj.ModuleAttributeId;
            bool IsActive = Obj.IsActive;
            DateTime CreatedOn = Obj.CreatedOn;
            string CreatedBy = Obj.CreatedBy;
            DateTime UpdatedOn = Obj.UpdatedOn;
            string UpdatedBy = Obj.UpdatedBy;

            sSQlUpdateRoleAttribute = string.Format(oQoeryRole.UpdateRoleAttribute(roleattributeId, roleId, moduleId, moduleattributeId, IsActive, CreatedOn, CreatedBy, UpdatedOn, UpdatedBy));


            if (DBContext.State == ConnectionState.Closed)
            {
                DBContext.Open();
            }

            int iCount = DBContext.Execute(sSQlUpdateRoleAttribute);
            if (iCount > 0)
            {
                oResult = new APiResult();
                oResult.iRet = 1;
                oResult.sMessage = "Success";
            }
            DBContext.Close();
            return oResult;
        }


        public APiResult GetAllRoleAttributes()
        {
            oResult = new APiResult();
            oQoeryRole = new Role();
            string ssqlGetAllRoleAttribute = oQoeryRole.GetAllRoleAttributes();
            if (DBContext.State == ConnectionState.Closed)
            {
                DBContext.Open();
            }

            var oReturn = DBContext.Query(ssqlGetAllRoleAttribute);
            DBContext.Close();
            oResult.iRet = 1;
            oResult.sMessage = "Success";
            oResult.sResult = oReturn;
            return oResult;

        }
        //getting roleAttribute querybank
        public APiResult GetRoleAttributeById(RoleAttributeSelectModel _roleAttrOut)
        {
            oResult = new APiResult();
            oQoeryRole = new Role();
            string ssqlGetRoleAttribute = oQoeryRole.GetARoleAttribute(_roleAttrOut);
            if (DBContext.State == ConnectionState.Closed)
            {
                DBContext.Open();
            }

            var oReturn = DBContext.Query(ssqlGetRoleAttribute);
            DBContext.Close();
            oResult.iRet = 1;
            oResult.sMessage = "Success";
            oResult.sResult = oReturn;
            return oResult;

        }
    }
}
