﻿using Dapper;
using EllowCart.DTO.Tax.InputModels;
using EllowCart.QueryBank.Roles;
using EllowCart.Shared;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace EllowCart.BusinessContracts.Roles
{
    public class TaxServices
    {


        private IDbConnection DBContext;
        private APiResult oResult = null;
        DAL.DBSettings _DBSettings = null;
        private QueryBank.Roles.TaxMasterManage OQueryTaxManage = null;

        public TaxServices(IDbConnection _DBContext)
        {
            this.DBContext = _DBContext;

        }
        public TaxServices()
        {
            _DBSettings = new DAL.DBSettings();  //-- New change
            DBContext = _DBSettings.GetCartDB();
        }


        public APiResult AddNewTax(TaxInputModel _input)
        {
            oResult = new APiResult();
            string sSQlAddTax = string.Empty;
            OQueryTaxManage = new TaxMasterManage();

            sSQlAddTax = string.Format(OQueryTaxManage.GetAddTax(),
                                            _input.TaxName, _input.TaxPercentage,
                                            _input.CreatedOn, _input.CreatedBy,
                                            _input.UpdatedOn, _input.UpdatedBy,
                                             _input.IsActive, _input.TaxType);

            if (DBContext.State == ConnectionState.Closed)
            {
                DBContext.Open();
            }
            int iCount = DBContext.Execute(sSQlAddTax);
            if (iCount > 0)
            {
                oResult = new APiResult();
                oResult.iRet = 1;
                oResult.sMessage = "Success";
            }
            DBContext.Close();
            return oResult;
        }
        //Tax Deletion by querybank
        public APiResult DeleteTax(TaxModel _obj)
        {

            oResult = new APiResult();
            string sSQlDeleteTax = string.Empty;
            OQueryTaxManage = new TaxMasterManage();

            sSQlDeleteTax = string.Format(OQueryTaxManage.DeleteAtax(_obj));


            if (DBContext.State == ConnectionState.Closed)
            {
                DBContext.Open();
            }

            int iCount = DBContext.Execute(sSQlDeleteTax);
            if (iCount > 0)
            {
                oResult = new APiResult();
                oResult.iRet = 1;
                oResult.sMessage = "Success";
            }
            DBContext.Close();
            return oResult;
        }
        //Tax Updation by querybank
        public APiResult UpdateTax(TaxInputForUpdation Obj)
        {

            oResult = new APiResult();
            string sSQlUpdateTax = string.Empty;
            OQueryTaxManage = new TaxMasterManage();
            int TaxId = Obj.TaxId;
            string TaxName = Obj.TaxName;
            string TaxPercentage = Obj.TaxPercentage;
            DateTime CreatedOn = Obj.CreatedOn;
            string CreatedBy = Obj.CreatedBy;
            DateTime UpdatedOn = Obj.UpdatedOn;
            string UpdatedBy = Obj.UpdatedBy;
            bool IsActive = Obj.IsActive;
            string TaxType = Obj.TaxType;

            sSQlUpdateTax = string.Format(OQueryTaxManage.UpdateAtax(TaxId, TaxName, TaxPercentage, CreatedOn, CreatedBy, UpdatedOn, UpdatedBy, IsActive, TaxType));


            if (DBContext.State == ConnectionState.Closed)
            {
                DBContext.Open();
            }

            int iCount = DBContext.Execute(sSQlUpdateTax);
            if (iCount > 0)
            {
                oResult = new APiResult();
                oResult.iRet = 1;
                oResult.sMessage = "Success";
            }
            DBContext.Close();
            return oResult;
        }
        //geting Tax
        public APiResult GetAllTaxes()
        {
            oResult = new APiResult();
            OQueryTaxManage = new TaxMasterManage();
            string ssqlGetAllTax = OQueryTaxManage.GetAllTaxes();
            if (DBContext.State == ConnectionState.Closed)
            {
                DBContext.Open();
            }

            var oReturn = DBContext.Query(ssqlGetAllTax);
            DBContext.Close();
            oResult.iRet = 1;
            oResult.sMessage = "Success";
            oResult.sResult = oReturn;
            return oResult;

        }

        //getting tax querybank
        public APiResult GetTaxById(TaxModel _obj)
        {
            oResult = new APiResult();
            OQueryTaxManage = new TaxMasterManage();
            string ssqlGetATax = OQueryTaxManage.GetAtax(_obj);
            if (DBContext.State == ConnectionState.Closed)
            {
                DBContext.Open();
            }

            var oReturn = DBContext.Query(ssqlGetATax);
            DBContext.Close();
            oResult.iRet = 1;
            oResult.sMessage = "Success";
            oResult.sResult = oReturn;
            return oResult;

        }

    }
}
