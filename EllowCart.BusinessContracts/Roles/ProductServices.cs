﻿using Dapper;
using EllowCart.DTO.Product.InputModels;
using EllowCart.DTO.Product.OutputModels;
using EllowCart.Shared;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace EllowCart.BusinessContracts.Roles
{
    public class ProductServices
    {
        private IDbConnection DBContext;
        private APiResult oResult = null;
        DAL.DBSettings _DBSettings = null;
        public ProductServices(IDbConnection _DBContext)
        {
            this.DBContext = _DBContext;

        }
        public ProductServices()
        {
            _DBSettings = new DAL.DBSettings();  //-- New change
            DBContext = _DBSettings.GetCartDB();
        }

        public ApiResult<List<addproductout>> AddProduct(Product _product)
        {
            ApiResult<List<addproductout>> oResult = new ApiResult<List<addproductout>>();

            if (DBContext.State == ConnectionState.Closed)
            {
                DBContext.Open();
            }

            var dt = new DataTable("ProductSpecData_Type");
            dt.Columns.Add("ProductSpecId", typeof(long));
            dt.Columns.Add("VarientValue", typeof(string));

            var spec = _product.productSpecification;

            int weightavailable = 0;

            for (int i = 0; i < spec.Count; i++)
            {
                dt.Rows.Add(spec[i].ProductSpecId, spec[i].VarientValue);

                if (spec[i].ProductSpecId == 3)
                {
                    weightavailable = 1;
                }
            }


            if (weightavailable == 1 && _product.UserId != 0 && _product.BranchId != 0 && _product.BrandId != 0 && _product.ProductCategoryId != 0 && _product.TaxId != 0 && _product.UserBussinessId != 0)
            {

                var oReturn = DBContext.Query<addproductout>("AddProductToBranch", new
                {
                    @UserId = _product.UserId,
                    @role = _product.role,
                    @BranchId = _product.BranchId,
                    @ProductCategoryId = _product.ProductCategoryId,
                    @ProductCode = _product.ProductCode,
                    @ProductName = _product.ProductName,
                    @ProductQty = _product.ProductQty,
                    @SKU = _product.SKU,
                    @MRP = _product.MRP,
                    @TaxId = _product.TaxId,
                    @TaxablePrice = _product.TaxablePrice,
                    @BrandId = _product.BrandId,
                    @HSNCode = _product.HSNCode,
                    @ProductDescr = _product.ProductDescr,
                    @IsNew = _product.IsNew,
                    @UserBussinessId = _product.UserBussinessId,
                    @DtProductSpecData = dt.AsTableValuedParameter("ProductSpecData_Type")

                }, commandType: CommandType.StoredProcedure) as List<addproductout>;

                DBContext.Close();

                oResult.Count = oReturn.Count;
                oResult.HasWarning = false;
                oResult.Message = "Success";
                oResult.Value = oReturn;
            }
            else
            {
                DBContext.Close();
                oResult.HasWarning = true;
                oResult.Message = "Please add weight to the product";
            }
            return oResult;

        }

        public ApiResult<List<EditProductDetailsOut>> EditProductDetails(EditProductDetailsInput _getproduct)
        {
            ApiResult<List<EditProductDetailsOut>> oResult = new ApiResult<List<EditProductDetailsOut>>();

            if (DBContext.State == ConnectionState.Closed)
            {
                DBContext.Open();
            }



            var dt = new DataTable("ProductSpecData_Type");
            dt.Columns.Add("ProductSpecId", typeof(long));
            dt.Columns.Add("VarientValue", typeof(string));

            var spec = _getproduct.dtProductSpec;
            int weightavailable = 0;
            for (int i = 0; i < spec.Count; i++)
            {
                dt.Rows.Add(spec[i].ProductSpecId, spec[i].VarientValue);
                if (spec[i].ProductSpecId == 3)
                {
                    weightavailable = 1;
                }
            }

            if (weightavailable == 1 && _getproduct.UserId != 0 && _getproduct.BranchId != 0 && _getproduct.BrandId != 0 && _getproduct.ProductCategoryId != 0 && _getproduct.TaxId != 0 )
            {
                var oReturn = DBContext.Query<EditProductDetailsOut>("EditProductDetails", new
                {
                    @UserId = _getproduct.UserId,
                    @role = _getproduct.role,
                    @BranchId = _getproduct.BranchId,
                    @ProductId = _getproduct.ProductId,
                    @ProductName = _getproduct.ProductName,
                    @ProductCategoryId = _getproduct.ProductCategoryId,
                    @ProductQty = _getproduct.ProductQty,
                    @SKU = _getproduct.SKU,
                    @MRP = _getproduct.MRP,
                    @TaxId = _getproduct.TaxId,
                    @TaxablePrice = _getproduct.TaxablePrice,
                    @BrandId = _getproduct.BrandId,
                    @HSNCode = _getproduct.HSNCode,
                    @ProductDescr = _getproduct.ProductDescr,
                    @IsNew = _getproduct.IsNew,
                    @IsActive = _getproduct.IsActive,
                    @DtProductSpecData = dt.AsTableValuedParameter("ProductSpecData_Type"),

                }, commandType: CommandType.StoredProcedure) as List<EditProductDetailsOut>;
                DBContext.Close();

                oResult.Count = oReturn.Count;
                oResult.HasWarning = false;
                oResult.Message = "Success";
                oResult.Value = oReturn;
            }
            else
            {
                DBContext.Close();
                oResult.HasWarning = true;
                oResult.Message = "Please add weight to the product";
            }
            return oResult;

        }


        public APiResult GetproductDetails(GetproductDetailsInput _getproduct)
        {
            APiResult oResult = new APiResult();

            if (DBContext.State == ConnectionState.Closed)
            {
                DBContext.Open();
            }

            var oReturn2 = DBContext.Query<GetProductImages>("GetProductImages", _getproduct, commandType: CommandType.StoredProcedure) as List<GetProductImages>;
            var oReturn = DBContext.Query<GetProductDetails>("GetProductDetails", _getproduct, commandType: CommandType.StoredProcedure) as List<GetProductDetails>;
            var oReturn1 = DBContext.Query<GetProductSpecificationOut>("GetProductSpecificationList", _getproduct, commandType: CommandType.StoredProcedure) as List<GetProductSpecificationOut>;

            ProductDetails details = new ProductDetails();
            details.Details = oReturn;
            details.Specification = oReturn1;
            details.Images = oReturn2;

            DBContext.Close();


            oResult.sResult = details;
            oResult.iRet = 1;
            oResult.sMessage = "Success";
            return oResult;

        }


        public APiResult GetproductDetailsCustomer(GetproductDetailsInput _getproduct)
        {
            APiResult oResult = new APiResult();

            if (DBContext.State == ConnectionState.Closed)
            {
                DBContext.Open();
            }

            var oReturn2 = DBContext.Query<GetProductImages>("GetProductImages", _getproduct, commandType: CommandType.StoredProcedure) as List<GetProductImages>;
            var oReturn = DBContext.Query<GetProductDetails>("GetProductDetailsForCustomer", _getproduct, commandType: CommandType.StoredProcedure) as List<GetProductDetails>;
            var oReturn1 = DBContext.Query<GetProductSpecificationOut>("GetProductSpecificationList", _getproduct, commandType: CommandType.StoredProcedure) as List<GetProductSpecificationOut>;

            ProductDetails details = new ProductDetails();
            details.Details = oReturn;
            details.Specification = oReturn1;
            details.Images = oReturn2;

            DBContext.Close();


            oResult.sResult = details;
            oResult.iRet = 1;
            oResult.sMessage = "Success";
            return oResult;

        }


        public ApiResult<List<GetProductSpecificationOut>> GetSpecificationList(GetSpecificationInput _getproduct)
        {
            ApiResult<List<GetProductSpecificationOut>> oResult = new ApiResult<List<GetProductSpecificationOut>>();

            if (DBContext.State == ConnectionState.Closed)
            {
                DBContext.Open();
            }

            var oReturn = DBContext.Query<GetProductSpecificationOut>("GetProductSpecificationList", _getproduct, commandType: CommandType.StoredProcedure) as List<GetProductSpecificationOut>;

            DBContext.Close();

            oResult.Count = oReturn.Count;
            oResult.HasWarning = false;
            oResult.Message = "Success";
            oResult.Value = oReturn;
            return oResult;

        }

        public ApiResult<List<GetProductListOut>> GetproductList(GetProductlistInput _getProductlist)
        {
            ApiResult<List<GetProductListOut>> oResult = new ApiResult<List<GetProductListOut>>();

            if (DBContext.State == ConnectionState.Closed)
            {
                DBContext.Open();
            }

            var oReturn = DBContext.Query<GetProductListOut>("ProductListSeller", _getProductlist, commandType: CommandType.StoredProcedure) as List<GetProductListOut>;
            DBContext.Close();

            oResult.Count = oReturn.Count;
            oResult.HasWarning = false;
            oResult.Message = "Success";
            oResult.Value = oReturn;
            return oResult;

        }

        public ApiResult<List<RemoveProductout>> RemoveProduct(RemoveProductInput _removeProduct)
        {
            ApiResult<List<RemoveProductout>> oResult = new ApiResult<List<RemoveProductout>>();

            if (DBContext.State == ConnectionState.Closed)
            {
                DBContext.Open();
            }

            var oReturn = DBContext.Query<RemoveProductout>("RemoveProduct", _removeProduct, commandType: CommandType.StoredProcedure) as List<RemoveProductout>;
            DBContext.Close();

            oResult.Count = oReturn.Count;
            oResult.HasWarning = false;
            oResult.Message = "Success";
            oResult.Value = oReturn;
            return oResult;

        }

        public ApiResult<List<GetProductSearchResultOut>> SearchProduct(GetProductSerachInput _search)
        {
            ApiResult<List<GetProductSearchResultOut>> oResult = new ApiResult<List<GetProductSearchResultOut>>();

            if (DBContext.State == ConnectionState.Closed)
            {
                DBContext.Open();
            }

            var oReturn = DBContext.Query<GetProductSearchResultOut>("GetProductSearchResult", _search, commandType: CommandType.StoredProcedure) as List<GetProductSearchResultOut>;
            DBContext.Close();

            oResult.Count = oReturn.Count;
            oResult.HasWarning = false;
            oResult.Message = "Success";
            oResult.Value = oReturn;
            return oResult;

        }

        public ApiResult<List<GetProductCategoryListOut>> GetProductCategoryList(GetProductCategoryListInput _getproductcategory)
        {
            ApiResult<List<GetProductCategoryListOut>> oResult = new ApiResult<List<GetProductCategoryListOut>>();

            if (DBContext.State == ConnectionState.Closed)
            {
                DBContext.Open();
            }

            var oReturn = DBContext.Query<GetProductCategoryListOut>("GetProductCategoryList", _getproductcategory, commandType: CommandType.StoredProcedure) as List<GetProductCategoryListOut>;
            DBContext.Close();

            oResult.Count = oReturn.Count;
            oResult.HasWarning = false;
            oResult.Message = "Success";
            oResult.Value = oReturn;
            return oResult;

        }

        public ApiResult<List<GetProductImages>> GetProductImages(GetProductImagesInput _imagesInput)
        {
            ApiResult<List<GetProductImages>> oResult = new ApiResult<List<GetProductImages>>();

            if (DBContext.State == ConnectionState.Closed)
            {
                DBContext.Open();
            }

            var oReturn = DBContext.Query<GetProductImages>("GetProductImages", _imagesInput, commandType: CommandType.StoredProcedure) as List<GetProductImages>;
            DBContext.Close();

            oResult.Count = oReturn.Count;
            oResult.HasWarning = false;
            oResult.Message = "Success";
            oResult.Value = oReturn;
            return oResult;

        }

        public ApiResult<List<GetProductOfferOut>> GetProductOfferList(GetProductOfferInput _offerInput)
        {
            ApiResult<List<GetProductOfferOut>> oResult = new ApiResult<List<GetProductOfferOut>>();

            if (DBContext.State == ConnectionState.Closed)
            {
                DBContext.Open();
            }

            var oReturn = DBContext.Query<GetProductOfferOut>("GetProductoffersList", _offerInput, commandType: CommandType.StoredProcedure) as List<GetProductOfferOut>;
            DBContext.Close();

            oResult.Count = oReturn.Count;
            oResult.HasWarning = false;
            oResult.Message = "Success";
            oResult.Value = oReturn;
            return oResult;

        }

        public ApiResult<List<GetComboProductImages>> GetComboProductimages(GetComboProductImagesInput _comboinput)
        {
            ApiResult<List<GetComboProductImages>> oResult = new ApiResult<List<GetComboProductImages>>();

            if (DBContext.State == ConnectionState.Closed)
            {
                DBContext.Open();
            }

            var oReturn = DBContext.Query<GetComboProductImages>("GetOfferProductImages", _comboinput, commandType: CommandType.StoredProcedure) as List<GetComboProductImages>;
            DBContext.Close();

            oResult.Count = oReturn.Count;
            oResult.HasWarning = false;
            oResult.Message = "Success";
            oResult.Value = oReturn;
            return oResult;

        }
        public ApiResult<List<GetProductOfferOut>> ProductOfferListForCustomer(GetProductOfferInput _offerInput)
        {
            ApiResult<List<GetProductOfferOut>> oResult = new ApiResult<List<GetProductOfferOut>>();

            if (DBContext.State == ConnectionState.Closed)
            {
                DBContext.Open();
            }

            var oReturn = DBContext.Query<GetProductOfferOut>("GetProductoffersListForCustomer", _offerInput, commandType: CommandType.StoredProcedure) as List<GetProductOfferOut>;
            DBContext.Close();

            oResult.Count = oReturn.Count;
            oResult.HasWarning = false;
            oResult.Message = "Success";
            oResult.Value = oReturn;
            return oResult;

        }
    }
}
