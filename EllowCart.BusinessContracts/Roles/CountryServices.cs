﻿using Dapper;
using EllowCart.DTO.Country.InputModels;
using EllowCart.DTO.Country.OutputModels;
using EllowCart.QueryBank.Roles;
using EllowCart.Shared;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace EllowCart.BusinessContracts.Roles
{
    public class CountryServices
    {
        private IDbConnection DBContext;
        private APiResult oResult = null;
        private QueryBank.Roles.Country oQoeryModule = null;
        DAL.DBSettings _DBSettings = null;
        public CountryServices(IDbConnection _DBContext)
        {
            this.DBContext = _DBContext;

        }
        public CountryServices()
        {
            _DBSettings = new DAL.DBSettings();  //-- New change
            DBContext = _DBSettings.GetCartDB();
        }

        public APiResult AddCountry(CreateCountryInput _input)
        {
            oResult = new APiResult();
            string sSQlAddModules = string.Empty;
            oQoeryModule = new Country();

            sSQlAddModules = string.Format(oQoeryModule.addcountry(),
                                           _input.CountryName,_input.Description,_input.Isactive,_input.CreatedBy,_input.UpdatedBy);

            if (DBContext.State == ConnectionState.Closed)
            {
                DBContext.Open();
            }
            int iCount = DBContext.Execute(sSQlAddModules);
            if (iCount > 0)
            {
                oResult = new APiResult();
                oResult.iRet = 1;
                oResult.sMessage = "Success";
            }
            DBContext.Close();
            return oResult;
        }






        public ApiResult<List<GetCountryListOut>> GetCountryList()
        {
            ApiResult<List<GetCountryListOut>> oResult = new ApiResult<List<GetCountryListOut>>();

            if (DBContext.State == ConnectionState.Closed)
            {
                DBContext.Open();
            }

            var oReturn = DBContext.Query<GetCountryListOut>("GetCountryList", commandType: CommandType.StoredProcedure) as List<GetCountryListOut>;

            DBContext.Close();
            oResult.Count= oReturn.Count;
            oResult.HasWarning = false;
            oResult.Message = "Success";
            oResult.Value = oReturn;

            return oResult;

        }
    }
}
