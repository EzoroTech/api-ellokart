﻿using System;
using System.Data;
using System.Collections.Generic;
using Dapper;
using EllowCart.QueryBank.Roles;
using EllowCart.Shared;
using EllowCart.DTO.Verification.InputModels;

namespace EllowCart.BusinessContracts.Roles
{
    public class VerificationServices
    {


        private IDbConnection DBContext;
        private APiResult oResult = null;

        private QueryBank.Roles.Verification oQoeryVerify = null;
        DAL.DBSettings _DBSettings = null;
        public VerificationServices(IDbConnection _DBContext)
        {
            this.DBContext = _DBContext;

        }
        public VerificationServices()
        {
            _DBSettings = new DAL.DBSettings();  //-- New change
            DBContext = _DBSettings.GetCartDB();
        }


        public APiResult GetAllBusinessForVerification()
        {
            oResult = new APiResult();
            oQoeryVerify = new Verification();
            string ssqlGetAllBusiness= oQoeryVerify.GetAllBusinesses();
            if (DBContext.State == ConnectionState.Closed)
            {
                DBContext.Open();
            }

            var oReturn = DBContext.Query(ssqlGetAllBusiness);
            DBContext.Close();
            oResult.iRet = 1;
            oResult.sMessage = "Success";
            oResult.sResult = oReturn;
            return oResult;

        }
        public APiResult GetBusinessById(VerifiedBusinessModel _businessInput)
        {
            oResult = new APiResult();
            oQoeryVerify = new Verification();
          //  string ssqlGetABusiness = oQoeryVerify.GetAUserBusiness(_businessInput);
            if (DBContext.State == ConnectionState.Closed)
            {
                DBContext.Open();
            }

            var oReturn = DBContext.Query("GetOneVerifiedSeller",_businessInput,commandType:CommandType.StoredProcedure); 

            DBContext.Close();
            oResult.iRet = 1;
            oResult.sMessage = "Success";
            oResult.sResult = oReturn;
            return oResult;

        }
        
        public APiResult RejectABusiness(BusinessVerificationModel _businessInput)
        {

            oResult = new APiResult();

            if (DBContext.State == ConnectionState.Closed)
            {
                DBContext.Open();
            }

            var oReturn = DBContext.Query("VerifyBusinesses", _businessInput, commandType: CommandType.StoredProcedure);


            DBContext.Close();
            oResult.iRet = 1;
            oResult.sMessage = "Success";
            oResult.sResult = oReturn;
            return oResult;

        }
        public APiResult GetVerifiedBusinesses()
        {
            oResult = new APiResult();
            oQoeryVerify = new Verification();
            string ssqlGetAllVerifiedSellers= oQoeryVerify.GetAllVerifiedBusinesses();
            if (DBContext.State == ConnectionState.Closed)
            {
                DBContext.Open();
            }

            var oReturn = DBContext.Query("GetAllVerifiedBusinesses", commandType: CommandType.StoredProcedure);

            DBContext.Close();
            oResult.iRet = 1;
            oResult.sMessage = "Success";
            oResult.sResult = oReturn;
            return oResult;

        }

        //Verified Business Updation 
        public APiResult UpdateAVerifiedBusiness(BusinessUpdateModel Obj)
        {

            oResult = new APiResult();

            if (DBContext.State == ConnectionState.Closed)
            {
                DBContext.Open();
            }

            var oReturn = DBContext.Query("UpdateVerifiedBusiness", Obj, commandType: CommandType.StoredProcedure);


            DBContext.Close();
            oResult.iRet = 1;
            oResult.sMessage = "Success";
            oResult.sResult = oReturn;
            return oResult;
        }


        public APiResult GetVerifiedSellerById(VerifiedBusiness _branch)
        {
            oResult = new APiResult();
            oQoeryVerify = new Verification();
            string ssqlGetABranch = oQoeryVerify.GetOneVerifiedSeller(_branch);
            if (DBContext.State == ConnectionState.Closed)
            {
                DBContext.Open();
            }

            var oReturn = DBContext.Query(ssqlGetABranch);
            DBContext.Close();
            oResult.iRet = 1;
            oResult.sMessage = "Success";
            oResult.sResult = oReturn;
            return oResult;

        }


        //public APiResult DecryptPassword(DecryptPassword _branch)
        //{
        //    oResult = new APiResult();

        //    var out1 = "";
        //    var out2 = "";
        //    var stat = EllowCart.Shared.Cryptography.Decrypt_String(_branch.Password, ref out1, ref out2);

        //    oResult.iRet = 1;
        //    oResult.sMessage = "Success";
        //    oResult.sResult = out1;
        //    return oResult;

        //}


        public APiResult UserToUpgradeList()
        {
            oResult = new APiResult();
            oQoeryVerify = new Verification();
            string ssqlGetAllSellersToUpgrade = oQoeryVerify.GetAllBusinessToUpgrade();
            if (DBContext.State == ConnectionState.Closed)
            {
                DBContext.Open();
            }

            var oReturn = DBContext.Query(ssqlGetAllSellersToUpgrade);
            DBContext.Close();
            oResult.iRet = 1;
            oResult.sMessage = "Success";
            oResult.sResult = oReturn;
            return oResult;

        }




        //Verified Business Updation 
        public APiResult UpdateBusinessStatus(UpgradeMdl Obj)
        {

            oResult = new APiResult();

            if (DBContext.State == ConnectionState.Closed)
            {
                DBContext.Open();
            }

            var oReturn = DBContext.Query("BusinessStatusUpdation", Obj, commandType: CommandType.StoredProcedure);


            DBContext.Close();
            oResult.iRet = 1;
            oResult.sMessage = "Success";
            oResult.sResult = oReturn;
            return oResult;
        }

        //public APiResult VerificationStatus(BusinessStatusMdl Obj)
        //{

        //    oResult = new APiResult();

        //    if (DBContext.State == ConnectionState.Closed)
        //    {
        //        DBContext.Open();
        //    }

        //    var oReturn = DBContext.Query("GetVerificationStatus", Obj, commandType: CommandType.StoredProcedure);


        //    DBContext.Close();
        //    oResult.iRet = 1;
        //    oResult.sMessage = "Success";
        //    oResult.sResult = oReturn;
        //    return oResult;
        //}


        public ApiResult<List<BusinessStatusOut>> VerificationStatus(BusinessStatusMdl Obj)
        {
            ApiResult<List<BusinessStatusOut>> oResult = new ApiResult<List<BusinessStatusOut>>();

            if (DBContext.State == ConnectionState.Closed)
            {
                DBContext.Open();
            }

            var oReturn = DBContext.Query<BusinessStatusOut>("GetVerificationStatus", Obj, commandType: CommandType.StoredProcedure)as List<BusinessStatusOut>;

            DBContext.Close();


            oResult.Count = oReturn.Count;
            oResult.HasWarning = false;
            oResult.Message = "Success";
            oResult.Value = oReturn;
            return oResult;



        }


        public APiResult GetUpgradedBusinesses()
        {
            oResult = new APiResult();
            oQoeryVerify = new Verification();
            string ssqlGetAllUpgradededSellers = oQoeryVerify.GetAllUpgradededBusinesses();
            if (DBContext.State == ConnectionState.Closed)
            {
                DBContext.Open();
            }

            var oReturn = DBContext.Query(ssqlGetAllUpgradededSellers);
            DBContext.Close();
            oResult.iRet = 1;
            oResult.sMessage = "Success";
            oResult.sResult = oReturn;
            return oResult;

        }

        public APiResult SetVisibilityForBusiness(VisibiltyOfBusiness _Visibility)
        {
            oResult = new APiResult();
            
            if (DBContext.State == ConnectionState.Closed)
            {
                DBContext.Open();
            }

            var oReturn = DBContext.Query("SetVisibilityOfABusiness",_Visibility, commandType:CommandType.StoredProcedure);

            DBContext.Close();
            oResult.iRet = 1;
            oResult.sMessage = "Success";
            oResult.sResult = oReturn;
            return oResult;

        }
    }
}
