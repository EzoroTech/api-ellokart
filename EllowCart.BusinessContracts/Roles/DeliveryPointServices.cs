﻿using Dapper;
using EllowCart.DTO.DeliveryPoint.Input_Model;
using EllowCart.QueryBank.Roles;
using EllowCart.Shared;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;


namespace EllowCart.BusinessContracts.Roles
{
    public class DeliveryPointServices
    {


        private IDbConnection DBContext;
        private APiResult oResult = null;
        DAL.DBSettings _DBSettings = null;
        private QueryBank.Roles.DeliveryPointManage OQueryDeliveryPoint = null;

        public DeliveryPointServices(IDbConnection _DBContext)
        {
            this.DBContext = _DBContext;

        }
        public DeliveryPointServices()
        {
            _DBSettings = new DAL.DBSettings();  //-- New change
            DBContext = _DBSettings.GetCartDB();
        }

        //adding delivery point
        public APiResult AddNewDeliveryPoint(DTO.DeliveryPoint.Input_Model.DeliveryPointCreateInput _input)
        {
            oResult = new APiResult();
            string sSQlAddFranchiseMaster = string.Empty;
            OQueryDeliveryPoint = new DeliveryPointManage();


            if (DBContext.State == ConnectionState.Closed)
            {
                DBContext.Open();
            }

            var oReturn = DBContext.Query("DeliveryPointRegistration", _input, commandType: CommandType.StoredProcedure);


            oResult = new APiResult();
            oResult.iRet = 1;
            oResult.sMessage = "Success";

            DBContext.Close();
            return oResult;
        }
        //deliveryPoint Deletion by querybank
        public APiResult DeleteDeliveryPoint(DeliveryPointInput _input)
        {

            oResult = new APiResult();
            string sSQlDeleteFeliveryPoint = string.Empty;
            OQueryDeliveryPoint = new DeliveryPointManage();

            sSQlDeleteFeliveryPoint = string.Format(OQueryDeliveryPoint.DeleteDeliveryPoint(_input));


            if (DBContext.State == ConnectionState.Closed)
            {
                DBContext.Open();
            }

            int iCount = DBContext.Execute(sSQlDeleteFeliveryPoint);
            if (iCount > 0)
            {
                oResult = new APiResult();
                oResult.iRet = 1;
                oResult.sMessage = "Success";
            }
            DBContext.Close();
            return oResult;
        }
        //getting delivery points
        public APiResult GetAllDeliveryPoints()
        {
            oResult = new APiResult();
            OQueryDeliveryPoint = new DeliveryPointManage();
            string ssqlGetDeliveryPoints = OQueryDeliveryPoint.GetAllDeliveryPoint();
            if (DBContext.State == ConnectionState.Closed)
            {
                DBContext.Open();
            }

            var oReturn = DBContext.Query(ssqlGetDeliveryPoints);
            DBContext.Close();
            oResult.iRet = 1;
            oResult.sMessage = "Success";
            oResult.sResult = oReturn;
            return oResult;

        }

        //getting deliverypoint querybank
        public APiResult GetDeliveryPointById(DeliveryPointInput _input)
        {
            oResult = new APiResult();
            OQueryDeliveryPoint = new DeliveryPointManage();
            string ssqlGetAeliveryPoint = OQueryDeliveryPoint.GetADeliveryPoint(_input);
            if (DBContext.State == ConnectionState.Closed)
            {
                DBContext.Open();
            }

            var oReturn = DBContext.Query(ssqlGetAeliveryPoint);
            DBContext.Close();
            oResult.iRet = 1;
            oResult.sMessage = "Success";
            oResult.sResult = oReturn;
            return oResult;

        }


        //deliverypoint Updation by querybank
        public APiResult UpdateDeliveryPoints(DeliveryPointUpdate Obj)
        {

            oResult = new APiResult();
            

            if (DBContext.State == ConnectionState.Closed)
            {
                DBContext.Open();
            }
            var oReturn = DBContext.Query("DeliveryPointUpdation", Obj, commandType: CommandType.StoredProcedure);


            oResult = new APiResult();
            oResult.iRet = 1;
            oResult.sMessage = "Success";

            DBContext.Close();
            return oResult;
        }
    }
}
