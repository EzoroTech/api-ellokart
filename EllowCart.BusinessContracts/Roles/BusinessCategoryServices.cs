﻿using Dapper;
using EllowCart.DTO.BusinessCategory.InputModels;
using EllowCart.DTO.BusinessCategory.OutputModels;
using EllowCart.Shared;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace EllowCart.BusinessContracts.Roles
{
   public class BusinessCategoryServices
    {
        private IDbConnection DBContext;
        private APiResult oResult = null;
        private QueryBank.Roles.BusinessCategoryManage oQoeryBusinessCateg = null;
        DAL.DBSettings _DBSettings = null;
        public BusinessCategoryServices(IDbConnection _DBContext)
        {
            this.DBContext = _DBContext;

        }
        public BusinessCategoryServices()
        {
            _DBSettings = new DAL.DBSettings();  //-- New change
            DBContext = _DBSettings.GetCartDB();
        }

        //BusnessCateg ADD by QUERBANK
        public APiResult AddBusinesscategory(DTO.BusinessCategory.InputModels.AddBusinesscategory _input)
        {
            oResult = new APiResult();
            string sSQlAddBusinessCategory = string.Empty;
            oQoeryBusinessCateg = new QueryBank.Roles.BusinessCategoryManage();

            sSQlAddBusinessCategory = string.Format(oQoeryBusinessCateg.GetAddBusinessCategory(),
                                            _input.BussinessCategoryName, _input.BussinessCategoryDecr,
                                            _input.BussinessTypeId, _input.CreatedOn, _input.CreatedBy,
                                            _input.UpdatedOn, _input.UpdatedBy, _input.IsActive);

            if (DBContext.State == ConnectionState.Closed)
            {
                DBContext.Open();
            }
            int iCount = DBContext.Execute(sSQlAddBusinessCategory);
            if (iCount > 0)
            {
                oResult = new APiResult();
                oResult.iRet = 1;
                oResult.sMessage = "Success";
            }
            DBContext.Close();
            return oResult;
        }


        //getting Business category querybank
        public APiResult GetAllBusinessCategory()
        {
            oResult = new APiResult();
            oQoeryBusinessCateg = new QueryBank.Roles.BusinessCategoryManage();
            string ssqlGetAllBusinessCateg = oQoeryBusinessCateg.GetAllBusinessCategory();
            if (DBContext.State == ConnectionState.Closed)
            {
                DBContext.Open();
            }

            var oReturn = DBContext.Query(ssqlGetAllBusinessCateg);
            DBContext.Close();

            oResult.iRet = 1;
            oResult.sMessage = "Success";
            oResult.sResult = oReturn;
            return oResult;

        }
        //getting BusinessCateg querybank
        public APiResult GetBusinessCategoryById(GetBusinessCategoryAdminInput _BCateginput)
        {
            oResult = new APiResult();
            oQoeryBusinessCateg = new QueryBank.Roles.BusinessCategoryManage();
            string ssqlGetOneBusinessCateg = oQoeryBusinessCateg.GetABusinessCategory(_BCateginput);
            if (DBContext.State == ConnectionState.Closed)
            {
                DBContext.Open();
            }

            var oReturn = DBContext.Query(ssqlGetOneBusinessCateg);

            oResult.iRet = 1;
            oResult.sMessage = "Success";
            oResult.sResult = oReturn;
            return oResult;

        }

        //BusinessCateg Deletion by querybank
        public APiResult DeleteBusinessCategory(GetBusinessCategoryAdminInput _input)
        {

            oResult = new APiResult();
            string sSQlDeleteBusinessCateg = string.Empty;
            oQoeryBusinessCateg = new QueryBank.Roles.BusinessCategoryManage();

            sSQlDeleteBusinessCateg = string.Format(oQoeryBusinessCateg.DeleteBusinessCategory(_input));


            if (DBContext.State == ConnectionState.Closed)
            {
                DBContext.Open();
            }

            int iCount = DBContext.Execute(sSQlDeleteBusinessCateg);
            if (iCount > 0)
            {
                oResult = new APiResult();
                oResult.iRet = 1;
                oResult.sMessage = "Success";
            }
            DBContext.Close();
            return oResult;
        }

        //BusinessCategory Updation by querybank
        public APiResult UpdateBusinessCategory(BusinesscategInputforUpdation Obj)
        {

            oResult = new APiResult();
            string sSQlUpdateBusinessType = string.Empty;
            oQoeryBusinessCateg = new QueryBank.Roles.BusinessCategoryManage();
            int BusinessCategId = Obj.BussinessCategoryId;
            string Name = Obj.BussinessCategoryName;
            string Descr = Obj.BussinessCategoryDecr;
            long BusinessTypeId = Obj.BussinessTypeId;
            DateTime CreatedOn = Obj.CreatedOn;
            string CreatedBy = Obj.CreatedBy;
            DateTime UpdatedOn = Obj.UpdatedOn;
            string UpdatedBy = Obj.UpdatedBy;
            bool IsActive = Obj.IsActive;

            sSQlUpdateBusinessType = string.Format(oQoeryBusinessCateg.UpdateBusinessCategory(BusinessCategId, Name, Descr, BusinessTypeId, CreatedOn, CreatedBy, UpdatedOn, UpdatedBy, IsActive));


            if (DBContext.State == ConnectionState.Closed)
            {
                DBContext.Open();
            }

            int iCount = DBContext.Execute(sSQlUpdateBusinessType);
            if (iCount > 0)
            {
                oResult = new APiResult();
                oResult.iRet = 1;
                oResult.sMessage = "Success";
            }
            DBContext.Close();
            return oResult;
        }




        public ApiResult<List<BusinessCategory>> GetBusinesscategory(GetBusinessCategory _getBusiness)
        {
            ApiResult<List<BusinessCategory>> oResult = new ApiResult<List<BusinessCategory>>();

            if (DBContext.State == ConnectionState.Closed)
            {
                DBContext.Open();
            }

            var oReturn = DBContext.Query<BusinessCategory>("SelectBusinessCategory", _getBusiness, commandType: CommandType.StoredProcedure)as List<BusinessCategory>;
            DBContext.Close();

            oResult.Count = oReturn.Count;
            oResult.HasWarning = false;
            oResult.Message = "Success";
            oResult.Value = oReturn;
            return oResult;

        }

        public ApiResult<List<ShopByBusinessCategoryOut>> ShopByBusinessCategory(ShopByBusinessCategory _shop)
        {
            ApiResult<List<ShopByBusinessCategoryOut>> oResult = new ApiResult<List<ShopByBusinessCategoryOut>>();

            if (DBContext.State == ConnectionState.Closed)
            {
                DBContext.Open();
            }

            var oReturn = DBContext.Query<ShopByBusinessCategoryOut>("ShopSearchByCategory", _shop, commandType: CommandType.StoredProcedure) as List<ShopByBusinessCategoryOut>;
            DBContext.Close();

            oResult.Count = oReturn.Count;
            oResult.HasWarning = false;
            oResult.Message = "Success";
            oResult.Value = oReturn;
            return oResult;

        }

    }
}
