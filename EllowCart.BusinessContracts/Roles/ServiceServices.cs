﻿using Dapper;
using EllowCart.DTO.Service.InputModels;
using EllowCart.DTO.Service.OutputModels;
using EllowCart.Shared;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace EllowCart.BusinessContracts.Roles
{
  public  class ServiceServices
    {

        private IDbConnection DBContext;
        private APiResult oResult = null;
        DAL.DBSettings _DBSettings = null;

        private QueryBank.Roles.BusinessTypeManage oQoeryBusinessType = null;

        public ServiceServices(IDbConnection _DBContext)
        {
            this.DBContext = _DBContext;

        }
        public ServiceServices()
        {
            _DBSettings = new DAL.DBSettings();  //-- New change
            DBContext = _DBSettings.GetCartDB();
        }

        public ApiResult<List<AddservicesOut>> AddServices(AddServices _addServices)
        {
            ApiResult<List<AddservicesOut>> oResult = new ApiResult<List<AddservicesOut>>();

            if (DBContext.State == ConnectionState.Closed)
            {
                DBContext.Open();
            }

            var oReturn = DBContext.Query<AddservicesOut>("AddServices", _addServices, commandType: CommandType.StoredProcedure) as List<AddservicesOut>;

            DBContext.Close();

            oResult.Count = oReturn.Count;
            oResult.HasWarning = false;
            oResult.Message = "Success";
            oResult.Value = oReturn;
            return oResult;

        }

        public ApiResult<List<GetServicesOfABranchOut>> GetServicesOfABranch(GetServiceByBranch _getService)
        {
            ApiResult<List<GetServicesOfABranchOut>> oResult = new ApiResult<List<GetServicesOfABranchOut>>();

            if (DBContext.State == ConnectionState.Closed)
            {
                DBContext.Open();
            }

            var oReturn = DBContext.Query<GetServicesOfABranchOut>("ServicesOfABranch", _getService, commandType: CommandType.StoredProcedure) as List<GetServicesOfABranchOut>;

            DBContext.Close();

            oResult.Count = oReturn.Count;
            oResult.HasWarning = false;
            oResult.Message = "Success";
            oResult.Value = oReturn;
            return oResult;

        }


        public APiResult ServiceDetails(ServiceDetailsInput _Input)
        {
            APiResult oResult = new APiResult();

            if (DBContext.State == ConnectionState.Closed)
            {
                DBContext.Open();
            }

            ServiceDetailsOut _details = new ServiceDetailsOut();

            var oReturn = DBContext.Query<ServiceDetails>("ServiceDetails", _Input , commandType: CommandType.StoredProcedure)as List<ServiceDetails>;

            _details.Details = oReturn;

            DBContext.Close();

            oResult.iRet = oReturn.Count;
           // oResult.sError = false;
            oResult.sMessage = "Success";
            oResult.sResult = oReturn;
            return oResult;

        }
    }
}
