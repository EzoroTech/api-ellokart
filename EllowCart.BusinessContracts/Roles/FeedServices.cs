﻿using Dapper;
using EllowCart.DTO.Feed.InputModels;
using EllowCart.DTO.Feed.OutputModels;
using EllowCart.Shared;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace EllowCart.BusinessContracts.Roles
{
   public class FeedServices
    {
        private IDbConnection DBContext;
        private APiResult oResult = null;
        DAL.DBSettings _DBSettings = null;
        public FeedServices(IDbConnection _DBContext)
        {
            this.DBContext = _DBContext;

        }
        public FeedServices()
        {
            _DBSettings = new DAL.DBSettings();  //-- New change
            DBContext = _DBSettings.GetCartDB();
        }

        public ApiResult<List<AddFeedPostOut>> AddFeedPost(AddFeedPostInput _feedPostInput)
        {
            ApiResult<List<AddFeedPostOut>> oResult = new ApiResult<List<AddFeedPostOut>>();

            if (DBContext.State == ConnectionState.Closed)
            {
                DBContext.Open();
            }

            


            var oReturn = DBContext.Query<AddFeedPostOut>("AddFeedPost", new
            {
                @UserId=_feedPostInput.UserId,
                @role=_feedPostInput.role,
                @BranchId=_feedPostInput.BranchId,
                @AccountType=_feedPostInput.AccountType,
                @Title=_feedPostInput.Title,
                @Description=_feedPostInput.Description,
                @IActive=_feedPostInput.IActive,
                @OfferSpecialId=_feedPostInput.OfferSpecialId,              
                @NewsfeedType=_feedPostInput.NewsfeedType,
               
            }, commandType: CommandType.StoredProcedure) as List<AddFeedPostOut>;
            DBContext.Close();

            oResult.Count = oReturn.Count;
            oResult.HasWarning = false;
            oResult.Message = "Success";
            oResult.Value = oReturn;
            return oResult;

        }

        public ApiResult<List<GetFeedPostOut>> GetFeedPostForPost(GetFeedPostInput _feedPostInput)
        {
            ApiResult<List<GetFeedPostOut>> oResult = new ApiResult<List<GetFeedPostOut>>();


            if (DBContext.State == ConnectionState.Closed)
            {
                DBContext.Open();
            }

            GetFeedPostOfferInput offerinput = new GetFeedPostOfferInput();
            offerinput.BranchId = _feedPostInput.BranchId;
            offerinput.PageSize = _feedPostInput.PageSize;
            offerinput.IntialIndex = _feedPostInput.IntialIndex;

            var oReturn = DBContext.Query<GetFeedPostOut>("GetFeedPostList_Post", _feedPostInput, commandType: CommandType.StoredProcedure) as List<GetFeedPostOut>;
           // var oReturn1 = DBContext.Query<GetFeedPostOut>("GetFeedPostList_Event", _feedPostInput, commandType: CommandType.StoredProcedure) as List<GetFeedPostOut>;
            var oReturn2 = DBContext.Query<GetfeedPostListForOffer>("GetFeedPostList_Offer", _feedPostInput, commandType: CommandType.StoredProcedure) as List<GetfeedPostListForOffer>;

            GetFeeds feed = new GetFeeds();
            feed.offer = oReturn2;
            feed.Post = oReturn;

            DBContext.Close();
            oResult.Count = oReturn.Count;
            oResult.HasWarning = false;
            oResult.Message = "Success";
            oResult.Value = oReturn;
            return oResult;

        }


        public ApiResult<List<GetFeedPostOut>> GetFeedPostForEvent(GetFeedPostInput _feedPostInput)
        {
            ApiResult<List<GetFeedPostOut>> oResult = new ApiResult<List<GetFeedPostOut>>();

            if (DBContext.State == ConnectionState.Closed)
            {
                DBContext.Open();
            }

            var oReturn = DBContext.Query<GetFeedPostOut>("GetFeedPostList_Event", _feedPostInput, commandType: CommandType.StoredProcedure) as List<GetFeedPostOut>;
            DBContext.Close();

            oResult.Count = oReturn.Count;
            oResult.HasWarning = false;
            oResult.Message = "Success";
            oResult.Value = oReturn;
            return oResult;

        }

        public ApiResult<List<GetFeedPostListForOfferImage>> GetFeedPostForOfferImages(GetFeedPostListForOfferImageInput _feedPostInput)
        {
            ApiResult<List<GetFeedPostListForOfferImage>> oResult = new ApiResult<List<GetFeedPostListForOfferImage>>();

            if (DBContext.State == ConnectionState.Closed)
            {
                DBContext.Open();
            }

            var oReturn = DBContext.Query<GetFeedPostListForOfferImage>("GetFeedPostList_OfferImage", _feedPostInput, commandType: CommandType.StoredProcedure) as List<GetFeedPostListForOfferImage>;
            DBContext.Close();

            oResult.Count = oReturn.Count;
            oResult.HasWarning = false;
            oResult.Message = "Success";
            oResult.Value = oReturn;
            return oResult;

        }


        public ApiResult<List<GetfeedPostListForOffer>> GetFeedPostForOffer(GetFeedPostOfferInput _feedPostInput)
        {
            ApiResult<List<GetfeedPostListForOffer>> oResult = new ApiResult<List<GetfeedPostListForOffer>>();

            if (DBContext.State == ConnectionState.Closed)
            {
                DBContext.Open();
            }

            var oReturn = DBContext.Query<GetfeedPostListForOffer>("GetFeedPostList_Offer", _feedPostInput, commandType: CommandType.StoredProcedure) as List<GetfeedPostListForOffer>;
            DBContext.Close();

            oResult.Count = oReturn.Count;
            oResult.HasWarning = false;
            oResult.Message = "Success";
            oResult.Value = oReturn;
            return oResult;

        }


        public ApiResult<List<updateFeedLikedout>> FeedResponse(UpdateFeedLikedInput _feedInput)
        {
            ApiResult<List<updateFeedLikedout>> oResult = new ApiResult<List<updateFeedLikedout>>();

            if (DBContext.State == ConnectionState.Closed)
            {
                DBContext.Open();
            }
            if (_feedInput.Flag == 1)
            {
                try
                {
                    var oReturn = DBContext.Query<updateFeedLikedout>("UpdateFeedLiked",new { @CustomerId =_feedInput.CustomerId, @FeedId =_feedInput.FeedId, @AccountType ="PST"}, commandType: CommandType.StoredProcedure) as List<updateFeedLikedout>;
                    DBContext.Close();
                }
                catch (Exception Ex)
                {
                    oResult.ErrorCode = -1;
                    oResult.HasWarning = true;
                    oResult.Message = Ex.Message;
                }
            }
            else if (_feedInput.Flag == 2)
            {
                try
                {
                    var oReturn = DBContext.Query<updateFeedLikedout>("UpdateFeedDisLiked", new { @CustomerId = _feedInput.CustomerId, @FeedId = _feedInput.FeedId }, commandType: CommandType.StoredProcedure) as List<updateFeedLikedout>;

                    DBContext.Close();
                }
                catch (Exception Ex)
                {
                    oResult.ErrorCode = -1;
                    oResult.HasWarning = true;
                    oResult.Message = Ex.Message;
                }
            }
            else if (_feedInput.Flag == 3)
            {
                try
                {
                    var oReturn = DBContext.Query<updateFeedLikedout>("NewsFeedViewsCount", new { @CustomerId = _feedInput.CustomerId, @FeedId = _feedInput.FeedId }, commandType: CommandType.StoredProcedure) as List<updateFeedLikedout>;

                    DBContext.Close();
                }
                catch (Exception Ex)
                {
                    oResult.ErrorCode = -1;
                    oResult.HasWarning = true;
                    oResult.Message = Ex.Message;
                }
            }
            DBContext.Close();
            oResult.Count = 1;
            oResult.HasWarning = false;
            oResult.Message = "Success";
           
            return oResult;

        }

     

        public ApiResult<List<RemoveFeedPostOut>> RemoveFeedPost(RemoveFeedPost _remove)
        {
            ApiResult<List<RemoveFeedPostOut>> oResult = new ApiResult<List<RemoveFeedPostOut>>();

            if (DBContext.State == ConnectionState.Closed)
            {
                DBContext.Open();
            }

            var oReturn = DBContext.Query<RemoveFeedPostOut>("RemoveFeedPost", _remove, commandType: CommandType.StoredProcedure) as List<RemoveFeedPostOut>;
            DBContext.Close();

            oResult.Count = oReturn.Count;
            oResult.HasWarning = false;
            oResult.Message = "Success";
            oResult.Value = oReturn;
            return oResult;

        }

        public ApiResult<List<GetFeedsCustomerout>> GetFeedsCustomer(GetFeedsCustomer _feeds)
        {
            ApiResult<List<GetFeedsCustomerout>> oResult = new ApiResult<List<GetFeedsCustomerout>>();

            if (DBContext.State == ConnectionState.Closed)
            {
                DBContext.Open();
            }

            var oReturn = DBContext.Query<GetFeedsCustomerout>("GetPostFeedOfaBranchForCustomer", _feeds, commandType: CommandType.StoredProcedure) as List<GetFeedsCustomerout>;
            DBContext.Close();

            oResult.Count = oReturn.Count;
            oResult.HasWarning = false;
            oResult.Message = "Success";
            oResult.Value = oReturn;
            return oResult;

        }



        public APiResult TestFeed(GetFeedPostInput _feedPostInput)
        {
            APiResult oResult = new APiResult();


            if (DBContext.State == ConnectionState.Closed)
            {
                DBContext.Open();
            }


            GetFeedPostOfferInput offerinput = new GetFeedPostOfferInput();
            offerinput.BranchId = _feedPostInput.BranchId;
            offerinput.PageSize = _feedPostInput.PageSize;
            offerinput.IntialIndex = _feedPostInput.IntialIndex;

            var oReturn = DBContext.Query<GetFeedPostOut>("GetFeedPostList_Post", _feedPostInput, commandType: CommandType.StoredProcedure) as List<GetFeedPostOut>;
             var oReturn1 = DBContext.Query<GetFeedEventsOut>("GetFeedPostList_Event", _feedPostInput, commandType: CommandType.StoredProcedure) as List<GetFeedEventsOut>;
            var oReturn2 = DBContext.Query<GetfeedPostListForOffer>("GetFeedPostList_Offer", _feedPostInput, commandType: CommandType.StoredProcedure) as List<GetfeedPostListForOffer>;

            GetFeeds feed = new GetFeeds();
            feed.offer = oReturn2;
            feed.Event = oReturn1;
            feed.Post = oReturn;

            DBContext.Close();

            oResult.sResult = feed;
            oResult.iRet = 1;
            oResult.sMessage = "Success";
            return oResult;

        }
        public ApiResult<List<UpdateFeedOut>> UpdateFeeds(UpdateFeed _feeds)
        {
            ApiResult<List<UpdateFeedOut>> oResult = new ApiResult<List<UpdateFeedOut>>();

            if (DBContext.State == ConnectionState.Closed)
            {
                DBContext.Open();
            }

            var oReturn = DBContext.Query<UpdateFeedOut>("UpdateFeeds", _feeds, commandType: CommandType.StoredProcedure) as List<UpdateFeedOut>;
            DBContext.Close();

            oResult.Count = oReturn.Count;
            oResult.HasWarning = false;
            oResult.Message = "Success";
            oResult.Value = oReturn;
            return oResult;

        }
          public APiResult FeedsBySeller(FeedsBySeller _feedPostInput)
        {
            APiResult oResult = new APiResult();


            if (DBContext.State == ConnectionState.Closed)
            {
                DBContext.Open();
            }


            var oReturn = DBContext.Query<GetFeedPostOut>("FeedsBySeller", _feedPostInput, commandType: CommandType.StoredProcedure) as List<GetFeedPostOut>;
            // var oReturn1 = DBContext.Query<GetFeedEventsOut>("GetFeedPostList_Event", _feedPostInput, commandType: CommandType.StoredProcedure) as List<GetFeedEventsOut>;
            //var oReturn2 = DBContext.Query<GetfeedPostListForOffer>("GetFeedPostList_Offer", _feedPostInput, commandType: CommandType.StoredProcedure) as List<GetfeedPostListForOffer>;

            GetFeeds feed = new GetFeeds();
            //feed.offer = oReturn2;
            //feed.Event = oReturn1;
            feed.Post = oReturn;

            DBContext.Close();

            oResult.sResult = feed;
            oResult.iRet = 1;
            oResult.sMessage = "Success";
            return oResult;

        }

    }
}
