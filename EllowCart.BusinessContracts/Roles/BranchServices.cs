﻿using Dapper;
using EllowCart.DTO.Branch.InputModels;
using EllowCart.DTO.Branch.OutputModels;
using EllowCart.Shared;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace EllowCart.BusinessContracts.Roles
{
    public class BranchServices
    {
        private IDbConnection DBContext;
        DAL.DBSettings _DBSettings = null;

        public BranchServices(IDbConnection _DBContext)
        {
            this.DBContext = _DBContext;

        }
        public BranchServices()
        {
            _DBSettings = new DAL.DBSettings();  //-- New change
            DBContext = _DBSettings.GetCartDB();
        }

        public ApiResult<List<CreateBranchOut>> BranchRegisteration(DTO.Branch.InputModels.BranchRegisteration _branch)
        {
            ApiResult<List<CreateBranchOut>> oResult = new ApiResult<List<CreateBranchOut>>();

            if (DBContext.State == ConnectionState.Closed)
            {
                DBContext.Open();
            }

            if (_branch.UserId != 0 || _branch.role == "USR" || _branch.role == "STF" || _branch.UserBussinessId != 0)
            {

                var oReturn = DBContext.Query<CreateBranchOut>("BranchRegistration", new
                {
                    @UserId = _branch.UserId,
                    @role = _branch.role,
                    @UserBussinessId = _branch.UserBussinessId,
                    @BranchName = _branch.BranchName,
                    @DisplayName = _branch.DisplayName,
                    @Addresslineone = _branch.Addresslineone,
                    @Addresslinetwo = _branch.Addresslinetwo,
                    @Addresslinethree = _branch.Addresslinethree,
                    @CityId = _branch.CityId,
                    @StateId = _branch.StateId,
                    @CountryId = _branch.CountryId,
                    @Pincode = _branch.Pincode,
                    @Lattittude = _branch.Lattittude,
                    @Longittude = _branch.Longittude,
                    @ContactPerson = _branch.ContactPerson,
                    @MobileNumber = _branch.MobileNumber,
                    @EmailId = _branch.EmailId
                }, commandType: CommandType.StoredProcedure) as List<CreateBranchOut>;

                DBContext.Close();

                oResult.Count = oReturn.Count;
                oResult.HasWarning = false;
                oResult.Message = "Success";
                oResult.Value = oReturn;
            }
            else
            {
                oResult.Message = "Must Insert UserId,Role and UserBusinessid. Role=USR or STF ";
            }
            return oResult;
        }

        public ApiResult<List<UpdateBranchOut>> UpdateBranch(UpdateBranch _update)
        {
            ApiResult<List<UpdateBranchOut>> oResult = new ApiResult<List<UpdateBranchOut>>();

            if (DBContext.State == ConnectionState.Closed)
            {
                DBContext.Open();
            }

            if (_update.UserId != 0 || _update.UserBussinessId != 0 || _update.role == "USR" || _update.role == "STF" || _update.BranchId != 0)
            {
                var oReturn = DBContext.Query<UpdateBranchOut>("UpdateBranchBasic", _update, commandType: CommandType.StoredProcedure) as List<UpdateBranchOut>;

                DBContext.Close();

                oResult.Count = oReturn.Count;
                oResult.HasWarning = false;
                oResult.Message = "Success";
                oResult.Value = oReturn;
            }
            else
            {
                oResult.Message = "Must insert UserId,BranchId,UserBusinessid and Role. Role=USR or STF";
            }
            DBContext.Close();
            return oResult;

        }


        public ApiResult<List<UpdateBranchOut>> UpdateBranchAddress(UpdateBranchAddress _address)
        {
            ApiResult<List<UpdateBranchOut>> oResult = new ApiResult<List<UpdateBranchOut>>();

            if (DBContext.State == ConnectionState.Closed)
            {
                DBContext.Open();
            }

            if (_address.UserId != 0 || _address.BranchId != 0)
            {

                var oReturn = DBContext.Query<UpdateBranchOut>("UpdateBranchAddressDetails", _address, commandType: CommandType.StoredProcedure) as List<UpdateBranchOut>;

                DBContext.Close();

                oResult.Count = oReturn.Count;
                oResult.HasWarning = false;
                oResult.Message = "Success";
                oResult.Value = oReturn;
            }
            else
            {
                oResult.HasWarning = true;
                oResult.Message = "must insert UerId,BranchId";
            }
            DBContext.Close();
            return oResult;

        }


        public ApiResult<List<UpdateBranchOut>> UpdateBranchBankDetails(UpdateBankBranchDetails _bank)
        {
            ApiResult<List<UpdateBranchOut>> oResult = new ApiResult<List<UpdateBranchOut>>();

            if (DBContext.State == ConnectionState.Closed)
            {
                DBContext.Open();
            }

            var dt = new DataTable("UserDocuments_type");
            dt.Columns.Add("ImageUrl", typeof(string));
            dt.Columns.Add("ImageName", typeof(string));
            dt.Columns.Add("Type", typeof(string));

            var document = _bank.dTUserDocuments;
            for (int i = 0; i < document.Count; i++)
            {
                dt.Rows.Add(document[i].ImageUrl, document[i].ImageName, document[i].Type);
            }

            if (_bank.UserId != 0 || _bank.BranchId != 0)
            {
                var oReturn = DBContext.Query<UpdateBranchOut>("UpdateBankDetails", new
                {
                    @UserId = _bank.UserId,
                    @role = _bank.role,
                    @BranchId = _bank.BranchId,
                    @AccntHolder = _bank.AccntHolder,
                    @AccntNumber = _bank.AccntNumber,
                    @IFSC_Code = _bank.IFSC_Code,
                    @BankName = _bank.BankName,
                    @Branch = _bank.Branch,
                    @Flag = _bank.Flag,
                    @dTUserDocuments = dt.AsTableValuedParameter("UserDocuments_type")
                }, commandType: CommandType.StoredProcedure) as List<UpdateBranchOut>;

                DBContext.Close();


                oResult.Count = oReturn.Count;
                oResult.HasWarning = false;
                oResult.Message = "Success";
                oResult.Value = oReturn;
            }
            else
            {
                oResult.HasWarning = true;
                oResult.Message = "Must Insert UserId and BranchId";
            }
            DBContext.Close();
            return oResult;

        }

        public ApiResult<List<GetBranches>> GetBranches(GetBranchesInput _branch)
        {
            ApiResult<List<GetBranches>> oResult = new ApiResult<List<GetBranches>>();

            if (DBContext.State == ConnectionState.Closed)
            {
                DBContext.Open();
            }

            var oReturn = DBContext.Query<GetBranches>("GetBranches", _branch, commandType: CommandType.StoredProcedure) as List<GetBranches>;

            DBContext.Close();


            oResult.Count = oReturn.Count;
            oResult.HasWarning = false;
            oResult.Message = "Success";
            oResult.Value = oReturn;
            return oResult;

        }

        public ApiResult<List<BranchDetailsOut>> GetBrancheDetails(GetBranchDetailsInput _input)
        {
            ApiResult<List<BranchDetailsOut>> oResult = new ApiResult<List<BranchDetailsOut>>();

            if (DBContext.State == ConnectionState.Closed)
            {
                DBContext.Open();
            }

            var oReturn = DBContext.Query<BranchDetailsOut>("LoadBranchDetails", _input, commandType: CommandType.StoredProcedure) as List<BranchDetailsOut>;

            DBContext.Close();


            oResult.Count = oReturn.Count;
            oResult.HasWarning = false;
            oResult.Message = "Success";
            oResult.Value = oReturn;
            return oResult;

        }

        public ApiResult<List<addBranchPhotoOut>> AddBranchPhoto(AddBranchPhotoInput _input)
        {
            ApiResult<List<addBranchPhotoOut>> oResult = new ApiResult<List<addBranchPhotoOut>>();

            if (DBContext.State == ConnectionState.Closed)
            {
                DBContext.Open();
            }

            if (_input.UserId != 0 || _input.BranchId != 0)
            {
                var oReturn = DBContext.Query<addBranchPhotoOut>("AddBranchPhoto", _input, commandType: CommandType.StoredProcedure) as List<addBranchPhotoOut>;

                DBContext.Close();


                oResult.Count = oReturn.Count;
                oResult.HasWarning = false;
                oResult.Message = "Success";
                oResult.Value = oReturn;
            }
            else
            {
                DBContext.Close();
                oResult.HasWarning = true;
                oResult.Message = "Must Insert Userid and BranchId";
            }
            
            return oResult;

        }


        public ApiResult<List<GetBranchPhotoOut>> GetBranchPhoto(GetBranchPhotoInput _input)
        {
            ApiResult<List<GetBranchPhotoOut>> oResult = new ApiResult<List<GetBranchPhotoOut>>();

            if (DBContext.State == ConnectionState.Closed)
            {
                DBContext.Open();
            }

            var oReturn = DBContext.Query<GetBranchPhotoOut>("GetBranchPhotos", _input, commandType: CommandType.StoredProcedure) as List<GetBranchPhotoOut>;
            DBContext.Close();

            oResult.Count = oReturn.Count;
            oResult.HasWarning = false;
            oResult.Message = "Success";
            oResult.Value = oReturn;
            return oResult;

        }

        public ApiResult<List<RemoveBranchPhotoOut>> RemoveBranchPhoto(RemoveBranchPhoto _input)
        {
            ApiResult<List<RemoveBranchPhotoOut>> oResult = new ApiResult<List<RemoveBranchPhotoOut>>();

            if (DBContext.State == ConnectionState.Closed)
            {
                DBContext.Open();
            }

            var oReturn = DBContext.Query<RemoveBranchPhotoOut>("RemoveBranchPhoto", _input, commandType: CommandType.StoredProcedure) as List<RemoveBranchPhotoOut>;
            DBContext.Close();

            oResult.Count = oReturn.Count;
            oResult.HasWarning = false;
            oResult.Message = "Success";
            oResult.Value = oReturn;
            return oResult;

        }

        public ApiResult<List<RemoveBranchOut>> RemoveBranch(RemoveBranch _input)
        {
            ApiResult<List<RemoveBranchOut>> oResult = new ApiResult<List<RemoveBranchOut>>();

            if (DBContext.State == ConnectionState.Closed)
            {
                DBContext.Open();
            }

            var oReturn = DBContext.Query<RemoveBranchOut>("RemoveBranch", _input, commandType: CommandType.StoredProcedure) as List<RemoveBranchOut>;
            DBContext.Close();

            oResult.Count = oReturn.Count;
            oResult.HasWarning = false;
            oResult.Message = "Success";
            oResult.Value = oReturn;
            return oResult;

        }

        public ApiResult<List<SetDeafaultBranchOut>> DefaultBranch(SetDeafultBranchInput _input)
        {
            ApiResult<List<SetDeafaultBranchOut>> oResult = new ApiResult<List<SetDeafaultBranchOut>>();

            if (DBContext.State == ConnectionState.Closed)
            {
                DBContext.Open();
            }

            var oReturn = DBContext.Query<SetDeafaultBranchOut>("SetDefaultBranch", _input, commandType: CommandType.StoredProcedure) as List<SetDeafaultBranchOut>;
            DBContext.Close();

            oResult.Count = oReturn.Count;
            oResult.HasWarning = false;
            oResult.Message = "Success";
            oResult.Value = oReturn;
            return oResult;

        }



        public ApiResult<List<GetBranchLogoOut>> GetBranchLogo(GetBranchLogo _input)
        {
            ApiResult<List<GetBranchLogoOut>> oResult = new ApiResult<List<GetBranchLogoOut>>();

            if (DBContext.State == ConnectionState.Closed)
            {
                DBContext.Open();
            }

            var oReturn = DBContext.Query<GetBranchLogoOut>("GetBranchLogo", _input, commandType: CommandType.StoredProcedure) as List<GetBranchLogoOut>;
            DBContext.Close();

            oResult.Count = oReturn.Count;
            oResult.HasWarning = false;
            oResult.Message = "Success";
            oResult.Value = oReturn;
            return oResult;

        }

        
    }
}
