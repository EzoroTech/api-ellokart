﻿using Dapper;
using EllowCart.DTO.AddImages.Inputmodels;
using EllowCart.DTO.AddImages.OutputModels;
using EllowCart.Shared;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;


namespace EllowCart.BusinessContracts.Roles
{
    public class AddImagesServices
    {
        private IDbConnection DBContext;
        public string code = "";
        ImageCodeOut img = new ImageCodeOut();
        DAL.DBSettings _DBSettings = null;  //-- New chage
        ImageUploadServices ius = new ImageUploadServices();

        public AddImagesServices(IDbConnection _DBContext)
        {
            this.DBContext = _DBContext;

        }
        public AddImagesServices()
        {
            _DBSettings = new DAL.DBSettings();  //-- New change
            DBContext = _DBSettings.GetCartDB();
            
        }
        public string ThumbUrl()
        {
           
            if (DBContext.State == ConnectionState.Closed)
            {
                DBContext.Open();
            }

            var result = DBContext.Query<ImageCodeOut>("GenerateUserCode", new { @CodeSection = 6, @CreatedBy = "preemy" }, commandType: CommandType.StoredProcedure) as List<ImageCodeOut>;

            DBContext.Close();

            code = result[0].CodeRefId;
           
            return code;
        }


    }
}
