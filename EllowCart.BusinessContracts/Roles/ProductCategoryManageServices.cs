﻿using Dapper;
using EllowCart.DTO.ProductCategory.InputModels;
using EllowCart.DTO.ProductCategory.OutputModels;
using EllowCart.Shared;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace EllowCart.BusinessContracts.Roles
{
    public class ProductCategoryManageServices
    {
        private IDbConnection DBContext;
        private APiResult oResult = null;
        DAL.DBSettings _DBSettings = null;
        private QueryBank.Roles.ProductCategoryManage pdtCategory = null;

        public ProductCategoryManageServices(IDbConnection _DBContext)
        {
            this.DBContext = _DBContext;

        }
        public ProductCategoryManageServices()
        {
            _DBSettings = new DAL.DBSettings();  //-- New change
            DBContext = _DBSettings.GetCartDB();
        }



        //ProductCategory Deletion by querybank
        public APiResult DeleteAProductCategory(int ProductCategoryId)
        {

            oResult = new APiResult();
            string sSQlProductCategory = string.Empty;
            pdtCategory = new QueryBank.Roles.ProductCategoryManage();

            sSQlProductCategory = string.Format(pdtCategory.DeleteProductCategory(ProductCategoryId));


            if (DBContext.State == ConnectionState.Closed)
            {
                DBContext.Open();
            }

            int iCount = DBContext.Execute(sSQlProductCategory);
            if (iCount > 0)
            {
                oResult = new APiResult();
                oResult.iRet = 1;
                oResult.sMessage = "Successfully deleted ProductCategory";
            }
            DBContext.Close();
            return oResult;
        }


        //ProductCategory Updation by querybank
        public APiResult UpdateProductCategory(ProductCategoryInputForAdmin Obj, int ProductCategoryId)
        {

            oResult = new APiResult();
            string sSQlUpdateProductCategory = string.Empty;
            pdtCategory = new QueryBank.Roles.ProductCategoryManage();
            string CategoryName = Obj.CategoryName;
            long parentCategId = Obj.ParentCategoryID;
            string Descr = Obj.Description;
          
            bool IsActive = Obj.IsActive;
          
            string UpdatedBy = Obj.UpdatedBy;
           
            sSQlUpdateProductCategory = string.Format(pdtCategory.UpdateProductCategory(CategoryName, parentCategId, Descr,  IsActive, UpdatedBy, ProductCategoryId));


            if (DBContext.State == ConnectionState.Closed)
            {
                DBContext.Open();
            }

            int iCount = DBContext.Execute(sSQlUpdateProductCategory);
            if (iCount > 0)
            {
                oResult = new APiResult();
                oResult.iRet = 1;
                oResult.sMessage = "Successfully updated the product Category";
            }
            DBContext.Close();
            return oResult;
        }

       


        //getting productCategory querybank
        public APiResult GetProductCategoryById(int ProductCategoryId)
        {
            oResult = new APiResult();
            pdtCategory = new QueryBank.Roles.ProductCategoryManage();
            string ssqlGetAProductCategory = pdtCategory.GetAProductCategory(ProductCategoryId);
            if (DBContext.State == ConnectionState.Closed)
            {
                DBContext.Open();
            }

            var oReturn = DBContext.Query<ProductCategoryInputForAdmin>(ssqlGetAProductCategory);
            DBContext.Close();
            oResult.iRet = 1;
            oResult.sMessage = "Success";
            oResult.sResult = oReturn;
            return oResult;

        }

    }
}
