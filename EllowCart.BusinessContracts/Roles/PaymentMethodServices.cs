﻿using Dapper;
using EllowCart.DTO.PaymentMethods.InputModels;
using EllowCart.DTO.PaymentMethods.OtputModels;
using EllowCart.Shared;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace EllowCart.BusinessContracts.Roles
{
    public class PaymentMethodServices
    {
        private IDbConnection DBContext;
        private APiResult oResult = null;
        private QueryBank.Roles.PaymentMethodmange oQoeryBusinessCateg = null;
        DAL.DBSettings _DBSettings = null;

        public PaymentMethodServices(IDbConnection _DBContext)
        {
            this.DBContext = _DBContext;

        }
        public PaymentMethodServices()
        {
            _DBSettings = new DAL.DBSettings();  //-- New change
            DBContext = _DBSettings.GetCartDB();
        }

        public APiResult GetAllPaymentMethods()
        {
            oResult = new APiResult();
            string sSQlAddBusinessCategory = string.Empty;
            oQoeryBusinessCateg = new QueryBank.Roles.PaymentMethodmange();

            sSQlAddBusinessCategory = oQoeryBusinessCateg.GetAllPaymentMethods();


            if (DBContext.State == ConnectionState.Closed)
            {
                DBContext.Open();
            }
            var oReturn = DBContext.Query(sSQlAddBusinessCategory);

            oResult = new APiResult();
            oResult.iRet = 1;
            oResult.sMessage = "Success";
            oResult.sResult = oReturn;

            DBContext.Close();
            return oResult;
        }


        public ApiResult<List<PaymentDetailsSellerOut>> AddPaymentDetailsBySeller(PaymentDetailsSeller _payment)
        {
            ApiResult<List<PaymentDetailsSellerOut>> oResult = new ApiResult<List<PaymentDetailsSellerOut>>();

            if (DBContext.State == ConnectionState.Closed)
            {
                DBContext.Open();
            }

            var oReturn = DBContext.Query<PaymentDetailsSellerOut>("AddPaymentOptionDetails", _payment, commandType: CommandType.StoredProcedure) as List<PaymentDetailsSellerOut>;

            DBContext.Close();

            oResult.Count = oReturn.Count;
            oResult.HasWarning = false;
            oResult.Message = "Success";
            oResult.Value = oReturn;
            return oResult;

        }

        public ApiResult<List<GetPaymentDetailsOut>> GetPaymentDetailsBySeller(GetPaymentDetailsSeller _payment)
        {
            ApiResult<List<GetPaymentDetailsOut>> oResult = new ApiResult<List<GetPaymentDetailsOut>>();

            if (DBContext.State == ConnectionState.Closed)
            {
                DBContext.Open();
            }

            var oReturn = DBContext.Query<GetPaymentDetailsOut>("GetSellersPaymentMethods", _payment, commandType: CommandType.StoredProcedure) as List<GetPaymentDetailsOut>;

            DBContext.Close();

            oResult.Count = oReturn.Count;
            oResult.HasWarning = false;
            oResult.Message = "Success";
            oResult.Value = oReturn;
            return oResult;

        }

        public ApiResult<List<UserPaidOrNotOut>> CheckUserPaidOrNot(UserPaidOrNot _input)
        {
            ApiResult<List<UserPaidOrNotOut>> oResult = new ApiResult<List<UserPaidOrNotOut>>();

            if (DBContext.State == ConnectionState.Closed)
            {
                DBContext.Open();
            }

            if (_input.TransId != "string")
            {
                var oReturn1 = DBContext.Query<UserPaidOrNotOut>("CheckUserPaidOrNot", new
                {
                    @UserId = _input.UserId,
                    @PlanId = _input.PlanId,
                    @TransId = _input.TransId,
                    @GatewayId = "RZR",
                    @Flag = "UPDATE"
                }, commandType: CommandType.StoredProcedure) as List<UserPaidOrNotOut>;
            }
        

            if (_input.Flag == "string")
            {
                _input.Flag = "";
            }



            var oReturn = DBContext.Query<UserPaidOrNotOut>("CheckUserPaidOrNot", _input, commandType: CommandType.StoredProcedure) as List<UserPaidOrNotOut>;

            DBContext.Close();

            oResult.Count = oReturn.Count;
            oResult.HasWarning = false;
            oResult.Message = "Success";
            oResult.Value = oReturn;
            return oResult;

        }

        public ApiResult<List<UserPaidOrNot>> CheckPaymentStatus(UserPaidOrNot _input)
        {
            ApiResult<List<UserPaidOrNot>> oResult = new ApiResult<List<UserPaidOrNot>>();

            if (DBContext.State == ConnectionState.Closed)
            {
                DBContext.Open();
            }

                var oReturn1 = DBContext.Query<UserPaidOrNot>("CheckUserPaidOrNot", new
                {
                    @UserId = _input.UserId,
                    @PlanId = 1,
                    @TransId = "",
                    @GatewayId = "rzr",
                    @Flag = "STATUS"
                }, commandType: CommandType.StoredProcedure) as List<UserPaidOrNot>;

            DBContext.Close();

           
            oResult.Value = oReturn1;
            return oResult;

        }


    }
}
