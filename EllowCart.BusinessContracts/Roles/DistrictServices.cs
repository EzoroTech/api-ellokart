﻿using Dapper;
using EllowCart.DTO.District.InputModels;
using EllowCart.DTO.District.OutputModels;
using EllowCart.Shared;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace EllowCart.BusinessContracts.Roles
{
  public  class DistrictServices
    {
        private IDbConnection DBContext;
        DAL.DBSettings _DBSettings = null;
        public DistrictServices(IDbConnection _DBContext)
        {
            this.DBContext = _DBContext;

        }
        public DistrictServices()
        {
            _DBSettings = new DAL.DBSettings();  //-- New change
            DBContext = _DBSettings.GetCartDB();
        }

        public ApiResult<List<GetDistrictListOut>> GetDistrictlist(GetDistrictListInput _getDistrict)
        {
            ApiResult<List<GetDistrictListOut>> oResult = new ApiResult<List<GetDistrictListOut>>();

            if (DBContext.State == ConnectionState.Closed)
            {
                DBContext.Open();
            }

            var oReturn = DBContext.Query<GetDistrictListOut>("GetDistrictList", _getDistrict, commandType: CommandType.StoredProcedure) as List<GetDistrictListOut>;
            DBContext.Close();
            oResult.Count = oReturn.Count;
            oResult.HasWarning = false;
            oResult.Message = "Success";
            oResult.Value = oReturn;

            return oResult;

        }
    }
}
