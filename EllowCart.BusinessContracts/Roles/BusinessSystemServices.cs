﻿using Dapper;
using EllowCart.DTO.BusinessSystem.OutputModels;
using EllowCart.Shared;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace EllowCart.BusinessContracts.Roles
{
  public  class BusinessSystemServices
    {
        private IDbConnection DBContext;
        private APiResult oResult = null;
        DAL.DBSettings _DBSettings = null;

        public BusinessSystemServices(IDbConnection _DBContext)
        {
            this.DBContext = _DBContext;

        }
        public BusinessSystemServices()
        {
            _DBSettings = new DAL.DBSettings();  //-- New change
            DBContext = _DBSettings.GetCartDB();
        }
        public ApiResult<List<BusinessSystemOut>> BusinessSystem(DTO.BusinessSystem.InputModels.Businesssystem _businesssyst)
        {
            ApiResult<List<BusinessSystemOut>> oResult = new ApiResult<List<BusinessSystemOut>>();
            if (DBContext.State == ConnectionState.Closed)
            {
                DBContext.Open();
            }

            var oReturn = DBContext.Query<BusinessSystemOut>("BussinessSystm_INSRT", _businesssyst,  commandType: CommandType.StoredProcedure)as List<BusinessSystemOut>;

            DBContext.Close();

            oResult.Count = oReturn.Count;
            oResult.HasWarning = false;
            oResult.Message = "Success";
            oResult.Value = oReturn;
            return oResult;

        }

        public ApiResult<List<BusinesssystemDetails>> GetBusinessSystemAdmin(DTO.BusinessSystem.InputModels.BusinessSystemSelect _businesssyst)
        {
            ApiResult<List<BusinesssystemDetails>> oResult = new ApiResult<List<BusinesssystemDetails>>();
            if (DBContext.State == ConnectionState.Closed)
            {
                DBContext.Open();
            }

            var oReturn = DBContext.Query<BusinesssystemDetails>("BussinessSystmSelect", _businesssyst,commandType: CommandType.StoredProcedure)as List<BusinesssystemDetails>;
            DBContext.Close();


            oResult.Count = oReturn.Count;
            oResult.HasWarning = false;
            oResult.Message = "Success";
            oResult.Value = oReturn;
            return oResult;
        }

        public ApiResult<List<BusinessSystemUser>> GetBusinessSystem()
        {
            ApiResult<List<BusinessSystemUser>> oResult = new ApiResult<List<BusinessSystemUser>>();
            if (DBContext.State == ConnectionState.Closed)
            {
                DBContext.Open();
            }

            var oReturn = DBContext.Query<BusinessSystemUser>("GetBussinessSystm", commandType: CommandType.StoredProcedure) as List<BusinessSystemUser>;
            DBContext.Close();


            oResult.Count = oReturn.Count;
            oResult.HasWarning = false;
            oResult.Message = "Success";
            oResult.Value = oReturn;
            return oResult;
        }

    }
}
