﻿using Dapper;
using EllowCart.DTO.Feed.OutputModels;
using EllowCart.DTO.Home.InputModels;
using EllowCart.DTO.Home.OutputModels;
using EllowCart.DTO.Upgrade.InputModel;
using EllowCart.Shared;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;




namespace EllowCart.BusinessContracts.Roles
{
    public class HomeServices
    {
        private IDbConnection DBContext;
        private APiResult oResult = null;
        DAL.DBSettings _DBSettings = null;

        private Shared.Email.MailSend _mailWithAttachment = null;


        public HomeServices(IDbConnection _DBContext)
        {
            this.DBContext = _DBContext;

        }
        public HomeServices()
        {
            _DBSettings = new DAL.DBSettings();  //-- New change
            DBContext = _DBSettings.GetCartDB();
        }


        public APiResult GetNearByShpos(GetNearByShopsInput _shopsInput)
        {
            APiResult oResult = new APiResult();

            if (DBContext.State == ConnectionState.Closed)
            {
                DBContext.Open();
            }

            var oReturn = DBContext.Query<GetNearByShopsOut>("GetNearByShopes", _shopsInput, commandType: CommandType.StoredProcedure) as List<GetNearByShopsOut>;

            var oReturn1 = DBContext.Query<HomeofferOut>("GetNearByOffers", new
            {
                @Lattitude = _shopsInput.Lattitude,
                @Longitude=_shopsInput.Longitude,
                @Index=_shopsInput.Index,
                @PageSize=_shopsInput.PageSize
            }, commandType: CommandType.StoredProcedure) as List<HomeofferOut>;

            ShopandOffersOutput Result = new ShopandOffersOutput();
            Result.Shops = oReturn;
            Result.Offers = oReturn1;

            DBContext.Close();

            oResult.iRet = oReturn.Count;
           // oResult.sError = false;
            oResult.sMessage = "Success";
            oResult.sResult = Result;
            return oResult;

        }

        public ApiResult<List<GetShopDetailsout>> GetShpoDetails(GetShopDetailsInput _shopsInput)
        {
            ApiResult<List<GetShopDetailsout>> oResult = new ApiResult<List<GetShopDetailsout>>();

            if (DBContext.State == ConnectionState.Closed)
            {
                DBContext.Open();
            }

            var oReturn = DBContext.Query<GetShopDetailsout>("GetShopDetails", _shopsInput, commandType: CommandType.StoredProcedure) as List<GetShopDetailsout>;
            DBContext.Close();

            oResult.Count = oReturn.Count;
            oResult.HasWarning = false;
            oResult.Message = "Success";
            oResult.Value = oReturn;
            return oResult;

        }

        public APiResult GetNearByOffers(GetNearByOffers _getoffers)
        {
            APiResult oResult = new APiResult();

            if (DBContext.State == ConnectionState.Closed)
            {
                DBContext.Open();
            }

            GetNearByOfferAndService _getNearBy = new GetNearByOfferAndService();

            var oReturn = DBContext.Query<GetNearByOffersOut>("GetOffersOfaRegion", _getoffers, commandType: CommandType.StoredProcedure) as List<GetNearByOffersOut>;

            var OReturn1 = DBContext.Query<GetNearByServices>("GetNearByServices",_getoffers, commandType:CommandType.StoredProcedure)as List<GetNearByServices>;

            _getNearBy.Offers = oReturn;
            _getNearBy.Services = OReturn1;

            DBContext.Close();

            oResult.iRet = oReturn.Count;
            oResult.sError = "false";
            oResult.sMessage = "Success";
            oResult.sResult = _getNearBy;
            return oResult;

        }

        public ApiResult<List<GetNearByStatusOut>> GetNearByStatus(GetNearByStatus _getStatus)
        {
            ApiResult<List<GetNearByStatusOut>> oResult = new ApiResult<List<GetNearByStatusOut>>();

            if (DBContext.State == ConnectionState.Closed)
            {
                DBContext.Open();
            }

            var oReturn = DBContext.Query<GetNearByStatusOut>("GetOfferStatusListForCustomer", _getStatus, commandType: CommandType.StoredProcedure) as List<GetNearByStatusOut>;
            DBContext.Close();

            oResult.Count = oReturn.Count;
            oResult.HasWarning = false;
            oResult.Message = "Success";
            oResult.Value = oReturn;
            return oResult;

        }

        public ApiResult<List<ProductListByShopOut>> ProductListByShop(ProductListByShop _shop)
        {
            ApiResult<List<ProductListByShopOut>> oResult = new ApiResult<List<ProductListByShopOut>>();

            if (DBContext.State == ConnectionState.Closed)
            {
                DBContext.Open();
            }

            var oReturn = DBContext.Query<ProductListByShopOut>("GetProductListForaBranch", _shop, commandType: CommandType.StoredProcedure) as List<ProductListByShopOut>;
            DBContext.Close();

            oResult.Count = oReturn.Count;
            oResult.HasWarning = false;
            oResult.Message = "Success";
            oResult.Value = oReturn;
            return oResult;

        }


        public APiResult GetFeeds(FeedPostListInput _event)
        {
            APiResult oResult = new APiResult();

            if (DBContext.State == ConnectionState.Closed)
            {
                DBContext.Open();
            }

            var oReturn = DBContext.Query<FeedPostListOutput>("GetHomeFeedsForCustomer", _event, commandType: CommandType.StoredProcedure) as List<FeedPostListOutput>;
            //var oReturn2 = DBContext.Query<FeedOfferListOutput>("GetOfferPostListForCustomer", _event, commandType: CommandType.StoredProcedure) as List<FeedOfferListOutput>;
            //var oReturn3 = DBContext.Query<FeedEventListOutput>("GetFeedEventListForCustomer", _event, commandType: CommandType.StoredProcedure) as List<FeedEventListOutput>;

            GetFeedsOut feeds = new GetFeedsOut();
            feeds.Post = oReturn;
            //feeds.Offer = oReturn2;
            //feeds.Event = oReturn3;

            DBContext.Close();

            oResult.sResult = feeds;
            oResult.iRet = 1;
            oResult.sMessage = "Success";
            return oResult;


        }

        public ApiResult<List<ShopSearchOutput>> ShopSearch(ShopSearchInput _search)
        {
            ApiResult<List<ShopSearchOutput>> oResult = new ApiResult<List<ShopSearchOutput>>();

            if (DBContext.State == ConnectionState.Closed)
            {
                DBContext.Open();
            }

            var oReturn = DBContext.Query<ShopSearchOutput>("StoresSearch", _search, commandType: CommandType.StoredProcedure) as List<ShopSearchOutput>;
            DBContext.Close();

            oResult.Count = oReturn.Count;
            oResult.HasWarning = false;
            oResult.Message = "Success";
            oResult.Value = oReturn;
            return oResult;

        }

        public ApiResult<List<ShopSearchByCategory>> ShopSearchByCategory(ShopSearchByCategoryInput _search)
        {
            ApiResult<List<ShopSearchByCategory>> oResult = new ApiResult<List<ShopSearchByCategory>>();

            if (DBContext.State == ConnectionState.Closed)
            {
                DBContext.Open();
            }

            var oReturn = DBContext.Query<ShopSearchByCategory>("ShopSearchByCategory", _search, commandType: CommandType.StoredProcedure) as List<ShopSearchByCategory>;
            DBContext.Close();

            oResult.Count = oReturn.Count;
            oResult.HasWarning = false;
            oResult.Message = "Success";
            oResult.Value = oReturn;
            return oResult;

        }

        public ApiResult<List<ProductSearchOut>> ProductSearch(SearchProduct _search)
        {
            ApiResult<List<ProductSearchOut>> oResult = new ApiResult<List<ProductSearchOut>>();

            if (DBContext.State == ConnectionState.Closed)
            {
                DBContext.Open();
            }

            var oReturn = DBContext.Query<ProductSearchOut>("ProductSearch", _search, commandType: CommandType.StoredProcedure) as List<ProductSearchOut>;
            DBContext.Close();

            oResult.Count = oReturn.Count;
            oResult.HasWarning = false;
            oResult.Message = "Success";
            oResult.Value = oReturn;
            return oResult;

        }

        public ApiResult<List<ProductSearchByCategoryOut>> ProductSearchByCategory(ProductSearchByCategory _search)
        {
            ApiResult<List<ProductSearchByCategoryOut>> oResult = new ApiResult<List<ProductSearchByCategoryOut>>();

            if (DBContext.State == ConnectionState.Closed)
            {
                DBContext.Open();
            }

            var oReturn = DBContext.Query<ProductSearchByCategoryOut>("ProductSearchByCategory", _search, commandType: CommandType.StoredProcedure) as List<ProductSearchByCategoryOut>;
            DBContext.Close();

            oResult.Count = oReturn.Count;
            oResult.HasWarning = false;
            oResult.Message = "Success";
            oResult.Value = oReturn;
            return oResult;

        }

        public ApiResult<List<GetAllProducts>> GetAllProducts()
        {
            ApiResult<List<GetAllProducts>> oResult = new ApiResult<List<GetAllProducts>>();

            if (DBContext.State == ConnectionState.Closed)
            {
                DBContext.Open();
            }

            var oReturn = DBContext.Query<GetAllProducts>("GetAllProducts", commandType: CommandType.StoredProcedure) as List<GetAllProducts>;
            DBContext.Close();

            oResult.Count = oReturn.Count;
            oResult.HasWarning = false;
            oResult.Message = "Success";
            oResult.Value = oReturn;
            return oResult;

        }

        public ApiResult<List<BusinessVerifiedOrNotOut>> BusinessVerifiedOrNot(BusinessVerifiedOrNot _verifiedOrNot)
        {
            ApiResult<List<BusinessVerifiedOrNotOut>> oResult = new ApiResult<List<BusinessVerifiedOrNotOut>>();

            if (DBContext.State == ConnectionState.Closed)
            {
                DBContext.Open();
            }

            var oReturn = DBContext.Query<BusinessVerifiedOrNotOut>("BusinesssVerifiedorNot", _verifiedOrNot, commandType: CommandType.StoredProcedure) as List<BusinessVerifiedOrNotOut>;
            DBContext.Close();

            oResult.Count = oReturn.Count;
            oResult.HasWarning = false;
            oResult.Message = "Success";
            oResult.Value = oReturn;
            return oResult;

        }


        public ApiResult<List<UserBusinessDtls>> BusinessDataToAdmin(GetBusinessMdl _BusinessDtls)
        {
            ApiResult<List<UserBusinessDtls>> oResult = new ApiResult<List<UserBusinessDtls>>();

            if (DBContext.State == ConnectionState.Closed)
            {
                DBContext.Open();
            }

            var oReturn = DBContext.Query<UserBusinessDtls>("UserDetailsToAdmin", _BusinessDtls, commandType: CommandType.StoredProcedure) as List<UserBusinessDtls>;



            //_mailWithAttachment = new Shared.Email.MailSend();

            //_mailWithAttachment.To = "jeseentha.ezoro@gmail.com";
            //_mailWithAttachment.Subject = "Business Details to admin";
            //_mailWithAttachment.body = "hiiii";



            //var stat = EllowCart.Shared.Email.MailwithAttachment(_mailWithAttachment);

            //////  var stat = EllowCart.Shared.Cryptography.Encrypt_String(_user.Password, ref out1, ref out2);

            //////var stat = EllowCart.Shared.Email.SendEmail(_mailWithAttachment.To, _mailWithAttachment.Subject,_mailWithAttachment.body);

            DBContext.Close();
            oResult.Count = oReturn.Count;
            oResult.HasWarning = false;
            oResult.Message = "Success";
            oResult.Value = oReturn;
            return oResult;

        }


        public APiResult CustomerSupportData(CustomerSupportDataMdl _input)
        {

            oResult = new APiResult();

            if (DBContext.State == ConnectionState.Closed)
            {
                DBContext.Open();
            }

            var oReturn = DBContext.Query("CustomerSupportData_INSERT", _input, commandType: CommandType.StoredProcedure);


            DBContext.Close();
            oResult.iRet = 1;
            oResult.sMessage = "Success";
            oResult.sResult = oReturn;
            return oResult;

        }

        public ApiResult<List<GetPremiumSellersOut>> GetPremiumSellers(GetPremiumSellersInput _Seller)
        {
            ApiResult<List<GetPremiumSellersOut>> oResult = new ApiResult<List<GetPremiumSellersOut>>();

            if (DBContext.State == ConnectionState.Closed)
            {
                DBContext.Open();
            }

            var oReturn = DBContext.Query<GetPremiumSellersOut>("GetNearByPremiumShopes", _Seller, commandType: CommandType.StoredProcedure) as List<GetPremiumSellersOut>;



            DBContext.Close();
            oResult.Count = oReturn.Count;
            oResult.HasWarning = false;
            oResult.Message = "Success";
            oResult.Value = oReturn;
            return oResult;

        }


        public ApiResult<List<GetNearByOffersByName>> GetOffersByName(GetOffersOfARegion _offersInput)
        {
            ApiResult<List<GetNearByOffersByName>> oResult = new ApiResult<List<GetNearByOffersByName>>();

            if (DBContext.State == ConnectionState.Closed)
            {
                DBContext.Open();
            }

            var oReturn = DBContext.Query<GetNearByOffersByName>("GetProductsByOfferName", _offersInput, commandType: CommandType.StoredProcedure) as List<GetNearByOffersByName>;
            DBContext.Close();

            oResult.Count = oReturn.Count;
            oResult.HasWarning = false;
            oResult.Message = "Success";
            oResult.Value = oReturn;
            return oResult;

        }










    }
}
