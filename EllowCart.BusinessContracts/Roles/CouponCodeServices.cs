﻿using System;
using System.Data;
using System.Collections.Generic;
using Dapper;
using EllowCart.QueryBank.Roles;
using EllowCart.Shared;
using EllowCart.DTO.CouponCode.InputModel;
namespace EllowCart.BusinessContracts.Roles
{
    public class CouponCodeServices
    {
        private IDbConnection DBContext;
        private APiResult oResult = null;
        DAL.DBSettings _DBSettings = null;
        private QueryBank.Roles.CouponCodeManage oQoeryCouponCode = null;

        public CouponCodeServices(IDbConnection _DBContext)
        {
            this.DBContext = _DBContext;

        }
        public CouponCodeServices()
        {
            _DBSettings = new DAL.DBSettings();  //-- New change
            DBContext = _DBSettings.GetCartDB();
        }


        public APiResult AddNewCouponCode(CouponCodeInput _input)
        {
            oResult = new APiResult();
            string sSQlAddCouponCode = string.Empty;
            oQoeryCouponCode = new CouponCodeManage();

            sSQlAddCouponCode = string.Format(oQoeryCouponCode.GetAddCouponCode(),
                                            _input.CouponCode, _input.Description,
                                            _input.IsActive, _input.CreatedOn,
                                            _input.CreatedBy, _input.UpdatedOn, _input.UpdatedBy);

            if (DBContext.State == ConnectionState.Closed)
            {
                DBContext.Open();
            }
            int iCount = DBContext.Execute(sSQlAddCouponCode);
            if (iCount > 0)
            {
                oResult = new APiResult();
                oResult.iRet = 1;
                oResult.sMessage = "Successfully created CouponCode";
            }
            DBContext.Close();
            return oResult;
        }
        //CouponCode Deletion by querybank
        public APiResult DeleteACouponCode(int CouponId)
        {

            oResult = new APiResult();
            string sSQlDeleteCouponCode = string.Empty;
            oQoeryCouponCode = new CouponCodeManage();

            sSQlDeleteCouponCode = string.Format(oQoeryCouponCode.DeleteCouponCode(CouponId));


            if (DBContext.State == ConnectionState.Closed)
            {
                DBContext.Open();
            }

            int iCount = DBContext.Execute(sSQlDeleteCouponCode);
            if (iCount > 0)
            {
                oResult = new APiResult();
                oResult.iRet = 1;
                oResult.sMessage = "Successfully deleted CouponCode";
            }
            DBContext.Close();
            return oResult;
        }
        //CouponCode Updation by querybank
        public APiResult UpdateCouponCode(CouponCodeInput Obj, int CouponId)
        {

            oResult = new APiResult();
            string sSQlUpdateCouponCode = string.Empty;
            oQoeryCouponCode = new CouponCodeManage();
            string CouponCode = Obj.CouponCode;
            string Description = Obj.Description;
            bool IsActive = Obj.IsActive;
            DateTime CreatedOn = Obj.CreatedOn;
            string CreatedBy = Obj.CreatedBy;
            DateTime UpdatedOn = Obj.UpdatedOn;
            string UpdatedBy = Obj.UpdatedBy;

            sSQlUpdateCouponCode = string.Format(oQoeryCouponCode.UpdateCouponCode(CouponCode, Description, IsActive, CreatedOn, CreatedBy, UpdatedOn, UpdatedBy, CouponId));


            if (DBContext.State == ConnectionState.Closed)
            {
                DBContext.Open();
            }

            int iCount = DBContext.Execute(sSQlUpdateCouponCode);
            if (iCount > 0)
            {
                oResult = new APiResult();
                oResult.iRet = 1;
                oResult.sMessage = "Successfully updated the CouponCode";
            }
            DBContext.Close();
            return oResult;
        }
        //getting module querybank
        public APiResult GetAllCouponCodes()
        {
            oResult = new APiResult();
            oQoeryCouponCode = new CouponCodeManage();
            string ssqlGetAllCouponCode = oQoeryCouponCode.GetAddCouponCode();
            if (DBContext.State == ConnectionState.Closed)
            {
                DBContext.Open();
            }

            var oReturn = DBContext.Query<CouponCodeInput>(ssqlGetAllCouponCode);
            DBContext.Close();
            oResult.iRet = 1;
            oResult.sMessage = "Successfully collected all coupon codes";
            oResult.sResult = oReturn;
            return oResult;

        }
        //getting couponCode querybank
        public APiResult GetCouponCodeById(int CouponId)
        {
            oResult = new APiResult();
            oQoeryCouponCode = new CouponCodeManage();
            string ssqlGetACouponCode= oQoeryCouponCode.GetACouponCode(CouponId);
            if (DBContext.State == ConnectionState.Closed)
            {
                DBContext.Open();
            }

            var oReturn = DBContext.Query<CouponCodeInput>(ssqlGetACouponCode);
            DBContext.Close();
            oResult.iRet = 1;
            oResult.sMessage = "Successfully collected the CouponCode";
            oResult.sResult = oReturn;
            return oResult;

        }
    }
}
