﻿using Dapper;
using EllowCart.DTO.CustomerAddress.InputModels;
using EllowCart.DTO.CustomerAddress.OutputModels;
using EllowCart.Shared;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace EllowCart.BusinessContracts.Roles
{
   public class CustomerAddressServices
    {
        private IDbConnection DBContext;
        DAL.DBSettings _DBSettings = null;
        EllowCart.Shared.GoogleAPI _googleAPI = new GoogleAPI();

        public CustomerAddressServices(IDbConnection _DBContext)
        {
            this.DBContext = _DBContext;

        }
        public CustomerAddressServices()
        {
            _DBSettings = new DAL.DBSettings();  //-- New change
            DBContext = _DBSettings.GetCartDB();
        }
        public ApiResult<List<AddCustomeraddressOut>> CustomerAddress(AddCustomerAddressInput _addressInput)
        {
            ApiResult<List<AddCustomeraddressOut>> oResult = new ApiResult<List<AddCustomeraddressOut>>();

            if (DBContext.State == ConnectionState.Closed)
            {
                DBContext.Open();
            }

           // var lat = _googleAPI.GetCoordinatesFromAddress();

            var oReturn = DBContext.Query<AddCustomeraddressOut>("AddCustomerAddress", _addressInput, commandType: CommandType.StoredProcedure) as List<AddCustomeraddressOut>;
            DBContext.Close();

            oResult.Count = oReturn.Count;
            oResult.HasWarning = false;
            oResult.Message = "Success";
            oResult.Value = oReturn;
            return oResult;

        }

        public ApiResult<List<GetAddressesOut>> GetAddresses(GetAddresses _addressInput)
        {
            ApiResult<List<GetAddressesOut>> oResult = new ApiResult<List<GetAddressesOut>>();

            if (DBContext.State == ConnectionState.Closed)
            {
                DBContext.Open();
            }

            var oReturn = DBContext.Query<GetAddressesOut>("GetCustomerAddress", _addressInput, commandType: CommandType.StoredProcedure) as List<GetAddressesOut>;
            DBContext.Close();

            oResult.Count = oReturn.Count;
            oResult.HasWarning = false;
            oResult.Message = "Success";
            oResult.Value = oReturn;
            return oResult;

        }

        public ApiResult<List<RemoveAddressOut>> RemoveCustomerAddress(RemoveCustomerAddress _addressInput)
        {
            ApiResult<List<RemoveAddressOut>> oResult = new ApiResult<List<RemoveAddressOut>>();

            if (DBContext.State == ConnectionState.Closed)
            {
                DBContext.Open();
            }

            var oReturn = DBContext.Query<RemoveAddressOut>("RemoveCustomerAddress", _addressInput, commandType: CommandType.StoredProcedure) as List<RemoveAddressOut>;
            DBContext.Close();

            oResult.Count = oReturn.Count;
            oResult.HasWarning = false;
            oResult.Message = "Success";
            oResult.Value = oReturn;
            return oResult;

        }


        public ApiResult<List<EditaddressOut>> EditCustomerAddress(EditCustomerAddress _addressInput)
        {
            ApiResult<List<EditaddressOut>> oResult = new ApiResult<List<EditaddressOut>>();
            
            if (DBContext.State == ConnectionState.Closed)
            {
                DBContext.Open();
            }

            var oReturn = DBContext.Query<EditaddressOut>("UpdateCustomerAddress", _addressInput, commandType: CommandType.StoredProcedure) as List<EditaddressOut>;
            DBContext.Close();

            oResult.Count = oReturn.Count;
            oResult.HasWarning = false;
            oResult.Message = "Success";
            oResult.Value = oReturn;
            return oResult;

        }

        public ApiResult<List<EditaddressOut>> SetDefaultAddress(SetDefaultAddress _addressInput)
        {
            ApiResult<List<EditaddressOut>> oResult = new ApiResult<List<EditaddressOut>>();

            if (DBContext.State == ConnectionState.Closed)
            {
                DBContext.Open();
            }

            var oReturn = DBContext.Query<EditaddressOut>("SetDefaultAddressForCustomer", _addressInput, commandType: CommandType.StoredProcedure) as List<EditaddressOut>;
            DBContext.Close();

            oResult.Count = oReturn.Count;
            oResult.HasWarning = false;
            oResult.Message = "Success";
            oResult.Value = oReturn;
            return oResult;

        }

    }
}
