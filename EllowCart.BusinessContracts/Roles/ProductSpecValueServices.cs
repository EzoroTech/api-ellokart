﻿using Dapper;
using EllowCart.DTO.ProductSpecValue.InputModel;
using EllowCart.Shared;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;


namespace EllowCart.BusinessContracts.Roles
{
    public class ProductSpecValueServices
    {

        private IDbConnection DBContext;
        private APiResult oResult = null;
        private QueryBank.Roles.ProductSpecValueManage oQoeryProductSpecValue = null;
        DAL.DBSettings _DBSettings = null;
        public ProductSpecValueServices(IDbConnection _DBContext)
        {
            this.DBContext = _DBContext;

        }
        public ProductSpecValueServices()
        {
            _DBSettings = new DAL.DBSettings();  //-- New change
            DBContext = _DBSettings.GetCartDB();
        }

        public APiResult AddNewProductSpecValue(ProductSpecValueInput _input)
        {
            oResult = new APiResult();
            string sSQlAddProductSpecValue = string.Empty;
            oQoeryProductSpecValue = new QueryBank.Roles.ProductSpecValueManage();

            sSQlAddProductSpecValue = string.Format(oQoeryProductSpecValue.GetAddProductSpecValue(),
                                            _input.ProductSpecId, _input.ProductId,
                                            _input.VarientValue, _input.UserBussinessID);

            if (DBContext.State == ConnectionState.Closed)
            {
                DBContext.Open();
            }
            int iCount = DBContext.Execute(sSQlAddProductSpecValue);
            if (iCount > 0)
            {
                oResult = new APiResult();
                oResult.iRet = 1;
                oResult.sMessage = "Successfully added ProductSpecValue";
            }
            DBContext.Close();
            return oResult;
        }

        //ProductSpecification Deletion by querybank
        public APiResult DeleteProductSpecValue(int ProductSpecValueId)
        {

            oResult = new APiResult();
            string sSQlDeleteProductSpecValue = string.Empty;
            oQoeryProductSpecValue = new QueryBank.Roles.ProductSpecValueManage();

            sSQlDeleteProductSpecValue = string.Format(oQoeryProductSpecValue.DeleteProductSpecValue(ProductSpecValueId));


            if (DBContext.State == ConnectionState.Closed)
            {
                DBContext.Open();
            }

            int iCount = DBContext.Execute(sSQlDeleteProductSpecValue);
            if (iCount > 0)
            {
                oResult = new APiResult();
                oResult.iRet = 1;
                oResult.sMessage = "Successfully deleted ProductSpec Value";
            }
            DBContext.Close();
            return oResult;
        }
        //Product specification Updation by querybank
        public APiResult UpdateProductSpecValue(ProductSpecValueInput Obj, int ProductSpecValueId)
        {

            oResult = new APiResult();
            string sSQlUpdateProductSpecValue = string.Empty;
            oQoeryProductSpecValue = new QueryBank.Roles.ProductSpecValueManage();
            int ProductSpecId= Obj.ProductSpecId;
            int ProductId = Obj.ProductId;
           string VarientValue = Obj.VarientValue;
            int UserBusinessId = Obj.UserBussinessID;
         

            sSQlUpdateProductSpecValue = string.Format(oQoeryProductSpecValue.UpdateProductSpecValue(ProductSpecId, ProductId, VarientValue, UserBusinessId, ProductSpecValueId));


            if (DBContext.State == ConnectionState.Closed)
            {
                DBContext.Open();
            }

            int iCount = DBContext.Execute(sSQlUpdateProductSpecValue);
            if (iCount > 0)
            {
                oResult = new APiResult();
                oResult.iRet = 1;
                oResult.sMessage = "Successfully updated the Productspec Value";
            }
            DBContext.Close();
            return oResult;
        }
        //geting product specification by SP
        public APiResult GetAllProductSpecValue( )
        {
            oResult = new APiResult();
            oQoeryProductSpecValue = new QueryBank.Roles.ProductSpecValueManage();
            string ssqlGetAllProductSpecValue = oQoeryProductSpecValue.GetAllProductSpecValue();
            if (DBContext.State == ConnectionState.Closed)
            {
                DBContext.Open();
            }

            var oReturn = DBContext.Query<ProductSpecValueInput>(ssqlGetAllProductSpecValue);
            DBContext.Close();
            oResult.iRet = 1;
            oResult.sMessage = "Successfully collected all ProductSpec Values";
            oResult.sResult = oReturn;
            return oResult;

        }
        //getting module ProductSpecValue
        public APiResult GetProductSpecValueById(int ProductSpecValueId)
        {
            oResult = new APiResult();
            oQoeryProductSpecValue = new QueryBank.Roles.ProductSpecValueManage();
            string ssqlGetProductSpecValue = oQoeryProductSpecValue.GetAProductSpecValue(ProductSpecValueId);
            if (DBContext.State == ConnectionState.Closed)
            {
                DBContext.Open();
            }

            var oReturn = DBContext.Query<ProductSpecValueInput>(ssqlGetProductSpecValue);
            DBContext.Close();
            oResult.iRet = 1;
            oResult.sMessage = "Successfully collected the ProductSpecValue";
            oResult.sResult = oReturn;
            return oResult;

        }

    }
}
