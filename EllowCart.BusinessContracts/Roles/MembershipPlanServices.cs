﻿using Dapper;
using EllowCart.DTO.MembershipPlan.InputModels;
using EllowCart.DTO.MembershipPlan.OutputModels;
using EllowCart.QueryBank.Roles;
using EllowCart.Shared;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace EllowCart.BusinessContracts.Roles
{
    public class MembershipPlanServices
    {


        private IDbConnection DBContext;
        private APiResult oResult = null;
        DAL.DBSettings _DBSettings = null;
        private QueryBank.Roles.MembershipPlanManage OQueryMembershipPlan = null;

        public MembershipPlanServices(IDbConnection _DBContext)
        {
            this.DBContext = _DBContext;

        }
        public MembershipPlanServices()
        {
            _DBSettings = new DAL.DBSettings();  //-- New change
            DBContext = _DBSettings.GetCartDB();
        }


        public APiResult AddNewMembershipPlan(MembershipPlanInputModel _input)
        {
            oResult = new APiResult();
            string sSQlAddMembershipPlan = string.Empty;
            OQueryMembershipPlan = new MembershipPlanManage();

            sSQlAddMembershipPlan = string.Format(OQueryMembershipPlan.GetAddMembershipPlan(),
                                            _input.MembershipPlan, _input.Price,
                                            _input.ValidityInMonths, _input.Description,
                                            _input.IsActive, _input.TaxId, _input.CreatedOn,
                                            _input.CreatedBy, _input.UpdatedOn, _input.UpdatedBy);

            if (DBContext.State == ConnectionState.Closed)
            {
                DBContext.Open();
            }
            int iCount = DBContext.Execute(sSQlAddMembershipPlan);
            if (iCount > 0)
            {
                oResult = new APiResult();
                oResult.iRet = 1;
                oResult.sMessage = "Successfully created Membership Plan";
            }
            DBContext.Close();
            return oResult;
        }
        //MembershipPlan Deletion by querybank
        public APiResult DeleteMembershipPlan(int MembershipPlanId)
        {

            oResult = new APiResult();
            string sSQlDeleteMembershipPlan = string.Empty;
            OQueryMembershipPlan = new MembershipPlanManage();

            sSQlDeleteMembershipPlan = string.Format(OQueryMembershipPlan.DeleteMembershipPlan(MembershipPlanId));


            if (DBContext.State == ConnectionState.Closed)
            {
                DBContext.Open();
            }

            int iCount = DBContext.Execute(sSQlDeleteMembershipPlan);
            if (iCount > 0)
            {
                oResult = new APiResult();
                oResult.iRet = 1;
                oResult.sMessage = "Successfully deleted MembershipPlan";
            }
            DBContext.Close();
            return oResult;
        }
        //Module Updation by querybank
        public APiResult UpdateMembershipPlan(MembershipPlanInputModel Obj, int MembershipPlanId)
        {

            oResult = new APiResult();
            string sSQlUpdateMembershipPlan = string.Empty;
            OQueryMembershipPlan = new MembershipPlanManage();
            string MembershipPlan = Obj.MembershipPlan;
            float Price = Obj.Price;
            int ValidityInMonths = Obj.ValidityInMonths;
            string Description = Obj.Description;
            DateTime CreatedOn = Obj.CreatedOn;
            string CreatedBy = Obj.CreatedBy;
            DateTime UpdatedOn = Obj.UpdatedOn;
            string UpdatedBy = Obj.UpdatedBy;
            bool IsActive = Obj.IsActive;

            sSQlUpdateMembershipPlan = string.Format(OQueryMembershipPlan.UpdateMembershipPlan(MembershipPlan, Price, ValidityInMonths, Description, IsActive, CreatedOn, CreatedBy, UpdatedOn, UpdatedBy, MembershipPlanId));


            if (DBContext.State == ConnectionState.Closed)
            {
                DBContext.Open();
            }

            int iCount = DBContext.Execute(sSQlUpdateMembershipPlan);
            if (iCount > 0)
            {
                oResult = new APiResult();
                oResult.iRet = 1;
                oResult.sMessage = "Successfully updated the membership plan";
            }
            DBContext.Close();
            return oResult;
        }
        //geting module by SP
        public APiResult GetAllMembershipPlanBySp(MembershipPlanOutputModel _obj)
        {
            oResult = new APiResult();

            if (DBContext.State == ConnectionState.Closed)
            {
                DBContext.Open();
            }

            var oReturn = DBContext.Query("MembershipPlanMasterSelect", _obj, commandType: CommandType.StoredProcedure);
            DBContext.Close();
            oResult.iRet = 1;
            oResult.sMessage = "Successfully collected all membership plans ";
            oResult.sResult = oReturn;
            return oResult;

        }

        public APiResult MembershipPlanBySp( )
        {
            oResult = new APiResult();

            if (DBContext.State == ConnectionState.Closed)
            {
                DBContext.Open();
            }

            var oReturn = DBContext.Query("GetAllMembershipPlan",  commandType: CommandType.StoredProcedure);
            DBContext.Close();
            oResult.iRet = 1;
            oResult.sMessage = "Successfully collected all membership plans ";
            oResult.sResult = oReturn;
            return oResult;

        }
        public ApiResult<List<CheckForMembershipPlanOut>> CheckMembershipPlan(CheckForMembershipPlan _membershipPlan)
        {
            ApiResult<List<CheckForMembershipPlanOut>> oResult = new ApiResult<List<CheckForMembershipPlanOut>>();

            if (DBContext.State == ConnectionState.Closed)
            {
                DBContext.Open();
            }

            var oReturn = DBContext.Query<CheckForMembershipPlanOut>("CheckForMembershipPlan", _membershipPlan, commandType: CommandType.StoredProcedure) as List<CheckForMembershipPlanOut>;
            DBContext.Close();
            oResult.Count = oReturn.Count;
            oResult.HasWarning = false;
            oResult.Message = "Success";
            oResult.Value = oReturn;
            return oResult;

        }

        public ApiResult<List<GetPlanAddingStatus>> BuyMembershipPlan(PurchaseMembershipPlan _input)
        {
            ApiResult<List<GetPlanAddingStatus>> oResult = new ApiResult<List<GetPlanAddingStatus>>();

            if (DBContext.State == ConnectionState.Closed)
            {
                DBContext.Open();
            }

            var oReturn = DBContext.Query<GetPlanAddingStatus>("BuyMembershipPlan", _input, commandType: CommandType.StoredProcedure) as List<GetPlanAddingStatus>;
            DBContext.Close();

            oResult.Count = oReturn.Count;
            oResult.HasWarning = false;
            oResult.Message = "Success";
            oResult.Value = oReturn;
            return oResult;

        }


        public ApiResult<List<CustomerPlanOut>> GetAllCustomerMembershipPlan()
        {
            ApiResult<List<CustomerPlanOut>> oResult = new ApiResult<List<CustomerPlanOut>>();

            if (DBContext.State == ConnectionState.Closed)
            {
                DBContext.Open();
            }

            var oReturn = DBContext.Query<CustomerPlanOut>("GetallcustomerPlan", commandType: CommandType.StoredProcedure) as List<CustomerPlanOut>;
            DBContext.Close();

            oResult.Count = oReturn.Count;
            oResult.HasWarning = false;
            oResult.Message = "Success";
            oResult.Value = oReturn;
            return oResult;

        }




    }
}
