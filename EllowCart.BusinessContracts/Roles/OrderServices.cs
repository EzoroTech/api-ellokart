﻿using Dapper;
using EllowCart.DTO.AddToCart.OutputModels;
using EllowCart.DTO.Customer.InputModels;
using EllowCart.DTO.Customer.OutputModels;
using EllowCart.DTO.CustomerAddress.OutputModels;
using EllowCart.DTO.Order.InputModels;
using EllowCart.DTO.Order.OutputModels;
using EllowCart.DTO.Product.OutputModels;
using EllowCart.Shared;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace EllowCart.BusinessContracts.Roles
{
    public class OrderServices
    {
        private IDbConnection DBContext;
        private APiResult oResult = null;
        DAL.DBSettings _DBSettings = null;
        public OrderServices(IDbConnection _DBContext)
        {
            this.DBContext = _DBContext;

        }
        public OrderServices()
        {
            _DBSettings = new DAL.DBSettings();  //-- New change
            DBContext = _DBSettings.GetCartDB();
        }

        public ApiResult<List<PlaceOrderOut>> PlaceOrder(PlaceOrderInput _placeOrder)
        {
            ApiResult<List<PlaceOrderOut>> oResult = new ApiResult<List<PlaceOrderOut>>();

            if (DBContext.State == ConnectionState.Closed)
            {
                DBContext.Open();
            }

            var dt = new DataTable("OrderItems_type");
            dt.Columns.Add("ProductId", typeof(long));
            dt.Columns.Add("OrderItemQty", typeof(int));
            dt.Columns.Add("OrderItemPrice", typeof(float));
            dt.Columns.Add("BranchId", typeof(long));
            dt.Columns.Add("TotalPayableAmount", typeof(float));
            dt.Columns.Add("DeliveryCharge", typeof(float));

            var dt1 = new DataTable("OrderIds");
            dt1.Columns.Add("OrderId", typeof(long));

            float totalamount = 0;
            long DeliveryCharge = 0;
            long Price = 0;
            string varientValue = "0";

            DistanceInput _Input = new DistanceInput();

            var order = _placeOrder.DtOrderItems;
            for (int y = 0; y < order.Count; y++)
            {
                var checkorder = DBContext.Query("CheckForOrderPlacing", new { @Branchid = order[y].BranchId, @CustomerAddressId = _placeOrder.AddressId }, commandType: CommandType.StoredProcedure);

            }


            for (int i = 0; i < order.Count; i++)
            {
                int y = i + 1;


                var oReturn2 = DBContext.Query<DelivertypeOut>("GetDeliveryDetailsOfCustomer", new { @BranchId = order[i].BranchId, @CustomerId = 0, @ProductId = order[i].ProductId, @flag = 2 }, commandType: CommandType.StoredProcedure) as List<DelivertypeOut>;

                var oReturn3 = DBContext.Query<PriceCalculation>("PriceCalculationsForPlaceOrder", new
                {
                    @BranchId = order[i].BranchId,
                    @CustomerId = _placeOrder.CustomerId,
                    @AddressId = _placeOrder.AddressId,
                    @SlNo = _placeOrder.SlNo,
                    @SpecId = oReturn2[0].Specid,
                    @VarientValue = oReturn2[0].VarientValue,
                    @DeliveryType = _placeOrder.DeliveryMode,
                    @OfferPrice = oReturn2[0].OfferPrice,
                    @OrderItemQty = order[i].OrderItemQty,
                    @MRP = oReturn2[0].MRP,
                    @MinimumOrderValue = oReturn2[0].MinimumOrderValue
                }, commandType: CommandType.StoredProcedure) as List<PriceCalculation>;

                DeliveryCharge = oReturn3[0].DeliveryCharge;

     
                for (int x = 0; x < oReturn2.Count; x++)
                {
                    if (oReturn2[x].OfferPrice == 0)
                    {
                        Price += (order[i].OrderItemQty * oReturn2[x].MRP);
                    }
                    else
                    {
                        if (oReturn2[x].MinimumOrderValue <= order[i].OrderItemQty)
                        {
                            Price += (order[i].OrderItemQty * oReturn2[x].OfferPrice);
                        }
                        else
                        {
                            Price += (order[i].OrderItemQty * oReturn2[x].MRP);

                        }

                    }

                    totalamount = (Price + DeliveryCharge);
                }



                dt.Rows.Add(order[i].ProductId, order[i].OrderItemQty, order[i].OrderItemPrice, order[i].BranchId, Price, DeliveryCharge);

                if (y == order.Count)
                {
                    var oReturn = DBContext.Query<PlaceOrderOut>("CustomerOrder", new
                    {
                        @CustomerId = _placeOrder.CustomerId,
                        @OrderAmt = totalamount,
                        @SlNo = _placeOrder.SlNo,
                        @AddressId = _placeOrder.AddressId,
                        @BranchId = order[i].BranchId,
                        @DeliveryMode = _placeOrder.DeliveryMode,
                        @DtOrderItems = dt.AsTableValuedParameter("OrderItems_type")
                    }, commandType: CommandType.StoredProcedure) as List<PlaceOrderOut>;

                    DBContext.Close();

                    oResult.Count = oReturn.Count;
                    oResult.HasWarning = false;
                    oResult.Message = "Success";
                    oResult.Value = oReturn;
                    dt.Clear();
                    totalamount = 0;
                    Price = 0;
                    DeliveryCharge = 0;
                    varientValue = "0";

                    dt1.Rows.Add(oReturn[0].OrderNo);
                }


                else if (order[i].BranchId != order[y].BranchId)
                {
                    var oReturn = DBContext.Query<PlaceOrderOut>("CustomerOrder", new
                    {
                        @CustomerId = _placeOrder.CustomerId,
                        @OrderAmt = totalamount,
                        @SlNo = _placeOrder.SlNo,
                        @AddressId = _placeOrder.AddressId,
                        @BranchId = order[i].BranchId,
                        @DeliveryMode = _placeOrder.DeliveryMode,
                        @DtOrderItems = dt.AsTableValuedParameter("OrderItems_type")
                    }, commandType: CommandType.StoredProcedure) as List<PlaceOrderOut>;

                    DBContext.Close();

                    oResult.Count = oReturn.Count;
                    oResult.HasWarning = false;
                    oResult.Message = "Success";
                    oResult.Value = oReturn;
                    dt.Clear();
                    totalamount = 0;
                    Price = 0;
                    DeliveryCharge = 0;
                    varientValue = "0";

                  //  dt1.Rows.Add(oReturn[0].OrderNo);

                }

               

            }

         //   var value = dt1.Rows[0][0];

            return oResult;

        }

        public ApiResult<List<OrderListOut>> GetOrderBySeller(GetOrderBySeller _orderBySeller)
        {
            ApiResult<List<OrderListOut>> oResult = new ApiResult<List<OrderListOut>>();

            if (DBContext.State == ConnectionState.Closed)
            {
                DBContext.Open();
            }


            var Result = DBContext.Query<OrderListOut>("OrderedCustomersWithSeller", new { @BranchId = _orderBySeller.BranchId, @Flag = _orderBySeller.Flag }, commandType: CommandType.StoredProcedure) as List<OrderListOut>;


            DBContext.Close();

            oResult.Count = Result.Count;
            oResult.HasWarning = false;
            oResult.Message = "Success";
            oResult.Value = Result;
            return oResult;

        }

        public ApiResult<List<GetOderBySellerOut>> GetOrderDetailSeller(OrderDetailInput _orderBySeller)
        {
            ApiResult<List<GetOderBySellerOut>> oResult = new ApiResult<List<GetOderBySellerOut>>();

            if (DBContext.State == ConnectionState.Closed)
            {
                DBContext.Open();
            }


            // var Result = DBContext.Query<OrderListOut>("OrderedCustomersWithSeller", new { @BranchId = _orderBySeller.BranchId, @Flag = _orderBySeller.Flag }, commandType: CommandType.StoredProcedure) as List<OrderListOut>;


            var oReturn = DBContext.Query<GetOderBySellerOut>("GetOrderListBySeller", new
            {
                @Branchid = _orderBySeller.BranchId,
                @OrderId = _orderBySeller.OrderId,
                // @Flag = _orderBySeller.Flag
            }, commandType: CommandType.StoredProcedure) as List<GetOderBySellerOut>;


            DBContext.Close();

            oResult.Count = oReturn.Count;
            oResult.HasWarning = false;
            oResult.Message = "Success";
            oResult.Value = oReturn;
            return oResult;

        }

        public APiResult AcceptorRejectOrder(AcceptOrRejectOrder _orderBySeller)
        {
            APiResult oResult = new APiResult();

            InputToApplication _input = new InputToApplication();
            List<metadatafordelivery> _Delivery = new List<metadatafordelivery>();
            List<DataForPickup> _Pickup = new List<DataForPickup>();


            if (DBContext.State == ConnectionState.Closed)
            {
                DBContext.Open();
            }


            var oReturn = DBContext.Query<AcceptorRejectOrderOut>("AcceptOrRejectOrder", _orderBySeller, commandType: CommandType.StoredProcedure) as List<AcceptorRejectOrderOut>;

            DBContext.Close();

            if (oReturn.Count != 0)
            {
                string DeliveryMode = "";

                if (oReturn[0].Status == 1 & _orderBySeller.Flag == 1)
                {
                    DBContext.Open();


                    var oReturn1 = DBContext.Query<RequestOutPut>("OrderDetailsForPickUp", new { @OrderId = _orderBySeller.OrderId, @BranchId = _orderBySeller.BranchId }, commandType: CommandType.StoredProcedure) as List<RequestOutPut>;

                    var oReturn2 = DBContext.Query<OrderProductList>("OrderProductDetails", new { OrderId = _orderBySeller.OrderId }, commandType: CommandType.StoredProcedure) as List<OrderProductList>;

                    var oReturn3 = DBContext.Query<OrderSummery>("GetOrderSummary", new { OrderId = _orderBySeller.OrderId }, commandType: CommandType.StoredProcedure) as List<OrderSummery>;

                    DBContext.Close();

                    #region Templates of Tookan

                    _Delivery.Add(new metadatafordelivery { label = "PAYMENT_TYPE", data = "Cash on delivery" });
                    _Delivery.Add(new metadatafordelivery { label = "PURCHASE_AMOUNT", data = oReturn3[0].Price.ToString() });
                    _Delivery.Add(new metadatafordelivery { label = "DELIVERY_AMOUNT", data = oReturn3[0].DeliveryCharge.ToString() });
                    _Delivery.Add(new metadatafordelivery { label = "GRAND_TOTAL", data = oReturn3[0].GrandTotal.ToString() });


                    if (oReturn1[0].DeliveryMode == 1)
                    {
                        DeliveryMode = "Expreess Delivery";
                    }
                    else if (oReturn1[0].DeliveryMode == 2)
                    {
                        DeliveryMode = "Normal Delivery";
                    }

                    if (oReturn2.Count != 0)
                    {
                        for (int i = 0; i < oReturn2.Count; i++)
                        {
                            _Pickup.Add(new DataForPickup { label = "DELIVERY_MODE", data = DeliveryMode });

                            _Pickup.Add(new DataForPickup { label = "ITEMS", data = oReturn2[0].ProductName });
                            _Pickup.Add(new DataForPickup { label = "QTY", data = oReturn2[0].OrderItemQty.ToString() });


                            // _Pickup.Add(new PickupMetdataForDelivery { DELIVERY_MODE = DeliveryMode, ITEMS = oReturn2[0].ProductName, QTY = oReturn2[0].OrderItemQty.ToString() });

                        }
                    }

                    _input.pickup_meta_data = _Pickup;
                    _input.meta_data = _Delivery;

                    #endregion


                    #region Time

                    DateTime delvery = DateTime.Now;
                    DateTime Pickup = DateTime.Now;
                    DateTime today = DateTime.Now;

                    TimeSpan WorkStart = new TimeSpan(7, 0, 0);
                    TimeSpan WorkEnd = new TimeSpan(17, 0, 0);

                    TimeSpan e = new TimeSpan(14, 0, 0);
                    TimeSpan todaydel = new TimeSpan(18, 0, 0);
                    TimeSpan nextday = new TimeSpan(12, 0, 0);
                    TimeSpan PickupNextDay = new TimeSpan(11, 0, 0);
                    TimeSpan now = DateTime.Now.TimeOfDay;

                    if (now < e && oReturn1[0].DeliveryMode == 2)
                    {
                        delvery = today + todaydel;
                        Pickup = today + WorkEnd;
                    }
                    else if (now > e && oReturn1[0].DeliveryMode == 2)
                    {
                        delvery = (today.AddDays(1)) + nextday;
                        Pickup = (today.AddDays(1)) + PickupNextDay;

                    }
                    else if (now > WorkStart && now < WorkEnd && oReturn1[0].DeliveryMode == 1)
                    {
                        delvery = today.AddMinutes(90);
                        Pickup = today.AddMinutes(30);
                    }
                    else
                    {
                        delvery = today + todaydel;
                        Pickup = today + WorkEnd;
                    }


                    //TimeSpan s = new TimeSpan(18, 0, 0);
                    //DateTime today = DateTime.Now;
                    //DateTime tomarrow = today.AddDays(1);
                    //today = today.Date + s;
                    //tomarrow = tomarrow.Date + s;

                    string today1 = Pickup.ToString("yyyy-MM-dd HH':'mm':'ss");
                    string tomarrow1 = delvery.ToString("yyyy-MM-dd HH':'mm':'ss");

                    #endregion

                    #region TookanInput

                    _input.order_id = _orderBySeller.OrderId.ToString();
                    _input.job_description = oReturn1[0].CategoryName;
                    _input.job_pickup_phone = oReturn1[0].SellerMobileNumber;
                    _input.job_pickup_name = oReturn1[0].SellerName;
                    _input.job_pickup_address = oReturn1[0].SellerAddress;
                    _input.job_pickup_latitude = oReturn1[0].SellerLattitude;
                    _input.job_pickup_longitude = oReturn1[0].SellerLongittude;
                    _input.job_pickup_datetime = today1;
                    _input.customer_email = oReturn1[0].CustomerEmailId;
                    _input.customer_username = oReturn1[0].CustomerName;
                    _input.customer_phone = oReturn1[0].CustomerAlternateMobNo;
                    _input.customer_address = oReturn1[0].CustomerAddress;
                    _input.latitude = oReturn1[0].CustomerLattitude;
                    _input.longitude = oReturn1[0].CustomerLongittude;
                    _input.job_delivery_datetime = tomarrow1;




                    #endregion

                    oResult.sResult = _input;
                    oResult.iRet = 2;

                }
                else
                {

                    oResult.sResult = oReturn;
                    oResult.iRet = oReturn[0].Status;
                }
            }



            return oResult;

        }

        public ApiResult<List<AcceptorRejectOrderOut>> AddDeliveryDetails(TookanTaskOutput _input)
        {
            ApiResult<List<AcceptorRejectOrderOut>> oResult = new ApiResult<List<AcceptorRejectOrderOut>>();


            if (DBContext.State == ConnectionState.Closed)
            {
                DBContext.Open();
            }
            AcceptorRejectOrderOut _out = new AcceptorRejectOrderOut();

            int ORDERID = Int32.Parse(_input.data.order_id);
            int PickupJobid = Int32.Parse(_input.data.pickup_job_id);
            int DeliveryJobId = Int32.Parse(_input.data.delivery_job_id);


            var oReturn = DBContext.Query<AcceptorRejectOrderOut>("AddDeliveryDetails", new
            {
                @OrderId = ORDERID,
                @PickupJobId = PickupJobid,
                @DeliveryJobId = DeliveryJobId
            }, commandType: CommandType.StoredProcedure) as List<AcceptorRejectOrderOut>;
            DBContext.Close();



            oResult.Count = oReturn.Count;
            oResult.Message = "Success";
            oResult.Value = oReturn;

            return oResult;
        }

        public ApiResult<List<CancelOrderOut>> CancelOrder(CancelOrder _orderBySeller)
        {
            ApiResult<List<CancelOrderOut>> oResult = new ApiResult<List<CancelOrderOut>>();

            if (DBContext.State == ConnectionState.Closed)
            {
                DBContext.Open();
            }


            var oReturn = DBContext.Query<CancelOrderOut>("CancelOrder", _orderBySeller, commandType: CommandType.StoredProcedure) as List<CancelOrderOut>;
            DBContext.Close();

            oResult.Count = oReturn.Count;
            oResult.HasWarning = false;
            oResult.Message = "Success";
            oResult.Value = oReturn;
            return oResult;

        }

        public ApiResult<List<TotalPaymentOut>> DeliveryChargeByProduct(DeliveryCharge _charge)
        {
            ApiResult<List<TotalPaymentOut>> oResult = new ApiResult<List<TotalPaymentOut>>();

            EllowCart.Shared.GoogleAPI _google = new GoogleAPI();

            //  var distance = _google.Distance();

            if (DBContext.State == ConnectionState.Closed)
            {
                DBContext.Open();
            }



            var oReturn = DBContext.Query<GetProductDetails>("GetProductDetails", new { @BranchId = _charge.BranchId, @ProductId = _charge.ProductId }, commandType: CommandType.StoredProcedure) as List<GetProductDetails>;
            var oReturn1 = DBContext.Query<GetProductSpecificationOut>("GetProductSpecificationList", new { @BranchId = _charge.BranchId, @ProductId = _charge.ProductId }, commandType: CommandType.StoredProcedure) as List<GetProductSpecificationOut>;
            var oReturn2 = DBContext.Query<DelivertypeOut>("GetDeliveryDetailsOfCustomer", _charge, commandType: CommandType.StoredProcedure) as List<DelivertypeOut>;
            var oReturn3 = DBContext.Query<TotalPaymentOut>("GetTotalPayableAmountByCustomer",
                new
                {
                    @ProductId = _charge.ProductId,
                    // @Distance=oReturn2[0].Distance,
                    @SpecId = oReturn1[0].ProductSpecId,
                    @VarientValue = oReturn1[0].VarientValue,
                    //   @DeliveryType=oReturn2[0].DeliveryType,
                    // @OfferPrice=oReturn[0].OfferPrice
                }, commandType: CommandType.StoredProcedure) as List<TotalPaymentOut>;

            oReturn3[0].Price = oReturn[0].OfferPrice;

            DBContext.Close();

            oResult.Count = oReturn.Count;
            oResult.HasWarning = false;
            oResult.Message = "Success";
            oResult.Value = oReturn3;
            return oResult;

        }

        public APiResult CheckOutByCustomer(CheckoutByCustomer _charge)
        {
            APiResult oResult = new APiResult();

            if (DBContext.State == ConnectionState.Closed)
            {
                DBContext.Open();
            }

            EllowCart.Shared.GoogleAPI _googleAPI = new GoogleAPI();
            DistanceInput _Input = new DistanceInput();

            var oReturn = DBContext.Query<TotalPaymentOut>("GetBranchFromCart", new { @CustomerId = _charge.CustomerId }, commandType: CommandType.StoredProcedure) as List<TotalPaymentOut>;
            var oReturn4 = DBContext.Query<GetAddressesOut>("DefaultAddressOfCustomer", new { @CustomerId = _charge.CustomerId, @AddressId = _charge.AddressId, @SlNo = _charge.SlNo }, commandType: CommandType.StoredProcedure) as List<GetAddressesOut>;


            for (int i = 0; i < oReturn.Count; i++)
            {
                float totalamount = 0;
                long DeliveryChargeExpreess = 0;
                long DeliveryCharge = 0;
                float Price = 0;
                float totalamountExpress = 0;
                float PriceExpress = 0;

                var oReturn2 = DBContext.Query<DelivertypeOut>("GetDeliveryDetailsOfCustomer", new { @BranchId = oReturn[i].BranchId, @CustomerId = _charge.CustomerId, @ProductId = 0, @flag = 1 }, commandType: CommandType.StoredProcedure) as List<DelivertypeOut>;

                oReturn[i].Products = oReturn2;

                if (oReturn2.Count > 0)
                {

                    var oReturn3 = DBContext.Query<CartCalculationOut>("PriceCalculationForCustomerCheckOut", new
                    {
                        @BranchId = oReturn[i].BranchId,
                        @CustomerId = _charge.CustomerId,
                        @AddressId = oReturn4[0].AddressId,
                        @SlNo = oReturn4[0].SlNo,
                        @VarientValue = oReturn2[0].VarientValue,
                        @SpecId = oReturn2[0].Specid,
                        @OfferPrice = oReturn2[0].OfferPrice
                    }, commandType: CommandType.StoredProcedure) as List<CartCalculationOut>;

                    DeliveryChargeExpreess = oReturn3[0].DeliveryChargeExpress;
                    DeliveryCharge = oReturn3[0].DeliveryCharge;

                    #region Has Delivery Checking

                    if (oReturn3[0].DeliveryCharge == 0 && oReturn3[0].DeliveryChargeExpress == 0)
                    {
                        oReturn[i].HasDelivery = true;
                    }
                    #endregion

                    #region Calculating Price,Delivery Charge And  Grand Total by Branch

                    for (int x = 0; x < oReturn2.Count; x++)
                    {
                        if (oReturn2[x].OfferPrice == 0)
                        {
                            Price += (oReturn2[x].OrderItemQty * oReturn2[x].MRP);
                            PriceExpress += (oReturn2[x].OrderItemQty * oReturn2[x].MRP);
                        }
                        else
                        {
                            if (oReturn2[x].MinimumOrderValue <= oReturn2[x].OrderItemQty)
                            {
                                Price += (oReturn2[x].OrderItemQty * oReturn2[x].OfferPrice);
                                PriceExpress += (oReturn2[x].OrderItemQty * oReturn2[x].OfferPrice);
                            }
                            else
                            {
                                Price += (oReturn2[x].OrderItemQty * oReturn2[x].MRP);
                                PriceExpress += (oReturn2[x].OrderItemQty * oReturn2[x].MRP);
                            }

                        }

                        totalamount = (Price + oReturn3[0].DeliveryCharge);
                        totalamountExpress = (PriceExpress + oReturn3[0].DeliveryChargeExpress);



                        #endregion

                    }

                    oReturn[i].BranchId = oReturn[i].BranchId;
                    oReturn[i].BranchName = oReturn[i].BranchName;
                    oReturn[i].DeliveryCharge = DeliveryCharge;
                    oReturn[i].PayableAmount = totalamount;
                    oReturn[i].Price = Price;
                    oReturn[i].DeliveryMode = oReturn3[0].DeliveryType;
                    oReturn[i].PayableAmountExpress = totalamountExpress;
                    oReturn[i].PriceExpress = PriceExpress;
                    oReturn[i].DeliveryChargeExpress = DeliveryChargeExpreess;
                    oReturn[i].Reason = oReturn3[0].Reason;
                }
            }

            CheckOutByCustomerOut _checkOut = new CheckOutByCustomerOut();
            _checkOut.Branch = oReturn;
            _checkOut.Address = oReturn4;

            DBContext.Close();

            oResult.iRet = 1;
            oResult.sMessage = "Success";
            oResult.sResult = _checkOut;

            return oResult;

        }


        //public ApiResult<List<RequestOutPut>> PickupRequest(PickupRequestinput _pickup)
        //{
        //    ApiResult<List<RequestOutPut>> oResult = new ApiResult<List<RequestOutPut>>();

        //    if (DBContext.State == ConnectionState.Closed)
        //    {
        //        DBContext.Open();
        //    }


        //    var oReturn = DBContext.Query<RequestOutPut>("OrderDetailsForPickUp", _pickup, commandType: CommandType.StoredProcedure) as List<RequestOutPut>;
        //    DBContext.Close();

        //    //oResult.Count = oReturn.Count;
        //    oResult.HasWarning = false;
        //    oResult.Message = "Success";
        //    oResult.Value = oReturn;
        //    return oResult;

        //}

        public APiResult OrderListForSupport()
        {
            APiResult oResult = new APiResult();

            if (DBContext.State == ConnectionState.Closed)
            {
                DBContext.Open();
            }

            var oReturn = DBContext.Query("OrderListForSupport", commandType: CommandType.StoredProcedure);

            DBContext.Close();

            oResult.iRet = 1;
            oResult.sMessage = "Success";
            oResult.sResult = oReturn;


            return oResult;
        }

        public APiResult OrderDetailsSupport(OrderDetailSupport _details)
        {
            APiResult oResult = new APiResult();

            if (DBContext.State == ConnectionState.Closed)
            {
                DBContext.Open();
            }


            DetailsSupport _orderdetails = new DetailsSupport();

            var oReturn = DBContext.Query<OrderDetailsOutput>("CustomerOrderDetails", _details, commandType: CommandType.StoredProcedure) as List<OrderDetailsOutput>;

            var oReturn1 = DBContext.Query<OrderProductListSupport>("OrderProductDetails", _details, commandType: CommandType.StoredProcedure) as List<OrderProductListSupport>;

            var oReturn2 = DBContext.Query<ShippingAddressSupport>("GetShippingAddress", _details, commandType: CommandType.StoredProcedure) as List<ShippingAddressSupport>;

            var oReturn3 = DBContext.Query<OrderSummerySupport>("GetOrderSummary", _details, commandType: CommandType.StoredProcedure) as List<OrderSummerySupport>;



            _orderdetails.OrderDetails = oReturn;
            _orderdetails.Products = oReturn1;
            _orderdetails.ShippingAddress = oReturn2;
            _orderdetails.OrderSummery = oReturn3;


            DBContext.Close();

            oResult.iRet = 1;
            oResult.sError = "false";
            oResult.sMessage = "Success";
            oResult.sResult = _orderdetails;
            return oResult;

        }

    }
}
