﻿using System;
using System.Data;
using System.Collections.Generic;
using Dapper;
using EllowCart.QueryBank.Roles;
using EllowCart.Shared;
using EllowCart.DTO.Module.InputModels;

namespace EllowCart.BusinessContracts.Roless
{
    public class ModuleServices 
    {
        private IDbConnection DBContext;
        private APiResult oResult = null;
        DAL.DBSettings _DBSettings = null;
        private QueryBank.Roles.VerificationServices oQoeryModule = null;

        public ModuleServices(IDbConnection  _DBContext)
        {
            this.DBContext = _DBContext;
             
        }
        public ModuleServices()
        {
            _DBSettings = new DAL.DBSettings();  //-- New change
            DBContext = _DBSettings.GetCartDB();
        }


        public APiResult AddNewModule(ModulesInputModel _input)
        {
            oResult = new APiResult();
            string sSQlAddModules = string.Empty;
            oQoeryModule = new VerificationServices();

            sSQlAddModules = string.Format(oQoeryModule.GetAddModules(),
                                            _input.ModuleName,
                                            _input.IsActive, _input.CreatedOn,
                                            _input.CreatedBy, _input.UpdatedOn, _input.UpdatedBy);

            if (DBContext.State == ConnectionState.Closed)
            {
                DBContext.Open();
            }
            int iCount = DBContext.Execute(sSQlAddModules);
            if (iCount > 0)
            {
                oResult = new APiResult();
                oResult.iRet = 1;
                oResult.sMessage = "Success";
            }
            DBContext.Close();
            return oResult;
        }
        //Module Deletion by querybank
        public APiResult DeleteAmodule(ModuleOuput _moduleout)
        {

            oResult = new APiResult();
            string sSQlDeleteModules = string.Empty;
            oQoeryModule = new VerificationServices();

            sSQlDeleteModules = string.Format(oQoeryModule.DeleteModules(_moduleout));


            if (DBContext.State == ConnectionState.Closed)
            {
                DBContext.Open();
            }

            int iCount = DBContext.Execute(sSQlDeleteModules);
            if (iCount > 0)
            {
                oResult = new APiResult();
                oResult.iRet = 1;
                oResult.sMessage = "Success";
            }
            DBContext.Close();
            return oResult;
        }
        //Module Updation by querybank
        public APiResult UpdateModule(ModuleInputModel Obj)
        {

            oResult = new APiResult();
            string sSQlUpdateModules = string.Empty;
            oQoeryModule = new VerificationServices();
            int ModuleId = Obj.ModuleId;
            string ModuleName = Obj.ModuleName;
            bool IsActive = Obj.IsActive;
            DateTime CreatedOn = Obj.CreatedOn;
            string CreatedBy = Obj.CreatedBy;
            DateTime UpdatedOn = Obj.UpdatedOn;
            string UpdatedBy = Obj.UpdatedBy;

            sSQlUpdateModules = string.Format(oQoeryModule.UpdateModules(ModuleName, IsActive, CreatedOn, CreatedBy, UpdatedOn, UpdatedBy, ModuleId));


            if (DBContext.State == ConnectionState.Closed)
            {
                DBContext.Open();
            }

            int iCount = DBContext.Execute(sSQlUpdateModules);
            if (iCount > 0)
            {
                oResult = new APiResult();
                oResult.iRet = 1;
                oResult.sMessage = "Success";
            }
            DBContext.Close();
            return oResult;
        }
        ////geting module by SP
        //public APiResult GetAllModulesBySp(ModulesOutputModel _obj)
        //{
        //    oResult = new APiResult();

        //    if (DBContext.State == ConnectionState.Closed)
        //    {
        //        DBContext.Open();
        //    }

        //    var oReturn = DBContext.Query("ModuleMasterSelect", _obj, commandType: CommandType.StoredProcedure);

        //    oResult.iRet = 1;
        //    oResult.sMessage = "Successfully collected all modules ";
        //    oResult.sResult = oReturn;
        //    return oResult;

        //}
        //getting module querybank
        public APiResult GetAllModules()
        {
            oResult = new APiResult();
            oQoeryModule = new VerificationServices();
            string ssqlGetAllModules = oQoeryModule.GetAllModules();
            if (DBContext.State == ConnectionState.Closed)
            {
                DBContext.Open();
            }

            var oReturn = DBContext.Query<ModuleInputModel>(ssqlGetAllModules);
            DBContext.Close();
            oResult.iRet = 1;
            oResult.sMessage = "Success";
            oResult.sResult = oReturn;
            return oResult;

        }
        //getting module querybank
        public APiResult GetModuleById(ModuleOuput _moduleout)
        {
            oResult = new APiResult();
            oQoeryModule = new VerificationServices();
            string ssqlGetAllModules = oQoeryModule.GetAModule(_moduleout);
            if (DBContext.State == ConnectionState.Closed)
            {
                DBContext.Open();
            }

            var oReturn = DBContext.Query(ssqlGetAllModules);
            DBContext.Close();
            oResult.iRet = 1;
            oResult.sMessage = "Success";
            oResult.sResult = oReturn;
            return oResult;

        }
        public APiResult AddNewModuleAttribute(ModuleAttributeModel _input)
        {
            oResult = new APiResult();
            string sSQlAddModuleAttribute = string.Empty;
            oQoeryModule = new VerificationServices();

            sSQlAddModuleAttribute = string.Format(oQoeryModule.GetAddModuleAttribute(),
                                            _input.moduleId, _input.Operation,
                                            _input.IsActive, _input.CreatedOn,
                                            _input.CreatedBy, _input.UpdatedOn, _input.UpdatedBy);

            if (DBContext.State == ConnectionState.Closed)
            {
                DBContext.Open();
            }
            int iCount = DBContext.Execute(sSQlAddModuleAttribute);
            if (iCount > 0)
            {
                oResult = new APiResult();
                oResult.iRet = 1;
                oResult.sMessage = "Success";
            }
            DBContext.Close();
            return oResult;
        }
        //ModuleAttribute Deletion by querybank
        public APiResult DeleteAmoduleAttribute(ModuleAttributeOutputModel _mattributeOut)
        {

            oResult = new APiResult();
            string sSQlDeleteModuleAttribute = string.Empty;
            oQoeryModule = new VerificationServices();

            sSQlDeleteModuleAttribute = string.Format(oQoeryModule.DeleteModuleAttribute(_mattributeOut));


            if (DBContext.State == ConnectionState.Closed)
            {
                DBContext.Open();
            }

            int iCount = DBContext.Execute(sSQlDeleteModuleAttribute);
            if (iCount > 0)
            {
                oResult = new APiResult();
                oResult.iRet = 1;
                oResult.sMessage = "Success";
            }
            DBContext.Close();
            return oResult;
        }
        //ModuleAttribute Updation by querybank
        public APiResult UpdateModuleAttribute(ModuleAttributeModelForUpdation Obj)
        {

            oResult = new APiResult();
            string sSQlUpdateModuleAttribute = string.Empty;
            oQoeryModule = new VerificationServices();
            int attributeId = Obj.AttributeId;
            int moduleId = Obj.moduleId;
            string Operation = Obj.Operation;
            bool IsActive = Obj.IsActive;
            DateTime CreatedOn = Obj.CreatedOn;
            string CreatedBy = Obj.CreatedBy;
            DateTime UpdatedOn = Obj.UpdatedOn;
            string UpdatedBy = Obj.UpdatedBy;

            sSQlUpdateModuleAttribute = string.Format(oQoeryModule.UpdateModuleAttribute(moduleId, Operation, IsActive, CreatedOn, CreatedBy, UpdatedOn, UpdatedBy, attributeId));


            if (DBContext.State == ConnectionState.Closed)
            {
                DBContext.Open();
            }

            int iCount = DBContext.Execute(sSQlUpdateModuleAttribute);
            if (iCount > 0)
            {
                oResult = new APiResult();
                oResult.iRet = 1;
                oResult.sMessage = "Success";
            }
            DBContext.Close();
            return oResult;
        }


        public APiResult GetAllModuleAttributes()
        {
            oResult = new APiResult();
            oQoeryModule = new VerificationServices();
            string ssqlGetAllModuleAttribute = oQoeryModule.GetAllModuleAttributes();
            if (DBContext.State == ConnectionState.Closed)
            {
                DBContext.Open();
            }

            var oReturn = DBContext.Query(ssqlGetAllModuleAttribute);
            DBContext.Close();
            oResult.iRet = 1;
            oResult.sMessage = "Success";
            oResult.sResult = oReturn;
            return oResult;

        }
        //getting moduleAttribute querybank
        public APiResult GetModuleAttributeById(ModuleAttributeOutputModel _mattributeOut)
        {
            oResult = new APiResult();
            oQoeryModule = new VerificationServices();
            string ssqlGetAllModules = oQoeryModule.GetAModuleAttribute(_mattributeOut);
            if (DBContext.State == ConnectionState.Closed)
            {
                DBContext.Open();
            }

            var oReturn = DBContext.Query(ssqlGetAllModules);
            DBContext.Close();
            oResult.iRet = 1;
            oResult.sMessage = "Success";
            oResult.sResult = oReturn;
            return oResult;

        }

        public APiResult ModuleAttributeByModuleId(ModuleOuput _mattributeOut)
        {
            oResult = new APiResult();
            oQoeryModule = new VerificationServices();
            string ssqlGetAllModules = oQoeryModule.ModuleAttributeGet(_mattributeOut);
            if (DBContext.State == ConnectionState.Closed)
            {
                DBContext.Open();
            }

            var oReturn = DBContext.Query(ssqlGetAllModules);
            DBContext.Close();
            oResult.iRet = 1;
            oResult.sMessage = "Success";
            oResult.sResult = oReturn;
            return oResult;

        }


        public ApiResult<List<ModuleInputModel>> GetAllModuleList()
        {
            ApiResult<List<ModuleInputModel>> oResult = new ApiResult<List<ModuleInputModel>>();
            oQoeryModule = new VerificationServices();
            string ssqlGetAllModules = oQoeryModule.GetAllModules();
            if (DBContext.State == ConnectionState.Closed)
            {
                DBContext.Open();
            }

            var oReturn = DBContext.Query<ModuleInputModel>(ssqlGetAllModules) as List<ModuleInputModel>;
            oResult.Count = oReturn.Count;
            oResult.HasWarning = false;
            oResult.Message = "Success";
            oResult.Value = oReturn;
            return oResult;
        }


        public ApiResult<List<ModuleAttributeModel>> CreateAttribute(ModuleAttributeModel _module)
        {
            ApiResult<List<ModuleAttributeModel>> oResult = new ApiResult<List<ModuleAttributeModel>>();
            if (DBContext.State == ConnectionState.Closed)
            {
                DBContext.Open();
            }

            var oReturn = DBContext.Query<ModuleAttributeModel>("ModuleAttributes", _module, commandType: CommandType.StoredProcedure) as List<ModuleAttributeModel>;
            oResult.Count = oReturn.Count;
            oResult.HasWarning = false;
            oResult.Message = "Success";
            oResult.Value = oReturn;
            return oResult;
        }

        public ApiResult<List<ModuleOuput>> GetAttribute(ModuleAttributeOutputModel _module)
        {
            ApiResult<List<ModuleOuput>> oResult = new ApiResult<List<ModuleOuput>>();
            if (DBContext.State == ConnectionState.Closed)
            {
                DBContext.Open();
            }
            var o = DBContext.Query("ModuleAttributeSelect", _module, commandType: CommandType.StoredProcedure);



            var oReturn2 = DBContext.Query("select moduleid, operation from moduleattribute");

            var oReturn1 = DBContext.Query<ModuleOuput>("ModuleAttributeSelect", _module, commandType: CommandType.StoredProcedure); ;

            var oReturn = DBContext.Query<ModuleOuput>("ModuleAttributeSelect", _module, commandType: CommandType.StoredProcedure) as List<ModuleOuput>;

            oResult.Count = oReturn.Count;
            oResult.HasWarning = false;
            oResult.Message = "Success";
            oResult.Value = oReturn;
            return oResult;
        }


    }
}
