﻿using Dapper;
using EllowCart.DTO.AddToWishList.InputModels;
using EllowCart.DTO.AddToWishList.OutputModels;
using EllowCart.Shared;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace EllowCart.BusinessContracts.Roles
{
  public  class AddToWishListServices
    {
        private IDbConnection DBContext;
        private APiResult oResult = null;
        DAL.DBSettings _DBSettings = null;
        public AddToWishListServices(IDbConnection _DBContext)
        {
            this.DBContext = _DBContext;

        }
        public AddToWishListServices()
        {
            _DBSettings = new DAL.DBSettings();  //-- New change
            DBContext = _DBSettings.GetCartDB();
        }

        public ApiResult<List<AddToWishListout>> AddToWishlist(AddToWishList _wishList)
        {
            ApiResult<List<AddToWishListout>> oResult = new ApiResult<List<AddToWishListout>>();

            if (DBContext.State == ConnectionState.Closed)
            {
                DBContext.Open();
            }

            var oReturn = DBContext.Query<AddToWishListout>("AddToWishList", _wishList, commandType: CommandType.StoredProcedure) as List<AddToWishListout>;


            DBContext.Close();

            oResult.Count = oReturn.Count;
            oResult.HasWarning = false;
            oResult.Message = "Success";
            oResult.Value = oReturn;
            return oResult;

        }

        public ApiResult<List<GetWishlistOut>> GetWishlist(SelectWishList _wishList)
        {
            ApiResult<List<GetWishlistOut>> oResult = new ApiResult<List<GetWishlistOut>>();

            if (DBContext.State == ConnectionState.Closed)
            {
                DBContext.Open();
            }

            var oReturn = DBContext.Query<GetWishlistOut>("SelectWishListData", _wishList, commandType: CommandType.StoredProcedure) as List<GetWishlistOut>;

            DBContext.Close();

            oResult.Count = oReturn.Count;
            oResult.HasWarning = false;
            oResult.Message = "Success";
            oResult.Value = oReturn;
            return oResult;

        }

        public ApiResult<List<RemoveWishlistout>> RemoveWishlist(RemoveWishListInput _wishList)
        {
            ApiResult<List<RemoveWishlistout>> oResult = new ApiResult<List<RemoveWishlistout>>();

            if (DBContext.State == ConnectionState.Closed)
            {
                DBContext.Open();
            }

            var oReturn = DBContext.Query<RemoveWishlistout>("RemoveFromWishList", _wishList, commandType: CommandType.StoredProcedure) as List<RemoveWishlistout>;

            DBContext.Close();

            oResult.Count = oReturn.Count;
            oResult.HasWarning = false;
            oResult.Message = "Success";
            oResult.Value = oReturn;
            return oResult;

        }
    }
}
