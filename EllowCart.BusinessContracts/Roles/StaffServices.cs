﻿using Dapper;
using EllowCart.DTO.Staff.InputModels;
using EllowCart.QueryBank.Roles;
using EllowCart.Shared;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace EllowCart.BusinessContracts.Roles
{
    public class StaffServices
    {
        private IDbConnection DBContext;
        private APiResult oResult = null;

        //private QueryBank.Roles.Admin oQoeryAdmin = null;
        DAL.DBSettings _DBSettings = null;
        private StaffManage oQoeryStaffManage;

        public StaffServices(IDbConnection _DBContext)
        {
            this.DBContext = _DBContext;

        }
        public StaffServices()
        {
            _DBSettings = new DAL.DBSettings();  //-- New change
            DBContext = _DBSettings.GetCartDB();
        }


        //staff insertion,updation,deletion
        public APiResult StaffProcess(DTO.Staff.InputModels.StaffInputModel _obj)
        {
            oResult = new APiResult();
            if (DBContext.State == ConnectionState.Closed)
            {
                DBContext.Open();
            }
            var oReturn = DBContext.Query("StaffMaster_INSERT", _obj, commandType: CommandType.StoredProcedure);
            DBContext.Close();
            oResult.iRet = 1;
            oResult.sMessage = "Success";
            oResult.sResult = oReturn;
            return oResult;

        }

        // get staff 
        public APiResult GetStaff(DTO.Staff.InputModels.StaffSelectModel _obj)
        {
            oResult = new APiResult();
            if (DBContext.State == ConnectionState.Closed)
            {
                DBContext.Open();
            }

            var oReturn = DBContext.Query("StaffSelect", _obj, commandType: CommandType.StoredProcedure);

            DBContext.Close();
            oResult.iRet = 1;
            oResult.sMessage = "Success";
            oResult.sResult = oReturn;

            return oResult;
        }



        //staff details insertion,updation,deletion
        public APiResult StaffDetailsProcess(DTO.Staff.InputModels.StaffDetailsInputModel _obj)
        {
            oResult = new APiResult();
            if (DBContext.State == ConnectionState.Closed)
            {
                DBContext.Open();
            }
            var oReturn = DBContext.Query("StaffDetails_INSERT", _obj, commandType: CommandType.StoredProcedure);
            DBContext.Close();
            oResult.iRet = 1;
            oResult.sMessage = "Success";
            oResult.sResult = oReturn;
            return oResult;

        }

        // get staff details
        public APiResult GetStaffDetails(DTO.Staff.InputModels.StaffSelectModel _obj)
        {
            oResult = new APiResult();
            if (DBContext.State == ConnectionState.Closed)
            {
                DBContext.Open();
            }

            var oReturn = DBContext.Query("StaffDetailsSelect", _obj, commandType: CommandType.StoredProcedure);
            DBContext.Close();
            oResult.iRet = 1;
            oResult.sMessage = "Successfully collected Staff details  ";
            oResult.sResult = oReturn;


            return oResult;
        }

        public APiResult GetStaffCode(DTO.Staff.InputModels.StaffCodeSelectModel _obj)
        {
            oResult = new APiResult();
            if (DBContext.State == ConnectionState.Closed)
            {
                DBContext.Open();
            }

            var oReturn = DBContext.Query("ForgotPassword_sTAFF", _obj, commandType: CommandType.StoredProcedure);
            DBContext.Close();
            oResult.iRet = 1;
            oResult.sMessage = "Success";
            oResult.sResult = oReturn;


            return oResult;
        }


        public APiResult StaffSignIn(DTO.Staff.InputModels.GetStaffCode _getstaff)
        {
            oResult = new APiResult();
            if (DBContext.State == ConnectionState.Closed)
            {
                DBContext.Open();
            }

            var oReturn = DBContext.Query("SignIn", _getstaff, commandType: CommandType.StoredProcedure);
            DBContext.Close();
            oResult.iRet = 1;
            oResult.sMessage = "Success";
            oResult.sResult = oReturn;
            return oResult;
        }

        public APiResult EzoroStaffRegisteration(EzoroStaffRegisteration _Staff)
        {

            oResult = new APiResult();
            string sSQlAddProductSpec = string.Empty;
            oQoeryStaffManage = new QueryBank.Roles.StaffManage();

            sSQlAddProductSpec = string.Format(oQoeryStaffManage.StaffRegisteration(), _Staff.EmployeeCode,
                                                _Staff.Name, _Staff.UserName, _Staff.Password, _Staff.IsActive,
                                                _Staff.CreatedOn, _Staff.CreatedBy, _Staff.AccountType);


            if (DBContext.State == ConnectionState.Closed)
            {
                DBContext.Open();
            }
            int iCount = DBContext.Execute(sSQlAddProductSpec);
            if (iCount > 0)
            {
                oResult = new APiResult();
                oResult.iRet = 1;
                oResult.sMessage = "Success";
            }
            DBContext.Close();

            return oResult;
        }


        public APiResult UpdateEzoroStaff(UpdateEzoroStaff _Staff)
        {

            oResult = new APiResult();
            string sSQlAddProductSpec = string.Empty;
            oQoeryStaffManage = new QueryBank.Roles.StaffManage();

            sSQlAddProductSpec = string.Format(oQoeryStaffManage.UpdateEzoroStaff(_Staff.EmployeeId,
                                               _Staff.EmployeeCode, _Staff.Name, _Staff.UserName, _Staff.Password,
                                               _Staff.IsActive, _Staff.UpdatedBy, _Staff.AccountType));


            if (DBContext.State == ConnectionState.Closed)
            {
                DBContext.Open();
            }
            int iCount = DBContext.Execute(sSQlAddProductSpec);
            if (iCount > 0)
            {
                oResult = new APiResult();
                oResult.iRet = 1;
                oResult.sMessage = "Success";
            }
            DBContext.Close();

            return oResult;
        }

        public APiResult DeleteEzoroStaff(GetOneEzorostaff _Staff)
        {

            oResult = new APiResult();
            string sSQlAddProductSpec = string.Empty;
            oQoeryStaffManage = new QueryBank.Roles.StaffManage();

            sSQlAddProductSpec = string.Format(oQoeryStaffManage.DeleteEzoroStaff(_Staff.EmployeeId));


            if (DBContext.State == ConnectionState.Closed)
            {
                DBContext.Open();
            }
            int iCount = DBContext.Execute(sSQlAddProductSpec);
            if (iCount > 0)
            {
                oResult = new APiResult();
                oResult.iRet = 1;
                oResult.sMessage = "Success";
            }
            DBContext.Close();

            return oResult;
        }

        public APiResult GetAllEzoroStaff()
        {

            oResult = new APiResult();
            string sSQlAddProductSpec = string.Empty;
            oQoeryStaffManage = new QueryBank.Roles.StaffManage();

            sSQlAddProductSpec = string.Format(oQoeryStaffManage.GetAllEzoroStaff());


            if (DBContext.State == ConnectionState.Closed)
            {
                DBContext.Open();
            }
            var oReturn = DBContext.Query(sSQlAddProductSpec);

            oResult = new APiResult();
            oResult.iRet = 1;
            oResult.sMessage = "Success";
            oResult.sResult = oReturn;

            DBContext.Close();

            return oResult;
        }

        public APiResult GetoneEzoroStaff(GetOneEzorostaff _staff)
        {

            oResult = new APiResult();
            string sSQlAddProductSpec = string.Empty;
            oQoeryStaffManage = new QueryBank.Roles.StaffManage();

            sSQlAddProductSpec = string.Format(oQoeryStaffManage.GetOneEzoroStaff(_staff.EmployeeId));


            if (DBContext.State == ConnectionState.Closed)
            {
                DBContext.Open();
            }
            var oReturn = DBContext.Query(sSQlAddProductSpec);

            oResult = new APiResult();
            oResult.iRet = 1;
            oResult.sMessage = "Success";
            oResult.sResult = oReturn;

            DBContext.Close();

            return oResult;
        }
    }
}
