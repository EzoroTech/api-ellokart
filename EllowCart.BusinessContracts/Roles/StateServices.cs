﻿using Dapper;
using EllowCart.DTO.State.InputModels;
using EllowCart.DTO.State.OutputModels;
using EllowCart.Shared;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace EllowCart.BusinessContracts.Roles
{
   public class StateServices
    {
        private IDbConnection DBContext;
        DAL.DBSettings _DBSettings = null;
        private APiResult oResult = null;
      
        public StateServices(IDbConnection _DBContext)
        {
            this.DBContext = _DBContext;

        }
        public StateServices()
        {
            _DBSettings = new DAL.DBSettings();  //-- New change
            DBContext = _DBSettings.GetCartDB();
        }


        public ApiResult<List<GetStateListOut>> GetstateList(GetStateListInput _getStateList)
        {
            ApiResult<List<GetStateListOut>> oResult = new ApiResult<List<GetStateListOut>>();

            if (DBContext.State == ConnectionState.Closed)
            {
                DBContext.Open();
            }

            var oReturn = DBContext.Query<GetStateListOut>("GetStateList", _getStateList, commandType: CommandType.StoredProcedure) as List<GetStateListOut>;
            DBContext.Close();
            oResult.Count = oReturn.Count;
            oResult.HasWarning = false;
            oResult.Message = "Success";
            oResult.Value = oReturn;

            return oResult;

        }


        //public APiResult GetstateList(GetStateListInput _getStateList)
        //{
        //    oResult = new APiResult();


        //    if (DBContext.State == ConnectionState.Closed)
        //    {
        //        DBContext.Open();
        //    }



        //    var oReturn = DBContext.Query("GetStateList", _getStateList, commandType: CommandType.StoredProcedure);

        //    DBContext.Close();

        //    oResult.iRet = 1;
        //    oResult.sMessage = "Success";
        //    oResult.sResult = oReturn;
        //    return oResult;

        //}




    }
}
