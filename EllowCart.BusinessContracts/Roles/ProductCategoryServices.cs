﻿using Dapper;
using EllowCart.DTO.ProductCategory.InputModels;
using EllowCart.DTO.ProductCategory.OutputModels;
using EllowCart.QueryBank.Roles;
using EllowCart.Shared;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace EllowCart.BusinessContracts.Roles
{
    public class ProductCategoryServices
    {
        private IDbConnection DBContext;
        private APiResult oResult = null;
        private ProductCategoryManage pdtCategory;
        private ProductCategoryManage oQoeryBusinessType;
        DAL.DBSettings _DBSettings = null;
        public ProductCategoryServices(IDbConnection _DBContext)
        {
            this.DBContext = _DBContext;

        }
        public ProductCategoryServices()
        {
            _DBSettings = new DAL.DBSettings();  //-- New change
            DBContext = _DBSettings.GetCartDB();
        }

        public APiResult ProductCategory(Productcategory _product)
        {
            oResult = new APiResult();
            if (DBContext.State == ConnectionState.Closed)
            {
                DBContext.Open();
            }


                var oReturn = DBContext.Query("ProductCategoryManage",_product ,commandType: CommandType.StoredProcedure);
            DBContext.Close();
            oResult.iRet = 1;
                oResult.sMessage = "Success";
                oResult.sResult = oReturn;
           
            return oResult;
        }

        public APiResult GetAllParentCategory()
        {
            oResult = new APiResult();
            oQoeryBusinessType = new QueryBank.Roles.ProductCategoryManage();
            string ssqlGetAllParentCategory = oQoeryBusinessType.GetParentCategory();
            if (DBContext.State == ConnectionState.Closed)
            {
                DBContext.Open();
            }

            var oReturn = DBContext.Query(ssqlGetAllParentCategory);
            DBContext.Close();
            oResult.iRet = 1;
            oResult.sMessage = "Success";
            oResult.sResult = oReturn;
            return oResult;

        }

        //getting Product Category querybank
        public APiResult GetAllProductCategory()
        {
            oResult = new APiResult();
            pdtCategory = new QueryBank.Roles.ProductCategoryManage();
            string ssqlGetProductCategory = pdtCategory.GetAllProductCategory();
            if (DBContext.State == ConnectionState.Closed)
            {
                DBContext.Open();
            }

            var oReturn = DBContext.Query(ssqlGetProductCategory);
            DBContext.Close();
            oResult.iRet = 1;
            oResult.sMessage = "Success";
            oResult.sResult = oReturn;
            return oResult;

        }

        //getting productCategory querybank
        public APiResult GetProductCategoryById(GetOneProductCategory _category)
        {
            oResult = new APiResult();
            pdtCategory = new QueryBank.Roles.ProductCategoryManage();
            string ssqlGetAProductCategory = pdtCategory.GetAProductCategory(_category.ProductCategoryId);
            if (DBContext.State == ConnectionState.Closed)
            {
                DBContext.Open();
            }

            var oReturn = DBContext.Query(ssqlGetAProductCategory);
            DBContext.Close();
            oResult.iRet = 1;
            oResult.sMessage = "Success";
            oResult.sResult = oReturn;
            return oResult;

        }

        //ProductCategory Deletion by querybank
        public APiResult DeleteAProductCategory(GetOneProductCategory _category)
        {

            oResult = new APiResult();
            string sSQlProductCategory = string.Empty;
            pdtCategory = new QueryBank.Roles.ProductCategoryManage();

            sSQlProductCategory = string.Format(pdtCategory.DeleteProductCategory(_category.ProductCategoryId));


            if (DBContext.State == ConnectionState.Closed)
            {
                DBContext.Open();
            }

            int iCount = DBContext.Execute(sSQlProductCategory);
            if (iCount > 0)
            {
                oResult = new APiResult();
                oResult.iRet = 1;
                oResult.sMessage = "Success";
            }
            DBContext.Close();
            return oResult;
        }


        //ProductCategory Updation by querybank
        public APiResult UpdateProductCategory(ProductCategoryInputForAdmin Obj)
        {

            oResult = new APiResult();
            string sSQlUpdateProductCategory = string.Empty;
            pdtCategory = new QueryBank.Roles.ProductCategoryManage();
            string CategoryName = Obj.CategoryName;
            long parentCategId = Obj.ParentCategoryID;
            string Descr = Obj.Description;
            bool IsActive = Obj.IsActive;
            string UpdatedBy = Obj.UpdatedBy;
          
            sSQlUpdateProductCategory = string.Format(pdtCategory.UpdateProductCategory(CategoryName, parentCategId, Descr,IsActive,UpdatedBy,Obj.ProductCategoryId));


            if (DBContext.State == ConnectionState.Closed)
            {
                DBContext.Open();
            }

            int iCount = DBContext.Execute(sSQlUpdateProductCategory);
            if (iCount > 0)
            {
                oResult = new APiResult();
                oResult.iRet = 1;
                oResult.sMessage = "Success";
            }
            DBContext.Close();
            return oResult;
        }


        public ApiResult<List<ProductCategoryDetails>> GetProductCategory(DTO.ProductCategory.InputModels.GetProductCategory _getProductCategory)
        {
            ApiResult<List<ProductCategoryDetails>> oResult = new ApiResult<List<ProductCategoryDetails>>();
            if (DBContext.State == ConnectionState.Closed)
            {
                DBContext.Open();
            }

            var oReturn = DBContext.Query<ProductCategoryDetails>("GetproductSubcategory_list", _getProductCategory, commandType: CommandType.StoredProcedure)as List<ProductCategoryDetails>;

            DBContext.Close();

            oResult.Count = oReturn.Count;
            oResult.HasWarning = false;
            oResult.Message = "Success";
            oResult.Value = oReturn;
            return oResult;
        }

        public ApiResult<List<ChildToNodeCategoryOut>> ChildToNodeCategory(DTO.ProductCategory.InputModels.ChildToNodeCategoryinput _childtonode)
        {
            ApiResult<List<ChildToNodeCategoryOut>> oResult = new ApiResult<List<ChildToNodeCategoryOut>>();
            if (DBContext.State == ConnectionState.Closed)
            {
                DBContext.Open();
            }

            var oReturn = DBContext.Query<ChildToNodeCategoryOut>("GetChildToNodeCategory", _childtonode, commandType: CommandType.StoredProcedure) as List<ChildToNodeCategoryOut>;

            DBContext.Close();

            oResult.Count = oReturn.Count;
            oResult.HasWarning = false;
            oResult.Message = "Success";
            oResult.Value = oReturn;
            return oResult;
        }

        public ApiResult<List<GetProductCategoryList>> GetProductCategoryList()
        {
            ApiResult<List<GetProductCategoryList>> oResult = new ApiResult<List<GetProductCategoryList>>();
            if (DBContext.State == ConnectionState.Closed)
            {
                DBContext.Open();
            }

            var oReturn = DBContext.Query<GetProductCategoryList>("GetProductCategoryList", commandType: CommandType.StoredProcedure) as List<GetProductCategoryList>;
            DBContext.Close();


            oResult.Count = oReturn.Count;
            oResult.HasWarning = false;
            oResult.Message = "Success";
            oResult.Value = oReturn;
            return oResult;
        }

        public ApiResult<List<GetProductCategoryList>> GetProductCategoryListByBranchId(ProductCategoryLisyByBranch _category)
        {
            ApiResult<List<GetProductCategoryList>> oResult = new ApiResult<List<GetProductCategoryList>>();
            if (DBContext.State == ConnectionState.Closed)
            {
                DBContext.Open();
            }

            var oReturn = DBContext.Query<GetProductCategoryList>("ProductcategorylistByBranchId", _category, commandType: CommandType.StoredProcedure) as List<GetProductCategoryList>;
            DBContext.Close();


            oResult.Count = oReturn.Count;
            oResult.HasWarning = false;
            oResult.Message = "Success";
            oResult.Value = oReturn;
            return oResult;
        }


        public ApiResult<List<GetProductCategoryList>> GetProductSubCategForParentCateg(ProductSubCategForParent _category)
        {
            ApiResult<List<GetProductCategoryList>> oResult = new ApiResult<List<GetProductCategoryList>>();
            if (DBContext.State == ConnectionState.Closed)
            {
                DBContext.Open();
            }

            var oReturn = DBContext.Query<GetProductCategoryList>("GetProductSubCategories", _category, commandType: CommandType.StoredProcedure) as List<GetProductCategoryList>;
            DBContext.Close();


            oResult.Count = oReturn.Count;
            oResult.HasWarning = false;
            oResult.Message = "Success";
            oResult.Value = oReturn;
            return oResult;
        }





    }
}
