﻿using Dapper;
using EllowCart.DTO.ProductSpecification.InputModels;
using EllowCart.DTO.ProductSpecification.OutputModels;
using EllowCart.QueryBank.Roles;
using EllowCart.Shared;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace EllowCart.BusinessContracts.Roles
{
    public class ProductSpecificationServices
    {
        private IDbConnection DBContext;
        private APiResult oResult = null;
        DAL.DBSettings _DBSettings = null;
        private ProductSpecificationManage oQoeryProductSpecificationManage;

        public ProductSpecificationServices(IDbConnection _DBContext)
        {
            this.DBContext = _DBContext;

        }
        public ProductSpecificationServices()
        {
            _DBSettings = new DAL.DBSettings();  //-- New change
            DBContext = _DBSettings.GetCartDB();
        }
        public APiResult ProductSpecification(ProductSpecificationInput _product)
        {
            oResult = new APiResult();
            string sSQlAddProductSpec = string.Empty;
            oQoeryProductSpecificationManage = new QueryBank.Roles.ProductSpecificationManage();

            sSQlAddProductSpec = string.Format(oQoeryProductSpecificationManage.AddProductSpecification(), _product.SpecificationName, _product.Description,
                _product.IsActive, _product.CreatedOn, _product.CreatedBy);

            if (DBContext.State == ConnectionState.Closed)
            {
                DBContext.Open();
            }
            int iCount = DBContext.Execute(sSQlAddProductSpec);
            if (iCount > 0)
            {
                oResult = new APiResult();
                oResult.iRet = 1;
                oResult.sMessage = "Success";
            }
            DBContext.Close();

            return oResult;
        }

        public APiResult UpdateProductSpecification(UpdateProductSpecification _product)
        {
            oResult = new APiResult();
            string sSQlAddProductSpec = string.Empty;
            oQoeryProductSpecificationManage = new QueryBank.Roles.ProductSpecificationManage();

            string SpecificationName = _product.SpecificationName;
            string Description = _product.Description;
            bool status = _product.IsActive;
            string UpdatedBy = _product.UpdatedBy;
            long ProductSpecificationId = _product.ProductSpecificationId;


            sSQlAddProductSpec = string.Format(oQoeryProductSpecificationManage.UpdateProductSpec(
                                               SpecificationName, Description, status, UpdatedBy, ProductSpecificationId));

            if (DBContext.State == ConnectionState.Closed)
            {
                DBContext.Open();
            }
            int iCount = DBContext.Execute(sSQlAddProductSpec);
            if (iCount > 0)
            {
                oResult = new APiResult();
                oResult.iRet = 1;
                oResult.sMessage = "Success";
            }
            DBContext.Close();

            return oResult;
        }

        public APiResult DeleteProductSpecification(DeleteProductSpecification _product)
        {
            oResult = new APiResult();
            string sSQlAddProductSpec = string.Empty;
            oQoeryProductSpecificationManage = new QueryBank.Roles.ProductSpecificationManage();


            sSQlAddProductSpec = string.Format(oQoeryProductSpecificationManage.DeleteProductSpec(_product.ProductSpecId));


            if (DBContext.State == ConnectionState.Closed)
            {
                DBContext.Open();
            }
            var oReturn = DBContext.Execute(sSQlAddProductSpec);

            oResult = new APiResult();
            oResult.iRet = 1;
            oResult.sMessage = "Success";
            oResult.sResult = oReturn;

            DBContext.Close();

            return oResult;
        }

        public APiResult GetAllProductSpecification()
        {
            oResult = new APiResult();
            string sSQlgetProductSpec = string.Empty;
            oQoeryProductSpecificationManage = new QueryBank.Roles.ProductSpecificationManage();


            sSQlgetProductSpec = string.Format(oQoeryProductSpecificationManage.GetAllProductSpec());


            if (DBContext.State == ConnectionState.Closed)
            {
                DBContext.Open();
            }
            var oReturn = DBContext.Query(sSQlgetProductSpec);

            oResult = new APiResult();
            oResult.iRet = 1;
            oResult.sMessage = "Success";
            oResult.sResult = oReturn;

            DBContext.Close();

            return oResult;
        }

        public APiResult GetoneProductSpecification(DeleteProductSpecification _specification)
        {
            oResult = new APiResult();
            string sSQlOneProductSpec = string.Empty;
            oQoeryProductSpecificationManage = new QueryBank.Roles.ProductSpecificationManage();



            sSQlOneProductSpec = string.Format(oQoeryProductSpecificationManage.GetAProductSpecification(_specification.ProductSpecId));


            if (DBContext.State == ConnectionState.Closed)
            {
                DBContext.Open();
            }
            var oReturn = DBContext.Query(sSQlOneProductSpec);

            oResult = new APiResult();
            oResult.iRet = 1;
            oResult.sMessage = "Success";
            oResult.sResult = oReturn;
            DBContext.Close();

            return oResult;
        }

        public ApiResult<List<ProductSpecification>> GetProductSpecification(DTO.ProductSpecification.InputModels.ProductSpecificationGet _product)
        {
            ApiResult<List<ProductSpecification>> oResult = new ApiResult<List<ProductSpecification>>();
            if (DBContext.State == ConnectionState.Closed)
            {
                DBContext.Open();
            }

            var oReturn = DBContext.Query<ProductSpecification>("GETProductSpecification", _product, commandType: CommandType.StoredProcedure) as List<ProductSpecification>;
            DBContext.Close();
            oResult.Count = oReturn.Count;
            oResult.HasWarning = false;
            oResult.Message = "Success";
            oResult.Value = oReturn;
            return oResult;
        }

        public APiResult AddSpecificationAttributes(AddSpecificationAttributeInput _attribute)
        {
            APiResult oResult = new APiResult();
            if (DBContext.State == ConnectionState.Closed)
            {
                DBContext.Open();
            }

            var dt = new DataTable("ProductSubCatSpec_Type");
            dt.Columns.Add("ProductSpecId", typeof(long));

            var spec = _attribute.DtProductSubCatSpec;
            for (int i = 0; i < spec.Count; i++)
            {
                dt.Rows.Add(spec[i].ProductSpecId);
            }

            var oReturn = DBContext.Query("ProductCategorySpecInsert", new
            {
                @ProductSubCategoryId = _attribute.ProductSubCategoryId,
                @DtProductSubCatSpec = dt.AsTableValuedParameter("ProductSubCatSpec_Type")
            }, commandType: CommandType.StoredProcedure);


            oResult = new APiResult();
            oResult.iRet = 1;
            oResult.sMessage = "Success";
            oResult.sResult = oReturn;
            DBContext.Close();

            return oResult;
        }

        public APiResult UpdateSpecificationAttributes(AddSpecificationAttributeInput _attribute)
        {
            APiResult oResult = new APiResult();
            if (DBContext.State == ConnectionState.Closed)
            {
                DBContext.Open();
            }

            var dt = new DataTable("ProductSubCatSpec_Type");
            dt.Columns.Add("ProductSpecId", typeof(long));

            var spec = _attribute.DtProductSubCatSpec;
            for (int i = 0; i < spec.Count; i++)
            {
                dt.Rows.Add(spec[i].ProductSpecId);
            }

            var oReturn = DBContext.Query("ProductCategorySpecUpdate", new
            {
                @ProductSubCategoryId = _attribute.ProductSubCategoryId,
                @DtProductSubCatSpec = dt.AsTableValuedParameter("ProductSubCatSpec_Type")
            }, commandType: CommandType.StoredProcedure);


            oResult = new APiResult();
            oResult.iRet = 1;
            oResult.sMessage = "Success";
            oResult.sResult = oReturn;
            DBContext.Close();

            return oResult;
        }

        public APiResult DeleteProductSpecificationAttribute(DeleteProductSpecificationAttribute _attribute)
        {
            oResult = new APiResult();
            string sSQlgetProductSpec = string.Empty;
            oQoeryProductSpecificationManage = new QueryBank.Roles.ProductSpecificationManage();


            sSQlgetProductSpec = string.Format(oQoeryProductSpecificationManage.DeleteProductSpecAttribute(_attribute.ProductCategoryId));


            if (DBContext.State == ConnectionState.Closed)
            {
                DBContext.Open();
            }
            var oReturn = DBContext.Query(sSQlgetProductSpec);

            oResult = new APiResult();
            oResult.iRet = 1;
            oResult.sMessage = "Success";
            oResult.sResult = oReturn;

            DBContext.Close();

            return oResult;
        }


        public APiResult GetAllProductSpecificationAttribute_Admin()
        {
            oResult = new APiResult();
            string sSQlgetProductSpec = string.Empty;
            oQoeryProductSpecificationManage = new QueryBank.Roles.ProductSpecificationManage();


            sSQlgetProductSpec = string.Format(oQoeryProductSpecificationManage.GetAllProductSpecAttribute());


            if (DBContext.State == ConnectionState.Closed)
            {
                DBContext.Open();
            }
            var oReturn = DBContext.Query(sSQlgetProductSpec);

            oResult = new APiResult();
            oResult.iRet = 1;
            oResult.sMessage = "Success";
            oResult.sResult = oReturn;

            DBContext.Close();

            return oResult;
        }


        public APiResult GetOneProductSpecificationAttribute(GetOneSpecAttribute _getOneSpec)
        {
            oResult = new APiResult();
            string sSQlgetProductSpec = string.Empty;
            oQoeryProductSpecificationManage = new QueryBank.Roles.ProductSpecificationManage();


            sSQlgetProductSpec = string.Format(oQoeryProductSpecificationManage.GetONEProductSpecAttribute(_getOneSpec.ProductCategoryId));


            if (DBContext.State == ConnectionState.Closed)
            {
                DBContext.Open();
            }
            var oReturn = DBContext.Query(sSQlgetProductSpec);

            oResult = new APiResult();
            oResult.iRet = 1;
            oResult.sMessage = "Success";
            oResult.sResult = oReturn;

            DBContext.Close();

            return oResult;
        }

        public ApiResult<List<SpecificationattributeOut>> GetSpecificationAttributes(GetSpecificationAttribute _attribute)
        {
            ApiResult<List<SpecificationattributeOut>> oResult = new ApiResult<List<SpecificationattributeOut>>();
            if (DBContext.State == ConnectionState.Closed)
            {
                DBContext.Open();
            }


            var oReturn = DBContext.Query<SpecificationattributeOut>("Getspec_attributes", _attribute, commandType: CommandType.StoredProcedure) as List<SpecificationattributeOut>;

            DBContext.Close();
            oResult.Count = oReturn.Count;
            oResult.HasWarning = false;
            oResult.Message = "Success";
            oResult.Value = oReturn;
            return oResult;
        }


    }
}
