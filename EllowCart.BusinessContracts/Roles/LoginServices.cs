﻿using Dapper;
using EllowCart.DTO.Login.InputModels;
using EllowCart.Shared;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace EllowCart.BusinessContracts.Roles
{
  public  class LoginServices
    {
        private IDbConnection DBContext;
        private APiResult oResult = null;
        DAL.DBSettings _DBSettings = null;

        private QueryBank.Roles.BusinessTypeManage oQoeryBusinessType = null;

        public LoginServices(IDbConnection _DBContext)
        {
            this.DBContext = _DBContext;

        }
        public LoginServices()
        {
            _DBSettings = new DAL.DBSettings();  //-- New change
            DBContext = _DBSettings.GetCartDB();
        }

        public APiResult AdminLogin(AdminLogin _login)
        {
            oResult = new APiResult();
            
            if (DBContext.State == ConnectionState.Closed)
            {
                DBContext.Open();
            }
            var oReturn = DBContext.Query("AdminLogin",_login, commandType: CommandType.StoredProcedure);

            DBContext.Close();
            
            oResult.iRet = 1;
            oResult.sMessage = "Success";
            oResult.sResult = oReturn;
            return oResult;
        }
    }
}
