﻿using EllowCart.BusinessContracts.Roles;
using Microsoft.AspNetCore.Http;
using System;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Net.Sockets;
using System.Reflection;
using System.Threading;
using Microsoft.AspNetCore.Http;
using EllowCart.Shared;
using Microsoft.AspNetCore.Http.Features;

namespace EllowCart.BusinessContracts
{
    public class ImageUploadServices
    {
        private IDbConnection DBContext;
        EllowCart.BusinessContracts.Roles.AddImagesServices _imgservices = null;
        private IHttpContextAccessor _accessor;
        DAL.DBSettings _DBSettings = null;
        public ImageUploadServices(IDbConnection _DBContext)
        {
            this.DBContext = _DBContext;

        }
        public ImageUploadServices()
        {
            _DBSettings = new DAL.DBSettings();  //-- New change
            DBContext = _DBSettings.GetCartDB();
        }
        public ImageUploadServices(IHttpContextAccessor accessor)
        {
            _accessor = accessor;
        }


        public string ImageUpload(string base64image)
        {

            string LP = "";
            string LIP = "";
            string _ImgUrl = "";
            Socket s = new Socket(SocketType.Stream, ProtocolType.Tcp);
            // Global_Operations_Manager_Class gom = new Global_Operations_Manager_Class();
            _imgservices = new EllowCart.BusinessContracts.Roles.AddImagesServices();

            if (base64image != null && base64image != "")
            {
                // a.base64image 
                //or full path to file in temp location
                //var filePath = Path.GetTempFileName();
                // full path to file in current project location
              

                string location = Path.GetDirectoryName(Assembly.GetAssembly(typeof(AddImagesServices)).CodeBase);
                string filedir = new Uri(Path.Combine(location, "../Images")).LocalPath;
                Debug.WriteLine(filedir);
                Debug.WriteLine(Directory.Exists(filedir));


                //check if the folder exists;
                if (!Directory.Exists(filedir))
                {
                    Directory.CreateDirectory(filedir);
                }

                //getting random file Name
                string fileName = _imgservices.ThumbUrl();
                string file = Path.Combine(filedir, fileName);

                //getting random file Name
                //===GET SERVER IP AND PORT

                //var x = HttpContext.Current.Request.ServerVariables["SERVER_PORT"];
                //var y = HttpContext.Current.Request.ServerVariables["LOCAL_ADDR"];

                //LP = x.ToString();
                //LIP = y.ToString();

                //===GET SERVER IP AND PORT

                //-----test

                LIP = "172.30.15.248";
                LP = "8080";

                

                //-----end test

                var xx = getPathURL(base64image, file + "" + ".jpg", LP, LIP, fileName);
                _ImgUrl = xx.ToString();
                return _ImgUrl;
            }
            else
            {
                return "";
            }


        }
        public string getPathURL(string img, string path, string port, string IP, string _filename)
        {
            string _url = "";
            try
            {
                
                var bytes = Convert.FromBase64String(img);

                if (bytes.Length > 0)
                {
                    using (var stream = new FileStream(path, FileMode.Create))
                    {
                        stream.Write(bytes, 0, bytes.Length);
                        stream.Flush();
                    }
                }
                Thread.Sleep(1000);
                _url = "http://" + IP + ":" + port + "/Images/" + _filename + ".jpg";

            }

            catch (Exception _ex)
            {
                // error_obj.Post_error(_ex, "getPathURL");
                return _url;
            }

            return _url;
        }


        public string ThumbImageUpload(string base64image)
        {

            string LP = "";
            string LIP = "";
            string _ImgUrl = "";
            Socket s = new Socket(SocketType.Stream, ProtocolType.Tcp);
            // Global_Operations_Manager_Class gom = new Global_Operations_Manager_Class();
            _imgservices = new EllowCart.BusinessContracts.Roles.AddImagesServices();

            try
            {

                if (base64image != null && base64image != "")
                {
                    // a.base64image 
                    //or full path to file in temp location
                    //var filePath = Path.GetTempFileName();
                    // full path to file in current project location
                    string location = Path.GetDirectoryName(Assembly.GetAssembly(typeof(AddImagesServices)).CodeBase);
                    string filedir = new Uri(Path.Combine(location, "../Thumbs")).LocalPath;
                    Debug.WriteLine(filedir);
                    Debug.WriteLine(Directory.Exists(filedir));


                    //check if the folder exists;
                    if (!Directory.Exists(filedir))
                    {
                        Directory.CreateDirectory(filedir);
                    }
                    //getting random file Name
                    string fileName = _imgservices.ThumbUrl();
                    string file = Path.Combine(filedir, fileName);
                    //getting random file Name
                    //===GET SERVER IP AND PORT
                    //var x = HttpContext.Current.Request.ServerVariables["SERVER_PORT"];
                    //var y = HttpContext.Current.Request.ServerVariables["LOCAL_ADDR"];
                    //LP = x.ToString();
                    //LIP = y.ToString();
                    //===GET SERVER IP AND PORT


                    //Debug.WriteLine(file);
                    //Task<string> task = new Task<string>(() => getPathURL(base64image, file + "" + ".jpg", LP, LIP, fileName));
                    //task.Start();
                    //_ImgUrl = task.Result;

                    LIP = "172.30.15.248";
                    LP = "8080";

                    var xx = thumbgetPathURL(base64image, file + "" + ".jpg", LP, LIP, fileName);
                    _ImgUrl = xx.ToString();
                    return _ImgUrl;
                }
                else
                {
                    return "";
                }
            }
            catch (Exception _ex)
            {
                // error_obj.Post_error(_ex, "ThumbImageUpload");
                return _ImgUrl;
            }
        }
        public string thumbgetPathURL(string img, string path, string port, string IP, string _filename)
        {
            string _url = "";
            try
            {
                var bytes = Convert.FromBase64String(img);

                //if (bytes.Length > 0)
                //{
                //    using (var stream = new FileStream(path, FileMode.Create))
                //    {
                //        stream.Write(bytes, 0, bytes.Length);
                //        stream.Flush();
                //    }
                //}
                //if (bytes.Length > 0)
                //{
                //    using (MemoryStream ms = new MemoryStream(bytes))
                //    {
                //        Bitmap thumb = new Bitmap(372, 327);
                //        using (Image bmp = Image.FromStream(ms))
                //        {
                //            using (Graphics g = Graphics.FromImage(thumb))
                //            {
                //                g.InterpolationMode = InterpolationMode.HighQualityBicubic;
                //                g.CompositingQuality = CompositingQuality.HighQuality;
                //                g.SmoothingMode = SmoothingMode.HighQuality;
                //                g.DrawImage(bmp, 0, 0, 372, 327);
                //                thumb.Save(path);

                //            }
                //        }

                //    }
                //}
                Thread.Sleep(1000);
                _url = "http://" + IP + ":" + port + "/Thumbs/" + _filename + ".jpg";

            }

            catch (Exception _ex)
            {
                // error_obj.Post_error(_ex, "thumbgetPathURL");
                return _url;
            }

            return _url;
        }
    }
}
