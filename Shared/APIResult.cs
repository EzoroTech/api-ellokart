﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EllowCart.Shared
{
    public class APiResult
    {
        public int iRet { get; set; }
        public object sResult { get; set; }
        public string sError { get; set; }
        public string sMessage { get; set; }
        public DateTime? CurDate { get; set; }
    }


    public class ApiResult<T>
    {
        public T Value { get; set; }
        public int Count { get; set; }
        public bool HasWarning { get; set; }
        public string Message { get; set; }
        public int ErrorCode { get; set; }
    }
   
}
