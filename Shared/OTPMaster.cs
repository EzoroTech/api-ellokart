﻿using EllowCart.DTO.General.InputModels;
using EllowCart.DTO.General.OutputModels;
using System;
using System.Collections.Generic;
using System.Data;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Dapper;


namespace EllowCart.Shared
{
    public class OTPMaster
    {

        private IDbConnection DBContext;
        DAL.DBSettings _DBSettings = null;
        private HttpClient client = null;

        public OTPMaster(IDbConnection _DBContext)
        {
            this.DBContext = _DBContext;

        }
        public OTPMaster()
        {
            _DBSettings = new DAL.DBSettings();  //-- New change
            DBContext = _DBSettings.GetCartDB();
        }



        //private string httpAPILink = @"http://sapteleservices.com/SMS_API/sendsms.php?username=EZORO&password=5d841fT&mobile=8592035376&sendername=GREENG&message=test&routetype=2";
        private string httpAPILink = @"http://sapteleservices.in/SMS_API/sendsms.php?username=EZORO&password=5d841fT&";



        public string CreateSMSAPI(ManageOTP smsAttributes)
        {

            try
            {
                httpAPILink = !httpAPILink.EndsWith("&") ? string.Format("{0}{1}", httpAPILink, "&") : httpAPILink;

                httpAPILink = httpAPILink + "&mobile=" + smsAttributes.mobile + "&sendername=ELOKRT&message=ElloKart Seller Registration OTP is " + smsAttributes.message + ".&routetype=1";

                //  httpAPILink = string.Format("{0}mobile={1}{2}sendername={3}{4}message={5}{6}routetype={7}", httpAPILink,"&",smsAttributes.mobile, smsAttributes.sendername, "&", smsAttributes.message, "&", smsAttributes.routeType);
            }
            catch (Exception eX)
            {
                httpAPILink = null;
            }

            return httpAPILink;
        }

        public async Task<int> SendSMS(string sURL)
        {
            int iRet = 1;

            try
            {

                client = new HttpClient();
                HttpResponseMessage response = await client.GetAsync(sURL);
            }
            catch (Exception eX)
            {
                iRet = -1;
            }

            return iRet;

        }

        public async Task<APiResult> SendSMS(ManageOTP smsAttributes)
        {

            ManageOTP_Out otp = new ManageOTP_Out();

            string sURL = string.Empty;
            APiResult _aPiResult = new APiResult();
            _aPiResult.iRet = 1;

            /////*---OTP Creation----*/

            string num = "0123456789";
            int len = num.Length;
            string OTP = string.Empty;
            int otpDigit = 5;
            string finalDigit;
            int getIndex;
            for (int i = 0; i < otpDigit; i++)
            {
                do
                {
                    getIndex = new Random().Next(0, len);
                    finalDigit = num.ToCharArray()[getIndex].ToString();

                } while (OTP.IndexOf(finalDigit) != -1);
                OTP += finalDigit;
            }

            smsAttributes.message = OTP;
            //var s = Convert.ToInt32(OTP);
             otp.OTP = OTP;
            _aPiResult.sResult = otp;

            ///*---OTP Creation----*/




            try
            {
                client = new HttpClient();
                sURL = CreateSMSAPI(smsAttributes);
                var response = await client.GetAsync(sURL);
                var status = response.EnsureSuccessStatusCode();
            }
            catch (Exception eX)
            {
                _aPiResult.iRet = -1;

            }

            return _aPiResult;

        }

        public async Task<APiResult> OTPGenerate(OTPInput _OTP)
        {
            APiResult _aPiResult = new APiResult();
            try
            {

                ManageOTP smsAttributes = new ManageOTP();
                ManageOTP_Out otp = new ManageOTP_Out();


                string sURL = string.Empty;

                _aPiResult.iRet = 1;
                int _flag = 0;

                if (DBContext.State == ConnectionState.Closed)
                {
                    DBContext.Open();
                }

                if (_OTP.OTPId == 0)
                {
                    var oReturn = DBContext.Query<ManageOTP_Out>("Manage_OTP", new
                    {
                        @MobileNumber = _OTP.MobileNumber,
                        @OTP = _OTP.OTP,
                        @OTPID = _OTP.OTPId,
                        @Flag = _flag
                    }, commandType: CommandType.StoredProcedure) as List<ManageOTP_Out>;

                    DBContext.Close();

                    smsAttributes.message = oReturn[0].OTP;
                    smsAttributes.mobile = _OTP.MobileNumber;
                    oReturn[0].OTP = "0";

                    _aPiResult.sResult = oReturn;

                    client = new HttpClient();
                    sURL = CreateSMSAPI(smsAttributes);
                    var response = await client.GetAsync(sURL);
                    var status = response.EnsureSuccessStatusCode();

                }
                else if(_OTP.OTPId != 0)
                {
                    var oReturn = DBContext.Query<ManageOTP_Out>("Manage_OTP", new
                    {
                        @MobileNumber = _OTP.MobileNumber,
                        @OTP = _OTP.OTP,
                        @OTPID = _OTP.OTPId,
                        @Flag = 1
                    }, commandType: CommandType.StoredProcedure) as List<ManageOTP_Out>;

                    DBContext.Close();

                    _aPiResult.sResult = oReturn;
                }




            }
            catch (Exception eX)
            {
                _aPiResult.iRet = -1;

            }

            return _aPiResult;

        }


    }
}

