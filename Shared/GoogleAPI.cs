﻿using Dapper;
using EllowCart.DTO.Order.InputModels;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Net;
using System.Text;
using System.Xml.Linq;

namespace EllowCart.Shared
{
    public class GoogleAPI
    {
        private IDbConnection DBContext;
        private APiResult oResult = null;
        DAL.DBSettings _DBSettings = null;

        DataTable dtGMap = null;


        public GoogleAPI(IDbConnection _DBContext)
        {
            this.DBContext = _DBContext;

        }
        public GoogleAPI()
        {
            _DBSettings = new DAL.DBSettings();  //-- New change
            DBContext = _DBSettings.GetCartDB();
        }


        public class Response
        {
            public string Status { get; set; }

            [JsonProperty(PropertyName = "destination_addresses")]
            public string[] DestinationAddresses { get; set; }

            [JsonProperty(PropertyName = "origin_addresses")]
            public string[] OriginAddresses { get; set; }

            public Row[] Rows { get; set; }

            public class Data
            {
                public int Value { get; set; }
                public string Text { get; set; }
            }

            public class Element
            {
                public string Status { get; set; }
                public Data Duration { get; set; }
                public Data Distance { get; set; }
            }

            public class Row
            {
                public Element[] Elements { get; set; }
            }
        }

        public class DistanceOut
        {
            public float Distance { get; set; }
            public string Duration { get; set; }
        }
        public class GoogleOut
        {
            public string destinationaddresses { get; set; }
            public string originaddresses { get; set; }
            public List<Destination> rows { get; set; }

        }
        public class Destination
        {
            public List<Element> elements { get; set; }
        }

        public class Element
        {
            public string distance { get; set; }
            public string duration { get; set; }
            public string status { get; set; }
        }


        public APiResult Distance(DistanceInput _distance)

        {
            oResult = new APiResult();


            if (DBContext.State == ConnectionState.Closed)
            {
                DBContext.Open();
            }




            var oReturn = DBContext.Query<DistanceOut>("GetDistanceUsingLatAndLong", _distance, commandType: CommandType.StoredProcedure) as List<DistanceOut>;

            DBContext.Close();

            if (oReturn.Count == 0)
            {
                /***** Call Google API*******/

                string url = @"https://maps.googleapis.com/maps/api/distancematrix/json?units=imperial&origins=10.0193,76.3352&destinations=9.9682,76.3182&key=AIzaSyDx_0ZC9DSpLB7Bh2fiN3pAg8MhY4dYKgM";

                // string url = @"https://maps.googleapis.com/maps/api/directions/json?origin=75+9th+Ave+New+York,+NY&destination=MetLife+Stadium+1+MetLife+Stadium+Dr+East+Rutherford,+NJ+07073&key=YOUR_API_KEY";

                WebRequest request = WebRequest.Create(url);
                WebResponse response = request.GetResponse();
                Stream data = response.GetResponseStream();
                StreamReader reader = new StreamReader(data);
                // json-formatted string from maps api
                string responseFromServer = reader.ReadToEnd();
                var data1 = JsonConvert.DeserializeObject<Response>(responseFromServer);
                
                response.Close();

                /***********END*************/

                var oReturn1 = DBContext.Query<DistanceOut>("AddDistanceUsingLatAndLong", _distance, commandType: CommandType.StoredProcedure) as List<DistanceOut>;

                oReturn[0].Distance = oReturn1[0].Distance;
            }

            oResult.sResult = oReturn;


            return oResult;
        }


      

        public APiResult GetCoordinatesFromAddress()
        {
            APiResult oResult = new APiResult();
            try
            {
                String strAddress = "Noida 201301,India";
                String url = "https://maps.google.com/maps/api/geocode/xml?address=" + strAddress + "&sensor=false&key=AIzaSyDx_0ZC9DSpLB7Bh2fiN3pAg8MhY4dYKgM";

              

                WebRequest request = WebRequest.Create(url);

                using (WebResponse response = (HttpWebResponse)request.GetResponse())

                {

                    using (StreamReader reader = new StreamReader(response.GetResponseStream(), Encoding.UTF8))

                    {

                        DataSet dsResult = new DataSet();

                        dsResult.ReadXml(reader);

                        DataTable dtCoordinates = new DataTable();

                        dtCoordinates.Columns.AddRange(new DataColumn[4] { new DataColumn("Id", typeof(int)),

                                                       new DataColumn("Address", typeof(string)),
                                                       
                                                       new DataColumn("Latitude",typeof(string)),
                                                       
                                                       new DataColumn("Longitude",typeof(string)) });

                        foreach (DataRow row in dsResult.Tables["result"].Rows)

                        {

                            string geometry_id = dsResult.Tables["geometry"].Select("result_id = " +row["result_id"].ToString())[0]["geometry_id"].ToString();

                            DataRow location = dsResult.Tables["location"].Select("geometry_id = " +geometry_id)[0];

                            dtCoordinates.Rows.Add(row["result_id"], row["formatted_address"], location["lat"], location["lng"]);

                        }
                        dtGMap = dtCoordinates;

                       string Lattitude= dtGMap.Rows[0][2].ToString();
                       string Longitude= dtGMap.Rows[0][3].ToString();

                    }
                }
            }
            catch (Exception ex)
            {
                oResult.iRet = -1;
                oResult.sMessage = ex.Message;
            }
            return oResult;
        }
    }
}
