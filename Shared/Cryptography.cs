﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Security.Cryptography;
using System.Text;

namespace EllowCart.Shared
{
        public class Cryptography
        {

            #region Class constructor
            /// <summary>
            /// Default constructor for the CryptoClass
            /// </summary>
            public Cryptography()
            {
            }
            #endregion

            #region Private methods

            private static byte[] GenerateByteKey(string vsIn)
            {
                int iCount = 0;
                int iLength = 0;
                byte[] key = { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 };

                iLength = vsIn.Length;
                if (iLength > 16)
                {
                    iLength = 16;
                }

                for (iCount = 0; iCount < iLength; iCount++)
                {
                    try
                    {
                        key[iCount] = Convert.ToByte(vsIn[iCount]);
                    }
                    catch
                    {
                        continue;
                    }
                }

                return (key);
            }

            #endregion

            #region Public methods for encryption

            #region String encryption

            /// <summary>
            /// Encrypt a given string, Success = 1 and Failure = -1 
            /// </summary>
            /// <param name="vsIn">String to encrypt</param>
            /// <param name="rsOut">Encrypted output string</param>
            /// <param name="rsErr">Error if any</param>
            /// <returns></returns>
            public static int Encrypt_String(string vsIn, ref string rsOut, ref string rsErr)
            {
                int iRet = -1;

                // Key Construction Logic
                // Key : 0210199816072001 <02-Oct-1998 + 16-Jul-2001> 
                string sPassword = "ezoro9876543201#@";

                try
                {
                    // Create a Rihndael class.  
                    RijndaelManaged RijndaelCipher = new RijndaelManaged();
                    RijndaelCipher.Padding = System.Security.Cryptography.PaddingMode.Zeros;

                    // First we need to turn the input strings into a byte array.
                    byte[] PlainText = System.Text.Encoding.Unicode.GetBytes(vsIn);

                    // We are using salt to make it harder to guess our key
                    // using a dictionary attack.
                    byte[] Salt = Encoding.ASCII.GetBytes(sPassword.Length.ToString());

                    // The (Secret Key) will be generated from the specified 
                    // password and salt.
                    PasswordDeriveBytes SecretKey = new PasswordDeriveBytes(sPassword, Salt);

                    // Create a encryptor from the existing SecretKey bytes.
                    // We use 32 bytes for the secret key 
                    // (the default Rijndael key length is 256 bit = 32 bytes) and
                    // then 16 bytes for the IV (initialization vector),
                    // (the default Rijndael IV length is 128 bit = 16 bytes)
                    ICryptoTransform Encryptor = RijndaelCipher.CreateEncryptor(SecretKey.GetBytes(32), SecretKey.GetBytes(16));

                    // Create a MemoryStream that is going to hold the encrypted bytes 
                    MemoryStream memoryStream = new MemoryStream();

                    // Create a CryptoStream through which we are going to be processing our data. 
                    // CryptoStreamMode.Write means that we are going to be writing data 
                    // to the stream and the output will be written in the MemoryStream
                    // we have provided. (always use write mode for encryption)
                    CryptoStream cryptoStream = new CryptoStream(memoryStream, Encryptor, CryptoStreamMode.Write);

                    // Start the encryption process.
                    cryptoStream.Write(PlainText, 0, PlainText.Length);

                    // Finish encrypting.
                    cryptoStream.FlushFinalBlock();

                    // Convert our encrypted data from a memoryStream into a byte array.
                    byte[] CipherBytes = memoryStream.ToArray();

                    // Close both streams.
                    memoryStream.Close();
                    cryptoStream.Close();

                    // Convert encrypted data into a base64-encoded string.
                    // A common mistake would be to use an Encoding class for that. 
                    // It does not work, because not all byte values can be
                    // represented by characters. We are going to be using Base64 encoding
                    // That is designed exactly for what we are trying to do. 
                    string EncryptedData = Convert.ToBase64String(CipherBytes);

                    // Return encrypted string.
                    rsOut = EncryptedData;
                }
                catch (Exception eX)
                {
                    rsErr = eX.Message;
                    goto er;
                }

                iRet = 1;

            er:
                {
                    return iRet;
                }
            }

            /// <summary>
            /// Encrypt a string based on a given password, Success = 1 and Failure = -1 
            /// </summary>
            /// <param name="vsIn">String to encrypt</param>
            /// <param name="vsPassword">Password to encrypt</param>
            /// <param name="rsOut">Encrypted output string</param>
            /// <param name="rsErr">Error if any</param>
            /// <returns></returns>
            public static int Encrypt_String(string vsIn, string vsPassword, ref string rsOut, ref string rsErr)
            {
                int iRet = -1;

                try
                {
                    // Create a Rihndael class.  
                    RijndaelManaged RijndaelCipher = new RijndaelManaged();
                    RijndaelCipher.Padding = System.Security.Cryptography.PaddingMode.Zeros;

                    // First we need to turn the input strings into a byte array.
                    byte[] PlainText = System.Text.Encoding.Unicode.GetBytes(vsIn);

                    // We are using salt to make it harder to guess our key
                    // using a dictionary attack.
                    byte[] Salt = Encoding.ASCII.GetBytes(vsPassword.Length.ToString());

                    // The (Secret Key) will be generated from the specified 
                    // password and salt.
                    PasswordDeriveBytes SecretKey = new PasswordDeriveBytes(vsPassword, Salt);

                    // Create a encryptor from the existing SecretKey bytes.
                    // We use 32 bytes for the secret key 
                    // (the default Rijndael key length is 256 bit = 32 bytes) and
                    // then 16 bytes for the IV (initialization vector),
                    // (the default Rijndael IV length is 128 bit = 16 bytes)
                    ICryptoTransform Encryptor = RijndaelCipher.CreateEncryptor(SecretKey.GetBytes(32), SecretKey.GetBytes(16));

                    // Create a MemoryStream that is going to hold the encrypted bytes 
                    MemoryStream memoryStream = new MemoryStream();

                    // Create a CryptoStream through which we are going to be processing our data. 
                    // CryptoStreamMode.Write means that we are going to be writing data 
                    // to the stream and the output will be written in the MemoryStream
                    // we have provided. (always use write mode for encryption)
                    CryptoStream cryptoStream = new CryptoStream(memoryStream, Encryptor, CryptoStreamMode.Write);

                    // Start the encryption process.
                    cryptoStream.Write(PlainText, 0, PlainText.Length);

                    // Finish encrypting.
                    cryptoStream.FlushFinalBlock();

                    // Convert our encrypted data from a memoryStream into a byte array.
                    byte[] CipherBytes = memoryStream.ToArray();

                    // Close both streams.
                    memoryStream.Close();
                    cryptoStream.Close();

                    // Convert encrypted data into a base64-encoded string.
                    // A common mistake would be to use an Encoding class for that. 
                    // It does not work, because not all byte values can be
                    // represented by characters. We are going to be using Base64 encoding
                    // That is designed exactly for what we are trying to do. 
                    string EncryptedData = Convert.ToBase64String(CipherBytes);

                    // Return encrypted string.
                    rsOut = EncryptedData;
                }
                catch (Exception eX)
                {
                    rsErr = eX.Message;
                    goto er;
                }

                iRet = 1;

            er:
                {
                    return iRet;
                }
            }

            #endregion

            #region File encryption

            /// <summary>
            /// Encrypt a given file, Success = 1 and Failure = -1 
            /// </summary>
            /// <param name="inName">File name to encrypt</param>
            /// <param name="outName">Out put file name</param>
            /// <param name="rsErr">Error if any</param>
            /// <returns></returns>
            public static int Encrypt_File(String inName, String outName, ref string rsErr)
            {
                int iRet = -1;
                byte[] bin = new byte[64];   //This is intermediate storage for the encryption.
                long rdlen = 0;              //This is the total number of bytes written.
                int len = 0;                 //This is the number of bytes to be written at a time.
                long totlen = 0;

                // Key and IV Construction Logic
                // Key : 0210199816072001 <02-Oct-1998 + 16-Jul-2001> 
                // IV  : 1002706189910120 <Key is just reversed>
                byte[] key = { 0x00, 0x02, 0x01, 0x00, 0x01, 0x09, 0x09, 0x08, 0x01, 0x06, 0x00, 0x07, 0x02, 0x00, 0x00, 0x01 };
                byte[] IV = { 0x01, 0x00, 0x00, 0x02, 0x07, 0x00, 0x06, 0x01, 0x08, 0x09, 0x09, 0x01, 0x00, 0x01, 0x02, 0x00 };

                try
                {
                    // Create the file streams to handle the input and output files.
                    FileStream fin = new FileStream(inName, FileMode.Open, FileAccess.Read);
                    FileStream fout = new FileStream(outName, FileMode.OpenOrCreate, FileAccess.Write);
                    fout.SetLength(0);

                    totlen = fin.Length;    //This is the total length of the input file.

                    SymmetricAlgorithm rijn = SymmetricAlgorithm.Create(); //Creates the default implementation, which is RijndaelManaged.         
                    CryptoStream encStream = new CryptoStream(fout, rijn.CreateEncryptor(key, IV), CryptoStreamMode.Write);

                    // Read from the input file, then encrypt and write to the output file.
                    while (rdlen < totlen)
                    {
                        len = fin.Read(bin, 0, 64);
                        encStream.Write(bin, 0, len);
                        rdlen = rdlen + len;
                    }

                    encStream.Close();
                    fout.Close();
                    fin.Close();
                }
                catch (Exception eX)
                {
                    rsErr = eX.Message;
                    goto er;
                }

                // On success set the success flag to one
                iRet = 1;

            er:
                {
                    return (iRet);
                }
            }

            /// <summary>
            /// Encrypt a given file, Success = 1 and Failure = -1 
            /// </summary>
            /// <param name="inName">File name to encrypt</param>
            /// <param name="outName">Out put file name</param>
            /// <param name="vsKey">Secret Key</param>
            /// <param name="rsErr">Error if any</param>
            /// <returns></returns>
            public static int Encrypt_File(String inName, String outName, string vsKey, ref string rsErr)
            {
                int iRet = -1;
                byte[] bin = new byte[64];   //This is intermediate storage for the encryption.
                long rdlen = 0;              //This is the total number of bytes written.
                int len = 0;                 //This is the number of bytes to be written at a time.
                long totlen = 0;
                byte[] bPrivateKey = null;

                // Key and IV Construction Logic
                // Key : 0210199816072001 <02-Oct-1998 + 16-Jul-2001> 
                // IV  : 1002706189910120 <Key is just reversed>
                byte[] IV = { 0x01, 0x00, 0x00, 0x02, 0x07, 0x00, 0x06, 0x01, 0x08, 0x09, 0x09, 0x01, 0x00, 0x01, 0x02, 0x00 };

                try
                {
                    // Generate Key
                    bPrivateKey = GenerateByteKey(vsKey);

                    // Create the file streams to handle the input and output files.
                    FileStream fin = new FileStream(inName, FileMode.Open, FileAccess.Read);
                    FileStream fout = new FileStream(outName, FileMode.OpenOrCreate, FileAccess.Write);
                    fout.SetLength(0);

                    totlen = fin.Length;    //This is the total length of the input file.

                    SymmetricAlgorithm rijn = SymmetricAlgorithm.Create(); //Creates the default implementation, which is RijndaelManaged.         
                    CryptoStream encStream = new CryptoStream(fout, rijn.CreateEncryptor(bPrivateKey, IV), CryptoStreamMode.Write);

                    // Read from the input file, then encrypt and write to the output file.
                    while (rdlen < totlen)
                    {
                        len = fin.Read(bin, 0, 64);
                        encStream.Write(bin, 0, len);
                        rdlen = rdlen + len;
                    }

                    encStream.Close();
                    fout.Close();
                    fin.Close();
                }
                catch (Exception eX)
                {
                    rsErr = eX.Message;
                    goto er;
                }

                // On success set the success flag to one
                iRet = 1;

            er:
                {
                    return (iRet);
                }
            }

            #endregion

            #endregion

            #region Public methods for decryption

            #region String decryption

            /// <summary>
            /// Decrypt a given string, Success = 1 and Failure = -1 
            /// </summary>
            /// <param name="vsIn">String to decrypt</param>
            /// <param name="rsOut">Decrypted output string</param>
            /// <param name="rsErr">Error if any</param>
            /// <returns></returns>
            public static int Decrypt_String(string vsIn, ref string rsOut, ref string rsErr)
            {
                int iRet = -1;

                // Key Construction Logic
                // Key : 0210199816072001 <02-Oct-1998 + 16-Jul-2001> 
                string sPassword = "ezoro9876543201#@";

                try
                {
                    RijndaelManaged RijndaelCipher = new RijndaelManaged();
                    RijndaelCipher.Padding = System.Security.Cryptography.PaddingMode.Zeros;

                    byte[] EncryptedData = Convert.FromBase64String(vsIn);
                    byte[] Salt = Encoding.ASCII.GetBytes(sPassword.Length.ToString());

                    PasswordDeriveBytes SecretKey = new PasswordDeriveBytes(sPassword, Salt);

                    // Create a decryptor from the existing SecretKey bytes.
                    ICryptoTransform Decryptor = RijndaelCipher.CreateDecryptor(SecretKey.GetBytes(32), SecretKey.GetBytes(16));

                    MemoryStream memoryStream = new MemoryStream(EncryptedData);

                    // Create a CryptoStream. (always use Read mode for decryption).
                    CryptoStream cryptoStream = new CryptoStream(memoryStream, Decryptor, CryptoStreamMode.Read);

                    // Since at this point we don't know what the size of decrypted data
                    // will be, allocate the buffer long enough to hold EncryptedData;
                    // DecryptedData is never longer than EncryptedData.
                    byte[] PlainText = new byte[EncryptedData.Length];
                
                    // Start decrypting.
                    int DecryptedCount = cryptoStream.Read(PlainText, 0, PlainText.Length);
                        

                    memoryStream.Close();
                    cryptoStream.Close();

                // Convert decrypted data into a string. 
                string DecryptedData = Encoding.Unicode.GetString(PlainText, 0, DecryptedCount);

                DecryptedData = DecryptedData.TrimEnd('\0');
                    // Return decrypted string.   

                    rsOut = DecryptedData;
                }
                catch (Exception eX)
                {
                    rsErr = eX.Message;
                    goto er;
                }

                iRet = 1;

            er:
                {
                    return (iRet);
                }
            }

            /// <summary>
            /// String to decrypt with a given password, Success = 1 and Failure = -1 
            /// </summary>
            /// <param name="vsIn">String to decrypt</param>
            /// <param name="vsPassword">Password to decrypt</param>
            /// <param name="rsOut">Decrypted string</param>
            /// <param name="rsErr">Error if any</param>
            /// <returns></returns>
            public static int Decrypt_String(string vsIn, string vsPassword, ref string rsOut, ref string rsErr)
            {
                int iRet = -1;

                try
                {
                    RijndaelManaged RijndaelCipher = new RijndaelManaged();
                    RijndaelCipher.Padding = System.Security.Cryptography.PaddingMode.Zeros;

                    byte[] EncryptedData = Convert.FromBase64String(vsIn);
                    byte[] Salt = Encoding.ASCII.GetBytes(vsPassword.Length.ToString());

                    PasswordDeriveBytes SecretKey = new PasswordDeriveBytes(vsPassword, Salt);

                    // Create a decryptor from the existing SecretKey bytes.
                    ICryptoTransform Decryptor = RijndaelCipher.CreateDecryptor(SecretKey.GetBytes(32), SecretKey.GetBytes(16));

                    MemoryStream memoryStream = new MemoryStream(EncryptedData);

                    // Create a CryptoStream. (always use Read mode for decryption).
                    CryptoStream cryptoStream = new CryptoStream(memoryStream, Decryptor, CryptoStreamMode.Read);

                    // Since at this point we don't know what the size of decrypted data
                    // will be, allocate the buffer long enough to hold EncryptedData;
                    // DecryptedData is never longer than EncryptedData.
                    byte[] PlainText = new byte[EncryptedData.Length];

                    // Start decrypting.
                    int DecryptedCount = cryptoStream.Read(PlainText, 0, PlainText.Length);

                    memoryStream.Close();
                    cryptoStream.Close();

                    // Convert decrypted data into a string. 
                    string DecryptedData = Encoding.Unicode.GetString(PlainText, 0, DecryptedCount);

                    // Return decrypted string.   
                    rsOut = DecryptedData;
                }
                catch (Exception eX)
                {
                    rsErr = eX.Message;
                    goto er;
                }

                iRet = 1;

            er:
                {
                    return (iRet);
                }
            }

            #endregion

            #region File decryption

            /// <summary>
            /// Decrypt a given file, Success = 1 and Failure = -1
            /// </summary>
            /// <param name="inName">File name to decrypt</param>
            /// <param name="outName">Out put file name</param>
            /// <param name="rsErr">Error if any</param>
            /// <returns></returns>
            public static int Decrypt_File(String inName, String outName, ref string rsErr)
            {
                int iRet = 0;
                int len = 0;                 //This is the number of bytes to be written at a time.
                long rdlen = 0;              //This is the total number of bytes written.
                long totlen = 0;
                byte[] bin = new byte[64];   //This is intermediate storage for the decryption.

                // Key and IV Construction Logic
                // Key : 0210199816072001 <02-Oct-1998 + 16-Jul-2001> 
                // IV  : 1002706189910120 <Key is just reversed>
                byte[] key = { 0x00, 0x02, 0x01, 0x00, 0x01, 0x09, 0x09, 0x08, 0x01, 0x06, 0x00, 0x07, 0x02, 0x00, 0x00, 0x01 };
                byte[] IV = { 0x01, 0x00, 0x00, 0x02, 0x07, 0x00, 0x06, 0x01, 0x08, 0x09, 0x09, 0x01, 0x00, 0x01, 0x02, 0x00 };

                try
                {
                    //Create the file streams to handle the input and output files.
                    FileStream fin = new FileStream(inName, FileMode.Open, FileAccess.Read);
                    FileStream fout = new FileStream(outName, FileMode.OpenOrCreate, FileAccess.Write);
                    fout.SetLength(0);

                    totlen = fin.Length;    //This is the total length of the input file.

                    //Creates the default implementation, which is RijndaelManaged.         
                    SymmetricAlgorithm rijn = SymmetricAlgorithm.Create();
                    CryptoStream encStream = new CryptoStream(fout, rijn.CreateDecryptor(key, IV), CryptoStreamMode.Write);

                    //Read from the input file, then decrypt and write to the output file.
                    while (rdlen < totlen)
                    {
                        len = fin.Read(bin, 0, 64);
                        encStream.Write(bin, 0, len);
                        rdlen = rdlen + len;
                    }

                    encStream.Close();
                    fout.Close();
                    fin.Close();
                }
                catch (Exception eX)
                {
                    rsErr = eX.Message;
                    goto er;
                }

                // On success set the success flag to one
                iRet = 1;

            er:
                {
                    return (iRet);
                }
            }

            /// <summary>
            /// Decrypt a given file, Success = 1 and Failure = -1
            /// </summary>
            /// <param name="inName">File name to decrypt</param>
            /// <param name="outName">Out put file name</param>
            /// <param name="vsKey">Secret Key</param>
            /// <param name="rsErr">Error if any</param>
            /// <returns></returns>
            public static int Decrypt_File(String inName, String outName, string vsKey, ref string rsErr)
            {
                int iRet = 0;
                int len = 0;                 //This is the number of bytes to be written at a time.
                long rdlen = 0;              //This is the total number of bytes written.
                long totlen = 0;
                byte[] bin = new byte[64];   //This is intermediate storage for the decryption.
                byte[] bPrivateKey = null;

                // Key and IV Construction Logic
                // Key : 0210199816072001 <02-Oct-1998 + 16-Jul-2001> 
                // IV  : 1002706189910120 <Key is just reversed>
                byte[] IV = { 0x01, 0x00, 0x00, 0x02, 0x07, 0x00, 0x06, 0x01, 0x08, 0x09, 0x09, 0x01, 0x00, 0x01, 0x02, 0x00 };

                try
                {
                    // Generate Key
                    bPrivateKey = GenerateByteKey(vsKey);

                    //Create the file streams to handle the input and output files.
                    FileStream fin = new FileStream(inName, FileMode.Open, FileAccess.Read);
                    FileStream fout = new FileStream(outName, FileMode.OpenOrCreate, FileAccess.Write);
                    fout.SetLength(0);

                    totlen = fin.Length;    //This is the total length of the input file.

                    //Creates the default implementation, which is RijndaelManaged.         
                    SymmetricAlgorithm rijn = SymmetricAlgorithm.Create();
                    CryptoStream encStream = new CryptoStream(fout, rijn.CreateDecryptor(bPrivateKey, IV), CryptoStreamMode.Write);

                    //Read from the input file, then decrypt and write to the output file.
                    while (rdlen < totlen)
                    {
                        len = fin.Read(bin, 0, 64);
                        encStream.Write(bin, 0, len);
                        rdlen = rdlen + len;
                    }

                    encStream.Close();
                    fout.Close();
                    fin.Close();
                }
                catch (Exception eX)
                {
                    rsErr = eX.Message;
                    goto er;
                }

                // On success set the success flag to one
                iRet = 1;

            er:
                {
                    return (iRet);
                }
            }

            #endregion

            #endregion
        }
   

}
