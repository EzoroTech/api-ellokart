﻿using System.Collections.Generic;
using EllowCart.DTO.Module.InputModels;
using EllowCart.Shared;

namespace EllowCart.BusinessPersistantContracts.Roles
{
    public interface IModuleServices
    {
        APiResult AddNewModule(ModuleInputModel _input);
        APiResult GetAllModules();

        ApiResult<List<ModuleInputModel>> GetAllModuleList();
    }
}
